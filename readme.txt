Repository Structure:

scs_hybris_64
  |
  +\impex
        +\customizing.impex
  |
  +\lib
        +\IPR_2.4.zip
        +\mysql-connector-java-5.1.44-bin.jar
  |
  +\references
        +\[SCS_Integration]_Development_Environment_Setup.doc
        +\[SCS_Integration]_Development_Environment_Setup_v3.txt
        +\Customizing Data Hub.txt
  |
  +\SCS
        +\hybris      (latest custom code)
             +\bin
                 +\...
             +\config
                 +\...
  |
  +\hosts             (sites configuration properties)