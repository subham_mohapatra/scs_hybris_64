/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sap.retail.oaa.model.constants;

/**
 * Global class for all Sapoaamodel constants. You can add global constants for your extension into this class.
 */
public final class SapoaamodelConstants extends GeneratedSapoaamodelConstants
{
	public static final String EXTENSIONNAME = "sapoaamodel";
	public static final String SAPOAA_CARHTTPDESTINATION = "sapoaa_carHttpDestination";
	public static final String SAPOAA_CARCLIENT = "sapoaa_carClient";

	public static final String PROPERTY_SAPOAA_OAAPROFILE = "sapoaa_oaaProfile";
	public static final String PROPERTY_SAPOAA_SALESCHANNEL = "sapoaa_salesChannel";
	public static final String PROPERTY_SAPOAA_MODE = "sapoaa_mode";
	public static final String PROPERTY_SAPOAA_CONSUMERID = "sapoaa_consumerId";
	public static final String PROPERTY_SAPOAA_VENDOR_ITEM_CATEGORY = "sapoaa_vendorItemCategory";


	private SapoaamodelConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
