/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sap.retail.oaa.orderexchange.constants;

/**
 * IDs of Actions as defined in order process
 *
 */
public final class ActionIds
{
	@SuppressWarnings("javadoc")
	public static final String CHECK_SAP_ORDER = "checkSAPOrder";


	private ActionIds()
	{
		// empty to avoid instantiating this constant class
	}
}
