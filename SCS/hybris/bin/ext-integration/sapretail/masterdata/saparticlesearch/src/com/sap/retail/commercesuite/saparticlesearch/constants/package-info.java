/*****************************************************************************
    Package:        com.sap.retail.commercesuite.saparticlesearch.constants
    Copyright (c) 2016, SAP SE, Germany, All rights reserved.

 *****************************************************************************/
package com.sap.retail.commercesuite.saparticlesearch.constants;