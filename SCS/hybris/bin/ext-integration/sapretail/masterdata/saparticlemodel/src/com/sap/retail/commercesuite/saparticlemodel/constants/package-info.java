/*****************************************************************************
    Package:        com.sap.retail.commercesuite.saparticlemodel.constants
    Copyright (c) 2016, SAP SE, Germany, All rights reserved.

 *****************************************************************************/
package com.sap.retail.commercesuite.saparticlemodel.constants;