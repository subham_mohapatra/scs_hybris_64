/*****************************************************************************
 Class:        DataContainerSmartDefaultImpl
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.container.impl;

import com.sap.retail.isce.container.DataContainerSmart;


/**
 * Default implementation for smart data container.
 *
 */
public abstract class DataContainerSmartDefaultImpl extends DataContainerDefaultImpl implements DataContainerSmart
{
	// nothing common to be implemented here
}
