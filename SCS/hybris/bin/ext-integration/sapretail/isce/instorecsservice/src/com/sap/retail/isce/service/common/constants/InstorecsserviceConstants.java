/*****************************************************************************
Class:        InstorecsserviceConstants
Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.service.common.constants;

/**
 * Global class for all Instorecsservice constants. You can add global constants for your extension into this class.
 */
public final class InstorecsserviceConstants extends GeneratedInstorecsserviceConstants
{
	public static final String EXTENSIONNAME = "instorecsservice";

	private InstorecsserviceConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
