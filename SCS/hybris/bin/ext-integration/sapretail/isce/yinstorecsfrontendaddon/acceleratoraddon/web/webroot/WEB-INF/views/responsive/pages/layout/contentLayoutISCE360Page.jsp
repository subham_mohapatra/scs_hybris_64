<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

	<cms:pageSlot position="BodyContentISCE" var="feature" >
		<cms:component component="${feature}" element="div"/>
	</cms:pageSlot>