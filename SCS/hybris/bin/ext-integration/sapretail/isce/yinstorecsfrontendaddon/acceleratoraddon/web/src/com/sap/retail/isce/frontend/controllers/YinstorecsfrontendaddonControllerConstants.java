/*****************************************************************************
 Class:        YinstorecsfrontendaddonControllerConstants
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.frontend.controllers;

/**
 * The interface for all Yinstorecsfrontendaddon controller constants. You can add controller constants for your
 * extension into this class.
 */
public interface YinstorecsfrontendaddonControllerConstants
{
	// implement here controller constants used by this extension
}
