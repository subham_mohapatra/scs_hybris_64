/*****************************************************************************
 Class:        CMSISCECustomer360ComponentController
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.frontend.controllers.cms;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sap.retail.isce.model.CMSISCECustomer360ComponentModel;


/**
 *
 * Controller for CMS ISCECustomer360Component
 */
@Controller
@RequestMapping(value = "/view/CMSISCECustomer360ComponentController")
public class CMSISCECustomer360ComponentController extends CMSISCECComponentBaseController<CMSISCECustomer360ComponentModel>
{
	//
}