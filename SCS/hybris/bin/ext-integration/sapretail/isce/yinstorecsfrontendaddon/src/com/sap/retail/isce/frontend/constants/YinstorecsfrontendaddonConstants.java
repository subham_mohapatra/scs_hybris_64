/*****************************************************************************
 Class:        YinstorecsfrontendaddonConstants
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.frontend.constants;

/**
 * Global class for all Yinstorecsfrontendaddon constants. You can add global constants for your extension into this
 * class.
 */
public final class YinstorecsfrontendaddonConstants extends GeneratedYinstorecsfrontendaddonConstants
{
	public static final String EXTENSIONNAME = "yinstorecsfrontendaddon";

	private YinstorecsfrontendaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
