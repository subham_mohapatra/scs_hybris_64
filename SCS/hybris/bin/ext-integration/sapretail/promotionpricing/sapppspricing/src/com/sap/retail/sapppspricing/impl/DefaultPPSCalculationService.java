/*****************************************************************************
Class: DefaultPPSCalculationService
 
@Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 
*****************************************************************************/
 
package com.sap.retail.sapppspricing.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestClientException;

import com.sap.retail.sapppspricing.PPSConfigService;
import com.sap.retail.sapppspricing.PricingBackend;
import com.sap.retail.sapppspricing.SapPPSPricingRuntimeException;


/**
 * Order calculation service using PPS if set to active
 */
public class DefaultPPSCalculationService extends DefaultCalculationService
{

	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;
	private CommonI18NService commonI18NService;
	private PricingBackend pricingBackend;
	private PPSConfigService configService;

	@Override
	public void calculate(final AbstractOrderModel order) throws CalculationException
	{
		if (getConfigService().isPpsActive(order))
		{
			if (orderRequiresCalculationStrategy.requiresCalculation(order))
			{
				// update prices from sap backend.
				updateOrderFromPPS(order);
				super.recalculate(order);
			}
		}
		else
		{
			super.calculate(order);
		}
	}

	@Override
	public void recalculate(final AbstractOrderModel order) throws CalculationException
	{
		if (getConfigService().isPpsActive(order))
		{
			// update prices from sap PPS backend.
			updateOrderFromPPS(order);
			super.recalculate(order);
		}
		else
		{
			super.recalculate(order);
		}
	}

	@Override
	public void calculateTotals(final AbstractOrderModel order, final boolean recalculate) throws CalculationException
	{
		if (getConfigService().isPpsActive(order))
		{
			// update prices from sap backend.
			updateOrderFromPPS(order);
			super.calculateTotals(order, recalculate, calculateSubtotal(order, recalculate));
		}
		else
		{
			super.calculateTotals(order, recalculate);
		}
	}

	protected void updateOrderFromPPS(final AbstractOrderModel order)
	{
		// set order currency to session currency
		order.setCurrency(commonI18NService.getCurrentCurrency());
		try
		{
			getPricingBackend().readPricesForCart(order);
		}
		catch (final RestClientException e)
		{
			throw new SapPPSPricingRuntimeException(e);
		}
	}

	protected void recalculateOrderEntryIfNeeded(final AbstractOrderEntryModel entry, final boolean forceRecalculation)
			throws CalculationException
	{
		if (forceRecalculation || orderRequiresCalculationStrategy.requiresCalculation(entry))
		{
			entry.setTaxValues(findTaxValues(entry));
			super.calculateTotals(entry, true);
		}
	}

	@Override
	public void recalculate(final AbstractOrderEntryModel entry) throws CalculationException
	{
		if (getConfigService().isPpsActive(entry.getOrder()))
		{
			this.recalculateOrderEntryIfNeeded(entry, false);
		}
		else
		{
			super.recalculate(entry);
		}
	}

	@Override
	public void calculateEntries(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException
	{
		if (getConfigService().isPpsActive(order))
		{
			order.setTotalPrice(Double.valueOf(orderTotal(order, forceRecalculate)));
		}
		else
		{
			super.calculateEntries(order, forceRecalculate);
		}
	}

	protected double orderTotal(final AbstractOrderModel order, final boolean forceRecalculate) throws CalculationException
	{
		double subtotal = 0.0;
		for (final AbstractOrderEntryModel e : order.getEntries())
		{
			this.recalculateOrderEntryIfNeeded(e, forceRecalculate);
			subtotal += e.getTotalPrice().doubleValue();
		}
		return subtotal;
	}

	@SuppressWarnings("javadoc")
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Override
	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
		super.setCommonI18NService(commonI18NService);
	}

	@SuppressWarnings("javadoc")
	public OrderRequiresCalculationStrategy getOrderRequiresCalculationStrategy()
	{
		return orderRequiresCalculationStrategy;
	}

	@Override
	@Required
	public void setOrderRequiresCalculationStrategy(final OrderRequiresCalculationStrategy orderRequiresCalculationStrategy)
	{
		super.setOrderRequiresCalculationStrategy(orderRequiresCalculationStrategy);
		this.orderRequiresCalculationStrategy = orderRequiresCalculationStrategy;
	}

	@SuppressWarnings("javadoc")
	public PricingBackend getPricingBackend()
	{
		return pricingBackend;
	}

	@SuppressWarnings("javadoc")
	public void setPricingBackend(final PricingBackend pricingBackend)
	{
		this.pricingBackend = pricingBackend;
	}

	@SuppressWarnings("javadoc")
	public PPSConfigService getConfigService()
	{
		return configService;
	}

	@SuppressWarnings("javadoc")
	public void setConfigService(final PPSConfigService configService)
	{
		this.configService = configService;
	}

}
