'use strict';

module.exports = function(grunt) {
    var path = require('path');
    var fs = require('fs');
    var task = grunt.cli.tasks[0]; //= Get the grunt command line task
    var store = grunt.file.readJSON('config.storefront.json');
    var pkg = grunt.file.readJSON('package.json');
    var themes = '';

    function msgColor(msg, severity) {
        var color = '';
        var wrapper = '';
        switch(severity) {
            case 1:
                color = 'yellow';
                wrapper = '\n' +
                    '\t\t!!!!!!!!!!!!!!!!! ' [color].bold + msg[color].bold + ' !!!!!!!!!!!!!!!!!\n' [color].bold;
                break;
            case 2:
                color = 'red';
                wrapper = '\n' +
                    '\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n' [color].bold +
                    '\t\t ' [color].bold + msg[color].bold + ' \n' [color].bold +
                    '\t\t!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n' [color].bold;
                break;
            default:
                color = 'green';
                wrapper = '\n' +
                    '\t\t' [color].bold + msg[color].bold + '\n' [color].bold;
        }
        return wrapper;
    }


    //= Check if the json file with all the theme folder infos exists
    if(fs.existsSync(pkg.grunt.generated.path + 'info.themes.json') === false && task !== 'folder_list') {
        grunt.log.writeln(msgColor('Info : File "' + pkg.grunt.generated.path + 'info.themes.json" doesn\'t exists !', 1));
        grunt.fail.warn(msgColor('Advice : Launch command "grunt folder_list" first please', 2));
    }

    // Plugins
    require('time-grunt')(grunt); //= Plugin to display the execute time of every tasks
    require('jit-grunt')(grunt); //= Plugin used with load-grunt-config

    //= Plugin to load tasks and configs with external files
    require('load-grunt-config')(grunt, {
        configPath: path.join(process.cwd(), 'grunt/config'),
        init: true,
        jitGrunt: {
            customTasksDir: 'grunt/tasks'
        },
        data: {
            pkg: pkg, // accessible with '<%= pkg %>' or grunt.config('pkg')
            store: store, // accessible with '<%= store %>' or grunt.config('store')
        },
        preMerge: function(config, data) {
            if(task !== 'folder_list') {

                data.themes = grunt.file.readJSON(data.pkg.grunt.generated.path + 'info.themes.json');
                //= check if the theme is specified in the grunt command line
                var themeType = grunt.option('tt') || 'all';
                var isWatching = grunt.option('watch');

                //= Construct the themes choices by parsing the themes src folders
                for(var oneTheme in data.themes) {
                    if(data.themes[oneTheme].depth === 1 && data.themes[oneTheme].location !== '' && data.themes[oneTheme].location !== 'shared') {
                        data.store.theme.themeType.listed.push(data.themes[oneTheme].location);

                    }
                }

                // Create the typeChoices array
                data.store.theme.themeType.choices.push.apply(data.store.theme.themeType.choices, data.store.theme.themeType.listed);

                //= Command line options validation
                if(themeType !== 0) {
                    if(typeof themeType === 'string') {
                        //= Check if the grunt option theme exists
                        if(data.store.theme.themeType.choices.indexOf(themeType) === -1) {
                            grunt.log.writeln(msgColor('MSG : grunt option theme doesn\'t really exists', 1));
                            grunt.fail.warn(msgColor('ERROR : This folder "' + data.store.baseSrc + themeType + '" doesn\'t exists, please define another value to the "tt" command line option', 2));
                        } else {
                            grunt.log.writeln(msgColor('MSG : grunt option theme exists'));
                            data.pkg.grunt.options.themeType = [themeType];
                            data.store.theme.themeType.selected = [themeType];
                        }
                    } else if(typeof themeType !== 'string') {
                        grunt.fail.warn(msgColor('The "tt" (theme type) option must be used with a string value to be activated', 1));
                    }
                }

                if(isWatching === 1) {
                    data.pkg.grunt.options.isWatching = true;
                } else if(typeof isWatching !== 'undefined' && isWatching !== 1) {
                    grunt.fail.warn(msgColor('The "w" (watch) option must be used with the value 1 to be activated, 0 to be desactivated', 1));
                }
                //= End Command line options validation


                var wrapperBeginningTplUglify = 'module.exports = { ';
                var tplUglify = '##taskTitle##NoMin: { files: [{ expand: true, cwd: "<%= store.baseSrc %>", src: ##srcJsTargets##, dest: "<%= store.baseDist %>", rename: function(dest, src) { var nsrc = src; if(src.indexOf("common") === -1){ nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/theme-$1/js"); }else{ nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/$1/js"); } return dest + nsrc; } }], options: { beautify: { indent_level: 2 }, preserveComments: "all",  sourceMap: true, sourceMapIncludeSources: true } }, ##taskTitle##Min: { files: [{ expand: true, cwd: "<%= store.baseSrc %>", src: ##srcJsTargets##, dest: "<%= store.baseDist %>", rename: function(dest, src) { var nsrc = src; if(src.indexOf("common") === -1){ nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/theme-$1/js"); }else{ nsrc = nsrc.replace(new RegExp("/themes/(.*)/js"), "/$1/js"); } nsrc = nsrc.replace(".js", ".min.js"); return dest + nsrc; } }], options: { compress: { drop_console: false, dead_code: true, drop_debugger: true, conditionals: true, }, report: "gzip" } }';
                var wrapperEndingTplUglify = ' }';
                var tplUglified = '';

                grunt.log.writeln(msgColor('===> Themes found : ' + data.store.theme.themeType.listed));


                //= For debugging
                // if (grunt.option('debug')) {
                //  console.log('==============');
                //  console.log('options : \n\t\t');
                //  console.log(JSON.stringify(store.src.js.jshint.ignore));
                //  console.log(JSON.stringify(data.store.theme));
                //  console.log('==============');
                //  grunt.fail.warn('stop');
                // }

            }
        },
        postProcess: function(config) {
            //= For debugging

            // if (grunt.option('debug')) {
            //  console.log('==============');
            //  console.log('options : \n\t\t');
            //  console.log(JSON.stringify(config.jshint, null, 4));
            //  // console.log(JSON.stringify(data.store.theme));
            //  console.log('==============');
            //  grunt.fail.warn('stop');
            // }
        }
    });

};
