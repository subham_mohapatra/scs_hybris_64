/*global require, module,  __dirname, console*/
module.exports = {
	nomin: {
		// task options
		options: {
			// rtlcss options
			opts: {
				clean: false
			},
			// rtlcss plugins
			plugins: [],
			// save unmodified files
			saveUnmodified: true,
		},
		files: [{
			expand: true,
			cwd: '<%= store.baseDist %>',
			dest: '<%= store.baseDist %>',
			src: ['responsive/theme-*/**/*<%= store.dist.css.nominExt %>', '!**/*-rtl*<%= store.dist.css.nominExt %>', '!**/*<%= store.dist.css.minExt %>'],
			rename: function(dest, matchedSrcPath, options) {
				// return the destination path and filename:
				return (dest + matchedSrcPath).replace('.css', '-rtl.css');
			}
		}]
	},
	min: {
		// task options
		options: {
			// rtlcss options
			opts: {
				clean: false
			},
			// rtlcss plugins
			plugins: [],
			// save unmodified files
			saveUnmodified: true,
		},
		files: [{
			expand: true,
			cwd: '<%= store.baseDist %>',
			dest: '<%= store.baseDist %>',
			src: ['responsive/theme-*/**/*<%= store.dist.css.minExt %>', '!**/*-rtl*<%= store.dist.css.minExt %>'],
			rename: function(dest, matchedSrcPath, options) {
				// return the destination path and filename:
				return (dest + matchedSrcPath).replace('.min.css', '-rtl.min.css');
			}
		}]
	}
};
