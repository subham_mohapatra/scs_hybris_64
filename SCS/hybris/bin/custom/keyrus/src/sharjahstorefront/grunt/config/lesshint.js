/*global require, module,  __dirname */
module.exports = {
	options: {
		// Task-specific options go here. 
		lesshintrc : '.lesshintrc'
	},
	targets: ['<%= store.src.less.lesshint.targets %>']
};
