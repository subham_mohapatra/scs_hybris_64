package com.sharjah.storefront.controllers.pages;
 

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharjah.storefront.controllers.ControllerConstants;


/**
 * @author Issam.Maiza
 *
 *         Error handler to show a CMS managed error page. This is the catch
 *         controller that handles GET requests that are not handled by
 *         other controllers.
 */

@Controller
@Scope("tenant")
public class ServerErrorPageController extends AbstractPageController {

	private static final String DESKTOP_ERROR_500_CMS_PAGE = "errorsystem";
	private static final String EN_CODE = "/en";
	private static final String AR_CODE = "/ar";
	private static final String REFERER = "referer";

	@Resource(name = "uiExperienceService")
	private UiExperienceService uiExperienceService;

	/**
	 * Return the view of the page requested.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the http request
	 * @return the view of the page
	 * @throws CMSItemNotFoundException
	 * @throws IOException
	 */
	@RequestMapping(value = "/serverErrorPage", method = RequestMethod.GET)
	public void error500(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException, IOException
	{
		final String referer = request.getHeader(REFERER);
		if (referer != null && referer.contains(EN_CODE))
		{
			response.sendRedirect(referer.substring(0, referer.indexOf(EN_CODE)) + "/en/serverError");
		}
		else if (referer != null && referer.contains(AR_CODE))
		{
			response.sendRedirect(referer.substring(0, referer.indexOf(AR_CODE)) + "/ar/serverError");
		}
		else
		{
			response.sendRedirect(referer + "/ar/serverError");
		}
	}

	@RequestMapping(value = "/{isoCodeLang}/serverError", method = RequestMethod.GET)
	public String error500Redirect(@PathVariable("isoCodeLang") final String isoCodeLang, final Model model,
			final HttpServletRequest request)
			throws CMSItemNotFoundException {

		storeCmsPageInModel(model, getContentPageForLabelOrId(DESKTOP_ERROR_500_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(DESKTOP_ERROR_500_CMS_PAGE));

			return ControllerConstants.Views.Pages.Error.CustomInternalServerError;

	}

}
