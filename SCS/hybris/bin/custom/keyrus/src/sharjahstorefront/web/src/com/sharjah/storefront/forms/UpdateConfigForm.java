package com.sharjah.storefront.forms;

/**
 * Form for updating field in the Database on cart page.
 * 
 */
public class UpdateConfigForm
{
	private String configuration;

	/**
	 * 
	 * @return Configuration
	 */
	public String getConfiguration()
	{
		return configuration;
	}

	/**
	 * set configuration attribute
	 * 
	 * @param configuration
	 */
	public void setConfiguration(final String configuration)
	{
		this.configuration = configuration;
	}
}
