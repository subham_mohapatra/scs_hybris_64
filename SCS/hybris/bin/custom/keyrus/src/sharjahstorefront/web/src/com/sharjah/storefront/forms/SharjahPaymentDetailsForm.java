package com.sharjah.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.PaymentDetailsForm;


/**
 * 
 * @author Abderrazzak.Blej
 * SharjahPaymentDetailsForm for Payment Form
 */
public class SharjahPaymentDetailsForm extends PaymentDetailsForm
{

	private boolean useDeliveryAddress;
	private SharjahAddressForm billingAddress;
	private String moneyChange;

	/**
	 * 
	 * @return useDeliveryAddress attribute
	 */
	public boolean isUseDeliveryAddress()
	{
		return useDeliveryAddress;
	}

	/**
	 * set useDeliveryAddress attribute
	 * 
	 * @param useDeliveryAddress
	 */
	public void setUseDeliveryAddress(final boolean useDeliveryAddress)
	{
		this.useDeliveryAddress = useDeliveryAddress;
	}

	/**
	 * return billingAddress
	 */
	@Override
	public SharjahAddressForm getBillingAddress()
	{
		return billingAddress;
	}

	/**
	 * set billingAddress attribute
	 * 
	 * @param billingAddress
	 */
	public void setBillingAddress(final SharjahAddressForm billingAddress)
	{
		this.billingAddress = billingAddress;
	}

	/**
	 * 
	 * @return moneyChange
	 */
	public String getMoneyChange()
	{
		return moneyChange;
	}

	/**
	 * set moneyChange attribute
	 * 
	 * @param moneyChange
	 */
	public void setMoneyChange(final String moneyChange)
	{
		this.moneyChange = moneyChange;
	}

}
