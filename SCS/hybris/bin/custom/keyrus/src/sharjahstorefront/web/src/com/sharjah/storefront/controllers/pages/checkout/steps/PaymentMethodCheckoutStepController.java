/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorservices.payment.constants.PaymentConstants;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.core.enums.MoneyChangeEnum;
import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;
import com.sharjah.facades.deliveryarea.SharjahDeliveryAreaFacade;
import com.sharjah.facades.payment.cybersource.impl.DefaultSharjahCyberSourceFacade;
import com.sharjah.facades.payment.impl.SharjahPaymentFacade;
import com.sharjah.facades.user.data.DeliveryAreaData;
import com.sharjah.facades.user.data.DeliveryCityData;
import com.sharjah.storefront.controllers.ControllerConstants;
import com.sharjah.storefront.forms.SharjahAddressForm;
import com.sharjah.storefront.forms.SharjahPaymentDetailsForm;
import com.sharjah.storefront.forms.SharjahSopPaymentDetailsForm;
import com.sharjah.storefront.forms.validation.SharjahPaymentDetailsValidator;
import com.sharjah.storefront.util.SharjahAddressDataUtil;
import com.sharjah.webservices.axon.services.AxonSMSService;


/**
 * DeliveryAddressCheckoutStepController to override method from AbstractCheckoutStepController and implement other method.
 */
@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{

	protected static final Logger LOG = LogManager.getLogger(PaymentMethodCheckoutStepController.class);

	protected static final Map<String, String> CYBERSOURCE_SOP_CARD_TYPES = new HashMap<>();

	private static final String PAYMENT_METHOD = "payment-method";
	private static final String CART_DATA_ATTR = "cartData";
	private static final String UAE_COUNTRY = "AE";
	private static final String CITY_DATA_ATTR = "citiesList";
	private static final String AREA_DATA_ATTR = "areasList";
	private static final String SHIPMENT_CITY = "Sharjah";
	private static final String DELIVERY_ADDRESS = "deliveryAddress";
	private static final String CODE_PAYMENT_MODE = "codePaymentMode";
	private static final String CREDIT_CARD = "creditcard";
	private static final String PAYMENT_FORM_URL = "paymentFormUrl";
	private static final String PLACE_ORDER_FAILED = "checkout.placeOrder.failed";
	private static final String ADDRESS = "Address";


	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.en')}")
	private String orderConfirmationMessageEN;

	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.ar.1')}")
	private String orderConfirmationMessageAR1;
	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.ar.2')}")
	private String orderConfirmationMessageAR2;
	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.ar.3')}")
	private String orderConfirmationMessageAR3;

	@Resource(name = "axonSMSService")
	private AxonSMSService axonSMSService;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "sharjahDeliveryAreaFacade")
	private SharjahDeliveryAreaFacade sharjahDeliveryAreaFacade;

	@Resource(name = "sharjahPaymentDetailsValidator")
	private SharjahPaymentDetailsValidator sharjahPaymentDetailsValidator;

	@Resource
	private SharjahPaymentFacade sharjahPaymentFacade;

	@Resource
	private DefaultSharjahCyberSourceFacade sharjahCyberSourceFacade;

	@Resource
	private AcceleratorCheckoutFacade acceleratorCheckoutFacade;

	@Resource
	private PaymentModeService paymentModeService;

	@Resource
	private EnumerationService enumerationService;

	@Resource
	private CartService cartService;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private ModelService modelService;

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@ModelAttribute("cardTypes")
	public Collection<CardTypeData> getCardTypes()
	{
		return getCheckoutFacade().getSupportedCardTypes();
	}

	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		final List<SelectOption> months = new ArrayList<>();

		months.add(new SelectOption("1", "01"));
		months.add(new SelectOption("2", "02"));
		months.add(new SelectOption("3", "03"));
		months.add(new SelectOption("4", "04"));
		months.add(new SelectOption("5", "05"));
		months.add(new SelectOption("6", "06"));
		months.add(new SelectOption("7", "07"));
		months.add(new SelectOption("8", "08"));
		months.add(new SelectOption("9", "09"));
		months.add(new SelectOption("10", "10"));
		months.add(new SelectOption("11", "11"));
		months.add(new SelectOption("12", "12"));

		return months;
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		final List<SelectOption> startYears = new ArrayList<>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i > calender.get(Calendar.YEAR) - 6; i--)
		{
			startYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return startYears;
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < calender.get(Calendar.YEAR) + 11; i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	@ModelAttribute("availableCashChange")
	public List<SelectOption> getAvailableCashChange()
	{
		final List<SelectOption> availableCashChange = new ArrayList<>();

		final LanguageData currentlanguage = getCurrentLanguage();
		final Locale currentLocale = new Locale(currentlanguage.getIsocode());

		enumerationService.getEnumerationValues(MoneyChangeEnum._TYPECODE).forEach((reason) -> {
			final SelectOption selectOption = new SelectOption(reason.getCode(),
					enumerationService.getEnumerationName(reason, currentLocale));
			availableCashChange.add(selectOption);
		});

		return availableCashChange;
	}

	/**
	 * Recalculate the cart.
	 *
	 * @param paymentMode
	 *           The payment mode
	 * @return The cart data
	 */
	@RequestMapping(value = "/{paymentMode}", method = RequestMethod.GET)
	@ResponseBody
	public CartData recalculateCart(@PathVariable(value = "paymentMode") final String paymentMode)
	{
		recalculateCartByPaymentMode(paymentMode);

		return getCheckoutFacade().getCheckoutCart();
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final CartModel cart = cartService.getSessionCart();
		getCheckoutFacade().setDeliveryModeIfAvailable();
		setupAddPaymentPage(model);

		// If not using HOP or SOP we need to build up the payment details form
		final SharjahPaymentDetailsForm sharjahPaymentDetailsForm = new SharjahPaymentDetailsForm();
		if (cart.getMoneyChange() != null)
		{
			sharjahPaymentDetailsForm.setMoneyChange(cart.getMoneyChange().getCode());
		}

		final SharjahAddressForm sharjahAddressForm = new SharjahAddressForm();
		sharjahPaymentDetailsForm.setBillingAddress(sharjahAddressForm);
		model.addAttribute(sharjahPaymentDetailsForm);
		


		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		final CartModel cartModel = cartService.getSessionCart();

		model.addAttribute(CART_DATA_ATTR, cartData);
		model.addAttribute(DELIVERY_ADDRESS, cartData.getDeliveryAddress());
		if (cartModel.getPaymentMode() != null)
		{
			model.addAttribute(CODE_PAYMENT_MODE, cartModel.getPaymentMode().getCode());
			//set creditcard payment mode
			recalculateCartByPaymentMode(cartModel.getPaymentMode().getCode());
		}else{
			model.addAttribute(CODE_PAYMENT_MODE,CREDIT_CARD);
			//set creditcard payment mode (default)
			recalculateCartByPaymentMode(CyberSourceConstants.PAYMENT_CREDITCARD);
		}
		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	@RequestMapping(value =
	{ "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, final SharjahPaymentDetailsForm sharjahPaymentDetailsForm,
			final BindingResult bindingResult, final HttpServletRequest request, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		sharjahPaymentDetailsValidator.validate(sharjahPaymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(CART_DATA_ATTR, cartData);
		model.addAttribute(DELIVERY_ADDRESS, cartData.getDeliveryAddress());
		final CartModel cartModel = cartService.getSessionCart();

		if (bindingResult.hasErrors())
		{
			if (cartModel.getPaymentMode() != null)
			{
				model.addAttribute(CODE_PAYMENT_MODE, cartModel.getPaymentMode().getCode());
			}
			else
			{
				model.addAttribute(CODE_PAYMENT_MODE, CREDIT_CARD);
			}
			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
		}

		final AddressData addressData;
		if (Boolean.TRUE.equals(sharjahPaymentDetailsForm.isUseDeliveryAddress()))
		{
			addressData = cartData.getDeliveryAddress();
			if (addressData == null)
			{
				GlobalMessages.addErrorMessage(model,
						"checkout.multi.paymentMethod.createSubscription.billingAddress.noneSelectedMsg");
				return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
			}

			addressData.setBillingAddress(true); // mark this as billing address
		}
		else
		{
			final SharjahAddressForm sharjahAddressForm = sharjahPaymentDetailsForm.getBillingAddress();
			addressData = new AddressData();
			fillInAddressData(addressData, sharjahAddressForm);
		}
		if (sharjahPaymentDetailsForm.getBillingAddress() != null && StringUtils.isNotEmpty(sharjahPaymentDetailsForm.getBillingAddress().getName()))
		{
			addressData.setName(sharjahPaymentDetailsForm.getBillingAddress().getName());
		}
		else
		{
			addressData.setName(ADDRESS +" "+ System.currentTimeMillis());
		}

		sharjahPaymentFacade.savePaymentAddress(addressData);

		final String cashChangeCode = sharjahPaymentDetailsForm.getMoneyChange();
		if (StringUtils.isNotBlank(cashChangeCode))
		{
			updateCartCashChange(cashChangeCode);
		}

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		final String paymentMode = sharjahPaymentDetailsForm.getPaymentId();
		recalculateCartByPaymentMode(paymentMode);

		return placeOrder(request, model, redirectModel);
	}

	/**
	 * Get the payment details form.
	 *
	 * @param countryIsoCode
	 *           The country's ISO code
	 * @param useDeliveryAddress
	 *           The user's delivery address
	 * @param model
	 *           The Sping MVC model
	 * @return The payment address form
	 */
	@RequestMapping(value = "/paymentAddressForm", method = RequestMethod.GET)
	public String getCountryAddressForm(@RequestParam("countryIsoCode") final String countryIsoCode,
			@RequestParam("useDeliveryAddress") final boolean useDeliveryAddress, final Model model)
	{
		model.addAttribute("supportedCountries", getCountries());
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIsoCode));
		model.addAttribute("country", countryIsoCode);
		model.addAttribute(CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());
		model.addAttribute(AREA_DATA_ATTR, StringUtils.EMPTY);

		final SharjahPaymentDetailsForm sharjahPaymentDetailsForm = new SharjahPaymentDetailsForm();
		model.addAttribute("sharjahPaymentDetailsForm", sharjahPaymentDetailsForm);
		if (useDeliveryAddress)
		{
			final AddressData deliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
			final SharjahAddressForm sharjahAddressForm = new SharjahAddressForm();
			final SharjahAddressDataUtil sharjahAddressDataUtil = new SharjahAddressDataUtil();
			sharjahAddressDataUtil.convert(deliveryAddress, sharjahAddressForm);
			sharjahPaymentDetailsForm.setBillingAddress(sharjahAddressForm);
			if (deliveryAddress.getDeliveryCity() != null)
			{
				model.addAttribute(AREA_DATA_ATTR,
						sharjahDeliveryAreaFacade.getDeliveryAreasForCityCode(deliveryAddress.getDeliveryCity().getCode()));
			}

		}
		return ControllerConstants.Views.Fragments.Checkout.PaymentAddressForm;
	}

	/**
	 *
	 * @param payTransactionMode
	 */
	private void recalculateCartByPaymentMode(final String payTransactionMode)
	{
		try
		{
			final CartModel cart = cartService.getSessionCart();
			final PaymentModeModel paymentMode = paymentModeService.getPaymentModeForCode(payTransactionMode);
			cart.setPaymentMode(paymentMode);
			modelService.save(cart);

			final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
			commerceCartParameter.setEnableHooks(true);
			commerceCartParameter.setCart(cart);
			commerceCartService.recalculateCart(commerceCartParameter);
		}
		catch (final CalculationException e)
		{
			LOG.warn("An error occured during the recalculation of the cart", e);
		}
	}

	private void updateCartCashChange(final String cashChangeCode)
	{

		final CartModel cart = cartService.getSessionCart();
		final MoneyChangeEnum moneyChangeEnum = enumerationService.getEnumerationValue(MoneyChangeEnum._TYPECODE, cashChangeCode);
		cart.setMoneyChange(moneyChangeEnum);
		modelService.save(cart);
	}

	protected boolean checkPaymentSubscription(final Model model, @Valid final SharjahPaymentDetailsForm sharjahPaymentDetailsForm,
			final CCPaymentInfoData newPaymentSubscription)
	{
		if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getSubscriptionId()))
		{
			if (Boolean.TRUE.equals(sharjahPaymentDetailsForm.getSaveInAccount())
					&& getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
			return false;
		}
		return true;
	}

	protected void fillInPaymentData(@Valid final SharjahPaymentDetailsForm sharjahPaymentDetailsForm,
			final CCPaymentInfoData paymentInfoData)
	{
		paymentInfoData.setId(sharjahPaymentDetailsForm.getPaymentId());
		paymentInfoData.setCardType(sharjahPaymentDetailsForm.getCardTypeCode());
		paymentInfoData.setAccountHolderName(sharjahPaymentDetailsForm.getNameOnCard());
		paymentInfoData.setCardNumber(sharjahPaymentDetailsForm.getCardNumber());
		paymentInfoData.setStartMonth(sharjahPaymentDetailsForm.getStartMonth());
		paymentInfoData.setStartYear(sharjahPaymentDetailsForm.getStartYear());
		paymentInfoData.setExpiryMonth(sharjahPaymentDetailsForm.getExpiryMonth());
		paymentInfoData.setExpiryYear(sharjahPaymentDetailsForm.getExpiryYear());
		if (Boolean.TRUE.equals(sharjahPaymentDetailsForm.getSaveInAccount())
				|| getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			paymentInfoData.setSaved(true);
		}
		paymentInfoData.setIssueNumber(sharjahPaymentDetailsForm.getIssueNumber());
	}

	protected void fillInAddressData(final AddressData addressData, final SharjahAddressForm sharjahAddressForm)
	{
		if (sharjahAddressForm != null)
		{
			addressData.setId(sharjahAddressForm.getAddressId());
			addressData.setFirstName(sharjahAddressForm.getFirstName());
			addressData.setLastName(sharjahAddressForm.getLastName());
			addressData.setCountry(getI18NFacade().getCountryForIsocode(UAE_COUNTRY));
			addressData.setLine1(sharjahAddressForm.getLine1());
			addressData.setPhone(sharjahAddressForm.getPhone());

			final String townCity = sharjahAddressForm.getTownCity();
			addressData.setTown(townCity);

			final DeliveryCityData deliveryCityData = new DeliveryCityData();
			deliveryCityData.setCode(townCity);
			addressData.setDeliveryCity(deliveryCityData);

			final DeliveryAreaData deliveryAreaData = new DeliveryAreaData();
			deliveryAreaData.setCode(sharjahAddressForm.getState());
			addressData.setDeliveryArea(deliveryAreaData);

			addressData.setBuilding(sharjahAddressForm.getBuilding());
			addressData.setApartment(sharjahAddressForm.getApartment());
			addressData.setLandmark(sharjahAddressForm.getLandmark());

			addressData.setBillingAddress(true);

			if (SHIPMENT_CITY.equals(townCity) && Boolean.TRUE.equals(sharjahAddressForm.getShippingAddress()))
			{
				addressData.setShippingAddress(true);
			}
			else
			{
				addressData.setShippingAddress(false);
			}
		}
	}


	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	@RequireHardLogIn
	public String remove(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getUserFacade().unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return getCheckoutStep().currentStep();
	}

	/**
	 * This method gets called when the "Use These Payment Details" button is clicked. It sets the selected payment
	 * method on the checkout facade and reloads the page highlighting the selected payment method.
	 *
	 * @param selectedPaymentMethodId
	 *           - the id of the payment method to use.
	 * @param model
	 *           The Spring MVC model
	 * @param request
	 *           The request
	 * @param redirectModel
	 *           The Spring MVC redirect attributes
	 * @return - a URL to the page to load.
	 * @throws CMSItemNotFoundException
	 *            If the CMS page does not exist
	 */
	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (StringUtils.isNotBlank(selectedPaymentMethodId))
		{
			getCheckoutFacade().setPaymentDetails(selectedPaymentMethodId);
		}
		recalculateCartByPaymentMode(CyberSourceConstants.PAYMENT_CREDITCARD);
		return placeOrder(request, model, redirectModel);
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CardTypeData createCardTypeData(final String code, final String name)
	{
		final CardTypeData cardTypeData = new CardTypeData();
		cardTypeData.setCode(code);
		cardTypeData.setName(name);
		return cardTypeData;
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		model.addAttribute(CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		model.addAttribute("paymentModes", findPaymentModes());

		final SharjahSopPaymentDetailsForm paymentDetailsForm = new SharjahSopPaymentDetailsForm();
		model.addAttribute("paymentDetailsForm", paymentDetailsForm);
		model.addAttribute(PAYMENT_FORM_URL, StringUtils.remove(REDIRECT_URL_ADD_PAYMENT_METHOD, REDIRECT_PREFIX));

		model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));
	}

	protected void setupSilentOrderPostPage(final SharjahSopPaymentDetailsForm sharjahSopPaymentDetailsForm, final Model model)
	{
		try
		{
			final PaymentData silentOrderPageData = getPaymentFacade().beginSopCreateSubscription("/checkout/multi/sop/response",
					"/integration/merchant_callback");
			model.addAttribute("silentOrderPageData", silentOrderPageData);
			sharjahSopPaymentDetailsForm.setParameters(silentOrderPageData.getParameters());
			model.addAttribute(PAYMENT_FORM_URL, silentOrderPageData.getPostUrl());
		}
		catch (final IllegalArgumentException e)
		{
			model.addAttribute(PAYMENT_FORM_URL, "");
			model.addAttribute("silentOrderPageData", null);
			LOG.warn("Failed to set up silent order post page", e);
			GlobalMessages.addErrorMessage(model, "checkout.multi.sop.globalError");
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute("silentOrderPostForm", new SharjahPaymentDetailsForm());
		model.addAttribute(CART_DATA_ATTR, cartData);
		model.addAttribute(DELIVERY_ADDRESS, cartData.getDeliveryAddress());
		model.addAttribute("sharjahSopPaymentDetailsForm", sharjahSopPaymentDetailsForm);
		model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));
		model.addAttribute("sopCardTypes", getSopCardTypes());
		if (StringUtils.isNotBlank(sharjahSopPaymentDetailsForm.getBillTo_country()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(sharjahSopPaymentDetailsForm.getBillTo_country()));
			model.addAttribute("country", sharjahSopPaymentDetailsForm.getBillTo_country());
		}
	}

	protected Collection<CardTypeData> getSopCardTypes()
	{
		final Collection<CardTypeData> sopCardTypes = new ArrayList<>();

		final List<CardTypeData> supportedCardTypes = getCheckoutFacade().getSupportedCardTypes();
		for (final CardTypeData supportedCardType : supportedCardTypes)
		{
			// Add credit cards for all supported cards that have mappings for cybersource SOP
			if (CYBERSOURCE_SOP_CARD_TYPES.containsKey(supportedCardType.getCode()))
			{
				sopCardTypes.add(
						createCardTypeData(CYBERSOURCE_SOP_CARD_TYPES.get(supportedCardType.getCode()), supportedCardType.getName()));
			}
		}
		return sopCardTypes;
	}

	private List<StandardPaymentModeModel> findPaymentModes()
	{
		final List<StandardPaymentModeModel> sharjahPaymentModes = new ArrayList<>();
		final List<PaymentModeModel> paymentModes = paymentModeService.getAllPaymentModes();

		for (final PaymentModeModel paymentModeModel : CollectionUtils.emptyIfNull(paymentModes))
		{
			if (paymentModeModel instanceof StandardPaymentModeModel && Boolean.TRUE.equals(paymentModeModel.getActive()))
			{
				sharjahPaymentModes.add((StandardPaymentModeModel) paymentModeModel);
			}
		}
		return sharjahPaymentModes;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}

	static
	{
		// Map hybris card type to Cybersource SOP credit card
		CYBERSOURCE_SOP_CARD_TYPES.put("visa", "001");
		CYBERSOURCE_SOP_CARD_TYPES.put("master", "002");
		CYBERSOURCE_SOP_CARD_TYPES.put("amex", "003");
		CYBERSOURCE_SOP_CARD_TYPES.put("diners", "005");
		CYBERSOURCE_SOP_CARD_TYPES.put("maestro", "024");
	}

	private String placeOrder(final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		if (validateOrderForm(request, model))
		{
			return enterStep(model, redirectModel);
		}

		//Validate the cart
		if (validateCart(redirectModel))
		{
			// Invalid cart. Bounce back to the cart page.
			return REDIRECT_PREFIX + "/cart";
		}

		final CheckoutPciOptionEnum subscriptionPciOption = getCheckoutFlowFacade().getSubscriptionPciOption();
		Assert.isTrue(CheckoutPciOptionEnum.HOP.equals(subscriptionPciOption),
				"The HOP checkout option is not set, check properties");

		setCheckoutStepLinksForModel(model, getCheckoutStep());
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));

		final CartModel cart = cartService.getSessionCart();
		if (cart != null && cart.getPaymentMode() != null)
		{
			switch (cart.getPaymentMode().getCode())
			{
				case CyberSourceConstants.PAYMENT_CREDITCARD:
					sharjahPaymentFacade.setCustomerPaymentAddress(cart);
					final String saveCardOption = request.getParameter("saveCard");
					return redirectToHop(model, redirectModel, Boolean.parseBoolean(saveCardOption));
				case CyberSourceConstants.PAYMENT_COD:
				case CyberSourceConstants.PAYMENT_CREDITCARD_OD:
					sharjahPaymentFacade.saveOnDeliveryPaymentTransactionEntry(cart);
					return placeOrderQuietly(model, redirectModel);
				default:
					break;
			}
		}

		LOG.error("Unimplemented payment mode");
		GlobalMessages.addErrorMessage(model, PLACE_ORDER_FAILED);
		return enterStep(model, redirectModel);
	}

	private String placeOrderQuietly(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		try
		{
			final OrderData orderData = acceleratorCheckoutFacade.placeOrder();

			//Send SMS Axon

			String phoneNumber = orderData.getShareholderPhoneNumber();
			if (StringUtils.isEmpty(phoneNumber) && orderData.getDeliveryAddress() != null)
			{
				phoneNumber = orderData.getDeliveryAddress().getPhone();
			}
			String smsLang = "english";
			if ("ar".equals(i18nService.getCurrentLocale().getLanguage()))
			{
				smsLang = "arabic";
			}

			axonSMSService.sendSMS(phoneNumber, getSMSOrderConfirmationMessage(orderData), smsLang);

			return redirectToOrderConfirmationPage(orderData);
		}
		catch (final InvalidCartException e)
		{
			LOG.error("Error on placeOrder", e);
			GlobalMessages.addErrorMessage(model, PLACE_ORDER_FAILED);
			return enterStep(model, redirectModel);
		}
	}

	/**
	 * 
	 * @param orderData
	 * @return
	 */
	private String getSMSOrderConfirmationMessage(final OrderData orderData)
	{
		final String orderConfirmationMessageAR = orderConfirmationMessageAR1 + " " + orderData.getCode() + " "
				+ orderConfirmationMessageAR2 + " " + orderData.getDeliverySlot().getDeliveryDate() + " "
				+ orderConfirmationMessageAR3 + " " + orderData.getDeliverySlot().getBeginTime() + " - "
				+ orderData.getDeliverySlot().getEndTime();

		final StringBuilder confirmationText = new StringBuilder(
				Locale.ENGLISH.equals(i18nService.getCurrentLocale()) ? orderConfirmationMessageEN : orderConfirmationMessageAR);
		if (Locale.ENGLISH.equals(i18nService.getCurrentLocale()))
		{
			confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1, orderData.getCode());
			confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1,
					orderData.getDeliverySlot().getDeliveryDate());
			confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1,
					orderData.getDeliverySlot().getBeginTime() + " - " + orderData.getDeliverySlot().getEndTime());
		}


		return confirmationText.toString();
	}

	private String redirectToHop(final Model model, final RedirectAttributes redirectModel, final boolean saveCard)
			throws CMSItemNotFoundException
	{
		// Redirect the customer to the HOP page or show error message if it fails (e.g. no HOP configurations).
		try
		{
			final PaymentData hostedOrderPageData = sharjahPaymentFacade.beginHopCreateSubscription("/checkout/multi/hop/response",
					"/integration/merchant_callback", saveCard);
			model.addAttribute("hostedOrderPageData", hostedOrderPageData);

			final boolean hopDebugMode = getSiteConfigService().getBoolean(PaymentConstants.PaymentProperties.HOP_DEBUG_MODE,
					false);
			model.addAttribute("hopDebugMode", Boolean.valueOf(hopDebugMode));

			return ControllerConstants.Views.Pages.MultiStepCheckout.HostedOrderPostPage;
		}
		catch (final Exception e)
		{
			LOG.error("Failed to place Order", e);
			GlobalMessages.addErrorMessage(model, PLACE_ORDER_FAILED);
			return enterStep(model, redirectModel);
		}
	}

	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final HttpServletRequest request, final Model model)
	{
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

 
		final String termsCheckOption = request.getParameter("termsCheck");
		if (!Boolean.parseBoolean(termsCheckOption))
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!cartData.isCalculated())
		{
			LOG.error(
					String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@Override
	public DefaultSharjahCyberSourceFacade getCheckoutFacade()
	{
		return sharjahCyberSourceFacade;
	}

}
