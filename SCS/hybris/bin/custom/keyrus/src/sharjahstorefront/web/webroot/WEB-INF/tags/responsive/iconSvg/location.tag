<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
 

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   version="1.1"
   id="Layer_1"
   x="0px"
   y="0px"
   viewBox="0 0 48 48"
   xml:space="preserve"
   sodipodi:docname="Find a store.svg"
   width="48"
   height="48"
   inkscape:version="0.92.2 (5c3e80d, 2017-08-06)"><metadata
     id="metadata2984"><rdf:RDF><cc:Work
         rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" /><dc:title></dc:title></cc:Work></rdf:RDF></metadata><defs
     id="defs2982" /><sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1920"
     inkscape:window-height="1000"
     id="namedview2980"
     showgrid="false"
     inkscape:zoom="2.9733131"
     inkscape:cx="140.12249"
     inkscape:cy="92.269325"
     inkscape:window-x="-11"
     inkscape:window-y="-11"
     inkscape:window-maximized="1"
     inkscape:current-layer="Layer_1" /><style
     type="text/css"
     id="style2201">
	.st0{fill:#CA1C1D;}
	.st1{fill:#7E8083;}
	.st2{fill-rule:evenodd;clip-rule:evenodd;fill:#CA1C1D;}
	.st3{fill-rule:evenodd;clip-rule:evenodd;fill:#7E8083;}
	.st4{fill:#D52027;}
	.st5{fill:#FFFFFF;}
	.st6{fill:#C64C43;}
	.st7{fill:#BC4A3F;}
	.st8{font-family:'Nexa-Book';}
	.st9{font-size:8.6293px;}
	.st10{font-size:6.6293px;}
	.st11{opacity:0.3;fill:#7E8083;}
	.st12{fill:#ED2224;}
	.st13{fill:#68696C;}
</style><g
     id="g2467"
     transform="matrix(2.8217613,0,0,2.9555158,-124.77663,-309.71392)"><g
       id="g2465"><g
         id="g2463"><g
           id="g2461"><path
             class="st1"
             d="m 58.6,108.4 c 0,-0.2 -0.1,-0.2 -0.2,-0.3 -0.5,-0.2 -1.6,-0.6 -2.1,-0.8 -0.2,-0.1 -0.3,-0.1 -0.4,0 -0.8,0.2 -3.1,0.9 -3.1,0.9 0,0 -2.5,-0.8 -3.3,-1 -0.2,-0.1 -0.3,-0.1 -0.4,0 -0.5,0.2 -1.6,0.6 -2.1,0.8 -0.2,0.1 -0.2,0.2 -0.2,0.3 -0.2,1.4 -1.1,9.8 -1.2,11.3 0,0.2 0.1,0.2 0.3,0.1 0.7,-0.3 2.4,-1 3,-1.3 0.1,-0.1 0.2,-0.1 0.4,0 0.7,0.3 2.7,1.2 3.4,1.5 0.1,0.1 0.3,0.1 0.4,0 0.7,-0.3 2.6,-1.2 3.3,-1.5 0.1,-0.1 0.3,-0.1 0.5,0 0.6,0.3 2.3,1 2.9,1.3 0.1,0 0.2,0 0.2,-0.1 -0.4,-1.4 -1.2,-9.7 -1.4,-11.2 z m -4,10.8 v 0 c 0,0 0,0 0,0 l -1.9,-1.9 v 0 l -0.1,-0.1 -2,2 c -0.4,-0.2 -0.8,-0.4 -1.1,-0.5 l 2.3,-2.3 -5.4,-5.4 c 0,-0.1 0,-0.2 0,-0.3 0,-0.5 0.1,-0.9 0.1,-1.2 l 1.2,1.2 1.5,-1.5 v 0 l 1.5,-1.5 c 0.4,0.1 0.9,0.3 1.3,0.4 l -2.9,2.9 v 0 l -0.6,0.6 7.2,7.1 c -0.3,0.1 -0.7,0.3 -1.1,0.5 z"
             id="path2455"
             inkscape:connector-curvature="0"
             style="fill:#7e8083" /><path
             class="st4"
             d="m 54.4,105.7 c 1.8,0 3.3,1.5 3.3,3.3 0,0 0,0.9 -0.3,1.5 -0.7,1.5 -2.3,3.2 -2.8,3.7 -0.1,0.1 -0.2,0.1 -0.4,0 -0.5,-0.5 -2.1,-2.1 -2.8,-3.7 -0.2,-0.4 -0.3,-1.4 -0.3,-1.5 0,-1.9 1.5,-3.3 3.3,-3.3 z"
             id="path2457"
             inkscape:connector-curvature="0"
             style="fill:#d52027" /><path
             class="st5"
             d="m 54.4,107.2 c -0.9,0 -1.7,0.8 -1.7,1.7 0,0.9 0.8,1.7 1.7,1.7 0.9,0 1.7,-0.8 1.7,-1.7 0,-0.9 -0.8,-1.7 -1.7,-1.7 z m 0,2.7 c -0.6,0 -1,-0.4 -1,-1 0,-0.6 0.4,-1 1,-1 0.6,0 1,0.4 1,1 0,0.6 -0.5,1 -1,1 z"
             id="path2459"
             inkscape:connector-curvature="0"
             style="fill:#ffffff" /></g></g></g></g></svg>