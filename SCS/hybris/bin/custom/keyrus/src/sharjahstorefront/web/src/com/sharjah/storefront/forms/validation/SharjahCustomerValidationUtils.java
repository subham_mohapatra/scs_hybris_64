package com.sharjah.storefront.forms.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.sharjah.shareholder.service.impl.DefaultSharjahShareholderService;



/**
 * Utility class for the validation of a Customer
 *
 */
@Component("sharjahCustomerValidationUtils")
public class SharjahCustomerValidationUtils
{

	private SharjahCustomerValidationUtils()
	{
		//empty
	}

	private static final Logger LOG = Logger.getLogger(SharjahCustomerValidationUtils.class);

	private static final Pattern PHONE_REGEX = Pattern.compile("^\\+[1-9]{1}[0-9]{3,14}$");
	private final static String DATE_FORMAT = "dd/MM/yyyy";

	private final static Set<String> SHAREHOLDER_NUMBER_PREFIXS = new TreeSet<>();

	@Resource(name = "sharjahShareholderService")
	private DefaultSharjahShareholderService defaultSharjahShareholderService;

	{
		SHAREHOLDER_NUMBER_PREFIXS.add("G");
		SHAREHOLDER_NUMBER_PREFIXS.add("W");
		SHAREHOLDER_NUMBER_PREFIXS.add("S");
	}

	/**
	 * Check if phone is not null/blank and if not > 255
	 * 
	 * @param phone
	 * @return
	 */
	public boolean isValidPhoneNumber(final String phone)
	{
		if (StringUtils.length(phone) > 255 || StringUtils.isEmpty(phone))
		{
			return false;
		}

		final Matcher matcher = PHONE_REGEX.matcher(phone);
		return matcher.matches();
	}

	/**
	 * Check if the value is a valid date
	 * 
	 * @param value
	 * @return
	 */
	public boolean isValidDateFormat(final String value)
	{
		Date date = null;
		try
		{
			final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date)))
			{
				date = null;
			}
		}
		catch (final ParseException ex)
		{
			LOG.error("There was an error parsing [" + value + "]", ex);
		}
		return date != null;
	}

	/**
	 * Check if the prefix is empty and if it's valid
	 * 
	 * @param errors
	 * @param membershipNumberPrefix
	 * @param propertyName
	 * @param property
	 */
	public void isValidShareholderNumber(final Errors errors, final String membershipNumberPrefix, final String membershipNumber,
			final String propertyName,
			final String property)
	{
		if (StringUtils.isEmpty(membershipNumber) && StringUtils.isNotEmpty(membershipNumberPrefix)
				&& !SHAREHOLDER_NUMBER_PREFIXS.contains(membershipNumberPrefix))
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 * Check if the shareholder number exist
	 * 
	 * @param errors
	 * @param membershipNumberPrefix
	 * @param membershipNumber
	 * @param propertyName
	 * @param property
	 */
	public void isShareholderExist(final Errors errors, final String membershipNumberPrefix, final String membershipNumber,
			final String propertyName, final String property)
	{
		if (StringUtils.isNotEmpty(membershipNumberPrefix) && StringUtils.isNotEmpty(membershipNumber)
				&& defaultSharjahShareholderService
						.findShareholderByNumber(membershipNumberPrefix.trim() + membershipNumber.trim()) == null)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 * Check if the name is not blank and not too long
	 * 
	 * @param errors
	 * @param name
	 * @param propertyName
	 * @param property
	 */
	public void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, property);
		}
		else if (StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 * Used to validate the Shareholder number
	 * 
	 * @param errors
	 * @param shareholderNumberPrefix
	 * @param shareholderNumber
	 * @param propertyName
	 * @param property
	 */
	public void validateShareholderNumber(final Errors errors, final String shareholderNumberPrefix,
			final String shareholderNumber, final String propertyName, final String property)
	{
		if (StringUtils.isEmpty(shareholderNumberPrefix)
				&& (StringUtils.isEmpty(shareholderNumber) || !StringUtils.isNumeric(shareholderNumber)))
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 * Used to validate the Shareholder prefix number
	 * 
	 * @param errors
	 * @param shareholderNumberPrefix
	 * @param shareholderNumber
	 * @param propertyName
	 * @param property
	 */
	public void validateShareholderPrefixNumber(final Errors errors, final String shareholderNumberPrefix,
			final String shareholderNumber, final String propertyName, final String property)
	{
		if (StringUtils.isNotEmpty(shareholderNumber) && StringUtils.isEmpty(shareholderNumberPrefix))
		{
			errors.rejectValue(propertyName, property);
		}
	}

}
