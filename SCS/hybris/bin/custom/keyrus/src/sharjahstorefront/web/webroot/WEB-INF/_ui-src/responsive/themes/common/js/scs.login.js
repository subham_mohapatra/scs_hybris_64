$(document).ready(function() {
	console.log('scs.login.js : start executed');
        $("#register\\.membershipNumber").blur(function(){
        var value = $(this).val();
        var prefix =  document.getElementById('register.membershipPrefixNumber').value;
        if(value !== null && prefix !== null && value !== '' && prefix !== ''){
        	 $.ajax({
                 url: ACC.config.encodedContextPath + '/login/verifyShareholder?number=' + prefix+value,
                 type: "GET",
                 data: {},
                 success: function(shareholderData) {
               	  document.getElementById('register.firstName').value=shareholderData.firstName;
               	  document.getElementById('register.lastName').value=shareholderData.lastName;
               	  document.getElementById('register.email').value=shareholderData.email;
               	  if(shareholderData.phoneNumber !== null){
               		var prefix = '00971';
               		var prefix1 = '+971';
               		var mobilePhone = shareholderData.phoneNumber;
               		if(mobilePhone.startsWith(prefix))
               			{
                          document.getElementById('register.mobileNumber').value=mobilePhone.replace(prefix,'');
               			}
               		if(mobilePhone.startsWith(prefix1))
           			   {
                          document.getElementById('register.mobileNumber').value=mobilePhone.replace(prefix1,'');
           			   }
               		if(!mobilePhone.startsWith(prefix) | !mobilePhone.startsWith(prefix1))
               		   {
               			  document.getElementById('register.mobileNumber').value=shareholderData.phoneNumber;
               		   }
                  }               	 
               	  if(shareholderData.gender !== null){
               		  if(shareholderData.gender === 'MALE'){
               			  document.getElementById('profile.genderCode1').checked = true;
               		  }else{
               			document.getElementById('profile.genderCode2').checked = true; 
               		  }              		  
               	  }
               	  if(shareholderData.email !== null & shareholderData.email !== ''){
               		document.getElementById('register.email').readOnly  = true; 
               	  }else{ 
               		document.getElementById('register.email').readOnly  = false; 
               	  }
                 }
               });
        	 
        }
         
       
        });
        
        
        $("#register\\.membershipPrefixNumber").blur(function(){
            var prefix = $(this).val();
            var  value =  document.getElementById('register.membershipNumber').value;
            if(value !== null && prefix !== null && value !== '' && prefix !== ''){
            	 $.ajax({
                     url: ACC.config.encodedContextPath + '/login/verifyShareholder?number=' + prefix+value,
                     type: "GET",
                     data: {},
                     success: function(shareholderData) {
                   	  document.getElementById('register.firstName').value=shareholderData.firstName;
                   	  document.getElementById('register.lastName').value=shareholderData.lastName;
                   	  document.getElementById('register.email').value=shareholderData.email; 
                   	  if(shareholderData.phoneNumber !== null){
                     		var prefix = '00971';
                       		var prefix1 = '+971';
                       		var mobilePhone = shareholderData.phoneNumber;
                       		if(mobilePhone.startsWith(prefix))
                       			{
                                  document.getElementById('register.mobileNumber').value=mobilePhone.replace(prefix,'');
                       			}
                       		if(mobilePhone.startsWith(prefix1))
                   			   {
                                  document.getElementById('register.mobileNumber').value=mobilePhone.replace(prefix1,'');
                   			   }
                       		if(!mobilePhone.startsWith(prefix) | !mobilePhone.startsWith(prefix1))
                       		   {
                       			  document.getElementById('register.mobileNumber').value=shareholderData.phoneNumber;
                       		   }
                   	  }
                 	  if(shareholderData.gender !== null){
                 		  if(shareholderData.gender === 'MALE'){
                 			  document.getElementById('profile.genderCode1').checked = true;
                 		  }else{
                 			document.getElementById('profile.genderCode2').checked = true; 
                 		  }              		  
                 	  }
                   	  if(shareholderData.email !== null & shareholderData.email !== ''){
                   		  document.getElementById('register.email').readOnly  = true; 
                   	  }else{
                   		document.getElementById('register.email').readOnly  = false; 
                   	  }
                     }
                   });
            	 
            }
             
           
            });

	console.log('scs.login.js :  end executed');
});