/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.ordergridform.OrderGridFormFacade;
import de.hybris.platform.acceleratorfacades.product.data.ReadOnlyOrderGridData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateEmailForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdatePasswordForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.EmailValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.PasswordValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.sharjah.core.event.CancelOrderEvent;
import com.google.common.collect.Lists;
import com.sharjah.core.order.OrderExportType;
import com.sharjah.core.order.service.SharjahOrderExportService;
import com.sharjah.core.stock.SharjahStockService;
import com.sharjah.facades.customer.SharjahCustomerFacade;
import com.sharjah.facades.deliveryarea.SharjahDeliveryAreaFacade;
import com.sharjah.facades.user.data.DeliveryCityData;
import com.sharjah.storefront.constants.SharjahStoreFrontConstants;
import com.sharjah.storefront.controllers.ControllerConstants;
import com.sharjah.storefront.forms.SharjahAddressForm;
import com.sharjah.storefront.forms.SharjahUpdateProfileForm;
import com.sharjah.storefront.forms.validation.SharjahAddressValidator;
import com.sharjah.storefront.forms.validation.SharjahProfileValidator;
import com.sharjah.storefront.util.SharjahAccountControllerUtil;
import com.sharjah.storefront.util.SharjahAddressDataUtil;


/**
 * Controller for home page
 */
@Controller
@RequestMapping("/my-account")
public class AccountPageController extends AbstractSearchPageController
{
	// Internal Redirects
	private static final String MY_ACCOUNT_ADDRESS_BOOK_URL = "/my-account/address-book";
	private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_ADDRESS_BOOK_URL;
	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";
	private static final String REDIRECT_TO_EDIT_ADDRESS_PAGE = REDIRECT_PREFIX + "/my-account/edit-address/";
	private static final String REDIRECT_TO_UPDATE_EMAIL_PAGE = REDIRECT_PREFIX + "/my-account/update-email";
	private static final String REDIRECT_TO_UPDATE_PROFILE = REDIRECT_PREFIX + "/my-account/update-profile";
	private static final String REDIRECT_TO_PASSWORD_UPDATE_PAGE = REDIRECT_PREFIX + "/my-account/update-password";
	private static final String REDIRECT_TO_ORDER_HISTORY_PAGE = REDIRECT_PREFIX + "/my-account/orders";
	private static final String ADDRESS = "Address";


	private static final Logger LOG = Logger.getLogger(AccountPageController.class);

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "customerFacade")
	private SharjahCustomerFacade customerFacade;

	@Resource(name = "sharjahAccountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "passwordValidator")
	private PasswordValidator passwordValidator;

	@Resource(name = "sharjahAddressValidator")
	private SharjahAddressValidator addressValidator;

	@Resource(name = "sharjahProfileValidator")
	private SharjahProfileValidator profileValidator;

	@Resource(name = "emailValidator")
	private EmailValidator emailValidator;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "addressVerificationFacade")
	private AddressVerificationFacade addressVerificationFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "orderGridFormFacade")
	private OrderGridFormFacade orderGridFormFacade;

	@Resource(name = "sharjahAddressDataUtil")
	private SharjahAddressDataUtil sharjahAddressDataUtil;

	@Resource(name = "sharjahDeliveryAreaFacade")
	private SharjahDeliveryAreaFacade sharjahDeliveryAreaFacade;

	@Resource(name = "customerAccountService")
	private CustomerAccountService customerAccountService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;

	@Resource(name = "sharjahOrderExportService")
	private SharjahOrderExportService sharjahOrderExportService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "sharjahAccountControllerUtil")
	private SharjahAccountControllerUtil sharjahAccountControllerUtil;

	@Resource
	private SharjahStockService stockService;

	@Resource
	private EventService eventService;

	@Resource
	private BusinessProcessService businessProcessService;

	public void setAddressValidator(final SharjahAddressValidator addressValidator) {
		this.addressValidator = addressValidator;
	}

	@RequestMapping(value = "/addressform", method = RequestMethod.GET)
	public String getCountryAddressForm(@RequestParam("addressCode") final String addressCode,
			@RequestParam("countryIsoCode") final String countryIsoCode, final Model model)
	{
		model.addAttribute("supportedCountries", sharjahAccountControllerUtil.getCountries());
		sharjahAccountControllerUtil.populateModelRegionAndCountry(model, countryIsoCode);

		//model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());

		final DeliveryCityData deliveryCityData = sharjahDeliveryAreaFacade
				.getDeliveryCityByCode(SharjahStoreFrontConstants.SHARJA_CITY);
		final List<DeliveryCityData> deliveryCities = new ArrayList<>();
		deliveryCities.add(deliveryCityData);
		model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, deliveryCities);

		model.addAttribute(SharjahStoreFrontConstants.AREA_DATA_ATTR, StringUtils.EMPTY);

		final SharjahAddressForm addressForm = new SharjahAddressForm();
		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_FORM_ATTR, addressForm);
		for (final AddressData addressData : userFacade.getAddressBook())
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode)
					&& countryIsoCode.equals(addressData.getCountry().getIsocode()))
			{
				model.addAttribute(SharjahStoreFrontConstants.ADDRESS_DATA_ATTR, addressData);
				sharjahAddressDataUtil.convert(addressData, addressForm);
				break;
			}
		}
		return ControllerConstants.Views.Fragments.Account.CountryAddressForm;
	}

	@RequestMapping(method = RequestMethod.GET)
	@RequireHardLogIn
	public String account(final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (ResponsiveUtils.isResponsive())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
			return REDIRECT_PREFIX + "/";
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ACCOUNT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ACCOUNT_CMS_PAGE));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(null));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		// Handle paged search results
		final PageableData pageableData = createPageableData(0, 0, "sort", ShowMode.All);
		final SearchPageData<OrderHistoryData> searchPageData = orderFacade.getPagedOrderHistoryForStatuses(pageableData);
		populateModel(model, searchPageData, ShowMode.All);

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ORDER_HISTORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ORDER_HISTORY_CMS_PAGE));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/order/" + SharjahStoreFrontConstants.ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String order(@PathVariable("orderCode") final String orderCode, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		try
		{
			final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
			model.addAttribute("orderData", orderDetails);

			if (CollectionUtils.isNotEmpty(orderDetails.getConsignments()))
			{
				for (final ConsignmentData consignmentData : orderDetails.getConsignments())
				{
					if (ConsignmentStatus.SHIPPED.equals(consignmentData.getStatus()))
					{
						model.addAttribute("showCancel", false);
					}
				}
			}

			final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("/my-account/orders", getMessageSource().getMessage("text.account.orderHistory", null,
					getI18nService().getCurrentLocale()), null));
			breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.order.orderBreadcrumb", new Object[]
					{ orderDetails.getCode() }, "Order {0}", getI18nService().getCurrentLocale()), null));
			model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR, breadcrumbs);

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a order that does not exist or is not visible", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "system.error.page.not.found", null);
			return REDIRECT_TO_ORDER_HISTORY_PAGE;
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ORDER_DETAIL_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ORDER_DETAIL_CMS_PAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/order/" + SharjahStoreFrontConstants.ORDER_CODE_PATH_VARIABLE_PATTERN
			+ "/getReadOnlyProductVariantMatrix", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getProductVariantMatrixForResponsive(@PathVariable("orderCode") final String orderCode,
			@RequestParam("productCode") final String productCode, final Model model)
	{
		final OrderData orderData = orderFacade.getOrderDetailsForCodeWithoutUser(orderCode);

		final Map<String, ReadOnlyOrderGridData> readOnlyMultiDMap = orderGridFormFacade.getReadOnlyOrderGridForProductInOrder(
				productCode, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES), orderData);
		model.addAttribute("readOnlyMultiDMap", readOnlyMultiDMap);

		return ControllerConstants.Views.Fragments.Checkout.ReadOnlyExpandedOrderForm;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String profile(final Model model) throws CMSItemNotFoundException
	{
		final List<TitleData> titles = userFacade.getTitles();

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		if (customerData.getTitleCode() != null)
		{
			model.addAttribute("title", sharjahAccountControllerUtil.findTitleForCode(titles, customerData.getTitleCode()));
		}

		model.addAttribute("customerData", customerData);

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.PROFILE_CMS_PAGE));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(SharjahStoreFrontConstants.TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editEmail(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final UpdateEmailForm updateEmailForm = new UpdateEmailForm();

		updateEmailForm.setEmail(customerData.getDisplayUid());

		model.addAttribute("updateEmailForm", updateEmailForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_EMAIL_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_EMAIL_CMS_PAGE));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(SharjahStoreFrontConstants.TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-email", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateEmail(final UpdateEmailForm updateEmailForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		emailValidator.validate(updateEmailForm, bindingResult);
		String returnAction = REDIRECT_TO_UPDATE_EMAIL_PAGE;

		if (!bindingResult.hasErrors() && !updateEmailForm.getEmail().equals(updateEmailForm.getChkEmail()))
		{
			bindingResult.rejectValue("chkEmail", "validation.checkEmail.equals", new Object[] {}, "validation.checkEmail.equals");
		}

		if (bindingResult.hasErrors())
		{
			returnAction = setErrorMessagesAndCMSPage(model,
					SharjahStoreFrontConstants.UPDATE_EMAIL_CMS_PAGE);
		}
		else
		{
			try
			{
				customerFacade.changeUid(updateEmailForm.getEmail(), updateEmailForm.getPassword());
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

				// Replace the spring security authentication with the new UID
				final String newUid = customerFacade.getCurrentCustomer().getUid().toLowerCase();
				final Authentication oldAuthentication = SecurityContextHolder.getContext().getAuthentication();
				final UsernamePasswordAuthenticationToken newAuthentication = new UsernamePasswordAuthenticationToken(newUid, null,
						oldAuthentication.getAuthorities());
				newAuthentication.setDetails(oldAuthentication.getDetails());
				SecurityContextHolder.getContext().setAuthentication(newAuthentication);
			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "profile.email.unique");

				returnAction = setErrorMessagesAndCMSPage(model,
						SharjahStoreFrontConstants.UPDATE_EMAIL_CMS_PAGE);
			}
			catch (final PasswordMismatchException passwordMismatchException)
			{
				bindingResult.rejectValue("password", SharjahStoreFrontConstants.PROFILE_CURRENT_PASSWORD_INVALID);

				returnAction = setErrorMessagesAndCMSPage(model,
						SharjahStoreFrontConstants.UPDATE_EMAIL_CMS_PAGE);
			}
		}

		return returnAction;
	}


	/**
	 * Set the error to the page
	 * 
	 * @param model
	 * @param cmsPageLabelOrId
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	public String setErrorMessagesAndCMSPage(final Model model, final String cmsPageLabelOrId) throws CMSItemNotFoundException
	{
		GlobalMessages.addErrorMessage(model, SharjahStoreFrontConstants.FORM_GLOBAL_ERROR);
		storeCmsPageInModel(model, getContentPageForLabelOrId(cmsPageLabelOrId));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(cmsPageLabelOrId));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(SharjahStoreFrontConstants.TEXT_ACCOUNT_PROFILE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(SharjahStoreFrontConstants.TITLE_DATA_ATTR, userFacade.getTitles());

		final CustomerData customerData = customerFacade.getCurrentCustomer();
		final SharjahUpdateProfileForm sharjahUpdateProfileForm = sharjahAccountControllerUtil
				.populateUpdateProfileForm(customerData);

		model.addAttribute("sharjahUpdateProfileForm", sharjahUpdateProfileForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PROFILE_CMS_PAGE));

		sharjahAccountControllerUtil.addUpdateFormData(model);

		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(SharjahStoreFrontConstants.TEXT_ACCOUNT_PROFILE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-profile", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(final SharjahUpdateProfileForm sharjahUpdateProfileForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		profileValidator.validate(sharjahUpdateProfileForm, bindingResult);

		String returnAction = REDIRECT_TO_UPDATE_PROFILE;
		final CustomerData customerData = sharjahAccountControllerUtil.populateCustomerData(sharjahUpdateProfileForm);

		model.addAttribute(SharjahStoreFrontConstants.TITLE_DATA_ATTR, userFacade.getTitles());

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PROFILE_CMS_PAGE));

		if (bindingResult.hasErrors())
		{
			sharjahUpdateProfileForm.setEmail(customerData.getDisplayUid());

			returnAction = setErrorMessagesAndCMSPage(model,
					SharjahStoreFrontConstants.UPDATE_PROFILE_CMS_PAGE);
		}
		else
		{
			try
			{
				customerFacade.updateProfile(customerData);
				GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
						"text.account.profile.confirmationUpdated", null);

			}
			catch (final DuplicateUidException e)
			{
				bindingResult.rejectValue("email", "registration.error.account.exists.title");

				returnAction = setErrorMessagesAndCMSPage(model,
						SharjahStoreFrontConstants.UPDATE_PROFILE_CMS_PAGE);
			}
		}

		sharjahAccountControllerUtil.addUpdateFormData(model);
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(SharjahStoreFrontConstants.TEXT_ACCOUNT_PROFILE));
		return returnAction;
	}

	@RequestMapping(value = "/update-password", method = RequestMethod.GET)
	@RequireHardLogIn
	public String updatePassword(final Model model) throws CMSItemNotFoundException
	{
		final UpdatePasswordForm updatePasswordForm = new UpdatePasswordForm();

		model.addAttribute("updatePasswordForm", updatePasswordForm);

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PASSWORD_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PASSWORD_CMS_PAGE));

		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/update-password", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updatePassword(final UpdatePasswordForm updatePasswordForm, final BindingResult bindingResult,
			final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		passwordValidator.validate(updatePasswordForm, bindingResult);
		if (!bindingResult.hasErrors())
		{
			if (updatePasswordForm.getNewPassword().equals(updatePasswordForm.getCheckNewPassword()))
			{
				try
				{
					customerFacade.changePassword(updatePasswordForm.getCurrentPassword(), updatePasswordForm.getNewPassword());
				}
				catch (final PasswordMismatchException localException)
				{
					bindingResult.rejectValue("currentPassword", SharjahStoreFrontConstants.PROFILE_CURRENT_PASSWORD_INVALID,
							new Object[] {}, SharjahStoreFrontConstants.PROFILE_CURRENT_PASSWORD_INVALID);
				}
			}
			else
			{
				bindingResult.rejectValue("checkNewPassword", "validation.checkPwd.equals", new Object[] {},
						"validation.checkPwd.equals");
			}
		}

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, SharjahStoreFrontConstants.FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PASSWORD_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.UPDATE_PASSWORD_CMS_PAGE));

			model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
					accountBreadcrumbBuilder.getBreadcrumbs("text.account.profile.updatePasswordForm"));
			return getViewForPage(model);
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.account.confirmation.password.updated", null);
			return REDIRECT_TO_PASSWORD_UPDATE_PAGE;
		}
	}

	@RequestMapping(value = "/address-book", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAddressBook(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_DATA_ATTR, userFacade.getAddressBook());

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADDRESS_BOOK_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADDRESS_BOOK_CMS_PAGE));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(SharjahStoreFrontConstants.TEXT_ACCOUNT_ADDRESS_BOOK));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/add-address", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addAddress(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(SharjahStoreFrontConstants.COUNTRY_DATA_ATTR, checkoutFacade.getDeliveryCountries());
		model.addAttribute(SharjahStoreFrontConstants.TITLE_DATA_ATTR, userFacade.getTitles());
		//model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());

		final DeliveryCityData deliveryCityData = sharjahDeliveryAreaFacade
				.getDeliveryCityByCode(SharjahStoreFrontConstants.SHARJA_CITY);
		final List<DeliveryCityData> deliveryCities = new ArrayList<>();
		deliveryCities.add(deliveryCityData);
		model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, deliveryCities);


		model.addAttribute(SharjahStoreFrontConstants.AREA_DATA_ATTR, StringUtils.EMPTY);
		final SharjahAddressForm sharjahAddressForm = sharjahAccountControllerUtil.getPreparedAddressForm();
		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_FORM_ATTR, sharjahAddressForm);
		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute(SharjahStoreFrontConstants.IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL,
				getMessageSource().getMessage(SharjahStoreFrontConstants.TEXT_ACCOUNT_ADDRESS_BOOK, null,
						getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/add-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addAddress(final SharjahAddressForm sharjahAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressValidator.validate(sharjahAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, SharjahStoreFrontConstants.FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
			sharjahAccountControllerUtil.setUpAddressFormAfterError(sharjahAddressForm, model);
			return getViewForPage(model);
		}
		sharjahAddressForm.setCountryIso(SharjahStoreFrontConstants.UAE_COUNTRY);
		final AddressData newAddress = sharjahAddressDataUtil.convertToVisibleAddressData(sharjahAddressForm);
		if (StringUtils.isNotEmpty(sharjahAddressForm.getName()))
		{
			newAddress.setName(sharjahAddressForm.getName());
		}
		else
		{
			newAddress.setName(ADDRESS +" "+ System.currentTimeMillis());
		}
		if (userFacade.isAddressBookEmpty())
		{
			newAddress.setDefaultAddress(true);
		}
		else
		{
			newAddress.setDefaultAddress(
					sharjahAddressForm.getDefaultAddress() != null && sharjahAddressForm.getDefaultAddress().booleanValue());
		}

		sharjahAccountControllerUtil.populateModelRegionAndCountry(model, SharjahStoreFrontConstants.UAE_COUNTRY);
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute(SharjahStoreFrontConstants.IS_DEFAULT_ADDRESS_ATTR,
				Boolean.valueOf(sharjahAccountControllerUtil.isDefaultAddress(sharjahAddressForm.getAddressId())));

		userFacade.addAddress(newAddress);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added",
				null);

		return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
	}

	@RequestMapping(value = "/edit-address/"
			+ SharjahStoreFrontConstants.ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddress(@PathVariable("addressCode") final String addressCode, final Model model)
			throws CMSItemNotFoundException
	{
		final SharjahAddressForm sharjahAddressForm = new SharjahAddressForm();
		model.addAttribute(SharjahStoreFrontConstants.COUNTRY_DATA_ATTR, checkoutFacade.getDeliveryCountries());
		model.addAttribute(SharjahStoreFrontConstants.TITLE_DATA_ATTR, userFacade.getTitles());
		//	model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());

		final DeliveryCityData deliveryCityData = sharjahDeliveryAreaFacade
				.getDeliveryCityByCode(SharjahStoreFrontConstants.SHARJA_CITY);
		final List<DeliveryCityData> deliveryCities = new ArrayList<>();
		deliveryCities.add(deliveryCityData);
		model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, deliveryCities);

		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_FORM_ATTR, sharjahAddressForm);
		final List<AddressData> addressBook = userFacade.getAddressBook();
		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_BOOK_EMPTY_ATTR,
				Boolean.valueOf(CollectionUtils.isEmpty(addressBook)));

		for (final AddressData addressData : addressBook)
		{
			if (addressData.getId() != null && addressData.getId().equals(addressCode))
			{
				model.addAttribute(SharjahStoreFrontConstants.REGIONS_ATTR,
						i18NFacade.getRegionsForCountryIso(addressData.getCountry().getIsocode()));
				model.addAttribute(SharjahStoreFrontConstants.COUNTRY_ATTR, addressData.getCountry().getIsocode());
				model.addAttribute(SharjahStoreFrontConstants.ADDRESS_DATA_ATTR, addressData);
				if (addressData.getDeliveryCity() != null)
				{
					model.addAttribute(SharjahStoreFrontConstants.AREA_DATA_ATTR,
							sharjahDeliveryAreaFacade.getDeliveryAreasForCityCode(addressData.getDeliveryCity().getCode()));
				}
				sharjahAddressDataUtil.convert(addressData, sharjahAddressForm);


				if (sharjahAccountControllerUtil.isDefaultAddress(addressData.getId()))
				{
					sharjahAddressForm.setDefaultAddress(Boolean.TRUE);
					model.addAttribute(SharjahStoreFrontConstants.IS_DEFAULT_ADDRESS_ATTR, Boolean.TRUE);
				}
				else
				{
					sharjahAddressForm.setDefaultAddress(Boolean.FALSE);
					model.addAttribute(SharjahStoreFrontConstants.IS_DEFAULT_ADDRESS_ATTR, Boolean.FALSE);
				}
				break;
			}
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));

		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb(MY_ACCOUNT_ADDRESS_BOOK_URL,
				getMessageSource().getMessage(SharjahStoreFrontConstants.TEXT_ACCOUNT_ADDRESS_BOOK, null,
						getI18nService().getCurrentLocale()), null));
		breadcrumbs.add(new Breadcrumb("#", getMessageSource().getMessage("text.account.addressBook.addEditAddress", null,
				getI18nService().getCurrentLocale()), null));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute("edit", Boolean.TRUE);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/edit-address/"
			+ SharjahStoreFrontConstants.ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	@RequireHardLogIn
	public String editAddress(final SharjahAddressForm sharjahAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressValidator.validate(sharjahAddressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, SharjahStoreFrontConstants.FORM_GLOBAL_ERROR);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
			sharjahAccountControllerUtil.setUpAddressFormAfterError(sharjahAddressForm, model);
			return getViewForPage(model);
		}

		sharjahAddressForm.setCountryIso(SharjahStoreFrontConstants.UAE_COUNTRY);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		final AddressData newAddress = sharjahAddressDataUtil.convertToVisibleAddressData(sharjahAddressForm);
		if (StringUtils.isNotEmpty(sharjahAddressForm.getName()))
		{
			newAddress.setName(sharjahAddressForm.getName());
		}
		else
		{
			newAddress.setName(ADDRESS +" "+ System.currentTimeMillis());
		}
		if (Boolean.TRUE.equals(sharjahAddressForm.getDefaultAddress()) || userFacade.getAddressBook().size() <= 1)
		{
			newAddress.setDefaultAddress(true);
		}

		model.addAttribute(SharjahStoreFrontConstants.REGIONS_ATTR,
				i18NFacade.getRegionsForCountryIso(SharjahStoreFrontConstants.UAE_COUNTRY));
		model.addAttribute(SharjahStoreFrontConstants.COUNTRY_ATTR, SharjahStoreFrontConstants.UAE_COUNTRY);
		model.addAttribute("edit", Boolean.TRUE);
		model.addAttribute(SharjahStoreFrontConstants.IS_DEFAULT_ADDRESS_ATTR,
				Boolean.valueOf(sharjahAccountControllerUtil.isDefaultAddress(sharjahAddressForm.getAddressId())));

		userFacade.editAddress(newAddress);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.updated",
				null);
		return REDIRECT_TO_EDIT_ADDRESS_PAGE + newAddress.getId();
	}

	@RequestMapping(value = "/select-suggested-address", method = RequestMethod.POST)
	public String doSelectSuggestedAddress(final SharjahAddressForm sharjahAddressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = sharjahAddressDataUtil.convertToVisibleAddressData(sharjahAddressForm);

		final CountryData countryData = selectedAddress.getCountry();

		if (!resolveCountryRegions.contains(countryData.getIsocode()))
		{
			selectedAddress.setRegion(null);
		}

		if (Boolean.TRUE.equals(sharjahAddressForm.getEditAddress()))
		{
			userFacade.editAddress(selectedAddress);
		}
		else
		{
			userFacade.addAddress(selectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.added");

		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/remove-address/" + SharjahStoreFrontConstants.ADDRESS_CODE_PATH_VARIABLE_PATTERN, method =
		{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		userFacade.removeAddress(addressData);

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/set-default-address/"
			+ SharjahStoreFrontConstants.ADDRESS_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String setDefaultAddress(@PathVariable("addressCode") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setDefaultAddress(true);
		addressData.setVisibleInAddressBook(true);
		addressData.setId(addressCode);
		userFacade.setDefaultAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
				"account.confirmation.default.address.changed");
		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	@RequestMapping(value = "/payment-details", method = RequestMethod.GET)
	@RequireHardLogIn
	public String paymentDetails(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("customerData", customerFacade.getCurrentCustomer());
		model.addAttribute("paymentInfoData", userFacade.getCCPaymentInfos(true));
		storeCmsPageInModel(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.PAYMENT_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(SharjahStoreFrontConstants.ADD_EDIT_ADDRESS_CMS_PAGE));
		model.addAttribute(SharjahStoreFrontConstants.BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs("text.account.paymentDetails"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/set-default-payment-details", method = RequestMethod.POST)
	@RequireHardLogIn
	public String setDefaultPaymentDetails(@RequestParam final String paymentInfoId)
	{
		CCPaymentInfoData paymentInfoData = null;
		if (StringUtils.isNotBlank(paymentInfoId))
		{
			paymentInfoData = userFacade.getCCPaymentInfoForCode(paymentInfoId);
		}
		userFacade.setDefaultPaymentInfo(paymentInfoData);
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	@RequestMapping(value = "/remove-payment-method", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removePaymentMethod(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		userFacade.unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	@ResponseBody
	@RequestMapping(value = "/address-area-list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequireHardLogIn
	public List<DeliveryCityData> getAreaFromCity(final Model model)
	{
		return sharjahDeliveryAreaFacade.getDeliveryCities();
	}

	@RequestMapping(value = "/cancelOrder", method = RequestMethod.POST)
	@RequireHardLogIn
	public String cancelOrder(@RequestParam(value = "orderCode") final String orderCode,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
		final BaseStoreModel store = baseStoreService.getCurrentBaseStore();
		final OrderModel orderModel = customerAccountService.getOrderForCode(orderCode, store);
		ServicesUtil.validateParameterNotNull(orderModel, "Order can not be null");

		final OrderCancelRequest orderCancelRequest = new OrderCancelRequest(orderModel,
				Lists.newArrayList(sharjahAccountControllerUtil.createCancellationEntries(orderModel)));

		try {
			orderCancelService.requestOrderCancel(orderCancelRequest, userService.getCurrentUser());
			orderModel.setStatus(OrderStatus.CANCELLED);
			modelService.save(orderModel);

			//export
			sharjahOrderExportService.exportOrder(orderModel, OrderExportType.CANCEL.getCode());

			stockService.releaseStockAfterCancelOrder(orderCancelRequest);
			LOG.info("succes cancelling order with code" + orderCode);
			final OrderProcessModel orderProcessModel = (OrderProcessModel) businessProcessService.createProcess(
					"sendOrderCancelledEmailProcess-" + orderCode + "-" + System.currentTimeMillis(),
					"sendOrderCancelledEmailProcess");
			orderProcessModel.setOrder(orderModel);
			modelService.save(orderProcessModel);
			businessProcessService.startProcess(orderProcessModel);
		} catch (final OrderCancelException e) {
			LOG.info("error when cancelling order with code" + orderCode, e);
		}
		finally{

			fireCancelEvent(orderModel);

		}
		//		try {
		//			final OrderProcessModel RefundOrderProcessModel = (OrderProcessModel) businessProcessService.createProcess(
		//					"sendOrderRefundEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
		//					"sendOrderRefundEmailProcess");
		//			RefundOrderProcessModel.setOrder(orderModel);
		//			modelService.save(RefundOrderProcessModel);
		//			businessProcessService.startProcess(RefundOrderProcessModel);
		//		} catch (final Exception e) {
		//			LOG.info("error when sending email Refund order with code" + orderCode, e);
		//		}
		return REDIRECT_TO_ORDER_HISTORY_PAGE;
	}

	@RequestMapping(value = "/reorder", method = RequestMethod.POST)
	@RequireHardLogIn
	public String reorder(@RequestParam(value = "orderCode") final String orderCode,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException {
		final BaseStoreModel store = baseStoreService.getCurrentBaseStore();
		final OrderModel orderModel = customerAccountService.getOrderForCode(orderCode, store);
		ServicesUtil.validateParameterNotNull(orderModel, "Order can not be null");
		final CartModel cart = cartService.getSessionCart();
		for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries()) {
			final ProductModel product = productService.getProduct(orderEntry.getProduct().getCode());
			final UnitModel unit = orderEntry.getUnit();
			final Long quantity = orderEntry.getQuantity();
			try {
				cartService.addToCart(cart, product, quantity, unit);
			} catch (final InvalidCartException e) {
				LOG.info("error when adding in cart the product with code " + product.getCode(), e);
			}
		}
		return REDIRECT_TO_ORDER_HISTORY_PAGE;
	}

	public void setCustomerAccountService(final CustomerAccountService customerAccountService) {
		this.customerAccountService = customerAccountService;
	}

	public void setOrderCancelService(final OrderCancelService orderCancelService) {
		this.orderCancelService = orderCancelService;
	}

	public void setUserService(final UserService userService) {
		this.userService = userService;
	}

	public void setBaseStoreService(final BaseStoreService baseStoreService) {
		this.baseStoreService = baseStoreService;
	}

	public void setProductService(final ProductService productService) {
		this.productService = productService;
	}

	public void setCartService(final CartService cartService) {
		this.cartService = cartService;
	}

	public void setModelService(final ModelService modelService) {
		this.modelService = modelService;
	}

	public void setStockService(final SharjahStockService stockService) {
		this.stockService = stockService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
		this.businessProcessService = businessProcessService;
	}

	public SharjahOrderExportService getSharjahOrderExportService()
	{
		return sharjahOrderExportService;
	}

	public void setSharjahOrderExportService(final SharjahOrderExportService sharjahOrderExportService)
	{
		this.sharjahOrderExportService = sharjahOrderExportService;
	}
	private void fireCancelEvent(OrderModel order){

		eventService.publishEvent(new CancelOrderEvent(order));

	}

}