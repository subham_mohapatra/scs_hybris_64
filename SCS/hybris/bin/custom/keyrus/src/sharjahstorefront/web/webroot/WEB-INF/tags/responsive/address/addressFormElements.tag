<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
  
  	<spring:theme code="please.select" var="selectElt"/>
 
   <formElement:formInputBox idKey="address.name" labelKey="address.name" path="name" inputCSS="form-control" />
	<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="firstName" inputCSS="form-control" mandatory="true" />
	<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="lastName" inputCSS="form-control" mandatory="true" />
	
	<spring:theme code="address.country.default" var="defaultCountry"/>
	<formElement:formInputBox idKey="defaultCountry" labelKey="address.country" path="line2" inputCSS="form-control" mandatory="true" disabled="true" value="${defaultCountry}"/>
	
	<formElement:formSelectBox idKey="address.townCity" labelKey="address.townCity" path="townCity"  mandatory="true"  
		skipBlankMessageKey="${selectElt}" items="${citiesList}" selectedValue="${sharjahAddressForm.townCity}" selectCSSClass="form-control" />
		
	<formElement:formSelectBox idKey="address.state" labelKey="address.state" path="state" mandatory="true"  
		skipBlankMessageKey="${selectElt}" items="${areasList}" selectedValue="${sharjahAddressForm.state}" selectCSSClass="form-control"  
		selectedValueArea="${sharjahAddressForm.state}"/>
   <div class="input-mobilephone">
   <c:choose>
		<c:when test="${currentLanguage.isocode eq 'en'}">
		<div class="input-mobilephone--number register-section">
			<div class="register-section--form-label form-label"><spring:theme code="address.phone.mandatory" /></div>
			<div class="input-membershipnumber--number-wrapper no-margin">
				<div class="input-mobilephone--prefix">
					<formElement:formInputBox idKey="address.mobileprefixNumber"
						path="mobileprefixNumber" 
						inputCSS="form-control" mandatory="true" value="+971"
						disabled="true" />
				</div>
	
				<div class="input-mobilephone--prefix0">
					<formElement:formInputBox idKey="address.mobileSecondPrefixNumber"
						path="mobileSecondPrefixNumber" inputCSS="form-control"
						mandatory="true" value="(0)" disabled="true" />
				</div>
				<div class="input-mobilephone--suffix">
					<formElement:formInputBox idKey="address.phone" path="phone" placeholder="XXXXXXXXX"
						inputCSS="form-control" mandatory="true" />
				</div>
			</div>
		</div>
		</c:when>
		<c:otherwise>
		<div class="input-mobilephone--number register-section">
			<div class="register-section--form-label form-label"><spring:theme code="address.phone.mandatory" /></div>
			<div class="input-membershipnumber--number-wrapper no-margin">
				<div class="input-mobilephone--suffix">
					<formElement:formInputBox idKey="address.phone" path="phone" placeholder="XXXXXXXXX"
						inputCSS="form-control" mandatory="true" />
				</div>
				<div class="input-mobilephone--prefix0">
					<formElement:formInputBox idKey="address.mobileSecondPrefixNumber"
						path="mobileSecondPrefixNumber" inputCSS="form-control"
						mandatory="true" value="(0)" disabled="true" />
				</div>
				<div class="input-mobilephone--prefix">
					<formElement:formInputBox idKey="address.mobileprefixNumber"
						path="mobileprefixNumber" 
						inputCSS="form-control" mandatory="true" value="971+"
						disabled="true" />
				</div>
			</div>
		</div>
			</c:otherwise>
		</c:choose>
   </div>
   <formElement:formInputBox idKey="address.street" labelKey="address.street" path="line1" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.building" labelKey="address.building" path="building" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.apartment" labelKey="address.apartment" path="apartment" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.landmark" labelKey="address.landmark" path="landmark" inputCSS="form-control" mandatory="false"/>
	<c:if test="${not empty availbleGrlandPostCode}">
	<formElement:formInputBox idKey="address.grl" labelKey="address.grl" path="grl" inputCSS="form-control" mandatory="false"/>
	<div style="display: none">
		<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="postcode" inputCSS="form-control" mandatory="false" labelCSS=""/>
		<formElement:formInputBox idKey="address.longitude" labelKey="address.longitude" path="longitude" inputCSS="form-control" mandatory="false"/>
		<formElement:formInputBox idKey="address.latitude" labelKey="address.latitude" path="latitude" inputCSS="form-control" mandatory="false" />
	</div>
	</c:if>
	
	
