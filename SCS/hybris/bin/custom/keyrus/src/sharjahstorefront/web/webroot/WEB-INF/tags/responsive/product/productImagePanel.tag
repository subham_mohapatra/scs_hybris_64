<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ attribute name="galleryImages" required="true" type="java.util.List"%>
<%@ attribute name="galleryImages3D" required="false" type="java.util.List"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="loggedIn" access="isFullyAuthenticated()" />

<c:set var="carouselClass" value="carousel" />

<c:set var="carouselMobileDots" value="true" />
<c:set var="carouselDesktopDots" value="false" />
<c:set var="carouselMobileArrows" value="false" />
<c:set var="carouselDesktopArrows" value="false" />
<c:set var="carouselMobileItems" value="1" />
<c:set var="carouselDesktopItems" value="1" />
<c:set var="carouselDir" value="${currentLanguage.isocode == 'ar' ? 'rtl' : 'ltr'}" />
<c:set var="slideToShow" value="0" />
<c:if test="${currentLanguage.isocode == 'ar'}" >
	<c:set var="slideToShow" value="${galleryImages.size() - 1}" />
</c:if>
<div class="image-gallery">

	<c:if test="${product.discountPrice ne null}">
		<c:choose>
			<c:when test="${currentLanguage.isocode eq 'ar'}">
				<div class="sticker">
					<span class="sticker--price">${product.percentageDiscount}%  off</span>
				</div>
			</c:when>
			<c:otherwise>
				<div class="sticker">
					<span class="sticker--price">${product.percentageDiscount}%  off</span>
				</div>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${product.stickerUrl ne null}">
		<div class="sticker">
			<img src="${product.stickerUrl}" />
		</div>
	</c:if>
	
						<!-- Add to Favourites Changes -->
					<div class="product-details--fav-heart">
						<div id="emptyHeart_div_${product.code}">
							<a id="emptyHeart_${product.code}" href="javascript:void('0');"
								onclick="javascript:addToWishList('${product.code}');"
								title="Add item to wishlist"> <span
								class="glyphicon glyphicon-heart product-details--fav-heart--gray"></span>
							</a>
						</div>
						<div id="filledHeart_div_${product.code}">
				 			<c:choose>
								<c:when test="${loggedIn}">
									<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
										<a id="emptyHeart_${product.code}" href="javascript:void('0');"
								onclick="javascript:addToWishList('${product.code}');"
											title="Add item to wishlist"> <span
											class="glyphicon glyphicon-heart gray-heart product-details--fav-heart--pink"></span>
										</a>
									</sec:authorize>
								</c:when>
								<c:otherwise>
									<%-- <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
										<a class="signIn" href="<c:url value='/login'/>"
											title="Remove item from wishlist"> <span
											class="glyphicon glyphicon-heartp pink-heart product-details--fav-heart--pink"></span>
										</a>
									</sec:authorize> --%>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				<!-- Add to Favourites Changes -->
	<c:choose>
		<c:when test="${galleryImages == null || galleryImages.size() == 0}">
			<div class="${carouselClass} js-carousel ${carouselClass}--pdp image-gallery__image"  data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-dir="${carouselDir}" style="visibility:hidden;">
				<div class="item">
						<spring:theme code="img.missingProductImage.responsive.product"
							text="/" var="imagePath" />
						<c:choose>
							<c:when test="${originalContextPath ne null}">
								<c:url value="${imagePath}" var="imageUrl"
									context="${originalContextPath}" />
							</c:when>
							<c:otherwise>
								<c:url value="${imagePath}" var="imageUrl" />
							</c:otherwise>
						</c:choose>
						<img class="lazyOwl" src="${imageUrl}" />
				</div>
			</div>
			<div class="empty--360--product">
				<product:productGalleryThumbnail galleryImages="${galleryImages}" galleryImages3D="${galleryImages3D}" />
			</div>
		</c:when>
		<c:otherwise>

			<div class="${carouselClass} js-carousel ${carouselClass}--pdp image-gallery__image"  data-carousel-mobile-items="${carouselMobileItems}" data-carousel-mobile-dots="${carouselMobileDots}" data-carousel-desktop-items="${carouselDesktopItems}" data-carousel-desktop-dots="${carouselDesktopDots}" data-carousel-desktop-arrows="${carouselDesktopArrows}" data-carousel-mobile-arrows="${carouselMobileArrows}" data-carousel-dir="${carouselDir}" data-carousel-slideToShow="${slideToShow}" style="visibility:hidden;">
				<c:forEach items="${galleryImages}" var="container" varStatus="loop">
					<div class="item zoom">
						<img id="imageProductZoom_${loop.index}" src="${container.zoom.url}" alt="" />
					</div>
				</c:forEach>
			</div>
			<product:productGalleryThumbnail galleryImages="${galleryImages}" galleryImages3D="${galleryImages3D}" />
		</c:otherwise>
	</c:choose>
</div>
<!-- UI Revamp change - check if the product is in user's wishlist start -->
  <script type="text/javascript">
 	<c:if test="${not empty pageContext.request.userPrincipal}">
        $(document).ready(function() {
                checkIfProductInWishList("${product.code}","${pageContext.request.contextPath}");
        });
    </c:if>
    </script>
 <!-- UI Revamp change - check if the product is in user's wishlist start -->
