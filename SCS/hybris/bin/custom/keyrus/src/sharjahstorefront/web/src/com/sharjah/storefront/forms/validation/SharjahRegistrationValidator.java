package com.sharjah.storefront.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.sharjah.storefront.forms.SharjahRegisterForm;

/**
 * Validator for Sharjah Registration Validator form
 * 
 */
@Component("SharjahRegistrationValidator")
public class SharjahRegistrationValidator extends RegistrationValidator
{
	@Resource(name = "sharjahCustomerValidationUtils")
	private SharjahCustomerValidationUtils sharjahCustomerValidationUtils;
	
	/**
	 * validate sharjah address form
	 * @param object
	 * @param errors
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final SharjahRegisterForm registerForm = (SharjahRegisterForm) object;
		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();
		final String email = registerForm.getEmail();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();
		final String shareholderNumberPrefix =registerForm.getMembershipPrefixNumber();
		final String shareholderNumber = registerForm.getMembershipNumber();
		
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);

		 

		if (StringUtils.isEmpty(registerForm.getMobileNumber()) || StringUtils.length(registerForm.getMobileNumber()) != 9
				||  !StringUtils.isNumeric(registerForm.getMobileNumber()) )
		{
			errors.rejectValue("mobilePrefixNumber", "register.mobilePrefixNumber.invalid");
		}
		// Validate Shareholder number and prefix

		if (StringUtils.isNotEmpty(shareholderNumberPrefix) || StringUtils.isNotEmpty(shareholderNumber))
		{
			sharjahCustomerValidationUtils.isShareholderExist(errors, shareholderNumberPrefix, shareholderNumber,
					"membershipPrefixNumber", "profile.shareholderNumberPrefix.dontexist");

			sharjahCustomerValidationUtils.isValidShareholderNumber(errors, shareholderNumberPrefix, shareholderNumber,
					"membershipPrefixNumber", "profile.shareholderNumberPrefix.invalid");
			sharjahCustomerValidationUtils.validateShareholderPrefixNumber(errors, shareholderNumberPrefix, shareholderNumber,
					"membershipPrefixNumber", "profile.shareholderNumberPrefix.invalid");

			sharjahCustomerValidationUtils.validateShareholderNumber(errors, shareholderNumberPrefix, shareholderNumber,
					"membershipNumber", "profile.shareholderNumber.invalid");
		}


	}

}
