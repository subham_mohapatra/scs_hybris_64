<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:set var="hideDescription" value="checkout.login.loginAndCheckout" />
<div class="login-section">
	<div class="login-section--title">
		<span class="icon icon--user">&nbsp;</span>
		<span class="title"><spring:theme code="login.title" /></span>
	</div>
	<c:if test="${actionNameKey ne hideDescription}">
		<div class="login-section--description"><spring:theme code="login.description" /></div>
	</c:if>
	<form:form action="${action}" method="post" commandName="loginForm" class="login-section--form form">
		<c:if test="${not empty message}">
			<div class="has-error"> <spring:theme code="${message}" /></div>
		</c:if>
		<formElement:formInputBox idKey="j_username" labelKey="login.email" path="j_username" mandatory="true" />
		<formElement:formPasswordBox idKey="j_password" labelKey="login.password" path="j_password" inputCSS="form-control" mandatory="true" />
		<div class="forgotten-password login-section--forgotten-password">
			<ycommerce:testId code="login_forgotPassword_link">
				<a data-link="<c:url value='/login/pw/request'/>" class="js-password-forgotten" data-cbox-title="<spring:theme code="forgottenPwd.title"/>"><spring:theme code="login.link.forgottenPwd" /></a>
			</ycommerce:testId>
		</div>
		<div class="login-section--buttons">
			<ycommerce:testId code="loginAndCheckoutButton">
				<button type="submit" class="button button--secondary"><spring:theme code="${actionNameKey}" /></button>
			</ycommerce:testId>
		</div>
	</form:form>
</div>
