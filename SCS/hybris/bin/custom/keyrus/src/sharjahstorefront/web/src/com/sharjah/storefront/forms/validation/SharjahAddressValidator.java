/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sharjah.storefront.forms.SharjahAddressForm;
 
/**
 * @author Ben Amor Bassem
 * 
 * Validator for address forms. Enforces the order of validation
 */
@Component("sharjahAddressValidator")
public class SharjahAddressValidator implements Validator
{

	private static final int MAX_FIELD_LENGTH = 255;

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	/**
	 * validate sharjah address form 
	 * @param object
	 * @param errors
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final SharjahAddressForm addressForm = (SharjahAddressForm) object;
		validateStandardFields(addressForm, errors);
	}

	/**
	 * validate Standard Fields
	 * 
	 * @param sharjahAddressForm
	 * @param errors
	 */
	protected void validateStandardFields(final SharjahAddressForm sharjahAddressForm, final Errors errors)
	{
		validateStringField(sharjahAddressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getTownCity(), AddressField.TOWN, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getState(), AddressField.STATE, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getPhone(), AddressField.PHONE, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getLine1(), AddressField.STREET, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getBuilding(), AddressField.BUILDING, MAX_FIELD_LENGTH, errors);
		validateStringField(sharjahAddressForm.getApartment(), AddressField.APARTMENT, MAX_FIELD_LENGTH, errors);
	}
 

	/**
	 * validate String Field
	 * 
	 * @param addressField
	 * @param fieldType
	 * @param maxFieldLength
	 * @param errors
	 */
	protected static void validateStringField(final String addressField, final AddressField fieldType,
											  final int maxFieldLength, final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	/**
	 * validate Field NotNull
	 * 
	 * @param addressField
	 * @param fieldType
	 * @param errors
	 */
	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType,
											   final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum AddressField
	{
		FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName", "address.lastName.invalid"), TOWN("townCity",
				"address.townCity.invalid"), STATE("state", "address.state.invalid"), STREET("line1",
						"address.street.invalid"), PHONE("phone", "address.phone.invalid"), BUILDING("building",
								"address.building.invalid"), APARTMENT("apartment", "address.apartment.invalid");

		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
