<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row margin0">
<div class="col-md-12">
<div class="product">
	<div class="product-details">
	
	<div class="row">
	<div class="col-md-5">
		<div class= "product-details--carousel">
		
		
	

	<%-- 	<product:productReviewSummary product="${product}" showLinks="true"/> --%>

			<product:productImagePanel galleryImages="${galleryImages}" galleryImages3D="${galleryImages3D}"
				product="${product}" />
				</div>
	</div>
		<div class="col-md-7">
		   <div class="product-details--info">
			<!-- <div class="product-details--info"> -->
             <div class="row">
               <div class="col-md-9">
 			 <ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
				<p class="pdp_productHead">${fn:escapeXml(product.name)}</p>
			</ycommerce:testId> 
			</div>
		   <c:if test="${product.stock.stockLevel gt 0}">
			<c:set var="productStockLevel"> <spring:theme code="product.variants.in.stock"/>
			</c:set>
		</c:if>
		<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
			<c:set var="productStockLevel">
				<spring:theme code="product.variants.in.stock" />
			</c:set>
		</c:if>
			<div class="col-md-3">
			 <c:if test="${not empty productStockLevel}">
				<div class="add-to-cart--availability">
					<p class="add-to-cart--availability--stock">${productStockLevel}</p>
				</div>
			</c:if>
			<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock'}">
				<div class="add-to-cart--out">
					<p class="add-to-cart--out--stock"><spring:theme code="product.variants.out.of.stock"/></p>
				</div>
			</c:if> 
 		  </div>
			</div>

		<div class="col-md-12">
		<div class="product-details--info--first-section-price price visible-md visible-lg">
						<product:productPromotionSection product="${product}"/>
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" noStrikeCurrency="true"/>
						</ycommerce:testId>
		</div>
		<div class= "col-md-12 mobileCost">
					<div class="product-details--info--first-section-price price visible-xs visible-sm">
						<product:productPromotionSection product="${product}"/>
						<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
							<product:productPricePanel product="${product}" noStrikeCurrency="true"/>
						</ycommerce:testId>
					</div>
			
		</div>
		
				<div class="product-details--info--second-section row margin0">
				<div class= "col-md-12">
				<cms:pageSlot position="VariantSelector" var="component" element="div" class="page-details-variants-select">
					<cms:component component="${component}" element="div" class="yComponentWrapper page-details-variants-select-component"/>
				</cms:pageSlot>
				</div>
				</div>
				
	<div class="col-md-12">
			<cms:pageSlot position="AddToCart" var="component" element="div" class="page-details-variants-select">
					<cms:component component="${component}" element="div" class="yComponentWrapper page-details-add-to-cart-component"/>
			</cms:pageSlot>
	</div>
 	<div class="col-md-12">
 	<div class="info-description">
		<p class="title"><spring:theme code="product.details.information.title"/></p>
		<product:productDetailsClassifications product="${product}" />
	    <p class="title"><spring:theme code="product.details.description.title"/></p>
		<product:productDetailsTab product="${product}" />
		</div>
	</div>
			<c:if test="${not empty product.brandName}">
				<div class="product-details--info--brand">
					${product.brandName}
					<a class="product-details--info--brand-img" href="${pageContext.request.contextPath}/c/${product.brandCode}"><img src="${product.brandLogo.url}" /></a>
				</div>
			</c:if>
			
			<c:forEach items="${product.classifications}" var="classification">
	<c:if test="${fn:toLowerCase(classification.name) eq 'nutrition'}">
		<c:set value="${classification}" var="nutritionClassification"></c:set>
	</c:if>
</c:forEach>
<c:if test="${not empty nutritionClassification}">
	<div class="product-nutrition">
		<p class="title"><spring:theme code="product.details.nutrition.title"/></p>
		<!-- <table class="table">
		<tbody> -->
		<div class="product-nutrition--container">
			<c:forEach items="${nutritionClassification.features}" var="feature">
				<!-- <tr> -->
				<div class="product-nutrition--container-category ${feature.name}">
					<p class="product-nutrition--container-category-title">${feature.name}</p>
					<!-- <td> -->
					<div class="product-nutrition--container-category-info">
						<c:forEach items="${feature.featureValues}" var="value" varStatus="status">
							<span>${fn:escapeXml(value.value)}</span>
							<c:choose>
								<c:when test="${feature.range}">
									${not status.last ? '-' : feature.featureUnit.symbol}
								</c:when>
								<c:otherwise>
									${feature.featureUnit.symbol}
									${(fn:length(feature.featureValues) - 1) == status.count ? '<br/>' : ''}
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</c:if>
		</div>
	</div>
	</div>
	</div>
</div>
</div></div>