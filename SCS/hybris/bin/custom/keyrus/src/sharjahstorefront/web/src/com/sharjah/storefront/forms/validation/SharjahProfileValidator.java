package com.sharjah.storefront.forms.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.sharjah.storefront.forms.SharjahUpdateProfileForm;

/**
 * 
 * Validator for Sharjah Update Profile Form
 */
@Component("sharjahProfileValidator")
public class SharjahProfileValidator extends ProfileValidator {

	@Resource(name = "sharjahCustomerValidationUtils")
	private SharjahCustomerValidationUtils sharjahCustomerValidationUtils;

	/**
	 * validate sharjah address form
	 * @param object
	 * @param errors
	 */
	@Override
	public void validate(final Object object, final Errors errors) {

		final SharjahUpdateProfileForm profileForm = (SharjahUpdateProfileForm) object;

		final String phoneNumber = (profileForm.getMobileprefixNumber() != null
				? profileForm.getMobileprefixNumber().trim() : "+971")
				+ (profileForm.getPhoneNumber() != null ? profileForm.getPhoneNumber().trim() : "");
		final String birthday = profileForm.getDayOfBirth() + "/" + profileForm.getMonthOfBirth() + "/"
				+ profileForm.getYearOfBirth();
		final String firstName = profileForm.getFirstName();
		final String lastName = profileForm.getLastName();
		final String shareholderNumberPrefix = profileForm.getShareholderNumberPrefix();
		final String shareholderNumber = profileForm.getShareholderNumber();

		sharjahCustomerValidationUtils.validateName(errors, firstName, "firstName", "register.firstName.invalid");
		sharjahCustomerValidationUtils.validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (!sharjahCustomerValidationUtils.isValidPhoneNumber(phoneNumber))
		{
			errors.rejectValue("phoneNumber", "profile.phoneNumber.invalid");
			errors.rejectValue("mobileprefixNumber", "profile.mobileprefixNumber.invalid");
		}


		if (StringUtils.isNotBlank(birthday) && !sharjahCustomerValidationUtils.isValidDateFormat(birthday))
		{
			errors.rejectValue("dayOfBirth", "profile.birthday.invalid");
		}

		if (StringUtils.isNotEmpty(shareholderNumberPrefix) || StringUtils.isNotEmpty(shareholderNumber))
		{
		//validate Shareholder number and prefix
		sharjahCustomerValidationUtils.isShareholderExist(errors, shareholderNumberPrefix, shareholderNumber,
				"shareholderNumberPrefix",
				"profile.shareholderNumberPrefix.dontexist");

		sharjahCustomerValidationUtils.isValidShareholderNumber(errors, shareholderNumberPrefix, shareholderNumber,
				"shareholderNumberPrefix",
				"profile.shareholderNumberPrefix.invalid");
		sharjahCustomerValidationUtils.validateShareholderPrefixNumber(errors, shareholderNumberPrefix, shareholderNumber,
				"shareholderNumberPrefix", "profile.shareholderNumberPrefix.invalid");

		sharjahCustomerValidationUtils.validateShareholderNumber(errors, shareholderNumberPrefix, shareholderNumber,
				"shareholderNumber",
				"profile.shareholderNumber.invalid");
		}
	}

}
