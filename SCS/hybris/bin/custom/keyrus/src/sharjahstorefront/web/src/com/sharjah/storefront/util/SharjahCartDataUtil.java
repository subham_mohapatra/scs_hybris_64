package com.sharjah.storefront.util;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;


/**
 * Class used to populate the classifications of a CartData's entries for multiple pages
 * 
 */
@Component("sharjahCartDataUtil")
public class SharjahCartDataUtil
{
	public static final String CONFIGURATION = "configuration";

	private static final Logger LOG = LogManager.getLogger(SharjahCartDataUtil.class);

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	/**
	 * populate the classifications of a CartData's entries
	 * 
	 * @param model
	 */
	public void populateClasificationForPages(final Model model, final CartData cartData) 
	{

		final List<OrderEntryData> entries = cartData.getEntries();

		if (CollectionUtils.isNotEmpty(entries)) {
			for (final OrderEntryData orderEntryData : entries) {

				setClassifProductCartData(orderEntryData);
				// set entry config if is empty to be displayed on the checkout page
				populateEntryConfig(orderEntryData);
			}
		}

		model.addAttribute("cartData", cartData);
	}

	/**
	 * 
	 * @param orderEntryData
	 */
	private void setClassifProductCartData(final OrderEntryData orderEntryData) 
	{
		// populate classification in cart page
		final List<ProductOption> extraOptions = Arrays.asList(ProductOption.CLASSIFICATION);
		final ProductData entryProductData = orderEntryData.getProduct();
		try {
			final ProductData productData = productFacade
					.getProductForCodeAndOptions(entryProductData.getCode(), extraOptions);
			if (productData != null && CollectionUtils.isNotEmpty(productData.getClassifications())) {
				orderEntryData.setClassifProductCartData(
						new ArrayList<ClassificationData>(productData.getClassifications()));
			}
		} catch (final UnknownIdentifierException exception) {
			if (LOG.isWarnEnabled()) {
				LOG.warn("Product with code : [" + entryProductData.getCode()
						+ "] not found during populating classification.", exception);
			}
		}
	}

	/**
	 * 
	 * @param orderEntryData
	 */
	private void populateEntryConfig(final OrderEntryData orderEntryData) {
		if (StringUtils.isEmpty(orderEntryData.getEntryConf())
				&& CollectionUtils.isNotEmpty(orderEntryData.getClassifProductCartData())) {
			for (final ClassificationData classification : orderEntryData.getClassifProductCartData()) {
				if (CONFIGURATION.equals(classification.getCode())) {
					final FeatureValueData feature = classification.getFeatures().iterator().next().getFeatureValues()
							.iterator().next();
					orderEntryData.setEntryConf(feature.getValue());

				}
			}
		}
	}
}
