ACC.content = {

	_autoload: [
		'bindContent'
	],
	id: '#underHeader',
	bindContent: function() {
		console.log('acc.content.js - ACC.content.bindContent() : executed');
		$(window).on('resize', function() {
			ACC.content.resizeSerachDiv();
			ACC.content.resizeContent();
			
			// Invoke the resize event immediately
		}).resize();
	},
	resizeContent: function() {
		console.log('acc.content.js - ACC.content.resizeContent() : executed');
		$(ACC.content.id).css({
			'height': window.innerHeight - $('.header').outerHeight() - $('#cookie-bar').outerHeight() + 'px',
		});
	},
	resizeSerachDiv: function() {
		var divSearch = $('.navigation--mobile-site-search');
		var searchInput = $(divSearch).find('.site-search');
		if($(searchInput).is(':visible')){
			$(divSearch).css("height", searchInput.outerHeight());
		}
	} 
};
