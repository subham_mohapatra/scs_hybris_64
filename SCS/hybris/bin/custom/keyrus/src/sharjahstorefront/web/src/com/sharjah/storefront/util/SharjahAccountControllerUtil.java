package com.sharjah.storefront.util;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.impl.DefaultEnumerationService;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.sharjah.core.enums.Hdfu;
import com.sharjah.core.enums.LanguagesEnum;
import com.sharjah.core.enums.MaritalStatus;
import com.sharjah.facades.customer.SharjahCustomerFacade;
import com.sharjah.facades.deliveryarea.SharjahDeliveryAreaFacade;
import com.sharjah.facades.product.data.GenderData;
import com.sharjah.facades.shareholder.data.SelectData;
import com.sharjah.facades.shareholder.data.ShareholderData;
import com.sharjah.facades.user.data.NationalityData;
import com.sharjah.storefront.constants.SharjahStoreFrontConstants;
import com.sharjah.storefront.forms.SharjahAddressForm;
import com.sharjah.storefront.forms.SharjahUpdateProfileForm;


/**
 * Utility class for the Account page Controller.
 *
 */
@Component("sharjahAccountControllerUtil")
public class SharjahAccountControllerUtil
{

	private static final Logger LOG = Logger.getLogger(SharjahAccountControllerUtil.class);

	@Resource(name = "customerFacade")
	private SharjahCustomerFacade customerFacade;

	@Resource(name = "acceleratorCheckoutFacade")
	private CheckoutFacade checkoutFacade;

	@Resource(name = "userFacade")
	private UserFacade userFacade;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	@Resource(name = "sharjahDeliveryAreaFacade")
	private SharjahDeliveryAreaFacade sharjahDeliveryAreaFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;
	
	@Resource(name = "enumerationService")
	private DefaultEnumerationService enumerationService;

	/**
	 * Return a {@link SharjahAddressForm} after populating with the User's attributes
	 * 
	 * @return
	 */
	public SharjahAddressForm getPreparedAddressForm()
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final SharjahAddressForm sharjahAddressForm = new SharjahAddressForm();
		sharjahAddressForm.setFirstName(currentCustomerData.getFirstName());
		sharjahAddressForm.setLastName(currentCustomerData.getLastName());
		sharjahAddressForm.setTitleCode(currentCustomerData.getTitleCode());
		return sharjahAddressForm;
	}

	/**
	 * get the countries
	 * 
	 * @return
	 */
	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return checkoutFacade.getDeliveryCountries();
	}

	/**
	 * Get the titles
	 * 
	 * @return
	 */
	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return userFacade.getTitles();
	}

	/**
	 * Get the country data map
	 * 
	 * @return
	 */
	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<>();
		for (final CountryData countryData : getCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}

	/**
	 * Create a collection of {@link OrderCancelEntry} from the list of order entries in the order.
	 *
	 * @param order
	 *           - the order
	 * @return collection of cancellation entries; never <tt>null</tt>
	 */
	public Collection<OrderCancelEntry> createCancellationEntries(final OrderModel order)
	{
		return order.getEntries().stream().map(entry -> createCancellationEntry(entry)).collect(Collectors.toList());
	}

	/**
	 * Create a {@link OrderCancelEntry} from an {@link AbstractOrderEntryModel}.
	 *
	 * @param orderEntry
	 *           - the order entry
	 * @return a new cancellation entry
	 */
	protected OrderCancelEntry createCancellationEntry(final AbstractOrderEntryModel orderEntry)
	{
		final OrderCancelEntry entry = new OrderCancelEntry(orderEntry,
				((OrderEntryModel) orderEntry).getQuantityPending().longValue());
		entry.setCancelReason(CancelReason.OTHER);
		return entry;
	}

	/**
	 * Method checks if address is set as default
	 *
	 * @param addressId
	 *           - identifier for address to check
	 * @return true if address is default, false if address is not default
	 */
	public boolean isDefaultAddress(final String addressId)
	{
		final AddressData defaultAddress = userFacade.getDefaultAddress();
		return defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressId);
	}

	/**
	 * Set the sharjahAddressForm to the model after an error was made
	 * 
	 * @param sharjahAddressForm
	 * @param model
	 */
	public void setUpAddressFormAfterError(final SharjahAddressForm sharjahAddressForm, final Model model)
	{
		model.addAttribute(SharjahStoreFrontConstants.COUNTRY_DATA_ATTR, checkoutFacade.getDeliveryCountries());
		model.addAttribute(SharjahStoreFrontConstants.TITLE_DATA_ATTR, userFacade.getTitles());
		model.addAttribute(SharjahStoreFrontConstants.CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());
		model.addAttribute(SharjahStoreFrontConstants.AREA_DATA_ATTR,
				sharjahDeliveryAreaFacade.getDeliveryAreasForCityCode(sharjahAddressForm.getTownCity()));
		model.addAttribute(SharjahStoreFrontConstants.ADDRESS_BOOK_EMPTY_ATTR, Boolean.valueOf(userFacade.isAddressBookEmpty()));
		model.addAttribute(SharjahStoreFrontConstants.IS_DEFAULT_ADDRESS_ATTR,
				Boolean.valueOf(isDefaultAddress(sharjahAddressForm.getAddressId())));
		populateModelRegionAndCountry(model, SharjahStoreFrontConstants.UAE_COUNTRY);
	}

	/**
	 * Add to the model the Region and Country
	 * 
	 * @param model
	 * @param countryIsoCode
	 */
	public void populateModelRegionAndCountry(final Model model, final String countryIsoCode)
	{
		model.addAttribute(SharjahStoreFrontConstants.REGIONS_ATTR, i18NFacade.getRegionsForCountryIso(countryIsoCode));
		model.addAttribute(SharjahStoreFrontConstants.COUNTRY_ATTR, countryIsoCode);
	}

	/**
	 * Check if the title is valid and return its TitleData
	 * 
	 * @param titles
	 * @param code
	 * @return
	 */
	public TitleData findTitleForCode(final List<TitleData> titles, final String code)
	{
		if (code != null && !code.isEmpty() && titles != null && !titles.isEmpty())
		{
			for (final TitleData title : titles)
			{
				if (code.equals(title.getCode()))
				{
					return title;
				}
			}
		}
		return null;
	}

	/**
	 * Add to the model the datas for the update form
	 * 
	 * @param model
	 */
	public void addUpdateFormData(final Model model)
	{

		List<SelectData> selectDatas = new ArrayList<>();

		for (int i = 0; i < 30; i++)
		{
			final SelectData selectData = new SelectData();
			selectData.setCode(String.valueOf(i + 1));
			selectData.setName(String.valueOf(i + 1));
			selectDatas.add(selectData);
		}

		model.addAttribute("householdSizeData", selectDatas);

		selectDatas = new ArrayList<>();

		for (int i = 0; i < 20; i++)
		{
			final SelectData selectData = new SelectData();
			selectData.setCode(String.valueOf(i + 1));
			selectData.setName(String.valueOf(i + 1));
			selectDatas.add(selectData);
		}

		model.addAttribute("dependantsData", selectDatas);

		final List<SelectData> nationalities = new ArrayList<>();

		for (final NationalityData entry : customerFacade.getAllNationality())
		{
			final SelectData nationalityData = new SelectData();
			nationalityData.setCode(entry.getCode());
			nationalityData.setName(entry.getName());
			nationalityData.setValue(entry.getCode());
			nationalities.add(nationalityData);
		}

		model.addAttribute("nationalityData", nationalities);

		final List<SelectData> status = new ArrayList<>();

		for (final HybrisEnumValue maritalStatus : enumerationService.getEnumerationValues(MaritalStatus._TYPECODE))
		{
			final SelectData statusSelect = new SelectData();
			statusSelect.setCode(maritalStatus.getCode());
			statusSelect.setName(enumerationService.getEnumerationName(maritalStatus, i18nService.getCurrentLocale()));
			status.add(statusSelect);
		}

		model.addAttribute("maritalStatusData", status);

		final List<SelectData> genders = new ArrayList<>();

		for (final HybrisEnumValue genderStatus : enumerationService.getEnumerationValues(Gender._TYPECODE))
		{
			final SelectData genderSelect = new SelectData();
			genderSelect.setCode(genderStatus.getCode());
			genderSelect.setName(enumerationService.getEnumerationName(genderStatus, i18nService.getCurrentLocale()));
			genderSelect.setValue(genderStatus.getCode());
			genders.add(genderSelect);
		}

		model.addAttribute("genderData", genders);

		final List<SelectData> languages = new ArrayList<>();

		for (final HybrisEnumValue languageEnum : enumerationService.getEnumerationValues(LanguagesEnum._TYPECODE))
		{
			final SelectData languageData = new SelectData();
			languageData.setCode(languageEnum.getCode());
			languageData.setName(enumerationService.getEnumerationName(languageEnum, i18nService.getCurrentLocale()));
			languageData.setValue(languageEnum.getCode());
			languages.add(languageData);
		}

		model.addAttribute("languageData", languages);

		final List<SelectData> hdfu = new ArrayList<>();

		for (final HybrisEnumValue hdfuEnum : enumerationService.getEnumerationValues(Hdfu._TYPECODE))
		{
			final SelectData hdfuData = new SelectData();
			hdfuData.setCode(hdfuEnum.getCode());
			hdfuData.setName(enumerationService.getEnumerationName(hdfuEnum, i18nService.getCurrentLocale()));
			hdfuData.setValue(hdfuEnum.getCode());
			hdfu.add(hdfuData);
		}

		model.addAttribute("hdfuData", hdfu);
	}

	/**
	 * Populate a {@link SharjahUpdateProfileForm} from a {@link CustomerData} and return it
	 * 
	 * @param customerData
	 * @return
	 */
	public SharjahUpdateProfileForm populateUpdateProfileForm(final CustomerData customerData)
	{
		final SharjahUpdateProfileForm sharjahUpdateProfileForm = new SharjahUpdateProfileForm();

		final SimpleDateFormat simpleFormat = new SimpleDateFormat("dd/MM/yyyy");

		sharjahUpdateProfileForm.setTitleCode(customerData.getTitleCode());

		sharjahUpdateProfileForm.setEmail(customerData.getUid());
		sharjahUpdateProfileForm.setGenderCode(customerData.getGender().getCode());
		sharjahUpdateProfileForm.setLanguageCode(customerData.getLanguage().getIsocode());

		if (customerData.getDateOfBirth() != null)
		{
			sharjahUpdateProfileForm.setBirthday(simpleFormat.format(customerData.getDateOfBirth()));
		}

		sharjahUpdateProfileForm.setMaritalStatus(customerData.getMaritalStatus());
		sharjahUpdateProfileForm.setNumberOfDependants(customerData.getNumberOfDependants());
		sharjahUpdateProfileForm.setHouseHoldSize(customerData.getHouseHoldSize());
		sharjahUpdateProfileForm.setHowDidYouFindUs(customerData.getHowDidYouFindUs());

		if (customerData.getShareholderData() != null)
		{
			final String number = customerData.getShareholderData().getShareholderNumber();
			if (StringUtils.isNotEmpty(number))
			{
				sharjahUpdateProfileForm.setShareholderNumber(number.substring(1, number.length()));
				sharjahUpdateProfileForm.setShareholderNumberPrefix(number.substring(0, 1));
			}
		}

		sharjahUpdateProfileForm.setNationality(customerData.getNationality());
		sharjahUpdateProfileForm.setPhoneNumber(customerData.getMobileNumber());
		sharjahUpdateProfileForm.setMobileprefixNumber(customerData.getMobileprefixNumber());
		sharjahUpdateProfileForm.setLandLine(customerData.getLandline());
		sharjahUpdateProfileForm.setFirstName(customerData.getFirstName());
		sharjahUpdateProfileForm.setLastName(customerData.getLastName());

		if (customerData.getDateOfBirth() != null)
		{
			final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			final String date = df.format(customerData.getDateOfBirth());
			final String[] parts = date.split("/");
			sharjahUpdateProfileForm.setDayOfBirth(parts[0]);
			sharjahUpdateProfileForm.setMonthOfBirth(parts[1]);
			sharjahUpdateProfileForm.setYearOfBirth(parts[2]);
		}
		return sharjahUpdateProfileForm;
	}

	/**
	 * Populate a {@link CustomerData} from a {@link SharjahUpdateProfileForm} and return it
	 * 
	 * @param sharjahUpdateProfileForm
	 * @return
	 */
	public CustomerData populateCustomerData(final SharjahUpdateProfileForm sharjahUpdateProfileForm)
	{
		final CustomerData currentCustomerData = customerFacade.getCurrentCustomer();
		final CustomerData customerData = new CustomerData();

		customerData.setTitleCode(sharjahUpdateProfileForm.getTitleCode());
		customerData.setFirstName(sharjahUpdateProfileForm.getFirstName());
		customerData.setLastName(sharjahUpdateProfileForm.getLastName());
		customerData.setUid(currentCustomerData.getUid());
		customerData.setDisplayUid(currentCustomerData.getDisplayUid());
		customerData.setTitleCode(currentCustomerData.getTitleCode());

		customerData.setFirstName(sharjahUpdateProfileForm.getFirstName());
		customerData.setLastName(sharjahUpdateProfileForm.getLastName());

		customerData.setHowDidYouFindUs(sharjahUpdateProfileForm.getHowDidYouFindUs());
		customerData.setMaritalStatus(sharjahUpdateProfileForm.getMaritalStatus());

		if (customerData.getShareholderData() == null)
		{
			customerData.setShareholderData(new ShareholderData());
		}

		customerData.getShareholderData().setShareholderNumber(
				sharjahUpdateProfileForm.getShareholderNumberPrefix() + sharjahUpdateProfileForm.getShareholderNumber());

		customerData.setFirstName(sharjahUpdateProfileForm.getFirstName());
		customerData.setLastName(sharjahUpdateProfileForm.getLastName());

		customerData.setLandline(sharjahUpdateProfileForm.getLandLine());
		customerData.setNumberOfDependants(sharjahUpdateProfileForm.getNumberOfDependants());

		customerData.setHouseHoldSize(sharjahUpdateProfileForm.getHouseHoldSize());
		customerData.setNationality(sharjahUpdateProfileForm.getNationality());

		if (StringUtils.isNotEmpty(sharjahUpdateProfileForm.getDayOfBirth())
				&& StringUtils.isNotEmpty(sharjahUpdateProfileForm.getMonthOfBirth())
				&& StringUtils.isNotEmpty(sharjahUpdateProfileForm.getYearOfBirth()))
		{
			try
			{
				final DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				sharjahUpdateProfileForm.setBirthday(sharjahUpdateProfileForm.getDayOfBirth().trim() + "/"
						+ sharjahUpdateProfileForm.getMonthOfBirth().trim() + "/" + sharjahUpdateProfileForm.getYearOfBirth().trim());
				customerData.setDateOfBirth(format.parse(sharjahUpdateProfileForm.getBirthday()));
			}
			catch (final ParseException e1)
			{
				//Silent kill
				LOG.error("Error occured when converting profile birthday date", e1);
			}
		}

		customerData.setMobileNumber(sharjahUpdateProfileForm.getPhoneNumber());
		customerData.setMobileprefixNumber(StringUtils.isNotEmpty(sharjahUpdateProfileForm.getMobileprefixNumber())
				? sharjahUpdateProfileForm.getMobileprefixNumber()
				: "+971");

		final GenderData gender = new GenderData();

		gender.setCode(Gender.MALE.toString().equals(sharjahUpdateProfileForm.getGenderCode()) ? Gender.MALE.toString()
				: Gender.FEMALE.toString());
		gender.setName(Gender.MALE.toString().equals(sharjahUpdateProfileForm.getGenderCode()) ? Gender.MALE.toString()
				: Gender.FEMALE.toString());

		customerData.setGender(gender);

		final LanguageData language = new LanguageData();
		language.setIsocode(sharjahUpdateProfileForm.getLanguageCode());
		customerData.setLanguage(language);

		return customerData;
	}

	public void setCustomerFacade(final SharjahCustomerFacade customerFacade)
	{
		this.customerFacade = customerFacade;
	}

	public void setCheckoutFacade(final CheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	public void setUserFacade(final UserFacade userFacade)
	{
		this.userFacade = userFacade;
	}

	public void setSharjahDeliveryAreaFacade(final SharjahDeliveryAreaFacade sharjahDeliveryAreaFacade)
	{
		this.sharjahDeliveryAreaFacade = sharjahDeliveryAreaFacade;
	}

	public void setI18NFacade(final I18NFacade i18nFacade)
	{
		i18NFacade = i18nFacade;
	}

	public void setAccountBreadcrumbBuilder(final ResourceBreadcrumbBuilder accountBreadcrumbBuilder)
	{
		this.accountBreadcrumbBuilder = accountBreadcrumbBuilder;
	}
}
