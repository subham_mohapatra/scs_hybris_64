package com.sharjah.storefront.controllers.misc;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;

import com.sharjah.facades.cart.SharjahCartFacade;


public abstract class AbstractAddToCartController extends AbstractController
{
	private static final Logger LOG = Logger.getLogger(AbstractAddToCartController.class);

	private static final String QUANTITY_ATTR = "quantity";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String BASKET_ERROR = "basket.error.occurred";
	private static final String BASKET_QUANTITY_ERROR = "basket.information.quantity.noItemsAdded.";

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "cartFacade")
	private SharjahCartFacade cartFacade;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	/**
	 *
	 * @param code
	 * @param quantity
	 * @param config
	 * @param model
	 * @return
	 */
	public boolean addToCart(final String code, final double quantity, final String config, final String productType,
			final Model model)
	{
		boolean isOk = false;
		final long qty = (long) quantity;


		if (qty <= 0)
		{
			model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
			model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
		}
		else
		{
			try
			{
				final CartModificationData cartModification = cartFacade.addToCart(code, config, qty);
				model.addAttribute(QUANTITY_ATTR, Long.valueOf(cartModification.getQuantityAdded()));
				model.addAttribute("entry", cartModification.getEntry());
				model.addAttribute("cartCode", cartModification.getCartCode());

				if (cartModification.getQuantityAdded() == 0L)
				{
					model.addAttribute(ERROR_MSG_TYPE, BASKET_QUANTITY_ERROR + cartModification.getStatusCode());
				}
				else if (cartModification.getQuantityAdded() < qty)
				{
					model.addAttribute(ERROR_MSG_TYPE,
							"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
				}
				else
				{
					//everything went well
					isOk = true;
				}

			}
			catch (final CommerceCartModificationException ex)
			{
				LOG.error("an error on the cart ", ex);
				model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR);
				model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
			}

		}

		model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));

		return isOk;
	}

	public MessageSource getMessageSource()
	{
		return messageSource;
	}

	public void setMessageSource(final MessageSource messageSource)
	{
		this.messageSource = messageSource;
	}

	public I18NService getI18nService()
	{
		return i18nService;
	}

	public void setI18nService(final I18NService i18nService)
	{
		this.i18nService = i18nService;
	}

}
