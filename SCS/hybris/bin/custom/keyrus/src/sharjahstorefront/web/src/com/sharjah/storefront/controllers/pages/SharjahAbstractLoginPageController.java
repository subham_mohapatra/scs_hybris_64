package com.sharjah.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractLoginPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.enumeration.impl.DefaultEnumerationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.facades.shareholder.data.SelectData;
import com.sharjah.storefront.controllers.ControllerConstants;
import com.sharjah.storefront.forms.SharjahRegisterForm;

/**
 * Controller for Sharjah Abstract Login Page Controller
 *
 */
public abstract class SharjahAbstractLoginPageController extends AbstractLoginPageController
{
	private static final Logger LOGGER = Logger.getLogger(SharjahAbstractLoginPageController.class);

	private static final String FORM_GLOBAL_ERROR = "form.global.error";

	protected static final String SPRING_SECURITY_LAST_USERNAME = "SPRING_SECURITY_LAST_USERNAME";

	public static final String CHECKOUT_URL = "/checkout";

	private static final String EMAIL = "email";

	@Resource(name = "SharjahRegistrationValidator")
	private Validator registrationValidator;
	
	@Resource(name = "enumerationService")
	private DefaultEnumerationService enumerationService;

	/**
	 * @return the registrationValidator
	 */
	@Override
	protected Validator getRegistrationValidator()
	{
		return registrationValidator;
	}

	@Override
	protected String getDefaultLoginPage(final boolean loginError, final HttpSession session, final Model model)
			throws CMSItemNotFoundException
	{
		getGenders(model);
		
		final LoginForm loginForm = new LoginForm();
		model.addAttribute(loginForm);
		
		final SharjahRegisterForm sharjahRegisterForm = new SharjahRegisterForm();
		sharjahRegisterForm.setGenderCode("MALE");
		model.addAttribute(sharjahRegisterForm);
		
		model.addAttribute(new GuestForm());

		final String username = (String) session.getAttribute(SPRING_SECURITY_LAST_USERNAME);
		if (username != null)
		{
			session.removeAttribute(SPRING_SECURITY_LAST_USERNAME);
		}

		loginForm.setJ_username(username);
		storeCmsPageInModel(model, getCmsPage());
		setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.INDEX_NOFOLLOW);

		final Breadcrumb loginBreadcrumbEntry = new Breadcrumb("#",
				getMessageSource().getMessage("header.link.login", null, "header.link.login", getI18nService().getCurrentLocale()),
				null);
		model.addAttribute("breadcrumbs", Collections.singletonList(loginBreadcrumbEntry));

		if (loginError)
		{
			model.addAttribute("loginError", Boolean.valueOf(loginError));
			GlobalMessages.addErrorMessage(model, "login.error.account.not.found.title");
		}

		return getView();
	}

	/**
	 * This method takes data from the registration form and create a new customer account and attempts to log in using
	 * the credentials of this new user.
	 *
	 * @return true if there are no binding errors or the account does not already exists.
	 * @throws CMSItemNotFoundException
	 */
	protected String sharjahProcessRegisterUserRequest(final String referer, final SharjahRegisterForm form,
			final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException // NOSONAR
	{
		getGenders(model);
		
		if (request.getParameter("isCheckoutRegister") != null)
		{
			model.addAttribute("isCheckoutRegister", Boolean.TRUE);
		}
		if (bindingResult.hasErrors())
		{
			if (checkErrorForEmail(bindingResult, form))
			{
				form.setMembershipNumber(null);
				form.setMembershipPrefixNumber(null);
			}
			if(StringUtils.isNotEmpty(form.getMembershipNumber())
			&& StringUtils.isNotEmpty(form.getMembershipPrefixNumber()))
			{
				model.addAttribute("hasErrors", Boolean.TRUE);
			}
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}

		final RegisterData data = new RegisterData();
		data.setFirstName(form.getFirstName());
		data.setLastName(form.getLastName());
		data.setLogin(form.getEmail());
		data.setPassword(form.getPwd());
		data.setTitleCode(form.getTitleCode());
		data.setMobileNumber(form.getMobileNumber());
		data.setMobileprefixNumber(StringUtils.isNotEmpty(form.getMobilePrefixNumber()) ? form.getMobilePrefixNumber() : "+971");
		data.setDefaultLanguage(form.getDefaultLanguage());
		data.setGenderCode(form.getGenderCode());
		
		
		final String membershipNumber = form.getMembershipPrefixNumber().concat(form.getMembershipNumber());
		data.setMembershipNumber(membershipNumber);

		try
		{
			getCustomerFacade().register(data);
			getAutoLoginStrategy().login(form.getEmail().toLowerCase(), form.getPwd(), request, response);

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"registration.confirmation.message.title");
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.warn("registration failed: " + e);
			form.setMembershipNumber(null);
			form.setMembershipPrefixNumber(null);
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new GuestForm());
			bindingResult.rejectValue("email", "registration.error.account.exists.title");
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}

		if (request.getParameter("isCheckoutRegister") != null)
		{
			return REDIRECT_PREFIX + CHECKOUT_URL;
		}

		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}

	@Override
	protected String handleRegistrationError(final Model model) throws CMSItemNotFoundException
	{
		if (model.containsAttribute("isCheckoutRegister"))
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId("checkout-login"));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId("checkout-login"));
			return ControllerConstants.Views.Pages.Checkout.CheckoutLoginPage;
		}
		else
		{
			storeCmsPageInModel(model, getCmsPage());
			setUpMetaDataForContentPage(model, (ContentPageModel) getCmsPage());
			return getView();
		}
	}

	/**
	 * Check if the form's error is related to the email and if the Shareholder number is set.
	 * 
	 * @param bindingResult
	 * @param form
	 * @return
	 */
	private boolean checkErrorForEmail(final BindingResult bindingResult, final SharjahRegisterForm form)
	{
		for (final ObjectError error : bindingResult.getAllErrors())
		{
			final FieldError errorField = (FieldError) error;
			if (EMAIL.equals(errorField.getField()) && StringUtils.isNotEmpty(form.getMembershipNumber())
					&& StringUtils.isNotEmpty(form.getMembershipPrefixNumber()))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param model
	 */
	private void getGenders( final Model model){
		final List<SelectData> genders = new ArrayList<>();
		for (final HybrisEnumValue genderStatus : enumerationService.getEnumerationValues(Gender._TYPECODE))
		{
			final SelectData genderSelect = new SelectData();
			genderSelect.setCode(genderStatus.getCode());
			genderSelect.setName(enumerationService.getEnumerationName(genderStatus, getI18nService().getCurrentLocale()));
			genderSelect.setValue(genderStatus.getCode());
			genders.add(genderSelect);
		}
		
		model.addAttribute("genderData", genders);

	}
}
