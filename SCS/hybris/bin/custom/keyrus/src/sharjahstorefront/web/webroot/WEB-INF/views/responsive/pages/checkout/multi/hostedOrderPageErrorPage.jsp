<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="${redirectUrl}" var="continueUrl"/>
<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
		<cms:component component="${component}" />
	</cms:pageSlot>
	<h2><spring:theme code="checkout.multi.hostedOrderPageError.header"/></h2>
	

	<div class="error-details-section">
		
			<spring:theme code="checkout.multi.hostedOrderPageError.CANCEL"/>
			<br>
			<spring:theme code="checkout.multi.hostedOrderPageError.cancel.details"/>
			<p>
			
		<div class="action errorHop">
			<a class="button button--secondary" href="${continueUrl}"><spring:theme code="checkout.multi.hostedOrderPageError.continue"/></a>
		</div>
		<p>
	
	</div>
	

	
	<cms:pageSlot position="SideContent" var="feature" element="div" class="side-content-slot cms_disp-img_slot">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<p>
</template:page>
