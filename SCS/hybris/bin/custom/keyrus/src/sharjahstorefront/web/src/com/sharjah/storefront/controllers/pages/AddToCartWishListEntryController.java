package com.sharjah.storefront.controllers.pages;

import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.facades.product.data.AddToCartWishListData;
import com.sharjah.facades.product.data.ProductFromWishListData;
import com.sharjah.storefront.controllers.misc.AbstractAddToCartController;


/**
 * Controller that received requests to add products from WishList to cart
 *
 * @author Luiz.Henriques
 *
 */
@Controller
@Scope("tenant")
@RequestMapping("/add-wish-to-cart")
public class AddToCartWishListEntryController extends AbstractAddToCartController
{
	private static final Logger LOG = LogManager.getLogger(AddToCartWishListEntryController.class);

	private static final String ALL_ADDED = "wishlist.added.all";
	private static final String PARTIAL_ADDED = "wishlist.added.partial";
	private static final String NONE_ADDED = "wishlist.added.none";

	@ResponseBody
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE })
	public AddToCartWishListData addToCart(@RequestBody final List<ProductFromWishListData> products,
			final HttpServletRequest request, final Model model, final RedirectAttributes redirectModel)
	{
		final List<ProductFromWishListData> productsNotAdded = new ArrayList<>();
		final List<ProductFromWishListData> productsAdded = new ArrayList<>();

		final AddToCartWishListData addToCartData = new AddToCartWishListData();
		for (final ProductFromWishListData p : products)
		{
			boolean isOK = false;
			try
			{
				isOK = addToCart(p.getProductCode(), 1, p.getConfiguration(), null, model);
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.error("There is no product with code [" + p.getProductCode() + "]", e);
			}
			if (!isOK)
			{
				productsNotAdded.add(p);
			}
			else
			{
				productsAdded.add(p);
			}
		}

		addToCartData.setProductsAdded(productsAdded);
		addToCartData.setProductsNotAdded(productsNotAdded);

		populateReturnMessage(addToCartData, request.getContextPath());

		return addToCartData;
	}

	/**
	 * populates the message to return to the customer
	 *
	 * @param addToCartData
	 */
	private void populateReturnMessage(final AddToCartWishListData addToCartData, final String path)
	{
		//success
		if (!addToCartData.getProductsAdded().isEmpty() && addToCartData.getProductsNotAdded().isEmpty())
		{
			addToCartData.setMessage(getMessageSource().getMessage(ALL_ADDED, new Object[]
			{ path }, getI18nService().getCurrentLocale()));
		}
		else if (!addToCartData.getProductsAdded().isEmpty() && !addToCartData.getProductsNotAdded().isEmpty())
		{
			addToCartData.setMessage(getMessageSource().getMessage(PARTIAL_ADDED, new Object[]
			{ path }, getI18nService().getCurrentLocale()));
		}
		else
		{
			addToCartData.setMessage(getMessageSource().getMessage(NONE_ADDED, null, getI18nService().getCurrentLocale()));
		}
	}
}
