package com.sharjah.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;


/**
 * Register's form class.
 * Sharjah Register Form for RegisterForm
 */
public class SharjahRegisterForm extends RegisterForm
{
	private String mobilePrefixNumber;
	private String mobileSecondPrefixNumber;
	private String defaultLanguage;
	private String membershipNumber;
	private String membershipPrefixNumber;
	private String genderCode;

	/**
	 * 
	 * @return Mobile Prefix Number
	 */
	public String getMobilePrefixNumber()
	{
		return mobilePrefixNumber;
	}

	/**
	 * set Mobile Prefix Number attribute
	 * 
	 * @param mobilePrefixNumber
	 */
	public void setMobilePrefixNumber(final String mobilePrefixNumber)
	{
		this.mobilePrefixNumber = mobilePrefixNumber;
	}

	/**
	 * 
	 * @return Default Language
	 */
	public String getDefaultLanguage()
	{
		return defaultLanguage;
	}

	/**
	 * set Default Language attribute
	 * 
	 * @param defaultLanguage
	 */
	public void setDefaultLanguage(final String defaultLanguage)
	{
		this.defaultLanguage = defaultLanguage;
	}

	/**
	 * 
	 * @return Membership Number
	 */
	public String getMembershipNumber()
	{
		return membershipNumber;
	}

	/**
	 * set Membership Number attribute
	 * 
	 * @param membershipNumber
	 */
	public void setMembershipNumber(final String membershipNumber)
	{
		this.membershipNumber = membershipNumber;
	}

	/**
	 * 
	 * @return Membership Prefix Number
	 */
	public String getMembershipPrefixNumber()
	{
		return membershipPrefixNumber;
	}

	/**
	 * 
	 * @param membershipPrefixNumber
	 */
	public void setMembershipPrefixNumber(final String membershipPrefixNumber)
	{
		this.membershipPrefixNumber = membershipPrefixNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getMobileSecondPrefixNumber() {
		return mobileSecondPrefixNumber;
	}

	/**
	 * 
	 * @param mobileSecondPrefixNumber
	 */
	public void setMobileSecondPrefixNumber(String mobileSecondPrefixNumber) {
		this.mobileSecondPrefixNumber = mobileSecondPrefixNumber;
	}

	/**
	 * 
	 * @return
	 */
	public String getGenderCode() {
		return genderCode;
	}

	/**
	 * 
	 * @param genderCode
	 */
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}

}
