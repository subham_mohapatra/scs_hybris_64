package com.sharjah.storefront.constants;

public class SharjahStoreFrontConstants
{

	// Constants for account controller
	public static final String TEXT_ACCOUNT_ADDRESS_BOOK = "text.account.addressBook";
	public static final String BREADCRUMBS_ATTR = "breadcrumbs";
	public static final String IS_DEFAULT_ADDRESS_ATTR = "isDefaultAddress";
	public static final String COUNTRY_DATA_ATTR = "countryData";
	public static final String ADDRESS_BOOK_EMPTY_ATTR = "addressBookEmpty";
	public static final String TITLE_DATA_ATTR = "titleData";
	public static final String FORM_GLOBAL_ERROR = "form.global.error";
	public static final String PROFILE_CURRENT_PASSWORD_INVALID = "profile.currentPassword.invalid";
	public static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";
	public static final String ADDRESS_DATA_ATTR = "addressData";
	public static final String ADDRESS_FORM_ATTR = "sharjahAddressForm";
	public static final String COUNTRY_ATTR = "country";
	public static final String REGIONS_ATTR = "regions";
	public static final String CITY_DATA_ATTR = "citiesList";
	public static final String AREA_DATA_ATTR = "areasList";
	public static final String UAE_COUNTRY = "AE";
	public static final String SHARJA_CITY = "Sharjah";

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	public static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	public static final String ADDRESS_CODE_PATH_VARIABLE_PATTERN = "{addressCode:.*}";

	// CMS Pages
	public static final String ACCOUNT_CMS_PAGE = "account";
	public static final String PROFILE_CMS_PAGE = "profile";
	public static final String UPDATE_PASSWORD_CMS_PAGE = "updatePassword";
	public static final String UPDATE_PROFILE_CMS_PAGE = "update-profile";
	public static final String UPDATE_EMAIL_CMS_PAGE = "update-email";
	public static final String ADDRESS_BOOK_CMS_PAGE = "address-book";
	public static final String ADD_EDIT_ADDRESS_CMS_PAGE = "add-edit-address";
	public static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";
	public static final String ORDER_HISTORY_CMS_PAGE = "orders";
	public static final String ORDER_DETAIL_CMS_PAGE = "order";

}
