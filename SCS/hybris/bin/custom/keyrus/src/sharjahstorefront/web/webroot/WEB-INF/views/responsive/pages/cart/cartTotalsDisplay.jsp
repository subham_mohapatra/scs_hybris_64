<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div id="cartTotalsDisplay" class="cartTotalsDisplay cart--bottom">
	<cart:cartVoucher cartData="${cartData}"/>
	<cart:cartTotals cartData="${cartData}" showTax="false"/>
		<div class="cart--continue-shopping-button-container">
		<a class="button cart--continue-shopping-button" href="/">
			<spring:theme  code="cart.page.continue"/>
		</a>
	</div>
	<%--<cart:ajaxCartTotals/>--%>
</div>
