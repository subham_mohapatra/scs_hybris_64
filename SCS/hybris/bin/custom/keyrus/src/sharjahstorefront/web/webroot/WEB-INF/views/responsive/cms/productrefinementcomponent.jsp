<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="product-facet" class="product-facet product__facet js-product-facet">
	<div class="product-facet--title">
		<span >
				<spring:theme code="search.nav.facet.title"/>
		</span>
	</div>
    <nav:facetNavAppliedFilters pageData="${searchPageData}"/>
    <nav:facetNavRefinements pageData="${searchPageData}"/>
</div>
