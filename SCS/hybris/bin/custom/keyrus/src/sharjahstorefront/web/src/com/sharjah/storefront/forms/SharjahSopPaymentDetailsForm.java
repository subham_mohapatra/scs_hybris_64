package com.sharjah.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.SopPaymentDetailsForm;


/**
 * 
 * @author Abderrazzak.Blej
 * Sharjah SopPayment Details Form fro SopPayment form
 */
public class SharjahSopPaymentDetailsForm extends SopPaymentDetailsForm
{
	private String billTo_building;
	private String billTo_apartment;
	private String billTo_landmark;

	/**
	 * 
	 * @return billTo_building
	 */
	public String getBillTo_building()
	{
		return billTo_building;
	}

	/**
	 * 
	 * @param set
	 *           billTo_building attribute
	 */
	public void setBillTo_building(final String billTo_building)
	{
		this.billTo_building = billTo_building;
	}

	/**
	 * 
	 * @return billTo_apartment
	 */
	public String getBillTo_apartment()
	{
		return billTo_apartment;
	}

	/**
	 * 
	 * @param set
	 *           billTo_apartment attribute
	 */
	public void setBillTo_apartment(final String billTo_apartment)
	{
		this.billTo_apartment = billTo_apartment;
	}

	/**
	 * 
	 * @return billTo_landmark
	 */
	public String getBillTo_landmark()
	{
		return billTo_landmark;
	}

	/**
	 * 
	 * @param set
	 *           billTo_landmark attribute
	 */
	public void setBillTo_landmark(final String billTo_landmark)
	{
		this.billTo_landmark = billTo_landmark;
	}
}
