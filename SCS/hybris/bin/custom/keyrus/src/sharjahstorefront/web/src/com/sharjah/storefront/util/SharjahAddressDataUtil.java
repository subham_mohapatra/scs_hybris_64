package com.sharjah.storefront.util;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.sharjah.facades.user.data.DeliveryAreaData;
import com.sharjah.facades.user.data.DeliveryCityData;
import com.sharjah.storefront.forms.SharjahAddressForm;

/**
 * Sharjah Address Data Utilil  for Address Data Util
 *
 */
@Component(value = "sharjahAddressDataUtil")
public class SharjahAddressDataUtil extends AddressDataUtil
{

	private static final String SHIPMENT_CITY = "Sharjah";
	private static final String ADDRESS = "Address";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void convertBasic(final AddressData source, final AddressForm target)
	{
		target.setAddressId(source.getId());
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setCountryIso(source.getCountry().getIsocode());
		target.setTownCity(source.getTown());
		target.setLine1(source.getLine1());
		if (target instanceof SharjahAddressForm)
		{
			if (source.getDeliveryArea() != null)
			{
				((SharjahAddressForm) target).setState(source.getDeliveryArea().getCode());
			}
			((SharjahAddressForm) target).setBuilding(source.getBuilding());
			((SharjahAddressForm) target).setApartment(source.getApartment());
			((SharjahAddressForm) target).setLandmark(source.getLandmark());
			((SharjahAddressForm) target).setName(source.getName());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void convert(final AddressData source, final AddressForm target)
	{
		convertBasic(source, target);
		target.setSaveInAddressBook(Boolean.valueOf(source.isVisibleInAddressBook()));
		target.setShippingAddress(Boolean.valueOf(source.isShippingAddress()));
		target.setBillingAddress(Boolean.valueOf(source.isBillingAddress()));
		target.setPhone(source.getPhone());
	}
	/**
	 * {@inheritDoc}
	 */
	public AddressData convertToVisibleAddressData(final SharjahAddressForm sharjahAddressForm)
	{
		final AddressData addressData = convertToAddressData(sharjahAddressForm);
		addressData.setVisibleInAddressBook(true);
		return addressData;
	}

	/**
	 * {@inheritDoc}
	 */

	public AddressData convertToAddressData(final SharjahAddressForm sharjahAddressForm)
	{
		final AddressData addressData = new AddressData();
		addressData.setId(sharjahAddressForm.getAddressId());
		addressData.setFirstName(sharjahAddressForm.getFirstName());
		addressData.setLastName(sharjahAddressForm.getLastName());
		addressData.setLine1(sharjahAddressForm.getLine1());
		addressData.setPhone(sharjahAddressForm.getPhone());
		addressData.setName(sharjahAddressForm.getName());

		final String townCity = sharjahAddressForm.getTownCity();
		addressData.setTown(townCity);

		if (SHIPMENT_CITY.equals(townCity))
		{
			addressData.setShippingAddress(true);
		}
		else
		{
			addressData.setShippingAddress(false);
		}
		addressData.setBillingAddress(false);

		final DeliveryCityData deliveryCityData = new DeliveryCityData();
		deliveryCityData.setCode(townCity);

			final DeliveryAreaData deliveryAreaData = new DeliveryAreaData();
			deliveryAreaData.setCode(sharjahAddressForm.getState());
			addressData.setDeliveryArea(deliveryAreaData);

			addressData.setBuilding(sharjahAddressForm.getBuilding());
			addressData.setApartment(sharjahAddressForm.getApartment());
			addressData.setLandmark(sharjahAddressForm.getLandmark());

		addressData.setDeliveryCity(deliveryCityData);

		if (sharjahAddressForm.getCountryIso() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(sharjahAddressForm.getCountryIso());
			addressData.setCountry(countryData);
		}
		if (StringUtils.isNotEmpty(sharjahAddressForm.getName()))
		{
			addressData.setName(sharjahAddressForm.getName());
		}
		else
		{
			addressData.setName(ADDRESS +" "+ System.currentTimeMillis());
		}
		if(StringUtils.isNotEmpty(sharjahAddressForm.getGrl()))
		{
			addressData.setGrl(sharjahAddressForm.getGrl());
		}
		if(StringUtils.isNotEmpty(sharjahAddressForm.getLongitude()))
		{
			addressData.setLongitude(sharjahAddressForm.getLongitude());
		}
		if (StringUtils.isNotEmpty(sharjahAddressForm.getLatitude()))
		{
			addressData.setLatitude(sharjahAddressForm.getLatitude());
		}
		if(StringUtils.isNotEmpty(sharjahAddressForm.getPostcode()))
		{
			addressData.setPostalCode(sharjahAddressForm.getPostcode());
		}
		return addressData;
	}

}
