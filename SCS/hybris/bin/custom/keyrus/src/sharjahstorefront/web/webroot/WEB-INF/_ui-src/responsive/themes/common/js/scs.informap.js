$(document).ready(function() {
	 PostMessageToAPAiFrame = function(url, jsonMessage) { 
         
         var iFrameContainer = $('#informap'); 
         var iFrameID = 'apaFrame'; 
         if(ACC.device.type === 'mobile') {
     	
         $('<iframe id="' + iFrameID + '" src="' + url + '" style="width: 98%; height: 650px; border: 0;" />').appendTo(iFrameContainer).load(function() { 
             var iFrame = document.getElementById(iFrameID);         
             iFrame.contentWindow.postMessage(jsonMessage, "*"); 
         }); 
         }else{
        	   $('<iframe id="' + iFrameID + '" src="' + url + '" style="width: 120%; height: 950px; border: 0;" />').appendTo(iFrameContainer).load(function() { 
                   var iFrame = document.getElementById(iFrameID);         
                   iFrame.contentWindow.postMessage(jsonMessage, "*"); 
               });
         }
      
     };
     callAPAAddressApp = function() { 
     	 
     	 
     	    var datajson1 = '{"RequestID": ' + Math.floor((Math.random() * 20) + 1) + ',"MessageType": 1,"AddressElements": [{"AddressElementTypeCode": 1,"Value": ""},{"AddressElementTypeCode": 2,"Value": ""},{         "AddressElementTypeCode": 3,"Value": ""},{"AddressElementTypeCode": 4,"Value": ""},{"AddressElementTypeCode": 5,"Value": ""},{"AddressElementTypeCode": 6,"Value": ""   },{"AddressElementTypeCode": 7, "Value": ""  },{"AddressElementTypeCode": 8,"Value": ""},{"AddressElementTypeCode": 9,"Value": ""},{"AddressElementTypeCode": 10,"Value": ""},{"AddressElementTypeCode": 11,"Value": ""}]}';     	 
     	    var datajson = datajson1; 
     	    var url =  document.getElementById("infromapUrl").value;
     	    if(ACC.device.type === 'mobile') 
     	    {
             url =   document.getElementById("infromapUrlMobile").value; 
     	    }
     	    PostMessageToAPAiFrame(url, datajson); 
     	 
     	};
    
	
        if(document.getElementById("informap") !== null){
        	console.log('scs.informap.js : start executed');
        	callAPAAddressApp(); 
        	console.log('scs.informap.js :  end executed');
        }
        function receiveMessage(evt) {  
     if(evt.origin === "https://www.grluae.com") {
        	console.log('scs.informap.receiveMessage : start executed');
        	 var data_from_apa = JSON.parse(evt.data);  
        	 
        	    if(data_from_apa.AddressElements[4].Value !== null){
        	    	document.getElementById("address.grl").value = data_from_apa.AddressElements[4].Value;
        	    	// document.getElementById("address.grl").setAttribute("disabled","true");
        	    }else{
        	    	// document.getElementById("address.grl").removeAttribute("disabled");    
        	    	 document.getElementById("address.grl").value = "";
        	    }

        	    if(data_from_apa.AddressElements[2].Value !== null){
        	    	document.getElementById("address.street").value = data_from_apa.AddressElements[2].Value;
        	    }else{
        	    	document.getElementById("address.street").value = "";
        	    }
        	    if(data_from_apa.AddressElements[5].Value !== null){
        	    	document.getElementById("address.building").value = data_from_apa.AddressElements[5].Value;
        	    }
        	    else{
       	    	 	document.getElementById("address.building").value = "";
        	    }
        	    if(data_from_apa.AddressElements[1].Value !== null){
        	    	document.getElementById("address.state").value = data_from_apa.AddressElements[1].Value +" - Sharjah";
        	    }else{
        	    	 document.getElementById("address.state").value = "";
        	    }
        	    
        	    if(data_from_apa.AddressElements[3].Value !== null){
        	    	document.getElementById("address.landmark").value = data_from_apa.AddressElements[3].Value;
        	    }
        	    else{
       	    	 	document.getElementById("address.landmark").value = "";
        	    }
        	     
        	    if(data_from_apa.AddressElements[10].Value !== null){
            	    document.getElementById("address.postcode").value = data_from_apa.AddressElements[10].Value;
            	 }
        	    
        	    if(data_from_apa.AddressElements[8].Value !== null){
            	    document.getElementById("address.longitude").value = data_from_apa.AddressElements[8].Value;
            	 }
        	    
        	    if(data_from_apa.AddressElements[9].Value !== null){
            	    document.getElementById("address.latitude").value = data_from_apa.AddressElements[9].Value;
            	 }
        	    
         	    document.getElementById("address.name").value ="";
         	    document.getElementById("address.apartment").value ="";
        	    
        	    console.log('scs.informap.receiveMessage : end executed');
        	    } 
        	}
        	    if (!window.addEventListener) {     
        			window.attachEvent("onmessage", receiveMessage);
        			} else {   
        				window.addEventListener("message", receiveMessage, false);
        			}
});