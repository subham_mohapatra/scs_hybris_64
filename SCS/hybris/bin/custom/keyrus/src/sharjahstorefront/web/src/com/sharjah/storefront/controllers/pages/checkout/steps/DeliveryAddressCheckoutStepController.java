/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.pages.checkout.steps;

import static de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages.ERROR_MESSAGES_HOLDER;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.commerceservices.delivery.DeliveryService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import de.hybris.platform.util.Config;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.core.data.DeliverySlotData;
import com.sharjah.core.deliveryslots.service.DeliverySlotService;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.facades.cart.SharjahCartFacade;
import com.sharjah.facades.converters.DeliverySlotConverter;
import com.sharjah.facades.deliveryarea.SharjahDeliveryAreaFacade;
import com.sharjah.facades.user.data.DeliveryCityData;
import com.sharjah.storefront.controllers.ControllerConstants;
import com.sharjah.storefront.forms.SharjahAddressForm;
import com.sharjah.storefront.forms.validation.SharjahAddressValidator;
import com.sharjah.storefront.util.SharjahAddressDataUtil;
import com.sharjah.storefront.util.SharjahCartDataUtil;

/**
 * DeliveryAddressCheckoutStepController to override method from AbstractCheckoutStepController and implement other method.
 */

@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class DeliveryAddressCheckoutStepController extends AbstractCheckoutStepController
{

	protected static final Logger LOG = LogManager.getLogger(DeliveryAddressCheckoutStepController.class);

	private static final String DELIVERY_ADDRESS = "delivery-address";
	private static final String SHOW_SAVE_TO_ADDRESS_BOOK_ATTR = "showSaveToAddressBook";

	private static final String OK_RESPONSE = "OK";
	private static final String DEFAULT_POS = "pos.default.pointofservice";
	private static final String SLOTS_RANGE_SELECTED = "slots.range.selected";
	private static final String SLOTS_RANGE_PREFERRED = "slots.range.preferred";
	private static final String SLOTS_ERROR_SELECT = "checkout.error.deliveryslot.select";
	private static final String UAE_COUNTRY = "AE";
	private static final String SHARJA_CITY = "Sharjah";
	private static final String CITY_DATA_ATTR = "citiesList";
	private static final String INFROMAP_URL_DESKTOP_EN = "storefront.informap.url.desktop.en";
	private static final String INFROMAP_URL_MOBILE_EN = "storefront.informap.url.mobile.en";
	private static final String INFROMAP_URL_DESKTOP_AR = "storefront.informap.url.desktop.ar";
	private static final String INFROMAP_URL_MOBILE_AR = "storefront.informap.url.mobile.ar";
	
	@Resource(name = "i18nService")
	private I18NService i18nService;
	
	@Resource(name = "sharjahAddressValidator")
	private SharjahAddressValidator sharjahAddressValidator;

	@Resource(name = "sharjahCartDataUtil")
	private SharjahCartDataUtil sharjahCartDataUtil;

	@Resource(name = "sharjahAddressDataUtil")
	private SharjahAddressDataUtil sharjahAddressDataUtil;

	@Resource(name = "defaultDeliverySlotService")
	private DeliverySlotService deliverySlotService;

	@Resource(name = "pointOfServiceService")
	private PointOfServiceService pointOfServiceService;

	@Resource(name = "deliverySlotConverter")
	private DeliverySlotConverter deliverySlotConverter;

	@Resource(name = "defaultSharjahCartFacade")
	private SharjahCartFacade cartFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "sharjahDeliveryAreaFacade")
	private SharjahDeliveryAreaFacade sharjahDeliveryAreaFacade;
	
	@Resource(name = "deliveryService")
	private DeliveryService deliveryService;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Resource
	private CartService cartService;

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getCheckoutFacade().setDeliveryAddressIfAvailable();
		final CartData cartData = getCheckoutFacade().getCheckoutCart();

		setInforMapConfig(model);

		populateCommonModelAttributes(model, cartData, getDefaultAddressIfAvailable());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final SharjahAddressForm sharjahAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		setInforMapConfig(model);

		final DeliveryModeModel deliveryModeModel = getDeliveryService().getDeliveryModeForCode("premium-gross");
		if (deliveryModeModel != null)
		{
			getCheckoutFacade().setDeliveryMode(deliveryModeModel.getCode());
		}
		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		sharjahAddressValidator.validate(sharjahAddressForm, bindingResult);
		if (cartFacade.getSelectedDeliverySlot() == null)
		{
			GlobalMessages.addErrorMessage(model, SLOTS_ERROR_SELECT);
			populateCommonModelAttributes(model, cartData, sharjahAddressForm);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}
		populateCommonModelAttributes(model, cartData, sharjahAddressForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}
		sharjahAddressForm.setCountryIso(UAE_COUNTRY);
		final AddressData newAddress = sharjahAddressDataUtil.convertToAddressData(sharjahAddressForm);

		processAddressVisibilityAndDefault(sharjahAddressForm, newAddress);

		getUserFacade().addAddress(newAddress);

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	protected void processAddressVisibilityAndDefault(final SharjahAddressForm sharjahAddressForm, final AddressData newAddress)
	{
		if (sharjahAddressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(sharjahAddressForm.getSaveInAddressBook().booleanValue());
			if (sharjahAddressForm.getSaveInAddressBook().booleanValue() && getUserFacade().isAddressBookEmpty())
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode") final String editAddressCode, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}

		AddressData addressData = null;
		if (StringUtils.isNotEmpty(editAddressCode))
		{
			addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
		}

		final SharjahAddressForm sharjahAddressForm = new SharjahAddressForm();
		final boolean hasAddressData = addressData != null;
		if (hasAddressData)
		{
			sharjahAddressDataUtil.convert(addressData, sharjahAddressForm);
		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		populateCommonModelAttributes(model, cartData, sharjahAddressForm);

		if (addressData != null)
		{
			model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
		}

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String edit(final SharjahAddressForm sharjahAddressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		sharjahAddressValidator.validate(sharjahAddressForm, bindingResult);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		if (cartFacade.getSelectedDeliverySlot() == null)
		{
			GlobalMessages.addErrorMessage(model, SLOTS_ERROR_SELECT);
			populateCommonModelAttributes(model, cartData, sharjahAddressForm);
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}
		populateCommonModelAttributes(model, cartData, sharjahAddressForm);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		final AddressData newAddress = sharjahAddressDataUtil.convertToAddressData(sharjahAddressForm);

		processAddressVisibility(sharjahAddressForm, newAddress);

		newAddress.setDefaultAddress(getUserFacade().isAddressBookEmpty() || getUserFacade().getAddressBook().size() == 1
				|| Boolean.TRUE.equals(sharjahAddressForm.getDefaultAddress()));

		// Verify the address data.
		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		if (addressRequiresReview)
		{
			if (StringUtils.isNotEmpty(sharjahAddressForm.getAddressId()))
			{
				final AddressData addressData = getCheckoutFacade().getDeliveryAddressForCode(sharjahAddressForm.getAddressId());
				if (addressData != null)
				{
					model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
					model.addAttribute("edit", Boolean.TRUE);
				}
			}

			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		getUserFacade().editAddress(newAddress);
		getCheckoutFacade().setDeliveryModeIfAvailable();
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	protected void processAddressVisibility(final SharjahAddressForm sharjahAddressForm, final AddressData newAddress)
	{

		if (sharjahAddressForm.getSaveInAddressBook() == null)
		{
			newAddress.setVisibleInAddressBook(true);
		}
		else
		{
			newAddress.setVisibleInAddressBook(Boolean.TRUE.equals(sharjahAddressForm.getSaveInAddressBook()));
		}
	}

	@RequestMapping(value = "/remove", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@RequestParam("addressCode") final String addressCode, final RedirectAttributes redirectModel,
			final Model model) throws CMSItemNotFoundException
	{
		if (getCheckoutFacade().isRemoveAddressEnabledForCart())
		{
			final AddressData addressData = new AddressData();
			addressData.setId(addressCode);
			getUserFacade().removeAddress(addressData);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"account.confirmation.address.removed");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute("sharjahAddressForm", new SharjahAddressForm());

		return getCheckoutStep().currentStep();
	}

	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final SharjahAddressForm sharjahAddressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils
				.commaDelimitedListToSet(Config.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = sharjahAddressDataUtil.convertToAddressData(sharjahAddressForm);
		final CountryData countryData = selectedAddress.getCountry();

		if (!resolveCountryRegions.contains(countryData.getIsocode()))
		{
			selectedAddress.setRegion(null);
		}

		if (sharjahAddressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(sharjahAddressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(sharjahAddressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		if (cartFacade.getSelectedDeliverySlot() == null)
		{
			GlobalMessages.addFlashMessage(redirectModel, ERROR_MESSAGES_HOLDER, SLOTS_ERROR_SELECT);
			return getCheckoutStep().currentStep();
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryAddress(@RequestParam("selectedAddressCode") final String selectedAddressCode,
			final RedirectAttributes redirectAttributes)
	{

		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
			final boolean hasSelectedAddressData = selectedAddressData != null;
			if (hasSelectedAddressData)
			{
				getCheckoutFacade().setDeliveryAddress(selectedAddressData);
				setDeliveryAddress(selectedAddressData);
			}
		}

		if (cartFacade.getSelectedDeliverySlot() == null)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, ERROR_MESSAGES_HOLDER, SLOTS_ERROR_SELECT);
			return getCheckoutStep().currentStep();
		}

		return getCheckoutStep().nextStep();
	}

	protected void setDeliveryAddress(final AddressData selectedAddressData)
	{
		final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		if (isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData))
		{
			getCheckoutFacade().setDeliveryAddress(selectedAddressData);
			if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook())
			{ // temporary address should be removed
				getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
			}
		}
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	/**
	 * Save the selected delivery slot for a customer.
	 *
	 * @param pos
	 *           Point of Service
	 * @param date
	 *           The date of the slot
	 * @param slotCode
	 *           The slot code
	 * @return If the operation was successful
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/save-selected-slot")
	@RequireHardLogIn
	public String saveSelectedDeliverySlot(@RequestParam final String pos, @RequestParam final String date,
			@RequestParam final String slotCode)
	{
		cartFacade.updateSelectedDeliverySlot(getSlotForDate(pos, date, slotCode));
		return OK_RESPONSE;
	}

	/**
	 * Save the preferred delivery slot for a customer.
	 *
	 * @param pos
	 *           Point of Service
	 * @param date
	 *           The date of the slot
	 * @param slotCode
	 *           The slot code
	 * @return If the operation was successful
	 */
	@ResponseBody
	@RequestMapping(method = RequestMethod.POST, value = "/save-preferred-slot")
	@RequireHardLogIn
	public String savePreferredDeliverySlot(@RequestParam final String pos, @RequestParam final String date,
			@RequestParam final String slotCode)
	{
		cartFacade.updatePreferredDeliverySlot(getSlotForDate(pos, date, slotCode));
		return OK_RESPONSE;
	}

	/**
	 * Get a slot for a given date.
	 */
	private CalendarDeliverySlotModel getSlotForDate(final String pos, final String date, final String slotCode)
	{
		LOG.debug("Saving slot: " + pos + "::" + date + "::" + slotCode);
		final LocalDate localDate = LocalDate.parse(date);
		final Date slotDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

		return deliverySlotService.findSlotWithDate(pos, slotDate, slotCode);
	}

	protected String getBreadcrumbKey()
	{
		return "checkout.multi." + getCheckoutStep().getProgressBarId() + ".breadcrumb";
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}

	protected void populateCommonModelAttributes(final Model model, final CartData cartData,
			final SharjahAddressForm sharjahAddressForm)
			throws CMSItemNotFoundException
	{
		model.addAttribute("cartData", cartData);
		sharjahCartDataUtil.populateClasificationForPages(model, cartFacade.getSessionCartWithEntryOrdering(false));
		model.addAttribute("sharjahAddressForm", sharjahAddressForm);
		model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("addressFormEnabled", Boolean.valueOf(getCheckoutFacade().isNewAddressEnabledForCart()));
		model.addAttribute("removeAddressEnabled", Boolean.valueOf(getCheckoutFacade().isRemoveAddressEnabledForCart()));
		model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.TRUE);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, getResourceBreadcrumbBuilder().getBreadcrumbs(getBreadcrumbKey()));
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(UAE_COUNTRY));
		model.addAttribute("country", UAE_COUNTRY);
		model.addAttribute("availbleGrlandPostCode", true);

		final DeliveryCityData deliveryCityData = sharjahDeliveryAreaFacade.getDeliveryCityByCode(SHARJA_CITY);
		final List<DeliveryCityData> shipmentDeliveryCities = new ArrayList<>();
		shipmentDeliveryCities.add(deliveryCityData);

		model.addAttribute(CITY_DATA_ATTR, shipmentDeliveryCities);

		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		if (CollectionUtils.isNotEmpty(cartData.getEntries()))
		{
			storeDeliverySlotsInModel(model, cartData);
		}
	}

	/**
	 * Store the delivery slot calendar available for the PoS chosen in the cart.
	 */
	private void storeDeliverySlotsInModel(final Model model, final CartData cartData)
	{
		final Configuration config = configurationService.getConfiguration();
		int maxDeliverySlots = 0;
		final Map<Date, List<DeliverySlotData>> slots = new LinkedHashMap<>();
		final long deliveryDateOffset = calculateDeliveryDateOffset(cartData);

		for (final CalendarDeliverySlotModel slot : findSlotsForDefaultPos(config))
		{
			List<DeliverySlotData> dataList = slots.get(slot.getDate());
			if (dataList == null)
			{
				dataList = new ArrayList<>();
				slots.put(slot.getDate(), dataList);
			}

			final DeliverySlotData slotData = new DeliverySlotData();
			slotData.setDeliveryDateOffset(deliveryDateOffset);
			dataList.add(deliverySlotConverter.convert(slot, slotData));
			if (dataList.size() > maxDeliverySlots)
			{
				maxDeliverySlots = dataList.size();
			}
		}

		model.addAttribute("maxDeliverySlots", Integer.valueOf(maxDeliverySlots));
		model.addAttribute("deliverySlots", slots);
		model.addAttribute("preferredSlotRange", config.getInt(SLOTS_RANGE_PREFERRED));

		final CalendarDeliverySlotModel preferredDeliverySlot = cartFacade.getPreferredDeliverySlot();
		if (preferredDeliverySlot != null)
		{
			final DeliverySlotData slotData = new DeliverySlotData();
			slotData.setDeliveryDateOffset(deliveryDateOffset);
			model.addAttribute("preferredSlot", deliverySlotConverter.convert(preferredDeliverySlot, slotData));
		}
	}

	/**
	 * Find the available delivery slots for the default PoS, taking into account the products in the current cart.
	 */
	private List<CalendarDeliverySlotModel> findSlotsForDefaultPos(final Configuration config)
	{
		final Instant today = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant();
		final Date beginDate = Date.from(today);
		final int selectedSlotRange = config.getInt(SLOTS_RANGE_SELECTED);
		final Date endDate = Date.from(today.plus(selectedSlotRange, ChronoUnit.DAYS));

		final PointOfServiceModel pos = pointOfServiceService.getPointOfServiceForName(config.getString(DEFAULT_POS));

		return deliverySlotService.findSlotsForPos(pos, beginDate, endDate);
	}

	/**
	 * Calculate the amount of extra days that should be added to the minimum delivery date depending on the selected
	 * products.
	 */
	private long calculateDeliveryDateOffset(final CartData cartData)
	{
		return cartData.getEntries().stream()
				.mapToLong(e -> e.getProduct().getDelayDays())
				.max()
				.orElse(0L);
	}
	
	/**
	 * create and fill a new SharjahAddressForm with the default shipment address.
	 * 
	 * @return SharjahAddressForm
	 */
	private SharjahAddressForm getDefaultAddressIfAvailable()
	{
		final CartModel cartModel = cartService.getSessionCart();

		final SharjahAddressForm newForm = new SharjahAddressForm();
		newForm.setTownCity("Sharjah");
		final CustomerModel currentUser = checkoutCustomerStrategy.getCurrentUserForCheckout();
		if (currentUser != null)
		{
			AddressModel defaultAddress = cartModel.getDeliveryAddress();
			if (defaultAddress == null)
			{
				defaultAddress = currentUser.getDefaultShipmentAddress();
			}
			if (defaultAddress != null)
			{
				newForm.setName(defaultAddress.getName());
				newForm.setFirstName(defaultAddress.getFirstname());
				newForm.setLastName(defaultAddress.getLastname());
				newForm.setTownCity(defaultAddress.getTown());
				if(defaultAddress.getDeliveryArea() != null)
				{
				newForm.setState(defaultAddress.getDeliveryArea().getCode());
				}
				newForm.setPhone(defaultAddress.getPhone1().replace("+971", ""));
				newForm.setLine1(defaultAddress.getStreetname());
				newForm.setBuilding(defaultAddress.getBuilding());
				newForm.setApartment(defaultAddress.getAppartment());
				newForm.setLandmark(defaultAddress.getLandmark());
			}
			else
			{
				newForm.setFirstName(currentUser.getFirstName());
				newForm.setLastName(currentUser.getLastName());
				if (currentUser.getPhoneContactInfo() != null)
				{
					newForm.setPhone(currentUser.getPhoneContactInfo().getPhoneNumber());
				}
			}
		}
		return newForm;
	}

	/**
	 * 
	 * @param model
	 */
	private void setInforMapConfig(final Model model)
	{
		String infromapUrl = Config.getParameter(INFROMAP_URL_DESKTOP_EN);
		String infromapUrlMobile = Config.getParameter(INFROMAP_URL_MOBILE_EN);
		if ("ar".equals(i18nService.getCurrentLocale().getLanguage()))
		{
			infromapUrl = Config.getParameter(INFROMAP_URL_DESKTOP_AR);
			infromapUrlMobile = Config.getParameter(INFROMAP_URL_MOBILE_AR);
		}
		model.addAttribute("infromapUrl", infromapUrl);
		model.addAttribute("infromapUrlMobile", infromapUrlMobile);
	}

	public DeliveryService getDeliveryService() {
		return deliveryService;
	}

	public void setDeliveryService(final DeliveryService deliveryService) {
		this.deliveryService = deliveryService;
	}

}
