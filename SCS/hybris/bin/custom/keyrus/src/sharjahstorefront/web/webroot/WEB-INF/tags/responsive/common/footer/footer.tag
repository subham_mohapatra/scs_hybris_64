<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<footer class="footer">
    <cms:pageSlot position="Footer" var="feature">
        <cms:component component="${feature}"/>
    </cms:pageSlot>
</footer>
<div id="chatBotDiv" style="opacity: 1;visibility: visible;z-index: 2147483639;position: fixed;bottom: 0px;max-width: 100%;max-height: calc(100% - 0px);min-height: 0px;min-width: 0px;background-color: transparent;border: 0px;overflow: hidden;right: 0px;transition: none 0s ease 0s !important;height: 410px;padding: 5px;padding-bottom:0px;margin-bottom:0px;">
     <!-- <iframe id="chatBotFrame" src="https://external.scs.ae:8081/ecf/11/sample/visitor_shj.html" width="300" height="400" style="position: absolute;bottom: 0;right: 0px;">
     </iframe> -->
     <iframe src="https://external.scs.ae:8081/ecf/11/sample/visitor_shj_new.html" 
	    allowtransparency="true"
	    allow="autoplay" 
	    id="chat-widget" 
	    name="chat-widget" 
	    scrolling="no" 
	    id="chatBotFrame"
		style="width: 100%; height: 100%; min-height: 0px; min-width: 0px; margin: 0px; padding: 0px; background: none; border: 0px; float: none; transition: none 0s ease 0s !important;">
      </iframe>
 </div>

<style>

/* iframe#chartBotFrame{width: 100%; height: 100%; min-height: 0px; min-width: 0px; margin: 0px; padding: 0px; background: none; border: 0px; float: none; transition: none 0s ease 0s !important;} */
@media (min-width: 768px) {
	.sapUiFormElementLbl label {text-align:right !important; margin-bottom:0px !important;}
	#chatContainer{width:300px !important; height:450px !important;}
}
</style>
