/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.forms.validation;

import de.hybris.platform.acceleratorservices.util.CalendarHelper;

import java.util.Calendar;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.sharjah.storefront.forms.SharjahAddressForm;
import com.sharjah.storefront.forms.SharjahPaymentDetailsForm;


/**
 * 
 * @author Ben Amor Bassem
 * Validator for address payment forms
 * 
 */
@Component("sharjahPaymentDetailsValidator")
public class SharjahPaymentDetailsValidator implements Validator
{
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return SharjahPaymentDetailsForm.class.equals(aClass);
	}

	/**
	 * validate sharjah address form 
	 * @param object
	 * @param errors
	 * 
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final SharjahPaymentDetailsForm sharjahPaymentDetailsForm = (SharjahPaymentDetailsForm) object;

		final Calendar start = CalendarHelper.parseDate(sharjahPaymentDetailsForm.getStartMonth(),
				sharjahPaymentDetailsForm.getStartYear());
		final Calendar expiration = CalendarHelper.parseDate(sharjahPaymentDetailsForm.getExpiryMonth(),
				sharjahPaymentDetailsForm.getExpiryYear());

		if (start != null && expiration != null && start.after(expiration))
		{
			errors.rejectValue("startMonth", "payment.startDate.invalid");
		}

		final boolean editMode = StringUtils.isNotBlank(sharjahPaymentDetailsForm.getPaymentId());
		final SharjahAddressForm sharjahAddressForm = sharjahPaymentDetailsForm.getBillingAddress();
		if (sharjahAddressForm != null && (editMode || Boolean.TRUE.equals(sharjahPaymentDetailsForm.getNewBillingAddress())))
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.firstName", "address.firstName.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.lastName", "address.lastName.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.townCity", "address.townCity.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.state", "address.state.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.phone", "address.phone.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.line1", "address.street.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.building", "address.building.invalid");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billingAddress.apartment", "address.apartment.invalid");
		}
	}
}
