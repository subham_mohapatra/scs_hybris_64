package com.sharjah.storefront.breadcrumb.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSource;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.DefaultResourceBreadcrumbBuilder;

/**
 * Controller for Sharjaha Resource BreadcrumbBuilder.
 */
public class SharjahaDefaultResourceBreadcrumbBuilder extends DefaultResourceBreadcrumbBuilder {


	private static final String LAST_LINK_CLASS = "active";
	private static final String TEXT_ACCOUNT_WISHLIST = "text.account.wishlist";
	private static final String TEXT_ACCOUNT_WISHLIST_DETAILS = "text.account.wishlistDetails";
	private static final String[] WishListBreadcrumbsValues =
	{ TEXT_ACCOUNT_WISHLIST, TEXT_ACCOUNT_WISHLIST_DETAILS };
	
	@Override
	public List<Breadcrumb> getBreadcrumbs(final String resourceKey)
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		final MessageSource messageSource = getMessageSource();


		if (getParentBreadcrumbResourceKey() != null && !getParentBreadcrumbResourceKey().isEmpty())
		{
			final String name = messageSource.getMessage(getParentBreadcrumbResourceKey(), null,
					getI18nService().getCurrentLocale());
			final String breadcrumbLinkPath = getParentBreadcrumbLinkPath();
			final String link = breadcrumbLinkPath != null && !breadcrumbLinkPath.isEmpty() ? breadcrumbLinkPath : "#";
			if (Arrays.asList(WishListBreadcrumbsValues).contains(resourceKey)
					&& TEXT_ACCOUNT_WISHLIST.equals(getParentBreadcrumbResourceKey()))
			{
				breadcrumbs.add(new Breadcrumb(link, name, null));
			}
		}

		if (StringUtils.isNotBlank(resourceKey))
		{
			final String name = messageSource.getMessage(resourceKey, null, getI18nService().getCurrentLocale());
			breadcrumbs.add(new Breadcrumb("#", name, null));
		}

		if (!breadcrumbs.isEmpty())
		{
			breadcrumbs.get(breadcrumbs.size() - 1).setLinkClass(LAST_LINK_CLASS);
		}

		return breadcrumbs;
	}
}
