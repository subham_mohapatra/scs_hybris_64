<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<button class="button button--secondary button--orderconfirmation--global mini-cart-checkout-button js-continue-shopping-button" data-continue-shopping-url="${continueShoppingUrl}">
	<spring:theme code="checkout.orderConfirmation.continueShopping" />
</button>
