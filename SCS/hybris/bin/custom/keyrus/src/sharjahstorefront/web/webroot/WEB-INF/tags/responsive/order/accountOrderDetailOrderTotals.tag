<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/button" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:if test="${not empty order}">
    <div class="account-orderdetail">
		<div id="cartTotalsDisplay" class="cartTotalsDisplay cart--bottom">
			<div class="cart--voucher">
				<order:appliedVouchers order="${order}" />
				<order:receivedPromotions order="${order}" />
			</div>
			<order:orderTotalsItem order="${order}" />
		</div>
		
	</div>
</c:if>
