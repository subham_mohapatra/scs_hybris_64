/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentErrorField;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.facades.deliveryarea.SharjahDeliveryAreaFacade;
import com.sharjah.storefront.controllers.ControllerConstants;
import com.sharjah.storefront.forms.SharjahSopPaymentDetailsForm;

/**
 * SopPaymentResponseController to override method from PaymentMethodCheckoutStepController 
 */
@Controller
@RequestMapping(value = "/checkout/multi/sop")
public class SopPaymentResponseController extends PaymentMethodCheckoutStepController
{
	protected static final Logger LOG = LogManager.getLogger(SopPaymentResponseController.class);

	private static final String CITY_DATA_ATTR = "citiesList";
	private static final String AREA_DATA_ATTR = "areasList";

	@Resource(name = "sharjahDeliveryAreaFacade")
	private SharjahDeliveryAreaFacade sharjahDeliveryAreaFacade;

	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doHandleSopResponse(final HttpServletRequest request,
			@Valid final SharjahSopPaymentDetailsForm sharjahSopPaymentDetailsForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		final Map<String, String> resultMap = getRequestParameterMap(request);

		final boolean savePaymentInfo = sharjahSopPaymentDetailsForm.isSavePaymentInfo()
				|| getCheckoutCustomerStrategy().isAnonymousCheckout();
		final PaymentSubscriptionResultData paymentSubscriptionResultData = this.getPaymentFacade().completeSopCreateSubscription(
				resultMap, savePaymentInfo);

		if (paymentSubscriptionResultData.isSuccess())
		{
			createNewPaymentSubscription(paymentSubscriptionResultData);
		}
		else if (paymentSubscriptionResultData.getDecision() != null && "error".equalsIgnoreCase(paymentSubscriptionResultData.getDecision())
				|| paymentSubscriptionResultData.getErrors() != null && !paymentSubscriptionResultData.getErrors().isEmpty())
		{
			return processErrors(sharjahSopPaymentDetailsForm, bindingResult, model,
					redirectAttributes, paymentSubscriptionResultData);
		}
		else
		{
			// SOP ERROR!
			LOG.error("Failed to create subscription.  Please check the log files for more information");
			return REDIRECT_URL_ERROR + "/?decision=" + paymentSubscriptionResultData.getDecision() + "&reasonCode="
					+ paymentSubscriptionResultData.getResultCode();
		}

		return getCheckoutStep().nextStep();
	}

	protected String processErrors(@Valid final SharjahSopPaymentDetailsForm sharjahSopPaymentDetailsForm,
								 final BindingResult bindingResult, final Model model,
								 final RedirectAttributes redirectAttributes,
								 final PaymentSubscriptionResultData paymentSubscriptionResultData)
			throws CMSItemNotFoundException {
		// Have SOP errors that we can display

		setupAddPaymentPage(model);

		// Build up the SOP form data and render page containing form
		try
        {
			setupSilentOrderPostPage(sharjahSopPaymentDetailsForm, model);
        }
        catch (final Exception e)
        {
			LOG.error("Failed to build beginCreateSubscription request", e);
            GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
            return enterStep(model, redirectAttributes);
        }

		processPaymentSubscriptionErrors(bindingResult, model, paymentSubscriptionResultData);

		return ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
	}

	protected void createNewPaymentSubscription(final PaymentSubscriptionResultData paymentSubscriptionResultData) {
		if (paymentSubscriptionResultData.getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId())) {
			final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

			if (getUserFacade().getCCPaymentInfos(true).size() <= 1) {
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
	}

	protected void processPaymentSubscriptionErrors(final BindingResult bindingResult, final Model model,
												  final PaymentSubscriptionResultData paymentSubscriptionResultData) {
		if (paymentSubscriptionResultData.getErrors() != null && !paymentSubscriptionResultData.getErrors().isEmpty())
        {
            GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
            // Add in specific errors for invalid fields
            for (final PaymentErrorField paymentErrorField : paymentSubscriptionResultData.getErrors().values())
            {
                if (paymentErrorField.isMissing())
                {
                    bindingResult.rejectValue(paymentErrorField.getName(), "checkout.error.paymentethod.formentry.sop.missing."
                            + paymentErrorField.getName(), "Please enter a value for this field");
                }
                if (paymentErrorField.isInvalid())
                {
                    bindingResult.rejectValue(paymentErrorField.getName(), "checkout.error.paymentethod.formentry.sop.invalid."
                            + paymentErrorField.getName(), "This value is invalid for this field");
                }
            }
        }
        else if (paymentSubscriptionResultData.getDecision() != null
					&& "error".equalsIgnoreCase(paymentSubscriptionResultData.getDecision()))
        {
			LOG.error("Failed to create subscription. Error occurred while contacting external payment services.");
            GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
        }
	}

	@Override
	@RequestMapping(value = "/billingaddressform", method = RequestMethod.GET)
	public String getCountryAddressForm(@RequestParam("countryIsoCode") final String countryIsoCode,
			@RequestParam("useDeliveryAddress") final boolean useDeliveryAddress, final Model model)
	{
		model.addAttribute("supportedCountries", getCountries());
		model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(countryIsoCode));
		model.addAttribute("country", countryIsoCode);
		model.addAttribute(CITY_DATA_ATTR, sharjahDeliveryAreaFacade.getDeliveryCities());
		model.addAttribute(AREA_DATA_ATTR, StringUtils.EMPTY);

		final SharjahSopPaymentDetailsForm sharjahSopPaymentDetailsForm = new SharjahSopPaymentDetailsForm();
		model.addAttribute("sharjahSopPaymentDetailsForm", sharjahSopPaymentDetailsForm);
		if (useDeliveryAddress)
		{
			final AddressData deliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();

			sharjahSopPaymentDetailsForm.setBillTo_firstName(deliveryAddress.getFirstName());
			sharjahSopPaymentDetailsForm.setBillTo_lastName(deliveryAddress.getLastName());
			sharjahSopPaymentDetailsForm.setBillTo_country(deliveryAddress.getCountry().getIsocode());
			sharjahSopPaymentDetailsForm.setBillTo_city(deliveryAddress.getTown());
			sharjahSopPaymentDetailsForm.setBillTo_phoneNumber(deliveryAddress.getPhone());

			if (deliveryAddress.getDeliveryCity() != null)
			{
				model.addAttribute(AREA_DATA_ATTR,
						sharjahDeliveryAreaFacade.getDeliveryAreasForCityCode(deliveryAddress.getDeliveryCity().getCode()));
			}
			if (deliveryAddress.getDeliveryArea() != null)
			{
				sharjahSopPaymentDetailsForm.setBillTo_state(deliveryAddress.getDeliveryArea().getCode());
			}
			sharjahSopPaymentDetailsForm.setBillTo_street1(deliveryAddress.getLine1());
			sharjahSopPaymentDetailsForm.setBillTo_building(deliveryAddress.getBuilding());
			sharjahSopPaymentDetailsForm.setBillTo_apartment(deliveryAddress.getApartment());
			sharjahSopPaymentDetailsForm.setBillTo_landmark(deliveryAddress.getLandmark());
		}
		return ControllerConstants.Views.Fragments.Checkout.BillingAddressForm;
	}


}
