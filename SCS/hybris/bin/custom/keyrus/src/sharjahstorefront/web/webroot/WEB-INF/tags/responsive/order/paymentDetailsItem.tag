<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<spring:htmlEscape defaultHtmlEscape="true"/>
<div class="label-order">
    <spring:theme code="text.account.paymentType"/>
</div>


<c:choose>
			<c:when test="${orderData.paymentInfo.id eq 'COD'}">
				<div class="delivery-method">
					    <spring:theme code="text.order.paymentmode.cod"/>
				</div>
			</c:when>
			<c:when test="${orderData.paymentInfo.id eq 'CreditCardOnDelivery'}">
				<div class="delivery-method">
					   <spring:theme code="text.order.paymentmode.creditcardondelivery"/>
				</div>
			</c:when>
			<c:otherwise>
					<div class="delivery-method">
							${fn:escapeXml(order.paymentInfo.cardTypeData.name)}<br>
            			${fn:escapeXml(order.paymentInfo.cardNumber)}	
					</div>
			</c:otherwise>
</c:choose>	
	
