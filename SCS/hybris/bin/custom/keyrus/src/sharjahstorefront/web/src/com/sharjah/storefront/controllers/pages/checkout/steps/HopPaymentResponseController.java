/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.payment.data.PaymentSubscriptionResultData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;
import com.sharjah.facades.payment.cybersource.SharjahCyberSourceFacade;
import com.sharjah.storefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/hop")
public class HopPaymentResponseController extends PaymentMethodCheckoutStepController
{
	private static final Logger LOGGER = Logger.getLogger(HopPaymentResponseController.class);

	@Resource
	private SharjahCyberSourceFacade sharjahCyberSourceFacade;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private CartService cartService;

	/**
	 * Handle the HOP response.
	 *
	 * @param request
	 *           The request from the CyberSource server
	 * @param model
	 *           Spring MVC model
	 * @param redirectAttributes
	 *           Spring MVC redirect attributes
	 * @return The next page
	 * @throws CMSItemNotFoundException
	 *            If the CMS page is not found
	 */
	@RequestMapping(value = "/response", method = RequestMethod.POST)
	@RequireHardLogIn
	public String handleHopResponse(final HttpServletRequest request, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final String decision = getValue(request, CyberSourceConstants.DECISION);
		//final String cardType = getValue(request, CyberSourceConstants.REQ_CARD_TYPE);
		//final String cardNumber = getValue(request, CyberSourceConstants.REQ_CARD_NUMBER);
		//final String expiryDate = getValue(request, CyberSourceConstants.REQ_CARD_EXPIRY_DATE);
		final String reasonCode = getValue(request, CyberSourceConstants.REASON_CODE);
		final String requestId = getValue(request, CyberSourceConstants.REQUEST_ID);
		final String requestToken = getValue(request, CyberSourceConstants.REQUEST_TOKEN);

		setupAddPaymentPage(model);

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		LOGGER.info("Start processing cart code: " + cartData.getCode());

		if (CollectionUtils.isEmpty(cartData.getEntries()))
		{
			return REDIRECT_URL_CART;
		}

		//final boolean formComplete = !StringUtils.equals("-", expiryDate) && StringUtils.isNotBlank(cardType) && StringUtils.isNotBlank(cardNumber);
		final boolean accepted = CyberSourceConstants.DECISION_ACCEPT.equals(decision)
				|| CyberSourceConstants.DECISION_REVIEW.equals(decision);

		final boolean reasonCodeSuccess = CyberSourceConstants.REASON_CODE_SUCCESS.equals(reasonCode);

		if (accepted || reasonCodeSuccess)
		{

			CreditCardPaymentInfoModel paymentInfoModel = null;
			try
			{
				final CartModel cartModel = cartService.getSessionCart();
				final CCPaymentInfoData paymentInfoData = populatePaymentInfoData(request, cartData);

				sharjahCyberSourceFacade.setPaymentDetails(paymentInfoData.getId());
				cartData.setPaymentInfo(paymentInfoData);

				paymentInfoModel = sharjahCyberSourceFacade.saveToken(request, paymentInfoData, cartModel);
				// Save the last used card as default after its model has been created
				sharjahCyberSourceFacade.setDefaultPaymentInfo(paymentInfoData);

				final HttpSession session = request.getSession(true);
				final String devID = getValue(request, CyberSourceConstants.REQ_DEVICE_FINGERPRINT_ID);
				if (StringUtils.isNotBlank(devID))
				{
					session.setAttribute(CyberSourceConstants.REQ_DEVICE_FINGERPRINT_ID, devID);
				}

				// Create all transactions
				sharjahCyberSourceFacade.savePaymentTransactionEntry(cartModel, requestId, requestToken, paymentInfoModel);

				setCheckoutStepLinksForModel(model, getCheckoutStep());
			}
			catch (final Exception e)
			{
				LOGGER.error("Failed to place Order from cart " + cartData.getCode(), e);
				GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
				return getCheckoutStep().previousStep();
			}

			OrderData orderData = null;
			try
			{
				final String finalDecision = isPossibleFraud(request) ? CyberSourceConstants.DECISION_REVIEW : decision.toUpperCase();
				orderData = sharjahCyberSourceFacade.placeOrder(finalDecision, paymentInfoModel);
			}
			catch (final Exception e)
			{
				LOGGER.error("Failed to place Order " + cartData.getCode(), e);
				GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
			}

			return redirectToOrderConfirmationPage(orderData);
		}
		else
		{

			if (CyberSourceConstants.DECISION_REJECT.equals(decision))
			{
				sharjahCyberSourceFacade.cancel(UUID.randomUUID().toString(), requestId, requestToken,
						CyberSourceConstants.PAYMENT_PROVIDER);
			}

			GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
			LOG.error(getErrorMessage(reasonCode));
			LOG.error(getValue(request, "message"));
			return REDIRECT_URL_ERROR + "/?decision=" + decision + "&reasonCode=" + reasonCode;
		}
	}

	private CCPaymentInfoData populatePaymentInfoData(final HttpServletRequest request, final CartData cartData)
	{
		CCPaymentInfoData paymentInfoData = cartData.getPaymentInfo();
		if (paymentInfoData == null)
		{
			paymentInfoData = new CCPaymentInfoData();
			paymentInfoData.setId(getValue(request, CyberSourceConstants.REQ_TRANSACTION_UUID));
			paymentInfoData.setCardType(CyberSourceConstants.CARD_TYPES.get(getValue(request, CyberSourceConstants.REQ_CARD_TYPE)));
			paymentInfoData.setCardNumber(getValue(request, CyberSourceConstants.REQ_CARD_NUMBER));

			final String forename = getValue(request, CyberSourceConstants.REQ_BILL_TO_FORENAME);
			final String surname = getValue(request, CyberSourceConstants.REQ_BILL_TO_SURNAME);
			final StringBuilder accountName = new StringBuilder(forename);
			if (surname != null && forename.compareToIgnoreCase(surname) != 0)
			{
				accountName.append(" ").append(surname);
			}
			paymentInfoData.setAccountHolderName(accountName.toString());

			final String[] expiryDateParts = getValue(request, CyberSourceConstants.REQ_CARD_EXPIRY_DATE).split("-");
			paymentInfoData.setExpiryMonth(expiryDateParts[0]);
			paymentInfoData.setExpiryYear(expiryDateParts[1]);

			final String subscriptionId = getValue(request, CyberSourceConstants.PAYMENT_TOKEN);
			if (StringUtils.isNotEmpty(subscriptionId))
			{
				paymentInfoData.setSubscriptionId(subscriptionId);
				paymentInfoData.setSaved(true);
			}
			else
			{
				paymentInfoData.setSaved(false);
			}
		}
		return paymentInfoData;
	}

	private String getValue(final HttpServletRequest request, final String key)
	{
		final Map<String, String[]> paramMap = request.getParameterMap();
		String value = "";
		final String[] array = paramMap.get(key);
		if (array != null && array.length > 0)
		{
			value = array[0];
		}
		if (array == null || value == null)
		{
			value = "";
		}
		return value;
	}

	private boolean isPossibleFraud(final HttpServletRequest request)
	{
		final String decision = getValue(request, CyberSourceConstants.DECISION);
		final String payerAuthenticationEci = getValue(request, CyberSourceConstants.PAYER_AUTHENTICATION_ECI);
		return CyberSourceConstants.DECISION_REVIEW.equals(decision) || CyberSourceConstants.ECI01.equals(payerAuthenticationEci)
				|| CyberSourceConstants.ECI06.equals(payerAuthenticationEci);
	}

	private String getErrorMessage(final String number)
	{
		return configurationService.getConfiguration().getString("cybersourceaddon.reason.code." + number);
	}

	/**
	 * Native hybris method for handling the "/response" POST request mapping.
	 *
	 * @param request
	 *           The request
	 * @return The next step
	 */
	public String doHandleHopResponse(final HttpServletRequest request)
	{
		final Map<String, String> resultMap = getRequestParameterMap(request);

		final PaymentSubscriptionResultData paymentSubscriptionResultData = getPaymentFacade()
				.completeHopCreateSubscription(resultMap, true);
		if (paymentSubscriptionResultData.isSuccess() && paymentSubscriptionResultData.getStoredCard() != null
				&& StringUtils.isNotBlank(paymentSubscriptionResultData.getStoredCard().getSubscriptionId()))
		{
			final CCPaymentInfoData newPaymentSubscription = paymentSubscriptionResultData.getStoredCard();

			if (getUserFacade().getCCPaymentInfos(true).size() <= 1)
			{
				getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
			}
			getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
		}
		else
		{
			// HOP ERROR!
			LOGGER.error("Failed to create subscription.  Please check the log files for more information");
			return REDIRECT_URL_ERROR + "/?decision=" + paymentSubscriptionResultData.getDecision() + "&reasonCode="
					+ paymentSubscriptionResultData.getResultCode();
		}

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String doHostedOrderPageError(@RequestParam(required = true) final String decision,
			@RequestParam(required = true) final String reasonCode, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{
		String redirectUrl = REDIRECT_URL_ADD_PAYMENT_METHOD;
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			redirectUrl = getCheckoutStep().onValidation(validationResults);
		}
		model.addAttribute("decision", decision);
		model.addAttribute("reasonCode", reasonCode);
		model.addAttribute("redirectUrl", redirectUrl.replace(REDIRECT_PREFIX, ""));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.hostedOrderPageError.breadcrumb"));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		//GlobalMessages.addErrorMessage(model, "checkout.multi.hostedOrderPageError.globalError");

		return ControllerConstants.Views.Pages.MultiStepCheckout.HostedOrderPageErrorPage;
	}

}
