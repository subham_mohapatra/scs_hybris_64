package com.sharjah.storefront.util;

import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

/**
 * Sharjah Quotes Util to set sorted comment
 *
 */
public class SharjahQuotesUtil
{
	private SharjahQuotesUtil()
	{
		// empty
	}

	/**
	 * 
	 * Sort the comments of the Order Entry Data and multi Order Entry
	 * 
	 * @param OrderEntryData
	 * @return
	 */
	public static void setSortedComments(final OrderEntryData orderEntry)
	{
		if (CollectionUtils.isNotEmpty(orderEntry.getComments()))
		{
			sortOrderEntryComments(orderEntry);
		}
		else if (orderEntry.getProduct() != null && orderEntry.getProduct().getMultidimensional() != null
				&& orderEntry.getProduct().getMultidimensional() && CollectionUtils.isNotEmpty(orderEntry.getEntries()))
		{
			for (final OrderEntryData multiDOrderEntry : orderEntry.getEntries())
			{
				if (CollectionUtils.isNotEmpty(multiDOrderEntry.getComments()))
				{
					sortOrderEntryComments(multiDOrderEntry);
				}
			}
		}
	}

	/**
	 * 
	 * Sort the comments of the Order Data and Cart Data
	 * 
	 * @param AbstractOrderData
	 * @return
	 */
	public static void sortComments(final AbstractOrderData orderData)
	{
		final List<CommentData> sortedComments = orderData.getComments().stream()
				.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
				.collect(Collectors.toList());
		orderData.setComments(sortedComments);
	}

	/**
	 * 
	 * Sort the comments of an Order Entry Data
	 * 
	 * @param OrderEntryData
	 * @return
	 */
	private static void sortOrderEntryComments(final OrderEntryData orderEntry)
	{
		final List<CommentData> sortedMultiDOrderEntryComments = orderEntry.getComments().stream()
				.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
				.collect(Collectors.toList());

		orderEntry.setComments(sortedMultiDOrderEntryComments);
	}
}
