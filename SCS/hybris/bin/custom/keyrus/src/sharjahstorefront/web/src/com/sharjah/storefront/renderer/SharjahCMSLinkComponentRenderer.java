package com.sharjah.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.CMSComponentRenderer;
import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;
import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.model.SharjahCMSLinkComponentModel;

/**
 * Sharjah cmslink render fro cms render
 *
 */

public class SharjahCMSLinkComponentRenderer implements CMSComponentRenderer<SharjahCMSLinkComponentModel>
{

	private static final Logger LOG = LogManager.getLogger(SharjahCMSLinkComponentRenderer.class);

	private Converter<ProductModel, ProductData> productUrlConverter;
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;

	/**
	 * Copy method from original renderer ands add custom elements to link.
	 */
	@Override
	public void renderComponent(final PageContext pageContext, final SharjahCMSLinkComponentModel component)
			throws ServletException, IOException
	{
		try
		{
			final String url = getUrl(component);
			final JspWriter out = pageContext.getOut();

			if (StringUtils.isNotBlank(component.getLinkName()) && !existUrl(component))
			{
				// Write additional attributes onto the link
				if (component.getStyleAttributes() != null)
				{
					out.write(" <span class=\"");
					out.write(component.getStyleAttributes());
					out.write("\"  >");
				}
				else
				{
					out.write("<span>");
				}

				out.write(generateImageElement(component.getLinkImage()));
				out.write(component.getLinkName());
				out.write("</span>");
			}
			else
			{
				final String encodedUrl = UrlSupport.resolveUrl(url, null, pageContext);

				out.write("<a href=\"");
				out.write(encodedUrl);
				out.write("\" ");

				// Write additional attributes onto the link
				if (component.getStyleAttributes() != null)
				{
					out.write(" class=\"");
					out.write(component.getStyleAttributes());
					out.write("\" ");
				}

				if (StringUtils.isNotBlank(component.getLinkName()))
				{
					out.write(" title=\"");
					out.write(component.getLinkName());
					out.write("\" ");
				}

				if (component.getTarget() != null && !LinkTargets.SAMEWINDOW.equals(component.getTarget()))
				{
					out.write(" target=\"_blank\"");
				}
				out.write(">");
				out.write(generateImageElement(component.getLinkImage()));
				if (StringUtils.isNotBlank(component.getLinkName()))
				{
					out.write(component.getLinkName());
				}
				out.write("</a>");
			}
		}
		catch (final JspException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug(e);
			}
		}
	}

	/**
	 * Generate the img HTML element.
	 */
	private String generateImageElement(final MediaModel image)
	{
		if (image == null)
		{
			return "";
		}

		final StringBuilder result = new StringBuilder();
		result.append("<img class=\"link-comp-img\" src=\"");
		result.append(image.getURL());
		result.append("\" alt=\"");
		result.append(image.getAltText());
		result.append("\" />");

		return result.toString();
	}

	/**
	 *
	 * @param component
	 * @return
	 */
	private boolean existUrl(final CMSLinkComponentModel component)
	{
		return StringUtils.isNotEmpty(component.getUrl()) || component.getCategory() != null || component.getProduct() != null
				|| component.getContentPage() != null;
	}

	protected Converter<ProductModel, ProductData> getProductUrlConverter()
	{
		return productUrlConverter;
	}

	@Required
	public void setProductUrlConverter(final Converter<ProductModel, ProductData> productUrlConverter)
	{
		this.productUrlConverter = productUrlConverter;
	}

	protected Converter<CategoryModel, CategoryData> getCategoryUrlConverter()
	{
		return categoryUrlConverter;
	}

	@Required
	public void setCategoryUrlConverter(final Converter<CategoryModel, CategoryData> categoryUrlConverter)
	{
		this.categoryUrlConverter = categoryUrlConverter;
	}

	protected String getUrl(final CMSLinkComponentModel component)
	{
		// Call the function getUrlForCMSLinkComponent so that this code is only in one place
		return Functions.getUrlForCMSLinkComponent(component, getProductUrlConverter(), getCategoryUrlConverter());
	}

}
