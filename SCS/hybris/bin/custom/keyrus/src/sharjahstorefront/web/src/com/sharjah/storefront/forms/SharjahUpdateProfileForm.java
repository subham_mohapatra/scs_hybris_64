package com.sharjah.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateProfileForm;

import com.sharjah.core.enums.MaritalStatus;


/**
 * Form object for updating profile.
 * Sharjah Update Profile Form for Update Profile Form
 */
public class SharjahUpdateProfileForm extends UpdateProfileForm
{

	private String genderCode;
	private String howDidYouFindUs;
	private MaritalStatus maritalStatus;
	private String landLine;
	private Integer numberOfDependants;
	private Integer houseHoldSize;
	private String nationality;
	private String shareholderNumber;
	private String shareholderNumberPrefix;
	private String birthday;
	private String dayOfBirth;
	private String monthOfBirth;
	private String yearOfBirth;
	private String email;
	private String phoneNumber;
	private String password;
	private String languageCode;
	private String mobileprefixNumber;
	private String mobileSecondPrefixNumber;

	/**
	 * @return the mobileSecondPrefixNumber
	 */
	public String getMobileSecondPrefixNumber() {
		return mobileSecondPrefixNumber;
	}

	/**
	 * @param mobileprefixNumber
	 *            the mobileSecondPrefixNumber to set
	 */
	public void setMobileSecondPrefixNumber(String mobileSecondPrefixNumber) {
		this.mobileSecondPrefixNumber = mobileSecondPrefixNumber;
	}
	
	/**
	 * @return the genderCode
	 */
	public String getGenderCode()
	{
		return genderCode;
	}

	/**
	 * @param genderCode
	 *           the genderCode to set
	 */
	public void setGenderCode(final String genderCode)
	{
		this.genderCode = genderCode;
	}

	/**
	 * @return the howDidYouFindUs
	 */
	public String getHowDidYouFindUs()
	{
		return howDidYouFindUs;
	}

	/**
	 * @param howDidYouFindUs
	 *           the howDidYouFindUs to set
	 */
	public void setHowDidYouFindUs(final String howDidYouFindUs)
	{
		this.howDidYouFindUs = howDidYouFindUs;
	}

	/**
	 * @return the maritalStatus
	 */
	public MaritalStatus getMaritalStatus()
	{
		return maritalStatus;
	}

	/**
	 * @param maritalStatus
	 *           the maritalStatus to set
	 */
	public void setMaritalStatus(final MaritalStatus maritalStatus)
	{
		this.maritalStatus = maritalStatus;
	}

	/**
	 * @return the landLine
	 */
	public String getLandLine()
	{
		return landLine;
	}

	/**
	 * @param landLine
	 *           the landLine to set
	 */
	public void setLandLine(final String landLine)
	{
		this.landLine = landLine;
	}


	/**
	 * @return the nationality
	 */
	public String getNationality()
	{
		return nationality;
	}

	/**
	 * @param nationality
	 *           the nationality to set
	 */
	public void setNationality(final String nationality)
	{
		this.nationality = nationality;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *           the phoneNumber to set
	 */
	public void setPhoneNumber(final String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *           the password to set
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * @return the languageCode
	 */
	public String getLanguageCode()
	{
		return languageCode;
	}

	/**
	 * @param languageCode
	 *           the languageCode to set
	 */
	public void setLanguageCode(final String languageCode)
	{
		this.languageCode = languageCode;
	}

	/**
	 * @return the numberOfDependants
	 */
	public Integer getNumberOfDependants()
	{
		return numberOfDependants;
	}

	/**
	 * @param numberOfDependants
	 *           the numberOfDependants to set
	 */
	public void setNumberOfDependants(final Integer numberOfDependants)
	{
		this.numberOfDependants = numberOfDependants;
	}

	/**
	 * @return the houseHoldSize
	 */
	public Integer getHouseHoldSize()
	{
		return houseHoldSize;
	}

	/**
	 * @param houseHoldSize
	 *           the houseHoldSize to set
	 */
	public void setHouseHoldSize(final Integer houseHoldSize)
	{
		this.houseHoldSize = houseHoldSize;
	}

	/**
	 * @return the shareholderNumber
	 */
	public String getShareholderNumber()
	{
		return shareholderNumber;
	}

	/**
	 * @param shareholderNumber
	 *           the shareholderNumber to set
	 */
	public void setShareholderNumber(final String shareholderNumber)
	{
		this.shareholderNumber = shareholderNumber;
	}

	/**
	 * @return the birthday
	 */
	public String getBirthday()
	{
		return birthday;
	}

	/**
	 * @param birthday
	 *           the birthday to set
	 */
	public void setBirthday(final String birthday)
	{
		this.birthday = birthday;
	}

	/**
	 * 
	 * @return Mobile prefix Number
	 */
	public String getMobileprefixNumber() {
		return mobileprefixNumber;
	}

	/**
	 * set Mobile prefix Number attribute
	 * 
	 * @param mobileprefixNumber
	 */
	public void setMobileprefixNumber(final String mobileprefixNumber) {
		this.mobileprefixNumber = mobileprefixNumber;
	}

	/**
	 * @return the dayOfBirth
	 */
	public String getDayOfBirth() {
		return dayOfBirth;
	}

	/**
	 * @param dayOfBirth the dayOfBirth to set
	 */
	public void setDayOfBirth(final String dayOfBirth) {
		this.dayOfBirth = dayOfBirth;
	}

	/**
	 * @return the monthOfBirth
	 */
	public String getMonthOfBirth() {
		return monthOfBirth;
	}

	/**
	 * @param monthOfBirth the monthOfBirth to set
	 */
	public void setMonthOfBirth(final String monthOfBirth) {
		this.monthOfBirth = monthOfBirth;
	}

	/**
	 * @return the yearOfBirth
	 */
	public String getYearOfBirth() {
		return yearOfBirth;
	}

	/**
	 * @param yearOfBirth the yearOfBirth to set
	 */
	public void setYearOfBirth(final String yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	/**
	 * @return the shareholderNumberPrefix
	 */
	public String getShareholderNumberPrefix() {
		return shareholderNumberPrefix;
	}

	/**
	 * @param shareholderNumberPrefix the shareholderNumberPrefix to set
	 */
	public void setShareholderNumberPrefix(final String shareholderNumberPrefix) {
		this.shareholderNumberPrefix = shareholderNumberPrefix;
	}

}
