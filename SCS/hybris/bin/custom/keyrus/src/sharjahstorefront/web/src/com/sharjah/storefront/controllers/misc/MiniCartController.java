/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.misc;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sharjah.core.cartthresholdconfiguration.service.SharjahCartThresholdConfigurationService;
import com.sharjah.core.model.CartThresholdConfigurationModel;
import com.sharjah.storefront.controllers.ControllerConstants;


/**
 * Controller for MiniCart functionality which is not specific to a page.
 */
@Controller
public class MiniCartController extends AbstractController
{
	private static final Logger LOG = LogManager.getLogger(MiniCartController.class);

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String TOTAL_DISPLAY_PATH_VARIABLE_PATTERN = "{totalDisplay:.*}";
	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";
	private static final String CART_THRESHOLD_CODE = "cart_threshold_code";
	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;
	
	@Resource(name = "sharjahCartThresholdConfigurationService")
	private SharjahCartThresholdConfigurationService sharjahCartThresholdConfigurationService;

	@Resource(name = "storeSessionFacade")
	private StoreSessionFacade storeSessionFacade;


	@RequestMapping(value = "/cart/miniCart/" + TOTAL_DISPLAY_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String getMiniCart(@PathVariable final String totalDisplay, final Model model)
	{
		final CartData cartData = cartFacade.getMiniCart();
		model.addAttribute("totalPrice", cartData.getTotalPrice());
		model.addAttribute("subTotal", cartData.getSubTotal());
		if (cartData.getDeliveryCost() != null)
		{
			final PriceData withoutDelivery = cartData.getDeliveryCost();
			withoutDelivery.setValue(cartData.getTotalPrice().getValue().subtract(cartData.getDeliveryCost().getValue()));
			model.addAttribute("totalNoDelivery", withoutDelivery);
		}
		else
		{
			model.addAttribute("totalNoDelivery", cartData.getTotalPrice());
		}
		model.addAttribute("vatTax", cartData.getTotalTax());
		model.addAttribute("totalItems", cartData.getTotalUnitCount());
		model.addAttribute("totalDiscounts", cartData.getTotalDiscounts());
		model.addAttribute("totalDisplay", totalDisplay);
		model.addAttribute("threshold", getCartThresholdConfigurationModel());
		final String language = storeSessionFacade.getCurrentLanguage().getIsocode();
		if(StringUtils.isNotEmpty(language))
		{
			model.addAttribute("language", language);
		}
		else
		{
			model.addAttribute("language", "en");
		}

		return ControllerConstants.Views.Fragments.Cart.MiniCartPanel;
	}

	@RequestMapping(value = "/cart/rollover/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String rolloverMiniCartPopup(@PathVariable final String componentUid, final Model model)
			throws CMSItemNotFoundException
	{
		final CartData cartData = cartFacade.getSessionCart();
		model.addAttribute("cartData", cartData);

		final MiniCartComponentModel component = (MiniCartComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);

		final List<OrderEntryData> entries = cartData.getEntries();
		if (entries != null)
		{
			Collections.reverse(entries);
			model.addAttribute("entries", entries);

			model.addAttribute("numberItemsInCart", Integer.valueOf(entries.size()));
			if (entries.size() < component.getShownProductCount())
			{
				model.addAttribute("numberShowing", Integer.valueOf(entries.size()));
			}
			else
			{
				model.addAttribute("numberShowing", Integer.valueOf(component.getShownProductCount()));
			}
		}
		model.addAttribute("lightboxBannerComponent", component.getLightboxBannerComponent());
		model.addAttribute("threshold", getCartThresholdConfigurationModel());

		return ControllerConstants.Views.Fragments.Cart.CartPopup;
	}

	/**
	 * Update quantity of a cart entry.
	 *
	 * @param totalDisplay
	 *           The total to display (TOTAL, SUBTOTAL, TOTAL_WITHOUT_DELIVERY)
	 * @param entryNumber
	 *           The entry number
	 * @param productCode
	 *           The code of the product to update
	 * @param quantity
	 *           The new quantity to set
	 * @param model
	 *           The Spring MVC model
	 * @return The miniCart
	 */
	@RequestMapping(value = "/cart/updateMiniCart/" + TOTAL_DISPLAY_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String updateMiniCartPopup(@PathVariable final String totalDisplay, @RequestParam("entryNumber") final long entryNumber,
			@RequestParam("productCode") final String productCode, @RequestParam("quantity") final long quantity,
			final Model model)
	{
		try
		{
			final CartModificationData cartModification = cartFacade.updateCartEntry(entryNumber, quantity);
			if (cartModification.getQuantity() == quantity)
			{
				// Success
			}
			else if (cartModification.getQuantity() > 0)
			{
				// Less than successful
				model.addAttribute("alertMessage",
						messageSource.getMessage("popup.cart.message.update.reducedNumberOfItemsAdded.lowStock", new Object[]
						{ cartModification.getEntry().getProduct().getName() }, null));
			}
			else
			{
				// No more stock available
				model.addAttribute("alertMessage",
						messageSource.getMessage("popup.cart.message.update.reducedNumberOfItemsAdded.noStock", new Object[]
						{ cartModification.getEntry().getProduct().getName() }, null));
			}
		}
		catch (final CommerceCartModificationException ex)
		{
			LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
		}

		// Reload the miniCart header
		return getMiniCart(totalDisplay, model);
	}

	/**
	 * 
	 * @return
	 */
	private String getCartThresholdConfigurationModel()
	{
		final CartThresholdConfigurationModel cartThresholdConfiguration = sharjahCartThresholdConfigurationService
				.getCartThresholdConfigurationByCode(CART_THRESHOLD_CODE);
		double threshold = 0;
		if (cartThresholdConfiguration != null && cartThresholdConfiguration.getThreshold() != null)
		{
			threshold = cartThresholdConfiguration.getThreshold().doubleValue();
		}

		return new DecimalFormat("0.00").format(threshold);

	}
}
