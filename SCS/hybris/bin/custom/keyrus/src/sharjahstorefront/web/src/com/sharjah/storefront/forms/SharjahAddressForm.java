package com.sharjah.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;


/**
 * 
 * @author Abderrazzak.Blej
 * Sharjah address Form  for Address Form
 * 
 */
public class SharjahAddressForm extends AddressForm
{
	private String state;
	private String building;
	private String apartment;
	private String landmark;
    private String mobileprefixNumber;
	private String mobileSecondPrefixNumber;
	private String name;
	private String grl;
	private String longitude;
	private String latitude;
	/**
	 * @return the grl
	 */
	public String getGrl() {
		return grl;
	}

	/**
	 * @param grl the grl to set
	 */
	public void setGrl(final String grl) {
		this.grl = grl;
	}

	/**
	 * @return the mobileSecondPrefixNumber
	 */
	public String getMobileSecondPrefixNumber() {
		return mobileSecondPrefixNumber;
	}

	/**
	 * @param mobileprefixNumber
	 *            the mobileSecondPrefixNumber to set
	 */
	public void setMobileSecondPrefixNumber(final String mobileSecondPrefixNumber) {
		this.mobileSecondPrefixNumber = mobileSecondPrefixNumber;
	}

	/**
	 * @return the mobileprefixNumber
	 */
	public String getMobileprefixNumber() {
		return mobileprefixNumber;
	}
	
	/**
	 * @param mobileprefixNumber
	 *            the mobileprefixNumber to set
	 */
	public void setMobileprefixNumber(final String mobileprefixNumber) {
		this.mobileprefixNumber = mobileprefixNumber;
	}

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param set
	 *           name attribute
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * 
	 * @param set
	 *           state attribute
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * 
	 * @return building
	 */
	public String getBuilding()
	{
		return building;
	}

	/**
	 * 
	 * @param set
	 *           building attribute
	 */
	public void setBuilding(final String building)
	{
		this.building = building;
	}

	/**
	 * 
	 * @return apartment
	 */
	public String getApartment()
	{
		return apartment;
	}

	/**
	 * 
	 * @param set
	 *           apartment attribute
	 */
	public void setApartment(final String apartment)
	{
		this.apartment = apartment;
	}

	/**
	 * 
	 * @return landmark
	 */
	public String getLandmark()
	{
		return landmark;
	}

	/**
	 * 
	 * @param set
	 *           landmark attribute
	 */
	public void setLandmark(final String landmark)
	{
		this.landmark = landmark;
	}

	/**
	 * 
	 * @return
	 */
	public String getLongitude()
	{
		return longitude;
	}

	/**
	 * 
	 * @param longitude
	 */
	public void setLongitude(final String longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * 
	 * @return
	 */
	public String getLatitude()
	{
		return latitude;
	}

	/**
	 * 
	 * @param latitude
	 */
	public void setLatitude(final String latitude)
	{
		this.latitude = latitude;
	}

}
