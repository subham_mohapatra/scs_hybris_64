$(document).ready(function() {
	console.log('on.click.conf.js - loaded');
	$('.js-close-target').on('click', function(e) {
		console.log('on.click.conf.js - click on : .js-close-target', $(this).data('close-slide'));
		e.preventDefault();

		var $target = ($(this).data('close-target') !== 'undefined' && typeof $(this).data('close-target') === 'string' ? $($(this).data('close-target')) : $(this));
		var toBeSaved = ($(this).data('close-remember') !== 'undefined' && $(this).data('close-remember') ? true : false);
		var slideOnClose = ($(this).data('close-slide') !== 'undefined' && typeof $(this).data('close-slide') === 'string' ? $(this).data('close-slide') : false);

		if (slideOnClose) {
			$target.find('> *').css({ 'visibility': 'hidden' });
			switch (slideOnClose) {
				case 'down':
					$target.hide('slide', { direction: 'down' }, 300);
					break;
				case 'left':
					$target.hide('slide', { direction: 'left' }, 300);
					break;
				case 'right':
					$target.hide('slide', { direction: 'right' }, 300);
					break;
				case 'up':
					$target.slideUp();
					break;
				default:
					$target.hide();
			}
		} else {
			$target.hide();
		}

		if (toBeSaved) {
			// to define
		}
	});

	// Trigger the resize of elements when cookie bar is closed
	$(document).on('click', '#cookie-bar .cb-enable', function() {
		$('.js-resize').trigger('click');
	});


	// START : Manage an active class on the targeted element
	var jsActiveMeClass = 'js-activeMe';
	var jsActiveMeAttr = 'data-active-me';
	$(window).on('click', function() {
		$('[' + jsActiveMeAttr + '="true"]').each(function() {
			$(this).removeClass('active');
			if (typeof $(this).attr(jsActiveMeAttr) !== 'undefined') {
				$(this).removeAttr(jsActiveMeAttr);
			}
		});
	});
	$('.' + jsActiveMeClass).on('click', function(e) {
		e.stopPropagation();
		$(this).toggleClass('active');
		if (typeof $(this).attr(jsActiveMeAttr) !== 'undefined') {
			$(this).removeAttr(jsActiveMeAttr);
		} else {
			$(this).attr(jsActiveMeAttr, 'true');
		}
	});
	// END : Manage an active class on the targeted element

	$('.js-open-view360').on('click', function(e) {
		console.log('Click on : .js-open-view360 To open View 360 Product', $(this).data('close-slide'));
		e.stopPropagation();
		ACC.view360.open360View();
	});


	$('.js-closeNavStoreFinder').on('click', function(e) {
		console.log('js-closeNavStoreFinder : CLICK');
		$('.store__finder--navigation').addClass('closedNav');
		$('.js-closeNavStoreFinder').hide();
		$('.js-openNavStoreFinder').show();
	});

	$('.js-openNavStoreFinder').on('click', function(e) {
		console.log('js-openNavStoreFinder : CLICK');
		$('.store__finder--navigation').removeClass('closedNav');
		$('.js-closeNavStoreFinder').show();
		$('.js-openNavStoreFinder').hide();
	});

});
