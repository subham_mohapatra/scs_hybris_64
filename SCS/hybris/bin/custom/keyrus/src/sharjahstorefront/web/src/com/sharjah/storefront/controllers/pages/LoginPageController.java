/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.storefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharjah.core.model.ShareholderModel;
import com.sharjah.facades.shareholder.data.ShareholderData;
import com.sharjah.facades.shareholder.populator.SharjahShareholderPopulator;
import com.sharjah.shareholder.service.ShareholderService;
import com.sharjah.storefront.controllers.ControllerConstants;
import com.sharjah.storefront.forms.SharjahRegisterForm;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@RequestMapping(value = "/login")
public class LoginPageController extends SharjahAbstractLoginPageController
{
	private HttpSessionRequestCache httpSessionRequestCache;
	public static final String CHECKOUT_URL = "/checkout";

	@Resource
	private ShareholderService shareholderService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource
	private SharjahShareholderPopulator<ShareholderModel, ShareholderData> sharjahShareholderPopulator;

	@Override
	protected String getView()
	{
		return ControllerConstants.Views.Pages.Account.AccountLoginPage;
	}

	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/";
	}

	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId("login");
	}

	@Resource(name = "httpSessionRequestCache")
	public void setHttpSessionRequestCache(final HttpSessionRequestCache accHttpSessionRequestCache)
	{
		this.httpSessionRequestCache = accHttpSessionRequestCache;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "error", defaultValue = "false") final boolean loginError, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		if (!loginError)
		{
			storeReferer(referer, request, response);
			if (request.getHeader("referer") != null && request.getHeader("referer").contains("/login")
					&& userService.getCurrentUser() != null
					&& !"Anonymous".equals(userService.getCurrentUser().getName()))
			{
				return REDIRECT_PREFIX + ROOT;
			}

		}
		return getDefaultLoginPage(loginError, session, model);
	}

	protected void storeReferer(final String referer, final HttpServletRequest request, final HttpServletResponse response)
	{
		if (StringUtils.isNotBlank(referer) && !StringUtils.endsWith(referer, "/login")
				&& StringUtils.contains(referer, request.getServerName()))
		{
			httpSessionRequestCache.saveRequest(request, response);
		}
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doRegister(@RequestHeader(value = "referer", required = false) final String referer,
			final SharjahRegisterForm form, final BindingResult bindingResult, final Model model, final HttpServletRequest request,
			final HttpServletResponse response, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		getRegistrationValidator().validate(form, bindingResult);
		return sharjahProcessRegisterUserRequest(referer, form, bindingResult, model, request, response, redirectModel);
	}

	@RequestMapping(value = "/verifyShareholder", method = RequestMethod.GET)
	@ResponseBody
	public ShareholderData verifyShareholder(@RequestParam(value = "number", required = false) final String number,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final ShareholderData shareholderData = new ShareholderData();
		final ShareholderModel shareholder = shareholderService.findShareholderByNumber(number);
		if (shareholder != null)
		{
			getSharjahShareholderPopulator().populate(shareholder, shareholderData);
		}
		return shareholderData;

	}

	public ShareholderService getShareholderService()
	{
		return shareholderService;
	}

	public void setShareholderService(final ShareholderService shareholderService)
	{
		this.shareholderService = shareholderService;
	}

	public SharjahShareholderPopulator<ShareholderModel, ShareholderData> getSharjahShareholderPopulator()
	{
		return sharjahShareholderPopulator;
	}

	public void setSharjahShareholderPopulator(
			final SharjahShareholderPopulator<ShareholderModel, ShareholderData> sharjahShareholderPopulator)
	{
		this.sharjahShareholderPopulator = sharjahShareholderPopulator;
	}

}
