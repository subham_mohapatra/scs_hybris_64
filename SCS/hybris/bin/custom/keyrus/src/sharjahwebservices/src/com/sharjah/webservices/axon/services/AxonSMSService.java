/**
 *
 */
package com.sharjah.webservices.axon.services;

/**
 * @author Wael Sakhri
 *
 */
public interface AxonSMSService
{

	/**
	 * Send SMS using API
	 *
	 * @param number
	 * @param message
	 * @param language
	 */
	void sendSMS(final String number, final String message, final String language);
}
