/**
 *
 */
package com.sharjah.webservices.axon.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.sharjah.webservices.axon.services.AxonSMSService;


/**
 * @author Wael Sakhri
 *
 */
public class DefaultAxonSMSService implements AxonSMSService
{
	private final static Logger LOGGER = Logger.getLogger(DefaultAxonSMSService.class);

	@Autowired
	private RestTemplate restTemplate;

	private String axonURL;
	private String axonSecret;


	/**
	 * Send SMS using axone API
	 *
	 */
	@Override
	@Retryable(maxAttempts = 2, value = RuntimeException.class)
	public void sendSMS(final String number, final String message, final String language)
	{
		Assert.notNull(number, "no number provided");
		Assert.notNull(message, "no message provided");
		Assert.notNull(language, "no language provided");

		if (LOGGER.isInfoEnabled())
		{
			LOGGER.info("Axon SMS API Sending message : " + message + ", To : " + number + " (language : " + language + ")");
		}

		ResponseEntity<String> result = null;

		final HttpHeaders tempHeaders = new HttpHeaders();
		final HttpEntity<String> entity = new HttpEntity<>(tempHeaders);

		try
		{

			result = restTemplate.exchange(buildURL(number, message, language), HttpMethod.GET, entity, String.class);

			if (LOGGER.isInfoEnabled())
			{
				LOGGER.info("Axon SMS successfully sent to :" + number);
			}

			if (LOGGER.isDebugEnabled())
			{
				LOGGER.debug("axone SMS API Response status: " + result.getStatusCode() + "\nResponse headers: " + result.getHeaders()
						+ "\nResponse body: " + result.getBody());
			}
		}
		catch (final RestClientException e)
		{
			LOGGER.error("Error Getting WebService due to : " + e.getMessage(), e);
		}

	}


	/**
	 * Build axone REST API URL using prerequisite data
	 *
	 * @param number
	 * @param message
	 * @param language
	 * @return url
	 */
	private String buildURL(final String number, final String message, final String language)
	{
		return new StringBuilder(this.getAxonURL()).append("/").append(number).append("?message=").append(message)
				.append("&language=").append(language).append("&secret=").append(getAxonSecret()).toString();
	}

	/**
	 * @return the axonURL
	 */
	public String getAxonURL()
	{
		return axonURL;
	}

	/**
	 * @param axonURL
	 *           the axonURL to set
	 */
	public void setAxonURL(final String axonURL)
	{
		this.axonURL = axonURL;
	}

	/**
	 * @return the axonSecret
	 */
	public String getAxonSecret()
	{
		return axonSecret;
	}

	/**
	 * @param axonSecret
	 *           the axonSecret to set
	 */
	public void setAxonSecret(final String axonSecret)
	{
		this.axonSecret = axonSecret;
	}

}
