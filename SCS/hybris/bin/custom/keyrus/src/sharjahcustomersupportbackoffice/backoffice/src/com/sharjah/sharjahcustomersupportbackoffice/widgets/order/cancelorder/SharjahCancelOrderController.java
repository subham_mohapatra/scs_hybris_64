/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.sharjah.sharjahcustomersupportbackoffice.widgets.order.cancelorder;

import de.hybris.platform.acceleratorservices.orderprocessing.model.OrderModificationProcessModel;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.basecommerce.enums.OrderCancelEntryStatus;
import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.omsbackoffice.dto.OrderEntryToCancelDto;
import de.hybris.platform.ordercancel.OrderCancelCallbackService;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelResponse;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.ordercancel.OrderCancelResponse.ResponseStatus;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.orderprocessing.events.SendOrderPartiallyCanceledMessageEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CancellationException;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelArray;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.impl.InputElement;
import com.hybris.backoffice.i18n.BackofficeLocaleService;

import com.hybris.backoffice.widgets.notificationarea.event.NotificationEvent.Type;
import com.hybris.backoffice.widgets.notificationarea.event.NotificationUtils;
import com.hybris.cockpitng.annotations.SocketEvent;
import com.hybris.cockpitng.annotations.ViewEvent;
import com.hybris.cockpitng.core.events.CockpitEventQueue;
import com.hybris.cockpitng.core.events.impl.DefaultCockpitEvent;
import com.hybris.cockpitng.util.DefaultWidgetController;
import com.sharjah.core.order.OrderExportType;
import com.sharjah.core.order.service.SharjahOrderExportService;
import com.sharjah.core.stock.SharjahStockService;


/**
 *
 *
 * @author issam.maiza
 *
 */
public class SharjahCancelOrderController extends DefaultWidgetController
{
	protected static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SharjahCancelOrderController.class);
	private static final long serialVersionUID = 1L;
	protected static final String IN_SOCKET = "inputObject";
	protected static final String CONFIRM_ID = "confirmcancellation";
	protected static final Object COMPLETED = "completed";
	protected static final Object PARTIALCANCEL = "PARTIAL";
	protected static final Object FULLCANCEL = "FULL";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_PICKUP = "customersupportbackoffice.cancelorder.pickup";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_TITLE = "customersupportbackoffice.cancelorder.confirm.title";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_ERROR = "customersupportbackoffice.cancelorder.confirm.error";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_CONFIRM_MSG = "customersupportbackoffice.cancelorder.confirm.msg";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_ERROR_QTYCANCELLED_INVALID = "customersupportbackoffice.cancelorder.error.qtycancelled.invalid";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_QUANTITY = "customersupportbackoffice.cancelorder.missing.quantity";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_ERROR_REASON = "customersupportbackoffice.cancelorder.error.reason";
	protected static final String CUSTOMERSUPPORTBACKOFFICE_CANCELORDER_MISSING_SELECTED_LINE = "customersupportbackoffice.cancelorder.missing.selectedLine";
	protected static final String CANCELORDER_CONFIRM_ICON = "oms-widget-cancelorder-confirm-icon";
	protected static final int COLUMN_INDEX_PENDING_QUANTITY = 4;
	protected static final int COLUMN_INDEX_CANCEL_QUANTITY = 5;
	protected static final int COLUMN_INDEX_CANCEL_REASON = 6;
	protected static final int COLUMN_INDEX_CANCEL_COMMENT = 7;
	@Wire
	private Textbox orderNumber;
	@Wire
	private Textbox customerName;
	@Wire
	private Combobox globalCancelReasons;
	@Wire
	private Textbox globalCancelComment;
	@Wire
	private Grid orderEntries;
	@Wire
	private Checkbox globalCancelEntriesSelection;
	private OrderModel orderModel;
	@WireVariable
	private BackofficeLocaleService cockpitLocaleService;
	@WireVariable
	private OrderCancelService orderCancelService;
	@WireVariable
	private EnumerationService enumerationService;
	@WireVariable
	private ModelService modelService;
	@WireVariable
	private CockpitEventQueue cockpitEventQueue;
	@WireVariable
	private UserService userService;
	@WireVariable
	private SharjahStockService stockService;
	@WireVariable
	private BusinessProcessService businessProcessService;
	@WireVariable
	private EventService eventService;
	@WireVariable
	private SharjahOrderExportService sharjahOrderExportService;
	
	private final List<String> cancelReasons = new ArrayList<String>();
	private Set<OrderEntryToCancelDto> orderEntriesToCancel;
	protected Map<AbstractOrderEntryModel, Long> orderCancellableEntries;


	@ViewEvent(componentID = "confirmcancellation", eventName = "onClick")
	public void confirmCancellation()
	{
		this.validateRequest();
		this.showMessageBox();
	}

	@SocketEvent(socketId = "inputObject")
	public void initCancellationOrderForm(final OrderModel inputObject)
	{
		this.cancelReasons.clear();
		this.globalCancelEntriesSelection.setChecked(false);
		this.setOrderModel(inputObject);
		this.getWidgetInstanceManager()
				.setTitle(this.getWidgetInstanceManager().getLabel("customersupportbackoffice.cancelorder.confirm.title") + " "
						+ this.getOrderModel().getCode());
		this.orderNumber.setValue(this.getOrderModel().getCode());
		this.customerName.setValue(this.getOrderModel().getUser().getDisplayName());
		final Locale locale = this.getLocale();
		this.getEnumerationService().getEnumerationValues(CancelReason.class).forEach((reason) -> {
			this.cancelReasons.add(this.getEnumerationService().getEnumerationName(reason, locale));
		});
		this.globalCancelReasons.setModel(new ListModelArray(this.cancelReasons));
		this.orderEntriesToCancel = new HashSet();
		this.orderCancellableEntries = this.getOrderCancelService().getAllCancelableEntries(this.getOrderModel(),
				this.getUserService().getCurrentUser());
		if (!this.orderCancellableEntries.isEmpty())
		{
			this.orderCancellableEntries.forEach((entry, cancellableQty) -> {
				this.orderEntriesToCancel
						.add(new OrderEntryToCancelDto(entry, this.cancelReasons, cancellableQty, this.determineDeliveryMode(entry)));
			});
		}

		this.getOrderEntries().setModel(new ListModelList(this.orderEntriesToCancel));
		this.getOrderEntries().renderAll();
		this.addListeners();
	}

	/**
	 *
	 * @param orderEntry
	 * @return
	 */
	protected String determineDeliveryMode(final AbstractOrderEntryModel orderEntry)
	{
		String deliveryModeResult;
		if (orderEntry.getDeliveryMode() != null)
		{
			deliveryModeResult = orderEntry.getDeliveryMode().getName();
		}
		else if (orderEntry.getDeliveryPointOfService() != null)
		{
			deliveryModeResult = this.getLabel("customersupportbackoffice.cancelorder.pickup");
		}
		else
		{
			deliveryModeResult = orderEntry.getOrder().getDeliveryMode() != null
					? (orderEntry.getOrder().getDeliveryMode().getName() != null ? orderEntry.getOrder().getDeliveryMode().getName()
							: orderEntry.getOrder().getDeliveryMode().getCode())
					: null;
		}

		return deliveryModeResult;
	}

	@ViewEvent(componentID = "undocancellation", eventName = "onClick")
	public void reset()
	{
		this.globalCancelReasons.setSelectedItem((Comboitem) null);
		this.globalCancelComment.setValue("");
		this.initCancellationOrderForm(this.getOrderModel());
	}

	protected void addListeners()
	{
		final List rows = this.getOrderEntries().getRows().getChildren();
		final Iterator arg2 = rows.iterator();

		while (arg2.hasNext())
		{
			final Component row = (Component) arg2.next();
			final Iterator arg4 = row.getChildren().iterator();

			while (arg4.hasNext())
			{
				final Component myComponent = (Component) arg4.next();
				if (myComponent instanceof Checkbox)
				{
					addListenerOnCheck(myComponent);
				}
				else if (myComponent instanceof Combobox)
				{
					addListenerOnCustomChange(myComponent);
				}
				else if (myComponent instanceof Intbox)
				{
					addListenerOnChange(myComponent);
				}
				else if (myComponent instanceof Textbox)
				{
					addListenerOnChanging(myComponent);
				}
			}
		}

		this.globalCancelReasons.addEventListener("onSelect", (event) -> {
			this.handleGlobalCancelReason(event);
		});
		this.globalCancelComment.addEventListener("onChanging", (event) -> {
			this.handleGlobalCancelComment(event);
		});
		this.globalCancelEntriesSelection.addEventListener("onCheck", (event) -> {
			this.selectAllEntries();
		});
	}

	/**
	 * Add a listener 'on Check' to the component
	 *
	 * @param myComponent
	 */
	private void addListenerOnCheck(final Component myComponent)
	{
		myComponent.addEventListener("onCheck", (event) -> {
			this.handleRow((Row) event.getTarget().getParent());
		});
	}

	/**
	 * Add a listener 'on Custom Change' to the component
	 *
	 * @param myComponent
	 */
	private void addListenerOnCustomChange(final Component myComponent)
	{
		myComponent.addEventListener("onCustomChange", (event) -> {
			Events.echoEvent("onLaterCustomChange", myComponent, event.getData());
		});
		myComponent.addEventListener("onLaterCustomChange", (event) -> {
			Clients.clearWrongValue(myComponent);
			myComponent.invalidate();
			this.handleIndividualCancelReason(event);
		});
	}

	/**
	 * Add a listener 'on Change' to the component
	 *
	 * @param myComponent
	 */
	private void addListenerOnChange(final Component myComponent)
	{
		myComponent.addEventListener("onChange", (event) -> {
			this.autoSelect(event);
			((OrderEntryToCancelDto) ((Row) event.getTarget().getParent()).getValue())
					.setQuantityToCancel(Long.valueOf(((InputEvent) event).getValue()));
		});
	}

	/**
	 * Add a listener 'on Changing' to the component
	 *
	 * @param myComponent
	 */
	private void addListenerOnChanging(final Component myComponent)
	{
		myComponent.addEventListener("onChanging", (event) -> {
			this.autoSelect(event);
			((OrderEntryToCancelDto) ((Row) event.getTarget().getParent()).getValue())
					.setCancelOrderEntryComment(((InputEvent) event).getValue());
		});
	}

	/**
	 *
	 * @param data
	 * @param childrenIndex
	 */
	protected void applyToGrid(final Object data, final int childrenIndex)
	{
		this.getOrderEntriesGridRows().stream().filter((entry) -> {
			return ((Checkbox) entry.getChildren().iterator().next()).isChecked();
		}).forEach((entry) -> {
			this.applyToRow(data, childrenIndex, entry);
		});
	}

	/**
	 *
	 * @param data
	 * @param childrenIndex
	 * @param row
	 */
	protected void applyToRow(final Object data, final int childrenIndex, final Component row)
	{
		int index = 0;

		for (final Iterator arg5 = row.getChildren().iterator(); arg5.hasNext(); ++index)
		{
			final Component myComponent = (Component) arg5.next();
			if (index == childrenIndex)
			{
				checkComponent(data, myComponent);
			}
		}
	}

	/**
	 *
	 * @param data
	 * @param myComponent
	 */
	private void checkComponent(final Object data, final Component myComponent)
	{
		if (myComponent instanceof Checkbox && data != null)
		{
			((Checkbox) myComponent).setChecked(((Boolean) data).booleanValue());
		}

		if (myComponent instanceof Combobox)
		{
			if (data == null)
			{
				((Combobox) myComponent).setSelectedItem((Comboitem) null);
			}
			else
			{
				((Combobox) myComponent).setSelectedIndex(((Integer) data).intValue());
			}
		}
		else if (myComponent instanceof Intbox)
		{
			((Intbox) myComponent).setValue((Integer) data);
		}
		else if (myComponent instanceof Textbox)
		{
			((Textbox) myComponent).setValue((String) data);
		}
	}

	/**
	 *
	 * @param event
	 */
	protected void autoSelect(final Event event)
	{
		((Checkbox) event.getTarget().getParent().getChildren().iterator().next()).setChecked(true);
	}

	/**
	 *
	 * @return
	 */
	protected OrderCancelRequest buildCancelRequest()
	{
		if (this.getOrderModel() != null)
		{
			final ArrayList orderCancelEntries = new ArrayList();
			this.getOrderEntriesGridRows().stream().filter((entry) -> {
				return ((Checkbox) entry.getFirstChild()).isChecked();
			}).forEach((entry) -> {
				this.createOrderCancelEntry(orderCancelEntries, ((Row) entry).getValue());
			});
			final OrderCancelRequest orderCancelRequest = new OrderCancelRequest(this.getOrderModel(), orderCancelEntries);
			orderCancelRequest.setCancelReason(this.matchingComboboxCancelReason(this.globalCancelReasons.getValue()).orElse(null));
			orderCancelRequest.setNotes(this.globalCancelComment.getValue());
			try
			{
				stockService.releaseStockAfterCancelOrder(orderCancelRequest);
			}
			catch (final OrderCancelException e)
			{
				if (LOGGER.isInfoEnabled())
				{
					LOGGER.info("error when cancelling order", e);
				}
			}


			return orderCancelRequest;
		}
		else
		{
			return null;
		}
	}

	/**
	 *
	 * @param orderCancelEntries
	 * @param entry
	 */
	protected void createOrderCancelEntry(final List<OrderCancelEntry> orderCancelEntries, final Object entry)
	{
		final OrderEntryToCancelDto orderEntryToCancel = (OrderEntryToCancelDto) entry;
		final OrderCancelEntry orderCancelEntry = new OrderCancelEntry(orderEntryToCancel.getOrderEntry(),
				orderEntryToCancel.getQuantityToCancel().longValue(), orderEntryToCancel.getCancelOrderEntryComment(),
				orderEntryToCancel.getSelectedReason());
		orderCancelEntries.add(orderCancelEntry);
	}

	/**
	 *
	 * @param cancelReason
	 * @return
	 */
	protected int getReasonIndex(final CancelReason cancelReason)
	{
		int index = 0;
		final String myReason = this.getEnumerationService().getEnumerationName(cancelReason,
				this.getCockpitLocaleService().getCurrentLocale());

		for (final Iterator arg4 = this.cancelReasons.iterator(); arg4.hasNext(); ++index)
		{
			final String reason = (String) arg4.next();
			if (myReason.equals(reason))
			{
				break;
			}
		}

		return index;
	}

	/**
	 *
	 * @param event
	 * @return
	 */
	protected Optional<CancelReason> getSelectedCancelReason(final Event event)
	{
		Optional result = Optional.empty();
		if (!((SelectEvent) event).getSelectedItems().isEmpty())
		{
			final Object selectedValue = ((Comboitem) ((SelectEvent) event).getSelectedItems().iterator().next()).getValue();
			result = this.matchingComboboxCancelReason(selectedValue.toString());
		}

		return result;
	}

	/**
	 *
	 * @param event
	 */
	protected void handleGlobalCancelComment(final Event event)
	{
		this.applyToGrid(((InputEvent) event).getValue(), 7);
		this.getOrderEntriesGridRows().stream().filter((entry) -> {
			return ((Checkbox) entry.getChildren().iterator().next()).isChecked();
		}).forEach((entry) -> {
			final OrderEntryToCancelDto myEntry = (OrderEntryToCancelDto) ((Row) entry).getValue();
			myEntry.setCancelOrderEntryComment(((InputEvent) event).getValue());
		});
	}

	/**
	 *
	 * @param event
	 */
	protected void handleGlobalCancelReason(final Event event)
	{
		final Optional cancelReason = this.getSelectedCancelReason(event);
		if (cancelReason.isPresent())
		{
			this.applyToGrid(Integer.valueOf(this.getReasonIndex((CancelReason) cancelReason.get())), 6);
			this.getOrderEntriesGridRows().stream().filter((entry) -> {
				return ((Checkbox) entry.getChildren().iterator().next()).isChecked();
			}).forEach((entry) -> {
				final OrderEntryToCancelDto myEntry = (OrderEntryToCancelDto) ((Row) entry).getValue();
				myEntry.setSelectedReason((CancelReason) cancelReason.get());
			});
		}

	}

	/**
	 *
	 * @param event
	 */
	protected void handleIndividualCancelReason(final Event event)
	{
		final Optional cancelReason = this.getCustomSelectedCancelReason(event);
		if (cancelReason.isPresent())
		{
			this.autoSelect(event);
			((OrderEntryToCancelDto) ((Row) event.getTarget().getParent()).getValue())
					.setSelectedReason((CancelReason) cancelReason.get());
		}

	}

	/**
	 *
	 * @param row
	 */
	protected void handleRow(final Row row)
	{
		final OrderEntryToCancelDto myEntry = (OrderEntryToCancelDto) row.getValue();
		if (!((Checkbox) row.getChildren().iterator().next()).isChecked())
		{
			this.applyToRow(Integer.valueOf(0), 5, row);
			this.applyToRow((Object) null, 6, row);
			this.applyToRow((Object) null, 7, row);
			myEntry.setQuantityToCancel(Long.valueOf(0L));
			myEntry.setSelectedReason((CancelReason) null);
			myEntry.setCancelOrderEntryComment((String) null);
		}
		else
		{
			this.applyToRow(Integer.valueOf(this.globalCancelReasons.getSelectedIndex()), 6, row);
			this.applyToRow(this.globalCancelComment.getValue(), 7, row);
			final Optional reason = this.matchingComboboxCancelReason(
					this.globalCancelReasons.getSelectedItem() != null ? this.globalCancelReasons.getSelectedItem().getLabel() : null);
			myEntry.setSelectedReason(reason.isPresent() ? (CancelReason) reason.get() : null);
			myEntry.setCancelOrderEntryComment(this.globalCancelComment.getValue());
		}

	}

	/**
	 *
	 * @param event
	 * @return
	 */
	protected Optional<CancelReason> getCustomSelectedCancelReason(final Event event)
	{
		Optional reason = Optional.empty();
		if (event.getTarget() instanceof Combobox)
		{
			final Object selectedValue = event.getData();
			reason = this.matchingComboboxCancelReason(selectedValue.toString());
		}

		return reason;
	}

	/**
	 *
	 * @param cancelReasonLabel
	 * @return
	 */
	protected Optional<CancelReason> matchingComboboxCancelReason(final String cancelReasonLabel)
	{
		return this.getEnumerationService().getEnumerationValues(CancelReason.class).stream().filter((reason) -> {
			return this.getEnumerationService().getEnumerationName(reason, this.getLocale()).equals(cancelReasonLabel);
		}).findFirst();
	}



	protected void updateOrderStatus(final OrderCancelRecordEntryModel orderCancelRecordEntryModel, final OrderModel order)
	{

		for (final AbstractOrderEntryModel orderEntry : order.getEntries())
		{
			if (!OrderEntryStatus.DEAD.equals(orderEntry.getQuantityStatus()))
			{
				break;
			}
			order.setStatus(OrderStatus.CANCELLED);
			this.modelService.save(order);
		}
	}

	/**
	 *
	 * @param obj
	 */
	protected void processCancellation(final Event obj)
	{
		if (Button.YES.event.equals(obj.getName()))
		{
			final OrderModel order = (OrderModel) this.getModelService().get(this.getOrderModel().getPk());
			final OrderStatus orderStatus = order.getStatus();

			try
			{
				final OrderCancelRecordEntryModel object = this.getOrderCancelService().requestOrderCancel(this.buildCancelRequest(),
						this.getUserService().getCurrentUser());
				switch (object.getCancelResult())
				{
					case FULL:
						order.setStatus(OrderStatus.CANCELLED);
						this.modelService.save(order);

						//cancel consignment
						if (CollectionUtils.isNotEmpty(order.getConsignments()))
						{
							for (final ConsignmentModel consignmentModel : order.getConsignments())
							{
								consignmentModel.setStatus(ConsignmentStatus.CANCELLED);
								this.modelService.save(consignmentModel);
							}
						}
						NotificationUtils.notifyUser(this.getLabel("customersupportbackoffice.cancelorder.confirm.success"),
								Type.SUCCESS);
						break;
					case PARTIAL:
						order.setStatus(orderStatus);
						this.modelService.save(order);
                  object.getModificationRecord().setInProgress(false);
                  this.modelService.save(object.getModificationRecord());
						updateOrderStatus(object, order);
						NotificationUtils.notifyUser(this.getLabel("customersupportbackoffice.cancelorder.confirm.success"),
								Type.SUCCESS);
						break;
					case DENIED:
						NotificationUtils.notifyUser(this.getLabel("customersupportbackoffice.cancelorder.confirm.error"),
								Type.FAILURE);
						break;
					default:
				}

				//export
				sharjahOrderExportService.exportOrder(order, OrderExportType.CANCEL.getCode());
				
				order.getEntries().forEach((entry) -> {
					this.getCockpitEventQueue().publishEvent(new DefaultCockpitEvent("objectUpdated", entry, (Object) null));
				});
				this.sendOutput("confirmcancellation", COMPLETED);

				sendCancelEmail(order, object);
			}
			catch (OrderCancelException | CancellationException arg2)
			{
				LOGGER.info(arg2.getMessage(), arg2);
				NotificationUtils.notifyUser(this.getLabel("customersupportbackoffice.cancelorder.confirm.error"), Type.FAILURE);
			}


		}

	}

	/**
	 * method to send cancel email or partial cancel email
	 *
	 * @OrderModel
	 * @OrderCancelRecordEntryModel
	 */
	private void sendCancelEmail(final OrderModel orderModel, final OrderCancelRecordEntryModel orderCancelRecordEntry)
	{
		LOGGER.info("this is a " + orderCancelRecordEntry.getCancelResult().getCode() + "cancel for order");
		if (orderCancelRecordEntry != null && PARTIALCANCEL.equals(orderCancelRecordEntry.getCancelResult().getCode()))
		{
			LOGGER.info("Partial cancel for order " + orderModel.getCode());
			final OrderModificationProcessModel orderModificationProcessModel = new OrderModificationProcessModel();
			orderModificationProcessModel.setOrder(orderModel);
			final OrderModificationRecordModel modificationRecord = orderModel.getModificationRecords().iterator().next();
			for (final OrderModificationRecordEntryModel orderModificationRecordEntryModel : modificationRecord
					.getModificationRecordEntries())
			{
				orderModificationProcessModel.setOrderModificationRecordEntry(orderModificationRecordEntryModel);
			}

			final SendOrderPartiallyCanceledMessageEvent event = new SendOrderPartiallyCanceledMessageEvent(
					orderModificationProcessModel);
			eventService.publishEvent(event);

		}
		if (orderCancelRecordEntry != null && FULLCANCEL.equals(orderCancelRecordEntry.getCancelResult().getCode()))
		{
			LOGGER.info("Full cancel for order " + orderModel.getCode());
			final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
					"sendOrderCancelledEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
					"sendOrderCancelledEmailProcess");
			orderProcessModel.setOrder(orderModel);
			getModelService().save(orderProcessModel);
			getBusinessProcessService().startProcess(orderProcessModel);
		}

	}

	/**
	 *
	 */
	protected void selectAllEntries()
	{
		this.applyToGrid(Boolean.TRUE, 0);
		final Iterator arg1 = this.getOrderEntriesGridRows().iterator();

		while (arg1.hasNext())
		{
			final Component row = (Component) arg1.next();
			final Component firstComponent = row.getChildren().iterator().next();
			if (firstComponent instanceof Checkbox)
			{
				((Checkbox) firstComponent).setChecked(this.globalCancelEntriesSelection.isChecked());
			}

			this.handleRow((Row) row);
			if (this.globalCancelEntriesSelection.isChecked())
			{
				final int cancellableQuantity = Integer.parseInt(((Label) row.getChildren().get(4)).getValue());
				this.applyToRow(Integer.valueOf(cancellableQuantity), 5, row);
			}
		}

		if (this.globalCancelEntriesSelection.isChecked())
		{
			this.orderEntriesToCancel.stream().forEach((entry) -> {
				entry.setQuantityToCancel(this.orderCancellableEntries.get(entry.getOrderEntry()));
			});
		}

	}

	/**
	 *
	 */
	protected void showMessageBox()
	{
		Messagebox.show(this.getLabel("customersupportbackoffice.cancelorder.confirm.msg"),
				this.getLabel("customersupportbackoffice.cancelorder.confirm.title") + " " + this.getOrderModel().getCode(), 48,
				"oms-widget-cancelorder-confirm-icon", (obj) -> {
					this.processCancellation(obj);
				});
	}

	/**
	 *
	 * @param stringToValidate
	 * @param indexLabelToCheck
	 * @param indexTargetComponent
	 * @return
	 */
	protected Component targetFieldToApplyValidation(final String stringToValidate, final int indexLabelToCheck,
			final int indexTargetComponent)
	{
		final Iterator arg4 = this.getOrderEntriesGridRows().iterator();

		while (arg4.hasNext())
		{
			final Component component = (Component) arg4.next();
			final Label label = (Label) component.getChildren().get(indexLabelToCheck);
			if (label.getValue().equals(stringToValidate))
			{
				return component.getChildren().get(indexTargetComponent);
			}
		}

		return null;
	}

	/**
	 *
	 * @param entry
	 */
	protected void validateOrderEntry(final OrderEntryToCancelDto entry)
	{
		InputElement reason1;
		if (entry.getQuantityToCancel().longValue() > this.orderCancellableEntries.get(entry.getOrderEntry()).longValue())
		{
			reason1 = (InputElement) this.targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(), 1, 5);
			throw new WrongValueException(reason1,
					this.getLabel("customersupportbackoffice.cancelorder.error.qtycancelled.invalid"));
		}
		else if (entry.getSelectedReason() != null && entry.getQuantityToCancel().longValue() == 0L)
		{
			reason1 = (InputElement) this.targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(), 1, 5);
			throw new WrongValueException(reason1, this.getLabel("customersupportbackoffice.cancelorder.missing.quantity"));
		}
		else if (entry.getSelectedReason() == null && entry.getQuantityToCancel().longValue() > 0L)
		{
			final Combobox reason = (Combobox) this.targetFieldToApplyValidation(entry.getOrderEntry().getProduct().getCode(), 1, 6);
			throw new WrongValueException(reason, this.getLabel("customersupportbackoffice.cancelorder.error.reason"));
		}
	}

	/**
	 *
	 */
	protected void validateRequest()
	{
		final Iterator arg1 = this.getOrderEntriesGridRows().iterator();

		while (arg1.hasNext())
		{
			final Component modelList = (Component) arg1.next();
			if (((Checkbox) modelList.getChildren().iterator().next()).isChecked())
			{
				final InputElement cancelQty = (InputElement) modelList.getChildren().get(5);
				if (cancelQty.getRawValue().equals(Integer.valueOf(0)))
				{
					throw new WrongValueException(cancelQty, this.getLabel("customersupportbackoffice.cancelorder.missing.quantity"));
				}
			}
		}

		final ListModelList modelList1 = (ListModelList) this.getOrderEntries().getModel();
		if (modelList1.stream().allMatch((entry) -> {
			return ((OrderEntryToCancelDto) entry).getQuantityToCancel().longValue() == 0L;
		}))
		{
			throw new WrongValueException(this.globalCancelEntriesSelection,
					this.getLabel("customersupportbackoffice.cancelorder.missing.selectedLine"));
		}
		else
		{
			modelList1.stream().forEach((entry) -> {
				this.validateOrderEntry((OrderEntryToCancelDto) entry);
			});
		}
	}

	protected List<Component> getOrderEntriesGridRows()
	{
		return this.getOrderEntries().getRows().getChildren();
	}

	protected Locale getLocale()
	{
		return this.getCockpitLocaleService().getCurrentLocale();
	}

	protected BackofficeLocaleService getCockpitLocaleService()
	{
		return this.cockpitLocaleService;
	}

	@Required
	public void setCockpitLocaleService(final BackofficeLocaleService cockpitLocaleService)
	{
		this.cockpitLocaleService = cockpitLocaleService;
	}

	protected Grid getOrderEntries()
	{
		return this.orderEntries;
	}

	public void setOrderEntries(final Grid orderEntries)
	{
		this.orderEntries = orderEntries;
	}

	protected OrderModel getOrderModel()
	{
		return this.orderModel;
	}

	public void setOrderModel(final OrderModel orderModel)
	{
		this.orderModel = orderModel;
	}

	protected OrderCancelService getOrderCancelService()
	{
		return this.orderCancelService;
	}

	@Required
	public void setOrderCancelService(final OrderCancelService orderCancelService)
	{
		this.orderCancelService = orderCancelService;
	}

	protected EnumerationService getEnumerationService()
	{
		return this.enumerationService;
	}

	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	protected ModelService getModelService()
	{
		return this.modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	protected CockpitEventQueue getCockpitEventQueue()
	{
		return this.cockpitEventQueue;
	}

	@Required
	public void setCockpitEventQueue(final CockpitEventQueue cockpitEventQueue)
	{
		this.cockpitEventQueue = cockpitEventQueue;
	}

	protected UserService getUserService()
	{
		return this.userService;
	}

	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	public SharjahStockService getStockService()
	{
		return stockService;
	}

	public void setStockService(final SharjahStockService stockService)
	{
		this.stockService = stockService;
	}

	/**
	 *
	 * @return
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 *
	 * @param eventService
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	public SharjahOrderExportService getSharjahOrderExportService()
	{
		return sharjahOrderExportService;
	}

	public void setSharjahOrderExportService(SharjahOrderExportService sharjahOrderExportService)
	{
		this.sharjahOrderExportService = sharjahOrderExportService;
	}

 

}