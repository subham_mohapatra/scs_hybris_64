/**
 * 
 */
package com.keyrus.ci.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;


/**
 * @author Olivier.Mortreux
 * 
 */
public class LocalizationSorter
{
	String extensionName;

	public LocalizationSorter(final String extensionName, final String extensionPath)
	{
		this.extensionName = extensionName;
		final File extensionRoot = new File(extensionPath);
		final Collection<File> propertyFiles = new TreeSet<File>();
		if (extensionRoot.exists() && extensionRoot.isDirectory())
		{
			discoverLocalesFile(extensionRoot, propertyFiles);
		}
		// Sorting and cleaning comment from property files
		for (final File propFile : propertyFiles)
		{
			System.out.println("- " + propFile + " => " + sortAndClean(propFile));
		}
	}

	private void discoverLocalesFile(final File f, final Collection<File> propertyFiles)
	{
		if (f.isDirectory())
		{
			for (final File subFile : f.listFiles())
			{
				discoverLocalesFile(subFile, propertyFiles);
			}
		}
		else if (f.getName().startsWith(extensionName + "-locales") && f.getName().endsWith(".properties"))
		{
			propertyFiles.add(f);
		}
	}

	private String sortAndClean(final File propertyFile)
	{
		String returnValue = "[CLEANED AND SORTED]";
		final Properties outProps = new Properties()
		{
			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Hashtable#keys()
			 */
			@Override
			public synchronized Enumeration<Object> keys()
			{
				return Collections.enumeration(new TreeSet<Object>(super.keySet()));
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Hashtable#keySet()
			 */
			@Override
			public Set<Object> keySet()
			{
				return new TreeSet<Object>(super.keySet());
			}
		};

		try
		{
			outProps.load(new FileInputStream(propertyFile));

			final PrintWriter writer = new PrintWriter(propertyFile, "UTF-8");
			for (final Object e : outProps.keySet())
			{
				final StringBuffer sb = new StringBuffer();
				sb.append(e.toString());
				sb.append("=");
				sb.append(outProps.get(e).toString());
				writer.println(sb.toString());
			}
			writer.close();
		}
		catch (final IOException e)
		{
			returnValue = "[ERROR] " + e.getMessage();
		}
		return returnValue;

	}

	/**
	 * @param args
	 */
	public static void main(final String[] args)
	{
		if (args != null && args.length == 2)
		{
			System.out.println("Launched LocalizationSorter on extension: " + args[0]);
			new LocalizationSorter(args[0], args[1]);
		}
		else if (args != null && args.length > 0)
		{
			System.err.println("Wrong number of argument: " + args.length);
		}
	}

}
