<?xml version="1.0" encoding="UTF-8"?>
<!--
 [y] hybris Platform

 Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.

 This software is the confidential and proprietary information of SAP
 ("Confidential Information"). You shall not disclose such Confidential
 Information and shall use it only in accordance with the terms of the
 license agreement you entered into with SAP.
-->

<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:util="http://www.springframework.org/schema/util"
	xmlns:context="http://www.springframework.org/schema/context"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

	<context:annotation-config/>

	<!-- Listeners -->
	
	<bean id="pickupConfirmationEventListener" class="com.sharjah.fulfilmentprocess.listeners.PickupConfirmationEventListener" parent="abstractEventListener">
		<property name="businessProcessService" ref="businessProcessService"/>
	</bean>
	
	<!-- Process Adapters -->

	<alias name="mockProcess2WarehouseAdapter" alias="process2WarehouseAdapter"/>
	<bean id="mockProcess2WarehouseAdapter" class="com.sharjah.fulfilmentprocess.warehouse.MockProcess2WarehouseAdapter" >
		<property name="modelService" ref="modelService"/>
		<property name="warehouse2ProcessAdapter" ref="warehouse2ProcessAdapter"/>
		<property name="timeService" ref="timeService"/>
	</bean>

	<alias name="defaultWarehouse2ProcessAdapter" alias="warehouse2ProcessAdapter"/>
	<bean id="defaultWarehouse2ProcessAdapter" class="com.sharjah.fulfilmentprocess.warehouse.DefaultWarehouse2ProcessAdapter" >
		<property name="modelService" ref="modelService"/>
		<property name="statusMap">
			<map key-type="de.hybris.platform.warehouse.WarehouseConsignmentStatus" value-type="de.hybris.platform.commerceservices.enums.WarehouseConsignmentState">
				<entry key="CANCEL" value="CANCEL"/>
				<entry key="COMPLETE" value="COMPLETE"/>
				<entry key="PARTIAL" value="PARTIAL"/>
			</map>
		</property>
		<property name="businessProcessService" ref="businessProcessService"/>
	</bean>
	
	<!-- Process Definitions -->

	<bean id="orderProcessDefinitionResource" class="de.hybris.platform.processengine.definition.ProcessDefinitionResource" >
		<property name="resource" value="classpath:/sharjahfulfilmentprocess/process/order-process.xml"/>
	</bean>

	<bean id="orderCancellationProcessDefinitionResource" class="de.hybris.platform.processengine.definition.ProcessDefinitionResource" >
		<property name="resource" value="classpath:/sharjahfulfilmentprocess/process/cancel-order-process.xml"/>
	</bean>

	<bean id="consignmentProcessDefinitionResource" class="de.hybris.platform.processengine.definition.ProcessDefinitionResource" >
		<property name="resource" value="classpath:/sharjahfulfilmentprocess/process/consignment-process.xml"/>
	</bean>

	<bean id="returnProcessDefinitionResource" class="de.hybris.platform.processengine.definition.ProcessDefinitionResource" >
		<property name="resource" value="classpath:/sharjahfulfilmentprocess/process/return-process.xml"/>
	</bean>

	<!-- Process Actions -->

	<import resource="/sharjahfulfilmentprocess/process/order-process-spring.xml"/>
	<import resource="/sharjahfulfilmentprocess/process/cancel-order-process-spring.xml"/>
	<import resource="/sharjahfulfilmentprocess/process/consignment-process-spring.xml"/>
	<import resource="/sharjahfulfilmentprocess/process/return-process-spring.xml"/>

	<!-- Order Check Service -->
	<!-- Performs some basic validation for the order as part of the first stage of the process. -->

	<alias name="defaultCheckOrderService" alias="checkOrderService"/>
	<bean id="defaultCheckOrderService" class="com.sharjah.fulfilmentprocess.impl.DefaultCheckOrderService" />

	<alias alias="fraudService" name="sharjahFraudService"/>
	<bean id="sharjahFraudService" class="de.hybris.platform.fraud.impl.DefaultFraudService">
		<property name="providers">
			<list>
				<ref bean="internalFraudServiceProvider"/>
				<ref bean="cyberSourceFraudServiceProvider"/>
			</list>
		</property>
	</bean>

	<bean id="cyberSourceFraudServiceProvider" class="de.hybris.platform.fraud.impl.mock.CommercialFraudMockService">
		<property name="providerName" value="CyberSource"/>
	</bean>

	<!-- Order Splitting -->
	<alias name="sharjahfulfilmentprocessOrderSplittingService" alias="orderSplittingService"/>
	<bean id="sharjahfulfilmentprocessOrderSplittingService" parent="defaultOrderSplittingService" >
		<property name="strategiesList">
			<list>
				<ref bean="splitByAvailableCount"/>
				<ref bean="splitByDeliveryMode" />
				<ref bean="splitByPoS" />
				<ref bean="splitByNamedDeliveryDate" />
				<ref bean="splitByEntryDeliveryAddress" />
				<ref bean="splitByWarehouse" />
			</list>
		</property>
	</bean>
	
	<!-- Split Strategies -->
	
	<alias name="defaultSplitByPoS" alias="splitByPoS"/>
	<bean id="defaultSplitByPoS" class="com.sharjah.fulfilmentprocess.strategy.impl.SplitByPoS" />
	
	<alias name="defaultSplitByEntryDeliveryAddress" alias="splitByEntryDeliveryAddress"/>
	<bean id="defaultSplitByEntryDeliveryAddress" class="com.sharjah.fulfilmentprocess.strategy.impl.SplitByEntryDeliveryAddress"  />
	
	<alias name="defaultSplitByDeliveryMode" alias="splitByDeliveryMode"/>
	<bean id="defaultSplitByDeliveryMode" class="com.sharjah.fulfilmentprocess.strategy.impl.SplitByDeliveryMode" >
		<property name="pickupDeliveryModeDao" ref="pickupDeliveryModeDao"/>
	</bean>
	
	<alias name="defaultSplitByAvailableCount" alias="splitByAvailableCount"/>
	<bean id="defaultSplitByAvailableCount" class="com.sharjah.fulfilmentprocess.strategy.impl.SplitByAvailableCount" >
		<property name="commerceStockService" ref="commerceStockService"/>
	</bean>
	
	<alias name="defaultSplitByWarehouse" alias="splitByWarehouse"/>
	<bean id="defaultSplitByWarehouse" class="com.sharjah.fulfilmentprocess.strategy.impl.SplitByWarehouse">
		<property name="stockService" ref="stockService"/>
		<property name="commerceAvailabilityCalculationStrategy" ref="commerceStockLevelCalculationStrategy"/>
	</bean>
	
	<!--  Jobs -->
	
	<bean id="cleanUpFraudOrderJob" class="com.sharjah.fulfilmentprocess.jobs.CleanUpFraudOrderJob"  >
		<property name="modelService" ref="modelService"/>
		<property name="flexibleSearchService" ref="flexibleSearchService"/>
		<property name="businessProcessService" ref="businessProcessService"/>
		<property name="sessionService" ref="sessionService"/>
	</bean>

	<!-- Cancellation -->
	<bean id="warehouseProcessingCancelRequestExecutor" class="de.hybris.platform.ordercancel.impl.executors.WarehouseProcessingCancelRequestExecutor"
		scope="prototype">
		<property name="modelService" ref="modelService" />
		<property name="orderStatusChangeStrategy" ref="enterCancellingStrategy" />
		<property name="warehouseAdapter" ref="orderCancelWarehouseAdapter" />
	</bean>

	<alias name="sharjahOrderCancelWarehouseAdapter" alias="orderCancelWarehouseAdapter"/>
	<bean id="sharjahOrderCancelWarehouseAdapter" class="com.sharjah.fulfilmentprocess.order.cancel.SharjahOrderCancelWarehouseAdapter"/>

	<bean id="sharjahPaymentServiceAdapter" class="com.sharjah.fulfilmentprocess.order.cancel.SharjahPaymentServiceAdapter" />

	<bean id="refundCalculationService" class="com.sharjah.fulfilmentprocess.order.returns.service.impl.RefundCalculationService" />

	<bean id="createReturnEventListener" class="com.sharjah.fulfilmentprocess.listeners.CreateReturnEventListener" parent="abstractSiteEventListener" />

	<util:set id="returnEventListenerSupportedSiteChannels" value-type="de.hybris.platform.commerceservices.enums.SiteChannel">
		<value>B2C</value>
		<value>B2B</value>
	</util:set>

</beans>
