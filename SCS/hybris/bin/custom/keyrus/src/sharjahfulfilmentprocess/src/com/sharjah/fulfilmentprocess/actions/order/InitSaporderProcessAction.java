package com.sharjah.fulfilmentprocess.actions.order;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.store.BaseStoreModel;


public class InitSaporderProcessAction extends AbstractSimpleDecisionAction<OrderProcessModel> {
	private static final Logger LOG = Logger.getLogger(InitSaporderProcessAction.class);

	@Resource
	private BusinessProcessService businessProcessService;

	@Override
	public Transition executeAction(final OrderProcessModel process) {
		LOG.info("Process: " + process.getCode() + " in step " + getClass());
		final OrderModel order = process.getOrder();

		try {
			initProcess(order);
			return Transition.OK;
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
		return Transition.NOK;
	}

	private void initProcess(OrderModel order) {
		final String fulfilmentProcessDefinitionName = "sap-oms-order-process";
		BaseStoreModel store = order.getStore();
		if (fulfilmentProcessDefinitionName == null || fulfilmentProcessDefinitionName.isEmpty()) {
			LOG.warn("Unable to start sap fulfilment process for order [" + order.getCode() + "]. Store ["
					+ store.getUid() + "] has missing SubmitOrderProcessCode");
		} else {
			final String processCode = fulfilmentProcessDefinitionName + "-" + order.getCode() + "-"
					+ System.currentTimeMillis();
			final OrderProcessModel businessProcessModel = businessProcessService.createProcess(processCode,
					fulfilmentProcessDefinitionName);
			businessProcessModel.setOrder(order);
			getModelService().save(businessProcessModel);
			businessProcessService.startProcess(businessProcessModel);
			if (LOG.isInfoEnabled()) {
				LOG.info(String.format("Started the process %s", processCode));
			}
		}
	}
}
