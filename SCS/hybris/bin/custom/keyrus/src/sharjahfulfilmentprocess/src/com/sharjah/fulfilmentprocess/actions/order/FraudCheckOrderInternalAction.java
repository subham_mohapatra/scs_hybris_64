/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.fulfilmentprocess.actions.order;

import de.hybris.platform.basecommerce.enums.FraudStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.fraud.FraudService;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.model.FraudReportModel;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.model.FraudScoreModel;
import com.sharjah.core.fraud.service.impl.FraudConfigService;
import com.sharjah.core.order.OrderExportType;
import com.sharjah.core.order.service.SharjahOrderExportService;
import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;
import com.sharjah.fulfilmentprocess.constants.SharjahFulfilmentProcessConstants;


public class FraudCheckOrderInternalAction extends AbstractFraudCheckAction<OrderProcessModel>
{

	private static final Logger LOG = LogManager.getLogger(FraudCheckOrderInternalAction.class);

	private static final String REVIEW_DECISION = "Order %s was marked as potential fraud by CyberSource Decision Manager";
	private static final String EXTERNAL_DESCRIPTION = "See CyberSource Decision Manager for details";
	private static final String INTERNAL_SCORE = "fraud.score.internal";

	private static final int DEFAULT_LIMIT = 500;
	private static final int DEFAULT_TOLERANCE = 5;

	private FraudService fraudService;
	private String providerName;

	@Resource(name = "sharjahOrderExportService")
	private SharjahOrderExportService orderExportService;
	@Resource
	private FraudConfigService fraudConfigService;

	protected FraudService getFraudService()
	{
		return fraudService;
	}

	@Required
	public void setFraudService(final FraudService fraudService)
	{
		this.fraudService = fraudService;
	}

	protected String getProviderName()
	{
		return providerName;
	}

	public void setProviderName(final String providerName)
	{
		this.providerName = providerName;
	}

	@Override
	public Transition executeAction(final OrderProcessModel process)//NOPMD
	{
		ServicesUtil.validateParameterNotNull(process, "Process can not be null");
		ServicesUtil.validateParameterNotNull(process.getOrder(), "Order can not be null");

		// Get the fraud-detection configuration
		final String scoreName = Config.getParameter(INTERNAL_SCORE);
		final FraudScoreModel fraudScore = fraudConfigService.findFraudScoreByName(scoreName);
		double scoreLimit;
		double scoreTolerance;
		if (fraudScore != null)
		{
			scoreLimit = fraudScore.getLimit();
			scoreTolerance = fraudScore.getTolerance();
		}
		else
		{
			scoreLimit = Config.getDouble(SharjahFulfilmentProcessConstants.EXTENSIONNAME + ".fraud.scoreLimit", DEFAULT_LIMIT);
			scoreTolerance = Config.getDouble(SharjahFulfilmentProcessConstants.EXTENSIONNAME + ".fraud.scoreTolerance", DEFAULT_TOLERANCE);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("scoreLimit: {}, scoreTolerance: {}", scoreLimit, scoreTolerance);
		}

		final OrderModel order = process.getOrder();

		if (OrderStatus.WAIT_FRAUD_MANUAL_CHECK.equals(order.getStatus()))
		{
			final String description = String.format(REVIEW_DECISION, order.getCode());
			if (LOG.isInfoEnabled())
			{
				LOG.info(description);
			}
			final String provider = CyberSourceConstants.PAYMENT_PROVIDER;
			final FraudServiceResponse response = new FraudServiceResponse(description, provider, EXTERNAL_DESCRIPTION, null);
			final FraudReportModel fraudReport = createFraudReport(provider, response, order, FraudStatus.CHECK);
			fraudReport.setExplanation(EXTERNAL_DESCRIPTION);
			final OrderHistoryEntryModel historyEntry = createHistoryLog(provider, order, FraudStatus.CHECK,
					fraudReport.getCode());
			order.setFraudulent(Boolean.FALSE);
			order.setPotentiallyFraudulent(Boolean.TRUE);
			order.setStatus(OrderStatus.FRAUD_CHECKED);
			modelService.save(fraudReport);
			modelService.save(historyEntry);
			modelService.save(order);
			return Transition.POTENTIAL;
		}

		final FraudServiceResponse response = getFraudService().recognizeOrderSymptoms(getProviderName(), order);
		final double score = response.getScore();
		if (score < scoreLimit)
		{
			final FraudReportModel fraudReport = createFraudReport(providerName, response, order, FraudStatus.OK);
			final OrderHistoryEntryModel historyEntry = createHistoryLog(providerName, order, FraudStatus.OK, null);
			order.setFraudulent(Boolean.FALSE);
			order.setPotentiallyFraudulent(Boolean.FALSE);
			order.setStatus(OrderStatus.FRAUD_CHECKED);
			modelService.save(fraudReport);
			modelService.save(historyEntry);
			modelService.save(order);
			return Transition.OK;
		}
		else if (score < scoreLimit + scoreTolerance)
		{
			final FraudReportModel fraudReport = createFraudReport(providerName, response, order, FraudStatus.CHECK);
			final OrderHistoryEntryModel historyEntry = createHistoryLog(providerName, order, FraudStatus.CHECK,
					fraudReport.getCode());
			order.setFraudulent(Boolean.FALSE);
			order.setPotentiallyFraudulent(Boolean.TRUE);
			order.setStatus(OrderStatus.FRAUD_CHECKED);
			modelService.save(fraudReport);
			modelService.save(historyEntry);
			modelService.save(order);
			return Transition.POTENTIAL;
		}
		else
		{
			final FraudReportModel fraudReport = createFraudReport(providerName, response, order, FraudStatus.FRAUD);
			final OrderHistoryEntryModel historyEntry = createHistoryLog(providerName, order, FraudStatus.FRAUD,
					fraudReport.getCode());
			order.setFraudulent(Boolean.TRUE);
			order.setPotentiallyFraudulent(Boolean.FALSE);
			order.setStatus(OrderStatus.FRAUD_CHECKED);
			modelService.save(fraudReport);
			modelService.save(historyEntry);
			modelService.save(order);
			orderExportService.exportOrder(order, OrderExportType.NEW.getCode());
			return Transition.FRAUD;
		}
	}
}
