/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.fulfilmentprocess.actions.returns;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sharjah.core.refund.enums.RefundsEnum;
import com.sharjah.core.refund.service.impl.SharjahRefundService;

import de.hybris.platform.acceleratorservices.orderprocessing.model.OrderModificationProcessModel;
import de.hybris.platform.basecommerce.enums.ReturnStatus;
import de.hybris.platform.commerceservices.event.OrderRefundEvent;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.orderprocessing.events.SendOrderPartiallyRefundedMessageEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.warehousing.returns.service.RefundAmountCalculationService;
import reactor.util.CollectionUtils;


/**
 * Implementation for refunding the money to the customer for the ReturnRequest.
 */
public class CaptureRefundAction extends AbstractSimpleDecisionAction<ReturnProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CaptureRefundAction.class);

	@Resource
	private RefundAmountCalculationService refundAmountCalculationService;
	@Resource
	private SharjahRefundService sharjahRefundService;
	@Resource
	private EventService eventService;
	@Resource
	 private BusinessProcessService businessProcessService;
	 
	@Override
	public Transition executeAction(final ReturnProcessModel process)
	{
		if (LOG.isInfoEnabled())
		{
			LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());
		}

		final ReturnRequestModel returnRequest = process.getReturnRequest();

		// Implement the logic to refund the money to the customer
		if (CollectionUtils.isEmpty(returnRequest.getOrder().getPaymentTransactions())
				||( returnRequest.getOrder().getPaymentStatus() != null
				&& PaymentStatus.NOTPAID.equals(returnRequest.getOrder().getPaymentStatus().getCode()))) {
			if (LOG.isInfoEnabled()) {
				LOG.info("Unable to refund for ReturnRequest " + returnRequest.getCode()
						+ ", no PaymentTransactions found");
			}
			updateReturnRequestStatus(returnRequest, ReturnStatus.PAYMENT_REVERSAL_FAILED);
			return Transition.NOK;
		}

		final BigDecimal customRefundAmount = refundAmountCalculationService.getCustomRefundAmount(returnRequest);
		BigDecimal amountToRefund;
		if (customRefundAmount != null && customRefundAmount.compareTo(BigDecimal.ZERO) > 0)
		{
			amountToRefund = customRefundAmount;
		}
		else
		{
			amountToRefund = refundAmountCalculationService.getOriginalRefundAmount(returnRequest);
		}
		
		sharjahRefundService.executeRefund(RefundsEnum.RETURN_BY_CS_AGENT, returnRequest.getOrder(), amountToRefund);
		
		updateReturnRequestStatus(returnRequest, ReturnStatus.PAYMENT_REVERSED);
		
		triggerRefundEmailNotication(returnRequest.getOrder(),returnRequest);
		
		return Transition.OK;
	}
	
	/**
	 * Send email of partial or total refund notification
	 * @param orderModel
	 * @param returnRequest
	 */
	private void triggerRefundEmailNotication(final OrderModel orderModel,final ReturnRequestModel returnRequest)
	{
		if(LOG.isDebugEnabled()){
		LOG.info("refundQuantity =  " + getReturnQuantity(returnRequest));
		LOG.info("orderQauntity =  " + getOrderQuantity(orderModel));
		}
		if (getReturnQuantity(returnRequest).compareTo(getOrderQuantity(orderModel)) < 0)
		{
			if(LOG.isDebugEnabled()){
			LOG.info("Partial refund for order " + orderModel.getCode());
			}
			OrderModificationProcessModel orderModificationProcessModel = new OrderModificationProcessModel();
			orderModificationProcessModel.setOrder(orderModel);
			OrderModificationRecordModel modificationRecord = orderModel.getModificationRecords().iterator().next();
				for(OrderModificationRecordEntryModel orderModificationRecordEntryModel : modificationRecord.getModificationRecordEntries()){
			        orderModificationProcessModel.setOrderModificationRecordEntry(orderModificationRecordEntryModel);
				}
			
			final SendOrderPartiallyRefundedMessageEvent event = new SendOrderPartiallyRefundedMessageEvent(orderModificationProcessModel);
			eventService.publishEvent(event);
			
		}
		else
		{
			if(LOG.isDebugEnabled()){
			 LOG.info("Full refund for order " + orderModel.getCode());
			}
			   final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
			     "sendOrderRefundEmailProcess-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
			     "sendOrderRefundEmailProcess");
			   orderProcessModel.setOrder(orderModel);
			   getModelService().save(orderProcessModel);
			   getBusinessProcessService().startProcess(orderProcessModel);
		}

	}

	/**
	 * Update the return status for all return entries in a {@link ReturnRequestModel}.
	 *
	 * @param returnRequest
	 *           - the return request
	 * @param returnStatus
	 *           - the return status
	 */
	private void updateReturnRequestStatus(final ReturnRequestModel returnRequest, final ReturnStatus returnStatus)
	{
		returnRequest.setStatus(returnStatus);
		returnRequest.getReturnEntries().stream().forEach(entry -> {
			entry.setStatus(returnStatus);
			getModelService().save(entry);
		});
		getModelService().save(returnRequest);
	}

	/**
	 * 
	 * @param returnRequest
	 * @return
	 */
	private BigDecimal getReturnQuantity(final ReturnRequestModel returnRequest)
	{
		double total = 0;
		for(final ReturnEntryModel entry: returnRequest.getReturnEntries())
		{
			total += entry.getReceivedQuantity();
		}
		return BigDecimal.valueOf(total);
	}
	/**
	 * 
	 * @param orderModel
	 * @return
	 */
	private BigDecimal getOrderQuantity(final OrderModel orderModel)
	{
		double total = 0;
		for(final AbstractOrderEntryModel entry: orderModel.getEntries())
		{
			total += entry.getQuantity();
		}
		return BigDecimal.valueOf(total);
	}
	
	/**
	 * 
	 * @return
	 */
	public EventService getEventService() {
		return eventService;
	}
	/**
	 * 
	 * @param eventService
	 */
	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	/**
	 * 
	 * @return
	 */
	 public BusinessProcessService getBusinessProcessService()
	 {
	  return businessProcessService;
	 }

	 /**
	  * 
	  * @param businessProcessService
	  */
	 public void setBusinessProcessService(BusinessProcessService businessProcessService)
	 {
	  this.businessProcessService = businessProcessService;
	 }
}
