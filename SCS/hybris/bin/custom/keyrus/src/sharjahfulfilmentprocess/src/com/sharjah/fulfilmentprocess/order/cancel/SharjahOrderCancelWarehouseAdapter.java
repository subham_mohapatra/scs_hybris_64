package com.sharjah.fulfilmentprocess.order.cancel;


import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelWarehouseAdapter;

import javax.annotation.Resource;

import com.sharjah.core.refund.enums.RefundsEnum;


/**
 * Implementation of {@link OrderCancelWarehouseAdapter}
 */
public class SharjahOrderCancelWarehouseAdapter implements OrderCancelWarehouseAdapter
{

	@Resource
	private SharjahPaymentServiceAdapter sharjahPaymentServiceAdapter;

	@Override
	public void requestOrderCancel(final OrderCancelRequest orderCancelRequest)
	{
		sharjahPaymentServiceAdapter.refundOrderCancelRequest(orderCancelRequest, RefundsEnum.CANCEL_BY_CS_AGENT);
	}
}
