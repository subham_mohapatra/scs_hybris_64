package com.sharjah.fulfilmentprocess.order.returns.service.impl;

import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.dao.OrderCancelDao;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.ordercancel.model.OrderEntryCancelRecordEntryModel;
import de.hybris.platform.ordermodify.model.OrderModificationRecordEntryModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sharjah.fulfilmentprocess.constants.SharjahFulfilmentProcessConstants;


/**
 * This class is used for calculating the amount to refund for an orderCancelRequest.
 *
 * @author Pablo François
 */
public class RefundCalculationService
{

	private static final Logger LOG = LogManager.getLogger(RefundCalculationService.class);

	@Resource
	private OrderCancelDao orderCancelDao;

	/**
	 * Calculate the total amount to refund for the orderCancelRequest given in parameter
	 *
	 * @param orderCancelRequest
	 *           the orderCancelRequest needed for calculate the total amount to refund
	 * @return the total amount to refund
	 */
	public BigDecimal calculateAmountToRefund(final OrderCancelRequest orderCancelRequest)
	{
		BigDecimal amountToRefund = BigDecimal.ZERO;
		final OrderModel order = orderCancelRequest.getOrder();

		// Calculate the cancelAmount for each orderCancelEntry
		final OrderCancelRecordEntryModel orderCancelRecordEntry = getPendingCancelRecordEntry(order);
		for (final OrderCancelEntry orderCancelEntry : orderCancelRequest.getEntriesToCancel())
		{
			final OrderEntryModel orderEntry = (OrderEntryModel) orderCancelEntry.getOrderEntry();
			final BigDecimal cancelQuantity = BigDecimal.valueOf(orderCancelEntry.getCancelQuantity());

			final BigDecimal basePrice = BigDecimal.valueOf(orderEntry.getBasePrice());

			final BigDecimal cancelAmount = basePrice.multiply(cancelQuantity);
			amountToRefund = amountToRefund.add(cancelAmount);

			if (orderCancelRecordEntry != null)
			{
				final OrderEntryCancelRecordEntryModel orderEntryCancelRecordEntry = orderCancelDao
						.getOrderEntryCancelRecord(orderEntry, orderCancelRecordEntry);
				orderEntryCancelRecordEntry.setCancelAmount(cancelAmount);
			}
		}

		// if the order is totally cancelled, recalculate the amountToRefund by taking the paymentTransactionEntry CAPTURE minus paymentTransactionEntry REFUND
		if (!orderCancelRequest.isPartialCancel() && orderCancelRequest.getOrder() != null)
		{
			final BigDecimal totalAmountAlreadyRefund = getTotalAmountAlreadyRefund(order);
			final BigDecimal totalPrice = BigDecimal.valueOf(order.getTotalPrice().doubleValue());
			amountToRefund = totalPrice.subtract(totalAmountAlreadyRefund);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("the order is totally cancelled : refund the total amount of the order " + amountToRefund);
			}
			return amountToRefund;
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("the order is partially cancelled : the amount for refund is " + amountToRefund);
		}

		return amountToRefund;
	}

	/**
	 * Get the total amount already refund for the order given in parameter. The calculation of this amount is based on
	 * the paymentTransaction and his paymentTransactionEntry.
	 *
	 * @param order
	 *           the order
	 * @return the total amount already refunded
	 */
	private BigDecimal getTotalAmountAlreadyRefund(final OrderModel order)
	{
		final BigDecimal totalAmountAlreadyRefund = BigDecimal.ZERO;
		for (final PaymentTransactionModel paymentTransaction : order.getPaymentTransactions())
		{
			for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransaction.getEntries())
			{
				getTotalAmountRefund(paymentTransactionEntry, totalAmountAlreadyRefund);
			}
		}
		return totalAmountAlreadyRefund;
	}

	/**
	 * 
	 * @param paymentTransactionEntry
	 * @param totalAmountAlreadyRefund
	 */
	private void getTotalAmountRefund(final PaymentTransactionEntryModel paymentTransactionEntry,
			BigDecimal totalAmountAlreadyRefund)
	{
		final PaymentTransactionType paymentTransactionType = paymentTransactionEntry.getType();
		if (PaymentTransactionType.REFUND_FOLLOW_ON.equals(paymentTransactionType)
				|| PaymentTransactionType.REFUND_STANDALONE.equals(paymentTransactionType))
		{
			final String transactionStatus = paymentTransactionEntry.getTransactionStatus();
			final String transactionStatusDetails = paymentTransactionEntry.getTransactionStatusDetails();
			if (TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(transactionStatus)
					&& TransactionStatusDetails.SUCCESFULL.toString().equalsIgnoreCase(transactionStatusDetails))
			{
				totalAmountAlreadyRefund = totalAmountAlreadyRefund.add(paymentTransactionEntry.getAmount());
			}
		}
	}

	/**
	 * Get the last OrderCancelRecordEntry for the order given in parameter
	 *
	 * @param order
	 *           the order
	 * @return the OrderCancelRecordEntry or null if an error occurred
	 */
	private OrderCancelRecordEntryModel getPendingCancelRecordEntry(final OrderModel order)
	{
		final OrderCancelRecordModel orderCancelRecord = this.orderCancelDao.getOrderCancelRecord(order);
		if (orderCancelRecord != null && orderCancelRecord.isInProgress())
		{
			final Collection<OrderModificationRecordEntryModel> entries = orderCancelRecord.getModificationRecordEntries();
			if (CollectionUtils.isNotEmpty(entries))
			{
				return getOrderCancelRecord(entries, order.getCode());
			}
			else
			{
				LOG.error(SharjahFulfilmentProcessConstants.ERROR_MESSAGE + order.getCode()
						+ "]: has no cancel records");
				return null;
			}
		}
		else
		{
			LOG.error(SharjahFulfilmentProcessConstants.ERROR_MESSAGE + order.getCode()
					+ "]: cancel is not currently in progress");
			return null;
		}
	}

	/**
	 * Get the order cancel record from entries with the order code and return it
	 * 
	 * @param entries
	 * @param orderCode
	 * @return
	 */
	private OrderCancelRecordEntryModel getOrderCancelRecord(final Collection<OrderModificationRecordEntryModel> entries,
			final String orderCode)
	{
		OrderCancelRecordEntryModel currentCancelEntry = null;
		final Iterator<OrderModificationRecordEntryModel> iter = entries.iterator();

		while (iter.hasNext())
		{
			final OrderCancelRecordEntryModel entry = (OrderCancelRecordEntryModel) iter.next();
			if (entry.getStatus().equals(OrderModificationEntryStatus.INPROGRESS))
			{
				if (currentCancelEntry != null)
				{
					LOG.error(SharjahFulfilmentProcessConstants.ERROR_MESSAGE + orderCode
							+ "]: cancel record has more than one entries with status \'IN PROGRESS\'");
					return null;
				}
				currentCancelEntry = entry;
			}
		}
		return currentCancelEntry;
	}
}
