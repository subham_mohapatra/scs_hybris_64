package com.sharjah.fulfilmentprocess.actions.returns;

import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import org.apache.log4j.Logger;

import com.sharjah.core.order.OrderExportType;
import com.sharjah.core.order.service.SharjahOrderExportService;


/**
 * @author Luiz Henriques
 */
public class ExportReturnOrderAction extends AbstractProceduralAction<ReturnProcessModel>
{
	private static final Logger LOG = Logger.getLogger(ExportReturnOrderAction.class);

	private SharjahOrderExportService orderExportService;

	@Override
	public void executeAction(final ReturnProcessModel process)
	{
		final ReturnRequestModel returnRequest = process.getReturnRequest();
		if (LOG.isInfoEnabled())
		{
			LOG.info("Process: " + process.getCode() + " in step " + getClass().getSimpleName());
		}

		orderExportService.exportOrder(returnRequest, OrderExportType.RETURN.getCode());
	}

	public void setOrderExportService(final SharjahOrderExportService orderExportService)
	{
		this.orderExportService = orderExportService;
	}
}
