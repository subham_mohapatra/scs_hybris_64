package com.sharjah.fulfilmentprocess.order.cancel;

import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelPaymentServiceAdapter;
import de.hybris.platform.ordercancel.OrderCancelRequest;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sharjah.core.refund.enums.RefundsEnum;
import com.sharjah.core.refund.service.impl.SharjahRefundService;
import com.sharjah.fulfilmentprocess.order.returns.service.impl.RefundCalculationService;


/**
 * Extend OrderCancelPaymentServiceAdapter and create a new method for refunding a CancelOrderRequest.
 *
 * @author Pablo François
 */
public class SharjahPaymentServiceAdapter implements OrderCancelPaymentServiceAdapter
{
	private static final Logger LOG = Logger.getLogger(SharjahPaymentServiceAdapter.class);

	@Resource
	private SharjahRefundService sharjahRefundService;
	@Resource
	private RefundCalculationService refundCalculationService;

	@Override
	public void recalculateOrderAndModifyPayments(final OrderModel order)
	{
		throw new UnsupportedOperationException(String.format("recalculateOrderAndModifyPayments by order is unsupported for %s",
				SharjahPaymentServiceAdapter.class.getSimpleName()));
	}

	/**
	 * Refund the orderCancelRequest given in parameter
	 *
	 * @param orderCancelRequest
	 *           the orderCancelRequest to refund
	 * @param refundsEnum
	 *           the refundsEnum use for create notifications if needed
	 */
	public void refundOrderCancelRequest(final OrderCancelRequest orderCancelRequest, final RefundsEnum refundsEnum)
	{
		final BigDecimal amountToRefund = refundCalculationService.calculateAmountToRefund(orderCancelRequest);
		final OrderModel order = orderCancelRequest.getOrder();
		if(order.getPaymentStatus() != null && PaymentStatus.PAID.equals(order.getPaymentStatus().getCode()))
		{
			LOG.debug("order with code : "+order.getCode()+ "has  payment status equal to PAID and it will be refunded");
		final boolean refundSuccessful = sharjahRefundService.executeRefund(refundsEnum, order, amountToRefund);
		if (refundSuccessful)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("The refund for the cancellation of the order " + order.getCode() + " was successful");
			}
		}
		else
		{
			LOG.error("An error occured during the refund of the order " + order.getCode());
		}
		}
		else
		{
			LOG.debug("order with code : "+order.getCode()+ "has no refund to do because payment status is not PAID");
		}
	}

}
