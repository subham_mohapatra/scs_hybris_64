package com.sharjah.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;

import org.apache.log4j.Logger;

import com.sharjah.core.order.OrderExportType;
import com.sharjah.core.order.service.SharjahOrderExportService;

/**
 * @author Luiz.Henriques
 */
public class ExportOrderAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(ExportOrderAction.class);

	private SharjahOrderExportService orderExportService;

	@Override
	public void executeAction(final OrderProcessModel process)
	{
		if (LOG.isInfoEnabled())
		{
			LOG.info("Process: " + process.getCode() + " in step " + getClass());
		}

		final OrderModel order = process.getOrder();
		orderExportService.exportOrder(order, OrderExportType.NEW.getCode());
	}

	public void setOrderExportService(final SharjahOrderExportService orderExportService)
	{
		this.orderExportService = orderExportService;
	}
}
