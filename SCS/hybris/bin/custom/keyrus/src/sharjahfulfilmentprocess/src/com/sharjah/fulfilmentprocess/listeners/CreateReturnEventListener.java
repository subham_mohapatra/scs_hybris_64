package com.sharjah.fulfilmentprocess.listeners;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.commerceservices.event.CreateReturnEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Listener for return creations.
 */
public class CreateReturnEventListener extends AbstractSiteEventListener<CreateReturnEvent>
{
	private static final Logger LOG = LogManager.getLogger(CreateReturnEventListener.class);

	@Resource
	private BusinessProcessService businessProcessService;
	@Resource
	private BaseStoreService baseStoreService;
	@Resource
	private ModelService modelService;
	@Resource(name = "returnEventListenerSupportedSiteChannels")
	private Set<SiteChannel> supportedSiteChannels;

	@Override
	protected void onSiteEvent(final CreateReturnEvent event)
	{
		final ReturnRequestModel returnRequest = event.getReturnRequest();
		ServicesUtil.validateParameterNotNullStandardMessage("event.returnRequest", returnRequest);

		// Try the store set on the Order first, then fallback to the session
		BaseStoreModel store = returnRequest.getOrder().getStore();
		if (store == null)
		{
			store = baseStoreService.getCurrentBaseStore();
		}

		if (store == null)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Unable to start return process for return request [" + returnRequest.getCode()
						+ "]. Store not set on Order linked to the return request and no current base store defined in session.");
			}
		}
		else
		{
			final String createReturnProcessDefinitionName = store.getCreateReturnProcessCode();
			if ((createReturnProcessDefinitionName == null || createReturnProcessDefinitionName.isEmpty()) && LOG.isInfoEnabled())
			{
				LOG.info("Unable to start return process for return request [" + returnRequest.getCode() + "]. Store ["
						+ store.getUid() + "] has missing CreateReturnProcessCode");
			}
			else
			{
				final String processCode = createReturnProcessDefinitionName + "-" + returnRequest.getCode() + "-"
						+ System.currentTimeMillis();
				final ReturnProcessModel businessProcessModel = businessProcessService.createProcess(processCode,
						createReturnProcessDefinitionName);
				businessProcessModel.setReturnRequest(returnRequest);
				modelService.save(businessProcessModel);
				businessProcessService.startProcess(businessProcessModel);
				if (LOG.isInfoEnabled())
				{
					LOG.info(String.format("Started the process %s", processCode));
				}
			}
		}
	}

	@Override
	protected boolean shouldHandleEvent(final CreateReturnEvent event)
	{
		final ReturnRequestModel returnRequest = event.getReturnRequest();
		ServicesUtil.validateParameterNotNullStandardMessage("event.return", returnRequest);
		final BaseSiteModel site = returnRequest.getOrder().getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.return.site", site);
		return supportedSiteChannels.contains(site.getChannel());
	}

}
