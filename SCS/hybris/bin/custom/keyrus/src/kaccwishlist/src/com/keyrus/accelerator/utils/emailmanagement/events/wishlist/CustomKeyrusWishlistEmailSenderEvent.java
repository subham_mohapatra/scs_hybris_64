/**
 * 
 */
package com.keyrus.accelerator.utils.emailmanagement.events.wishlist;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;




/**
 * The {@link CustomKeyrusWishlistEmailSenderEvent} that will be used by {@link CustomKeyrusEmailSenderEventListener} to
 * add new attribute for generating the new customized event.
 * 
 * @author Ghazi.Kerfahi
 * @version 0.1.0
 * @since 0.1.0
 */
public class CustomKeyrusWishlistEmailSenderEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

	private static final long serialVersionUID = 1L;

	private String receiverEmail;

	private String customerName;

	private String receiverName;

	private String printableShoppingList;

	/**
	 * 
	 */
	public CustomKeyrusWishlistEmailSenderEvent()
	{
		//empty
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(final String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(final String customerName) {
		this.customerName = customerName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(final String receiverName) {
		this.receiverName = receiverName;
	}

	public String getPrintableShoppingList() {
		return printableShoppingList;
	}

	public void setPrintableShoppingList(final String printableShoppingList) {
		this.printableShoppingList = printableShoppingList;
	}
}
