package com.keyrus.accelerator.kaccwishlist.facades;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;

import com.keyrus.accelerator.kaccwishlist.core.exceptions.KaccWishlistException;
import com.keyrus.accelerator.wishlist.data.WishlistData;
import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


/**
 * The Kacc Wishlist Facade.
 *
 * @author Mohamed.Othmani
 */
public interface KaccWishlistFacade
{
	/**
	 * Get all the wishlist datas for user.
	 *
	 * @param paramUserModel
	 *
	 * @return list of Wishlist2Datas.
	 */
	List<WishlistData> getWishlists(UserModel paramUserModel);

	/**
	 * remove wishlist entry
	 *
	 * @param code
	 *
	 * @param wishlistName
	 *
	 * @param userUid
	 *
	 * @return kacc wishlist result
	 */
	KaccWishlistResult removeWishlistEntry(String code, String wishlistName, String userUid);

	/**
	 * @param wishlistName
	 *
	 * @param userUid
	 *
	 * @return WishlistData
	 */
	WishlistData getWishListDataForCurrentUser(String wishlistName, String userUid);

	/**
	 * 
	 * @param wishlist2Model
	 * @return WishlistData
	 */
	WishlistData getWishListData(Wishlist2Model wishlist2Model);

	/**
	 * @param wishlistName
	 *
	 * @param userUid
	 *
	 * @return boolean
	 */
	KaccWishlistResult removeWishlist(String wishlistName, String userUid);

	/**
	 * @param user
	 *
	 * @param wishlistName
	 *
	 * @param description
	 *
	 * @return KaccWishlistResult
	 */
	KaccWishlistResult createEmptyWishlist(UserModel user, String wishlistName, String description);

	/**
	 * @param userUid
	 * 
	 * @param wishlistName
	 * @param newWishlistName
	 * @param description
	 * 
	 * @return KaccWishlistResult
	 */
	KaccWishlistResult renameWishlist(String userUid, String wishlistName, String newWishlistName, String description);

	/**
	 * 
	 * @param wishlist2Model
	 * @return KaccWishlistResult
	 */
	KaccWishlistResult updateWishlist(Wishlist2Model wishlist2Model);

	/**
	 * @param wishlistName
	 *
	 * @param userUid
	 *
	 * @return Wishlist2Model
	 */
	Wishlist2Model getWishListForCurrentUser(String wishlistName, String userUid);

	/**
	 * Get all the wishlists for a user, ordered by date of last modification in descending order.
	 *
	 * @param userModel
	 *           The user
	 *
	 * @return The wishlists
	 */
	List<WishlistData> getLastModifiedWishlists(UserModel userModel);

	/**
	 * Add or update an existing entry in a wishlist.
	 *
	 * @param entryData
	 *           The entry to create/update
	 * @param wishlistName
	 *           The wishlist name
	 * @param userUid
	 *           The user id
	 * @return The result message
	 * @throws KaccWishlistException
	 */
	KaccWishlistResult addWishlistEntry(WishlistEntryData entryData, String wishlistName, String userUid)
			throws KaccWishlistException;

	/**
	 * Add an entry to a new wishlist.
	 *
	 * @param entryData
	 *           The entry to create/update
	 * @param wishlistName
	 *           The wishlist name
	 * @param userId
	 *           The user id
	 * @return The result message
	 * @throws KaccWishlistException
	 */
	KaccWishlistResult addEntryToNewWishlist(WishlistEntryData entryData, String wishlistName, String userId)
			throws KaccWishlistException;

}
