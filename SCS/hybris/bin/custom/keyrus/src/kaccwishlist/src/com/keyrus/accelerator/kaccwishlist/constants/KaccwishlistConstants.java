/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.keyrus.accelerator.kaccwishlist.constants;

/**
 * Global class for all Kaccwishlist constants. You can add global constants for your extension into this class.
 */
public final class KaccwishlistConstants extends GeneratedKaccwishlistConstants
{
	/** The Constant EXTENSIONNAME. */
	public static final String EXTENSIONNAME = "kaccwishlist";

	/** The Constant DEFAULT_CONTENT_CATALOG. */
	public static final String DEFAULT_CONTENT_CATALOG = "electronics";

	/** The Constant CONTENT_TARGET_KEY. */
	public static final String CONTENT_TARGET_KEY = "kaccwishlist.target.content";

	public static final String WISHLIT_UNIQUE_EXCEPTION_MESSAGE = "kacc.wishlist.unique.error";

	public static final String WISHLIST_EXISTING_EXCEPTION_MESSAGE = "kacc.wishlist.existing.error";

	public static final String WISHLIST_SUCCESS_MESSAGE = "kacc.wishlist.add.success";

	public static final String WISHLIST_CONTAINIG_SPACE_EXCEPTION_MESSAGE = "kacc.wishlist.existing.space.error";



	private KaccwishlistConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
