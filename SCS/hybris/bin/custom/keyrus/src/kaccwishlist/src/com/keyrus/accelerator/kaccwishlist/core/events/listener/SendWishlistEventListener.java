package com.keyrus.accelerator.kaccwishlist.core.events.listener;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import com.keyrus.accelerator.kaccwishlist.core.events.SendWishlistEvent;
import com.keyrus.accelerator.kaccwishlist.model.process.SendWishlistProcessModel;


/**
 * The listener interface for receiving sendWishlistEvent events. The class that is interested in processing a
 * sendWishlistEvent event implements this interface, and the object created with that class is registered with a
 * component using the component's <code>addSendWishlistEventListener<code> method. When
 * the sendWishlistEvent event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see AbstractEvent
 * 
 * @author Fahmi.Allani
 * 
 * 
 */
public class SendWishlistEventListener extends AbstractEventListener<SendWishlistEvent>
{

	/** The site id. */
	private String siteId;
	private ModelService modelService;
	private FlexibleSearchService flexibleSearchService;

	/**
	 * Gets the business process service.
	 * 
	 * @return the business process service
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return (BusinessProcessService) Registry.getApplicationContext().getBean("businessProcessService");
	}

	/**
	 * Gets the site id.
	 * 
	 * @return the siteId
	 */
	public String getSiteId()
	{
		return siteId;
	}

	/**
	 * Sets the site id.
	 * 
	 * @param siteId
	 *           the siteId to set
	 */
	public void setSiteId(final String siteId)
	{
		this.siteId = siteId;
	}

	@Override
	protected void onEvent(final SendWishlistEvent sendWishlistEvent)
	{
		final SendWishlistProcessModel sendWishlistProcessModel = (SendWishlistProcessModel) getBusinessProcessService()
				.createProcess("sendWishlistEmailProcess" + System.currentTimeMillis(), "sendWishlistEmailProcess");
		
		final CMSSiteModel cmsSiteModel = new CMSSiteModel();
		cmsSiteModel.setUid(siteId);
		sendWishlistProcessModel.setSite(flexibleSearchService.getModelByExample(cmsSiteModel));
		modelService.save(sendWishlistProcessModel);
		getBusinessProcessService().startProcess(sendWishlistProcessModel);

	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}