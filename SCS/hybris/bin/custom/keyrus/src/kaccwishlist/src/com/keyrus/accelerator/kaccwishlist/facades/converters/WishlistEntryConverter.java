/**
 * 
 */
package com.keyrus.accelerator.kaccwishlist.facades.converters;

import de.hybris.platform.commercefacades.converter.ConfigurablePopulator;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


/**
 * @author Islam.Elghaoui
 * 
 */
public class WishlistEntryConverter implements Converter<Wishlist2EntryModel, WishlistEntryData>
{

	private ModelService modelService;
	private static final String FRENCH_DATE_FORMAT = "dd/MM/yyyy";

	@Autowired
	private Converter<ProductModel, ProductData> productConverter;

	@Autowired
	private ConfigurablePopulator<ProductModel, ProductData, ProductOption> productConfiguredPopulator;

	public List<WishlistEntryData> convertAllModel(final Collection<Wishlist2EntryModel> sources)
	{
		if (CollectionUtils.isEmpty(sources))
		{
			return Collections.emptyList();
		}

		final List<WishlistEntryData> result = new ArrayList<WishlistEntryData>(sources.size());

		for (final Wishlist2EntryModel source : sources)
		{
			if (source.getProduct() != null)
			{
				result.add(convert(source));
			}
		}
		return result;
	}


	@Override
	public WishlistEntryData convert(final Wishlist2EntryModel source) throws ConversionException
	{
		return convert(source, new WishlistEntryData());
	}

	/**
	 * Converter for Wishlist2EntryModel
	 * 
	 * @param source
	 *           : Wishlist2EntryModel
	 * 
	 * @param prototype
	 *           : WishlistEntryData
	 * 
	 * @return WishlistEntryData
	 */
	@Override
	public WishlistEntryData convert(final Wishlist2EntryModel source, final WishlistEntryData prototype)
			throws ConversionException
	{
		prototype.setAddedDate(source.getAddedDate());
		final DateFormat dateFrench = new SimpleDateFormat(FRENCH_DATE_FORMAT);
		prototype.setAddedDateFormatted(dateFrench.format(source.getAddedDate()));
		prototype.setPk(source.getPk().getLongValueAsString());
		prototype.setComment(source.getComment());
		int i = (source.getDesired() == null) ? 1 : source.getDesired().intValue();
		prototype.setDesired(i);
		i = (source.getReceived() == null) ? 0 : source.getReceived().intValue();
		prototype.setReceived(i);

		switch (source.getPriority())
		{
			case LOW:
				prototype.setPriority("1");
				break;
			case MEDIUM:
				prototype.setPriority("2");
				break;
			case HIGH:
				prototype.setPriority("3");
				break;
			default:
				prototype.setPriority("2");
				break;
		}
		if (source.getProduct() != null)
		{
			final Collection<ProductOption> options = Arrays.asList(ProductOption.BASIC, ProductOption.SUMMARY, ProductOption.PRICE,
					ProductOption.REVIEW, ProductOption.STOCK, ProductOption.GALLERY, ProductOption.PROMOTIONS);

			final ProductData productData = getProductConverter().convert(source.getProduct());
			populateThumbnailPicture(source.getProduct(), productData);
			productConfiguredPopulator.populate(source.getProduct(), productData, options);
			prototype.setProduct(productData);
		}

		if (StringUtils.isNotEmpty(source.getConfiguration()))
		{
			prototype.setConfProduct(source.getConfiguration());
		}

		return prototype;
	}

	private void populateThumbnailPicture(final ProductModel source, final ProductData target)
	{
		final Collection<ImageData> images = new ArrayList<ImageData>();

		if (source.getThumbnail() != null)
		{
			final ImageData image = new ImageData();
			image.setUrl(source.getThumbnail().getURL());
			image.setAltText(source.getThumbnail().getAltText());
			image.setFormat("thumbnail");
			images.add(image);
		}

		target.setImages(images);
	}


	/**
	 * @return the productConverter
	 */
	public Converter<ProductModel, ProductData> getProductConverter()
	{
		return productConverter;
	}


	/**
	 * @param productConverter
	 *           the productConverter to set
	 */
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}


	/**
	 * @return the modelService
	 */

	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}