package com.keyrus.accelerator.kaccwishlist.facades;

/**
 * The Class KaccWishlistResult.
 * 
 * @author mohamed.othmani
 */
public class KaccWishlistResult
{

	/** The error. */
	private boolean error;

	/** The message. */
	private String message;

	/**
	 * Instantiates a new kacc wishlist result.
	 * 
	 * @param error
	 *           the error
	 * @param message
	 *           the message
	 */
	public KaccWishlistResult(final boolean error, final String message)
	{
		this.error = error;
		this.message = message;
	}

	/**
	 * Checks if is error.
	 * 
	 * @return the error
	 */
	public boolean isError()
	{
		return error;
	}

	/**
	 * Sets the error.
	 * 
	 * @param error
	 *           the error to set
	 */
	public void setError(final boolean error)
	{
		this.error = error;
	}

	/**
	 * Gets the message.
	 * 
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * Sets the message.
	 * 
	 * @param message
	 *           the message to set
	 */
	public void setMessage(final String message)
	{
		this.message = message;
	}
}