/**
 * 
 */
package com.keyrus.accelerator.kaccwishlist.utils.comparators;

import java.util.Comparator;

import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


/**
 * @author Islam.Elghaoui
 * 
 */
public class WishListEntryComparator implements Comparator<WishlistEntryData>
{

	@Override
	public int compare(final WishlistEntryData arg0, final WishlistEntryData arg1)
	{
		return arg1.getAddedDate().compareTo(arg0.getAddedDate());
	}

}
