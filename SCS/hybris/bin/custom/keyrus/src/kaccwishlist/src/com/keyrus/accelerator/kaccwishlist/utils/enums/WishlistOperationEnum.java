/**
 *
 */
package com.keyrus.accelerator.kaccwishlist.utils.enums;

/**
 * The Enum WishlistOperationEnum.
 * 
 * @author Mohamed.Othmani
 */
public enum WishlistOperationEnum
{

	/** The new. */
	NEW,

	/** The update. */
	UPDATE,

	/** the remove entry */
	REMOVE_ENTRY;


}
