package com.keyrus.accelerator.kaccwishlist.facades;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;


/**
 * Kacc Product Facade.
 * 
 * @author Mohamed.Othmani
 */
public interface KaccProductFacade extends ProductFacade
{
	/**
	 * Get Product data
	 * 
	 * @param code
	 * 
	 * @return ProductData
	 */
	ProductData getProductForCode(String code);
}
