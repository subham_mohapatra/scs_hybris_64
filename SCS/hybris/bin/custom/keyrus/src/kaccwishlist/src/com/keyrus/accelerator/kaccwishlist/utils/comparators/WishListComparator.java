/**
 * 
 */
package com.keyrus.accelerator.kaccwishlist.utils.comparators;

import java.util.Comparator;

import com.keyrus.accelerator.wishlist.data.WishlistData;


/**
 * @author Islam.Elghaoui
 * 
 */
public class WishListComparator implements Comparator<WishlistData>
{

	@Override
	public int compare(final WishlistData arg0, final WishlistData arg1)
	{
		return arg1.getCreationTime().compareTo(arg0.getCreationTime());
	}

}
