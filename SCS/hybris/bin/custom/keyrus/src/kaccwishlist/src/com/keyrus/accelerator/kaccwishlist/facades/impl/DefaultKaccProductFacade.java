package com.keyrus.accelerator.kaccwishlist.facades.impl;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import org.springframework.beans.factory.annotation.Autowired;

import com.keyrus.accelerator.kaccwishlist.facades.KaccProductFacade;


/**
 * @author Mohamed.Othmani
 * 
 * @see KaccProductFacade
 */
public class DefaultKaccProductFacade extends DefaultProductFacade implements KaccProductFacade
{
	@Autowired
	ProductService productService;

	@Autowired
	ProductPopulator productPopulator;


	@Override
	public ProductData getProductForCode(final String code)
	{
		final ProductModel productModel = productService.getProductForCode(code);
		final ProductData data = new ProductData();
		productPopulator.populate(productModel, data);
		return data;
	}

}
