package com.keyrus.accelerator.kaccwishlist.core.events;

import de.hybris.platform.catalog.jalo.CatalogVersion;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;
import de.hybris.platform.wishlist2.model.Wishlist2Model;


/**
 * The Class SendWishlistEvent.
 * 
 * @author Fahmi.Allani
 */
public class SendWishlistEvent extends AbstractEvent
{

	/** The wish list. */
	private final Wishlist2Model wishList;

	/** The email. */
	private final String email;
	private CatalogVersion catalog;

	/**
	 * Instantiates a new send wishlist event.
	 * 
	 * @param wishList
	 *           the wish list
	 */
	public SendWishlistEvent(final Wishlist2Model wishList)
	{
		this.wishList = wishList;
		this.email = null;
	}

	/**
	 * Instantiates a new send wishlist event.
	 * 
	 * @param wishList
	 *           the wish list
	 * @param email
	 *           the email
	 */
	public SendWishlistEvent(final Wishlist2Model wishList, final String email)
	{

		this.wishList = wishList;
		this.email = email;

	}
	/**
	 * Gets the wish list.
	 * 
	 * @return the wishList
	 */
	public Wishlist2Model getWishList()
	{
		return wishList;
	}


	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}
}