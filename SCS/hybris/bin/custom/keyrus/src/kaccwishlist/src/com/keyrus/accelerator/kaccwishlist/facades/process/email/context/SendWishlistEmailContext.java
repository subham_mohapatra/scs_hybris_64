package com.keyrus.accelerator.kaccwishlist.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.model.process.StoreFrontProcessModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import com.keyrus.accelerator.kaccwishlist.model.process.SendWishlistProcessModel;


/**
 * Velocity context for a order notification email.
 * 
 * @author Fahmi.Allani
 * 
 */
public class SendWishlistEmailContext extends AbstractEmailContext<SendWishlistProcessModel>
{
	private static final String RECEIVER_NAME = "receiverName";
	private static final String CUSTOMER_NAME = "customerName";
	private static final String PRINTABLE_SHOPPING_LIST = "printableShoppingList";


	@Override
	public void init(final SendWishlistProcessModel sendWishlistProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(sendWishlistProcessModel, emailPageModel);
		put(RECEIVER_NAME, sendWishlistProcessModel.getReceiverName());
		put(EMAIL, sendWishlistProcessModel.getReceiverEmail());
		put(DISPLAY_NAME, sendWishlistProcessModel.getReceiverName());
		put(CUSTOMER_NAME, sendWishlistProcessModel.getCustomerName());
		put(PRINTABLE_SHOPPING_LIST, sendWishlistProcessModel.getPrintableShoppingList());
	}


	@Override
	protected BaseSiteModel getSite(final SendWishlistProcessModel sendWishlistProcessModel)
	{
		return ((StoreFrontProcessModel) sendWishlistProcessModel).getSite();
	}


	@Override
	protected CustomerModel getCustomer(final SendWishlistProcessModel sendWishlistProcessModel)
	{
		return (CustomerModel) sendWishlistProcessModel.getUser();
	}


	@Override
	protected LanguageModel getEmailLanguage(final SendWishlistProcessModel sendWishlistProcessModel)
	{
		return null;
	}
}