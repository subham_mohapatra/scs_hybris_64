package com.keyrus.accelerator.kaccwishlist.facades.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.keyrus.accelerator.kaccwishlist.utils.comparators.WishListComparator;
import com.keyrus.accelerator.kaccwishlist.utils.comparators.WishListEntryComparator;
import com.keyrus.accelerator.wishlist.data.WishlistData;
import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


/**
 * The converter for the Wishlist2Model.
 * 
 * @author Islam.Elghaoui
 */
public class WishlistConverter implements Converter<Wishlist2Model, WishlistData>
{
	private WishlistEntryConverter wishlistEntryConverter;

	/**
	 * Convert all Wishlist2Models which correspond to a specific User.
	 * 
	 * @param sources
	 * @return List
	 */
	public List<WishlistData> convertAllModel(final Collection<Wishlist2Model> sources)
	{
		if (CollectionUtils.isEmpty(sources))
		{
			return Collections.emptyList();
		}

		final List<WishlistData> result = new ArrayList<WishlistData>();

		for (final Wishlist2Model source : sources)
		{
			result.add(convert(source));
		}

		Collections.sort(result, new WishListComparator());
		return result;
	}


	@Override
	public WishlistData convert(final Wishlist2Model source) throws ConversionException
	{
		return convert(source, new WishlistData());
	}


	@Override
	public WishlistData convert(final Wishlist2Model source, final WishlistData target) throws ConversionException
	{
		if (source.getCreationtime() != null)
		{
			target.setCreationTime(source.getCreationtime());
		}
		if (StringUtils.isNotEmpty(source.getName()))
		{
			target.setName(source.getName());
		}
		if (source.getPk() != null && StringUtils.isNotEmpty(source.getPk().getLongValueAsString()))
		{
			target.setPk(source.getPk().getLongValueAsString());
		}
		if (StringUtils.isNotEmpty(source.getDescription()))
		{
			target.setDescription(source.getDescription());
		}

		target.setDefaultWishlist(BooleanUtils.toBoolean(source.getDefault()));

		final List<Wishlist2EntryModel> sourceEntries = source.getEntries();
		List<WishlistEntryData> entries = new ArrayList<WishlistEntryData>();
		if (CollectionUtils.isNotEmpty(sourceEntries))
		{
			entries = getWishlistEntryConverter().convertAllModel(sourceEntries);
		}

		Collections.sort(entries, new WishListEntryComparator());
		target.setEntries(entries);
		return target;
	}

	/**
	 * @return the wishlistEntryConverter
	 */
	public WishlistEntryConverter getWishlistEntryConverter()
	{
		return wishlistEntryConverter;
	}

	/**
	 * @param wishlistEntryConverter
	 *           the wishlistEntryConverter to set
	 */
	public void setWishlistEntryConverter(final WishlistEntryConverter wishlistEntryConverter)
	{
		this.wishlistEntryConverter = wishlistEntryConverter;
	}
}