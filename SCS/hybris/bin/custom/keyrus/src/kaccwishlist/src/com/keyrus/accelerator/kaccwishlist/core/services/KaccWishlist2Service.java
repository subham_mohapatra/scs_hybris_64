package com.keyrus.accelerator.kaccwishlist.core.services;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.Wishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;

import com.keyrus.accelerator.kaccwishlist.core.exceptions.KaccWishlistException;


/**
 * @author Mohamed.Othmani
 */
public interface KaccWishlist2Service extends Wishlist2Service
{
	/**
	 * Getting a WishlistModel for a user
	 *
	 * @param wishlistName
	 * @param userUid
	 * @return Wishlist2Model
	 */
	Wishlist2Model getWishlistForCode(String wishlistName, String userUid);

	/**
	 * This methode remove a specific product from a wishlist
	 *
	 * @param wishlistName
	 *           : The name and ID of the wishlist
	 *
	 * @param userUid
	 *           : The ID of the user
	 *
	 * @param productCode
	 *           : The ID of the product
	 *
	 * @throws KaccWishlistException
	 *
	 */
	void removeWishlistEntry(String productCode, String wishlistName, String userUid) throws KaccWishlistException;

	/**
	 * This methode remove a wishlist with his entries
	 *
	 * @param wishlistName
	 *           : The name and ID of the wishlist
	 *
	 * @param userUid
	 *           : The ID of the user
	 * @throws KaccWishlistException
	 *
	 */
	void removeWishlist(String wishlistName, String userUid) throws KaccWishlistException;


	/**
	 * @param user
	 *
	 * @param wishlistName
	 *
	 * @param description
	 *
	 * @throws KaccWishlistException
	 */
	public void createEmptyWishlist(final UserModel user, final String wishlistName, final String description)
			throws KaccWishlistException;

	/**
	 * Get all the wishlists for a user, ordered by date of last modification in descending order.
	 *
	 * @param userModel
	 *           The user
	 *
	 * @return The wishlists
	 */
	List<Wishlist2Model> getLastModifiedWishlists(UserModel userModel);

	/**
	 * 
	 * @param userUid
	 * @param wishlistName
	 * @param newWishlistName
	 * @throws KaccWishlistException
	 */
	public void updateWishlistName(final String userUid, final String wishlistName, final String newWishlistName)
			throws KaccWishlistException;

	/**
	 * 
	 * @param wishlist2Model
	 * @throws KaccWishlistException
	 */
	public void updateWishlist(Wishlist2Model wishlist2Model) throws KaccWishlistException;

}