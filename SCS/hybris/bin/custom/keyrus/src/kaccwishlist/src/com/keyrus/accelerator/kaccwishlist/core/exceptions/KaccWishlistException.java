package com.keyrus.accelerator.kaccwishlist.core.exceptions;

/**
 * @author Islam.Elghaoui
 * 
 */
public class KaccWishlistException extends Exception
{
	/**
	 * instanciate the exception
	 */
	public KaccWishlistException(final String message)
	{
		super(message);
	}
}
