package com.keyrus.accelerator.kaccwishlist.core.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.keyrus.accelerator.kaccwishlist.constants.KaccwishlistConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @author Mohamed.Othmani
 */
@SystemSetup(extension = KaccwishlistConstants.EXTENSIONNAME)
public class WishlistSystemSetup extends AbstractSystemSetup
{
	/** The Constant IMPORT_SAMPLE_DATA. */
	private static final String IMPORT_CORE_DATA = "importCoreData";

	/** The Constant IMPORT_SAMPLE_DATA. */
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";

	private static final String PROJECT_CONTENT_CATALOG_NAME = "scs";


	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import.
	 *
	 * @return the initialization options
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();
		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", false));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", false));
		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<>();
		final ImportData kaccImportData = new ImportData();
		kaccImportData.setContentCatalogNames(Arrays.asList(PROJECT_CONTENT_CATALOG_NAME));
		kaccImportData.setProductCatalogName(PROJECT_CONTENT_CATALOG_NAME);
		kaccImportData.setStoreNames(Arrays.asList(PROJECT_CONTENT_CATALOG_NAME));
		importData.add(kaccImportData);
		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	}

	/**
	 * Gets the configuration service.
	 *
	 * @return the configuration service
	 */
	protected ConfigurationService getConfigurationService()
	{
		return ((ConfigurationService) Registry.getApplicationContext().getBean("configurationService"));
	}

	/**
	 * @return the sampleDataImportService
	 */
	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	/**
	 * @param sampleDataImportService
	 *           the sampleDataImportService to set
	 */
	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}

	/**
	 * @return the coreDataImportService
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	/**
	 * @param coreDataImportService
	 *           the coreDataImportService to set
	 */
	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}
}