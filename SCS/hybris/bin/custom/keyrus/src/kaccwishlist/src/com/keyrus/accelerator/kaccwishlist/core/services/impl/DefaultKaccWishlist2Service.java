package com.keyrus.accelerator.kaccwishlist.core.services.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelRemovalException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.keyrus.accelerator.kaccwishlist.constants.KaccwishlistConstants;
import com.keyrus.accelerator.kaccwishlist.core.dao.KaccWishlistDao;
import com.keyrus.accelerator.kaccwishlist.core.exceptions.KaccWishlistException;
import com.keyrus.accelerator.kaccwishlist.core.services.KaccWishlist2Service;


/**
 * @author Mohamed.Othmani
 *
 * DefaultKaccWishlist2Service implement method define in KaccWishlist2Service
 */
public class DefaultKaccWishlist2Service extends DefaultWishlist2Service implements KaccWishlist2Service
{

	private static final Logger LOG = Logger.getLogger(DefaultKaccWishlist2Service.class);

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Autowired
	private ProductService productService;

	@Autowired
	ModelService modelService;

	@Resource
	private KaccWishlistDao kaccWishlistDao;

	private static final String WISHLIST_QUERY = "SELECT { " + Wishlist2Model.PK + " } FROM { " + Wishlist2Model._TYPECODE
			+ " AS w JOIN " + UserModel._TYPECODE + " AS u ON {w: " + Wishlist2Model.USER + " }={u:" + UserModel.PK + "}"
			+ " } WHERE {w:" + Wishlist2Model.NAME + " } = ?name" + "  AND {u:" + UserModel.UID + " } = ?uid";

	@Override
	public Wishlist2Model getWishlistForCode(final String wishlistName, final String userUid)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(WISHLIST_QUERY);
		query.addQueryParameter("name", wishlistName);
		query.addQueryParameter("uid", userUid);
		final List<Wishlist2Model> queryResult = flexibleSearchService.<Wishlist2Model> search(query).getResult();
		return CollectionUtils.isEmpty(queryResult) ? null : queryResult.iterator().next();
	}

	@Override
	public void createEmptyWishlist(final UserModel user, final String wishlistName, final String description)
			throws KaccWishlistException
	{

			try
			{
			if (StringUtils.isBlank(wishlistName))
			{
				throw new KaccWishlistException(
						Localization.getLocalizedString(KaccwishlistConstants.WISHLIST_CONTAINIG_SPACE_EXCEPTION_MESSAGE));
			}
			else
			{
				createWishlist(user, wishlistName, description);
				LOG.debug("Create empty WishList");
			}
			}
			catch (final ModelSavingException e)
			{
			LOG.error("error in saving wishlist with name " + wishlistName, e);
				throw new KaccWishlistException(
						Localization.getLocalizedString(KaccwishlistConstants.WISHLIST_EXISTING_EXCEPTION_MESSAGE));

			}
	}

	@Override
	public void updateWishlistName(final String userUid, final String wishlistName, final String newWishlistName)
			throws KaccWishlistException
	{
		final Wishlist2Model wishlist2Model = getWishlistForCode(wishlistName, userUid);
		try
		{
			wishlist2Model.setName(newWishlistName);
			modelService.save(wishlist2Model);
		}
		catch (final ModelSavingException e)
		{
			LOG.error("Exception updating wishlist : " + e);
			throw new KaccWishlistException(
					Localization.getLocalizedString(KaccwishlistConstants.WISHLIST_EXISTING_EXCEPTION_MESSAGE));
		}
	}

	@Override
	public void updateWishlist(final Wishlist2Model wishlist2Model) throws KaccWishlistException
	{
		try
		{
			modelService.save(wishlist2Model);
		}
		catch (final ModelSavingException e)
		{
			LOG.error("Exception updating wishlist : " + e);
			throw new KaccWishlistException(
					Localization.getLocalizedString(KaccwishlistConstants.WISHLIST_EXISTING_EXCEPTION_MESSAGE));
		}
	}

	@Override
	public void removeWishlistEntry(final String productCode, final String wishlistName, final String userUid)
			throws KaccWishlistException
	{
		try
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			final Wishlist2Model wishlist2Model = getWishlistForCode(wishlistName, userUid);
			removeWishlistEntryForProduct(productModel, wishlist2Model);
			LOG.debug("Remove WishList Entry");
		}
		catch (final ModelNotFoundException | AmbiguousIdentifierException | UnknownIdentifierException exception)
		{
			LOG.error("error removing wishlist entry :" + exception);
			throw new KaccWishlistException("error removing wishlist entry :" + exception);
		}
	}

	@Override
	public void removeWishlist(final String wishlistName, final String userUid) throws KaccWishlistException
	{
		try
		{
			final Wishlist2Model wishlist = getWishlistForCode(wishlistName, userUid);
			final List<Wishlist2EntryModel> wishlistEntries = wishlist.getEntries();
			if (CollectionUtils.isNotEmpty(wishlistEntries))
			{
				for (final Wishlist2EntryModel entry : wishlistEntries)
				{
					removeWishlistEntry(wishlist, entry);
				}
			}
			modelService.remove(wishlist);
			LOG.debug("Remove WishList");
		}
		catch (ModelNotFoundException | ModelRemovalException exception)
		{
			if(LOG.isDebugEnabled()){
			LOG.info("error when removing wishlist ",exception);
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Wishlist2Model> getLastModifiedWishlists(final UserModel userModel)
	{
		return kaccWishlistDao.findAllLastModifiedWishlists(userModel);
	}

}