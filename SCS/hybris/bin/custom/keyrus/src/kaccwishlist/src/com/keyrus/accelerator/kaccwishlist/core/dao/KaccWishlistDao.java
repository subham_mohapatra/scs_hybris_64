package com.keyrus.accelerator.kaccwishlist.core.dao;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.wishlist2.impl.daos.impl.DefaultWishlist2Dao;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.List;


/**
 * @author juanpablo.francois@keyrus.com s
 */
public class KaccWishlistDao extends DefaultWishlist2Dao
{

	private static final String PARAM_USER = "user";
	private static final String QUERY_LAST_MODIFIED = "SELECT {PK} FROM {" + Wishlist2Model._TYPECODE + "}"
			+ " WHERE {" + Wishlist2Model.USER + "} = ?" + PARAM_USER
			+ " ORDER BY {" + Wishlist2Model.MODIFIEDTIME + "} DESC";

	/**
	 * Get all the wishlists for a user, ordered by date of last modification in descending order.
	 *
	 * @param userModel
	 *           The user
	 *
	 * @return The wishlists
	 */
	public List<Wishlist2Model> findAllLastModifiedWishlists(final UserModel userModel)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_LAST_MODIFIED);
		query.addQueryParameter(PARAM_USER, userModel);
		return this.<Wishlist2Model> search(query).getResult();
	}

}
