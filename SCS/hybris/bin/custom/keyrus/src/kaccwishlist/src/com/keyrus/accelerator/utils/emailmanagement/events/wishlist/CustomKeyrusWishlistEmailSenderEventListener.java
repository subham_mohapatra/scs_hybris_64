/**
 * 
 */
package com.keyrus.accelerator.utils.emailmanagement.events.wishlist;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.keyrus.accelerator.kaccwishlist.model.process.SendWishlistProcessModel;



/**
 * Extends {@link AbstractEventListener} to add a new behavior to onEvent method.
 * 
 * @author Ghazi.Kerfahi
 * @version 0.1.0
 * @since 0.1.0
 * 
 */
public class CustomKeyrusWishlistEmailSenderEventListener extends AbstractEventListener<CustomKeyrusWishlistEmailSenderEvent>
{

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger
			.getLogger(
					com.keyrus.accelerator.utils.emailmanagement.events.wishlist.CustomKeyrusWishlistEmailSenderEventListener.class
					.getName());

	/** The business process service. */
	private BusinessProcessService businessProcessService;

	/** The configuration service. */
	private ConfigurationService configurationService;

	/** The model service. */
	private ModelService modelService;


	/**
	 * Gets the business process service.
	 * 
	 * @return the business process service
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * Gets the configuration service.
	 * 
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * Gets the model service.
	 * 
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.servicelayer.event.impl.AbstractEventListener#onEvent(de.hybris.platform.servicelayer.event
	 * .events.AbstractEvent)
	 */
	@Override
	protected void onEvent(final CustomKeyrusWishlistEmailSenderEvent wishlistEvent)
	{
		final SendWishlistProcessModel customwishlistProcess = getBusinessProcessService().createProcess(
				"sendWishlistEmailProcess" + System.currentTimeMillis(), "sendWishlistEmailProcess");
		customwishlistProcess.setSite(wishlistEvent.getSite());
		customwishlistProcess.setStore(wishlistEvent.getBaseStore());
		customwishlistProcess.setReceiverName(wishlistEvent.getReceiverName());
		customwishlistProcess.setReceiverEmail(wishlistEvent.getReceiverEmail());
		customwishlistProcess.setCustomerName(wishlistEvent.getCustomerName());
		customwishlistProcess.setPrintableShoppingList(wishlistEvent.getPrintableShoppingList());
		getModelService().save(customwishlistProcess);
		getBusinessProcessService().startProcess(customwishlistProcess);
	}

	/**
	 * Sets the business process service.
	 * 
	 * @param businessProcessService
	 *           the new business process service
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * Sets the configuration service.
	 * 
	 * @param configurationService
	 *           the configurationService to set
	 */
	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * Sets the model service.
	 * 
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
