package com.keyrus.accelerator.kaccwishlist.facades.impl;

import static com.keyrus.accelerator.kaccwishlist.constants.KaccwishlistConstants.WISHLIST_EXISTING_EXCEPTION_MESSAGE;
import static com.keyrus.accelerator.kaccwishlist.constants.KaccwishlistConstants.WISHLIST_SUCCESS_MESSAGE;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.wishlist2.enums.Wishlist2EntryPriority;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.keyrus.accelerator.kaccwishlist.core.exceptions.KaccWishlistException;
import com.keyrus.accelerator.kaccwishlist.core.services.KaccWishlist2Service;
import com.keyrus.accelerator.kaccwishlist.facades.KaccWishlistFacade;
import com.keyrus.accelerator.kaccwishlist.facades.KaccWishlistResult;
import com.keyrus.accelerator.kaccwishlist.facades.converters.WishlistConverter;
import com.keyrus.accelerator.wishlist.data.WishlistData;
import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


/**
 * @author Mohamed.Othmani
 *
 * @see KaccWishlistFacade
 */
public class DefaultKaccWishlistFacade implements KaccWishlistFacade
{

	private final static Logger LOG = Logger.getLogger(DefaultKaccWishlistFacade.class);

	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String ERROR_SIZE = "errorSize";
	@Autowired
	private KaccWishlist2Service wishlistService;

	@Autowired
	private WishlistConverter wishlistConverter;

	@Autowired
	private ModelService modelService;

	@Resource
	@Autowired
	private ProductService productService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KaccWishlistResult addWishlistEntry(final WishlistEntryData entryData, final String wishlistName, final String userId)
			throws KaccWishlistException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("entryData", entryData);
		ServicesUtil.validateParameterNotNullStandardMessage("entryData.quantity", entryData.getQuantity());
		ServicesUtil.validateParameterNotNullStandardMessage("entryData.product", entryData.getProduct());

		final Wishlist2Model wishlist2Model = wishlistService.getWishlistForCode(wishlistName, userId);
		final ProductModel productModel = productService.getProductForCode(entryData.getProduct().getCode());
		Wishlist2EntryModel entryModel;
		try
		{
			entryModel = wishlistService.getWishlistEntryForProduct(productModel, wishlist2Model);
		}
		catch (final UnknownIdentifierException e) // NOSONAR
		{
			entryModel = null;
			LOG.error("Unknowing identifient when creating WishlistEntry for wishList" + wishlist2Model.getName(), e);
		}

		final int newDesiredQty = (int) entryData.getQuantity();
		if (entryModel == null)
		{
			entryModel = modelService.create(Wishlist2EntryModel.class);
			entryModel.setProduct(productModel);
			entryModel.setPriority(Wishlist2EntryPriority.HIGH);
			entryModel.setAddedDate(new Date());
			entryModel.setDesired(Integer.valueOf(newDesiredQty));
			if (StringUtils.isNotEmpty(entryData.getConfProduct()))
			{
				entryModel.setConfiguration(entryData.getConfProduct());
			}
			modelService.save(entryModel);
			wishlistService.addWishlistEntry(wishlist2Model, entryModel);
		}
		else
		{
			final int desired = entryModel.getDesired() == null ? 0 : entryModel.getDesired().intValue();
			entryModel.setDesired(Integer.valueOf(desired + newDesiredQty));
			// Update modifiedTime so the list gets selected as the last modified one on the storefront
			wishlist2Model.setModifiedtime(new Date());
			if (StringUtils.isNotEmpty(entryData.getConfProduct()))
			{
				entryModel.setConfiguration(entryData.getConfProduct());
			}
			modelService.saveAll(entryModel, wishlist2Model);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Added wishList entry");
		}

		final String successMessage = Localization.getLocalizedString(WISHLIST_SUCCESS_MESSAGE).concat(" ").concat(wishlistName);
		return new KaccWishlistResult(false, successMessage);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KaccWishlistResult addEntryToNewWishlist(final WishlistEntryData entryData, final String wishlistName,
			final String userId)
			throws KaccWishlistException
	{
		try
		{
			wishlistService.createWishlist(wishlistName, StringUtils.EMPTY);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Created new WishList");
			}
		}
		catch (final ModelSavingException e) // NOSONAR
		{
			throw new KaccWishlistException(Localization.getLocalizedString(WISHLIST_EXISTING_EXCEPTION_MESSAGE));
		}

		return addWishlistEntry(entryData, wishlistName, userId);
	}

	@Override
	public KaccWishlistResult createEmptyWishlist(final UserModel user, final String wishlistName, final String description)
	{
		try
		{
			wishlistService.createEmptyWishlist(user, wishlistName, description);
			return new KaccWishlistResult(false, StringUtils.EMPTY);
		}
		catch (final KaccWishlistException exception)
		{
			if(LOG.isDebugEnabled()){
				LOG.info("error when creating a wishlist",exception);
			}
			return new KaccWishlistResult(true, exception.getMessage());
		}
	}

	@Override
	public KaccWishlistResult renameWishlist(final String userUid, final String wishlistName, final String newWishlistName,
			final String description)
	{
		try
		{
			wishlistService.updateWishlistName(userUid, wishlistName, newWishlistName);

			return new KaccWishlistResult(false, StringUtils.EMPTY);
		}
		catch (final KaccWishlistException exception)
		{
			if(LOG.isDebugEnabled()){
				LOG.info("error when renaming a wishlist",exception);
			}
			return new KaccWishlistResult(true, exception.getMessage());
		}
	}

	@Override
	public KaccWishlistResult updateWishlist(final Wishlist2Model wishlist2Model)
	{
		try
		{
			wishlistService.updateWishlist(wishlist2Model);

			return new KaccWishlistResult(false, StringUtils.EMPTY);
		}
		catch (final KaccWishlistException exception)
		{
			if(LOG.isDebugEnabled()){
				LOG.info("error when updating a wishlist",exception);
			}
			return new KaccWishlistResult(true, exception.getMessage());
		}
	}

	@Override
	public Wishlist2Model getWishListForCurrentUser(final String wishlistName, final String userUid)
	{
		return wishlistService.getWishlistForCode(wishlistName, userUid);
	}


	@Override
	public List<WishlistData> getWishlists(final UserModel paramUserModel)
	{
		final List<Wishlist2Model> wishlists = wishlistService.getWishlists(paramUserModel);

		if (CollectionUtils.isNotEmpty(wishlists))
		{
			return wishlistConverter.convertAllModel(wishlists);
		}

		return new ArrayList<WishlistData>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<WishlistData> getLastModifiedWishlists(final UserModel userModel)
	{
		final List<Wishlist2Model> wishlists = wishlistService.getLastModifiedWishlists(userModel);

		final List<WishlistData> datas = new ArrayList<>();
		for (final Wishlist2Model wishlist : CollectionUtils.emptyIfNull(wishlists))
		{
			// Use "convert" method because "convertAll" sorts the list and alters the order
			datas.add(wishlistConverter.convert(wishlist));
		}

		return datas;
	}


	@Override
	public KaccWishlistResult removeWishlistEntry(final String productCode, final String wishlistName, final String userUid)
	{
		KaccWishlistResult result = null;
		try
		{
			wishlistService.removeWishlistEntry(productCode, wishlistName, userUid);
			result = new KaccWishlistResult(false, StringUtils.EMPTY);
		}
		catch (final KaccWishlistException exception)
		{
			if(LOG.isDebugEnabled()){
				LOG.info("error when removing wishlistEntry",exception);
			}
			result = new KaccWishlistResult(true, exception.getMessage());
		}
		return result;
	}


	@Override
	public KaccWishlistResult removeWishlist(final String wishlistName, final String userUid)
	{
		try
		{
			wishlistService.removeWishlist(wishlistName, userUid);
		}
		catch (final KaccWishlistException e)
		{
			if(LOG.isDebugEnabled()){
				LOG.info("error when removing a wishlist",e);
			}
			return new KaccWishlistResult(true, e.getMessage());
		}
		return new KaccWishlistResult(false, StringUtils.EMPTY);
	}


	@Override
	public WishlistData getWishListData(final Wishlist2Model wishlist2Model)
	{
		return wishlistConverter.convert(wishlist2Model);
	}

	@Override
	public WishlistData getWishListDataForCurrentUser(final String wishlistName, final String userUid)
	{
		return wishlistConverter.convert(getWishListForCurrentUser(wishlistName, userUid));
	}
}