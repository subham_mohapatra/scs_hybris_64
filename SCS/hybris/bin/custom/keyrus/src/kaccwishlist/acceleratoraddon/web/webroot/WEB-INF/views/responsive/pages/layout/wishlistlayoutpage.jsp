<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

</head>

<template:page pageTitle="${pageTitle}">

	<div class="wishlist">
        <span class="title-envie">liste d'envies</span> 
        <p>Découvrez les envies  de ${nameOfUser}, et offrez lui des articles présents dans la liste ci-dessus</p>
        <div><a href="#" OnClick="javascript:window.print()">Imprimer</a></div>
        <div class="list-content">
           <c:choose>
			<c:when test="${not empty wishlistEntryData}">
			<div class="list-content">
				<c:forEach var="entry" items="${wishlistEntryData}" varStatus="status">
					<c:if test="${status.count % 2 == 1}">
						<c:set var="even" value="" />
						<c:set var="stock" value="indisponible" />
						<c:set var="classStock" value="indisponible" />
						<c:set var="btnpanier" value="disable" />
					</c:if>
					<c:if test="${status.count % 2 != 1}">
						<c:set var="even" value="even" />
						<c:set var="stock" value="En stock" />
						<c:set var="classStock" value="en-stock" />
						<c:set var="btnpanier" value="" />
					</c:if>
					<div class="item-row ${even}">
						<div class="img">
							<a href="#" title="Nom du produit"> <c:choose>
									<c:when test="${not empty entry.product.images}">
										<c:set var="image" value="${entry.product.images[0]}" />
										<c:choose>
											<c:when test="${not empty image.altText}">
												<img src="${image.url}" alt="${fn:escapeXml(image.altText)}" title="${fn:escapeXml(image.altText)}" />
											</c:when>
											<c:otherwise>
												<img src="${image.url}"
													alt="${fn:escapeXml(entry.product.name)}"
													title="${fn:escapeXml(entry.product.name)}" />
											</c:otherwise>
										</c:choose>
									</c:when>
								</c:choose>
							</a>
						</div>
						<div class="context">
							<div class="pad">
								<div class="top">
									<div class="info">
										<div class="pad">
											<span class="date"> <spring:theme code="kacc.wishlist.account.add.date"/> ${entry.addedDateFormatted }</span> 
											<span class="description">
												<a title="Nom du produit" href="#"> ${entry.product.name }</a>
											</span>											
										</div>
									</div>
									<div class="price">
											<format:fromPrice priceData="${entry.product.price}" />
									</div>
									<c:choose>
										<c:when test="${entry.product.stock.stockLevelStatus.code eq 'inStock'}">
											<div class="disponibilty disponible"> 
													<spring:theme code=""/> <spring:theme code="kacc.wishlist.account.product.instock"/>
											</div>
											<div class="action">
													<addonProduct:productAddToCartButton product="${entry.product}"/>
											</div>
										</c:when>
										<c:otherwise>
											<div class="disponibilty indisponible">
												<spring:theme code="kacc.wishlist.account.product.outstock"/>
											</div>	
											<div class="action">
												<a class="btn-primary btn-primary-round disable" href="#">
												<i class="ico ico-cart-small"></i> <spring:theme code="kacc.wishlist.account.add.wishlistEntry"/></a>
											</div>
										</c:otherwise>
									</c:choose>	
								</div>
							</div>
						</div>
						<div></div>
					</div>
				</c:forEach>
				<div class="pager">
					<script type="text/javascript">
						var enablePaginate = true;
						var paginationItemsClass = '.list-content';
						var itemsPerPage = 10;
					</script>
				 </div>
			</div>
			</c:when>
			<c:otherwise>
				<h2><spring:theme code="kacc.wishlist.account.empty.entries.message"/></h2>
			</c:otherwise>
			</c:choose>
        </div>

    </div>
</template:page>