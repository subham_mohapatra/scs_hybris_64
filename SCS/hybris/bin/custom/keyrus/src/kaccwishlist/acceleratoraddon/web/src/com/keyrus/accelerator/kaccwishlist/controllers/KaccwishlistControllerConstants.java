package com.keyrus.accelerator.kaccwishlist.controllers;

/**
 */
public interface KaccwishlistControllerConstants
{

	final String ADDON_PREFIX = "addon:/kaccwishlist/";

	interface Views
	{

		interface Pages
		{

			interface Account
			{
				String AccountVisitorWishListProductPage = ADDON_PREFIX + "pages/account/accountvisitorwishlistproductpage";
			}

			interface Guest
			{
				String GuestWishListPage = ADDON_PREFIX + "pages/guest/guestWishListPage";
			}


			interface Layout
			{
				String WishListPageLayout = ADDON_PREFIX + "pages/layout/wishlistlayoutpage";

			}

			interface Popins
			{
				String ShareWishlistPopin = ADDON_PREFIX + "popins/shareWishlistPopin";
				String AddProductWishListPopup = ADDON_PREFIX + "popins/addProductWishlistPopup";
				String AddWishListPopup = ADDON_PREFIX + "popins/addWishListPopup";
				String RenameWishListPopup = ADDON_PREFIX + "popins/renameWishListPopup";
			}
		}

		interface Fragments
		{
			interface product
			{
				String AddProductWishListResult = ADDON_PREFIX + "fragments/product/addProductWishListResult";
				String RemoveWishlistEntry = ADDON_PREFIX + "fragments/product/removeWishlistEntry";
			}

			interface wishlist
			{
				String SendMailWishlist = ADDON_PREFIX + "fragments/wishlist/sendEmailValidation";
			}
		}

		interface Cms
		{
			String AccountWishListPage = ADDON_PREFIX + "cms/accountwishlistpage";
		}
	}
}