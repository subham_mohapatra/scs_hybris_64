package com.keyrus.accelerator.kaccwishlist.controllers.cms;

import de.hybris.platform.addonsupport.renderer.impl.DefaultAddOnCMSComponentRenderer;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang3.StringUtils;


/**
 * Add to wishlist component Render
 * 
 * @author Islam.Elghaoui
 */
public class AccountViewWishListComponentRender<C extends SimpleCMSComponentModel> extends DefaultAddOnCMSComponentRenderer<C>

{
	private final String EXP_LEVEL = "uilevel";
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	//
	@Override
	public void renderComponent(final PageContext pageContext, final C component) throws ServletException, IOException
	{
		final Map<String, Object> exposedVariables = exposeVariables(pageContext, component);
		pageContext.include(getView(component));
		unExposeVariables(pageContext, component, exposedVariables);
	}


	@Override
	protected String getView(final C component)
	{
		final String exp_level = configurationService.getConfiguration().getString(EXP_LEVEL);
		if (StringUtils.isNotEmpty(exp_level))
		{
			if ("responsive".equals(exp_level))
			{
			return "/WEB-INF/views/addons/" + getAddonUiExtensionName(component) + "/" + exp_level + "/" + getCmsComponentFolder()
					+ "/" + getViewResourceName(component) + ".jsp";
			}
			else
			{
				return getViewPath(component);
			}
	   }
		else
		{
			return getViewPath(component);
		}

	}
	
	/**
	 * 
	 * get the view path
	 * 
	 * @param component
	 * @return ViewPath
	 */
	private String getViewPath(final C component)
	{
		return "/WEB-INF/views/addons/" + getAddonUiExtensionName(component) + "/" + getUIExperienceFolder() + "/"
				+ getCmsComponentFolder() + "/" + getViewResourceName(component) + ".jsp";
	}

}
