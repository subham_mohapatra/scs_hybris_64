package com.keyrus.accelerator.kaccwishlist.core.events.listener;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.keyrus.accelerator.kaccwishlist.core.events.SendWishlistEvent;


/**
 * Class Test for SendWishlistEventListener
 * 
 */

@UnitTest
public class SendWishlistEventListenerTest
{
	@InjectMocks
	private final SendWishlistEventListener sendWishlistEventListener = new SendWishlistEventListener();

	@Mock
	private FlexibleSearchService flexibleSearchService;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void onEventTest()
	{
		final SendWishlistEventListener sendEventListener = Mockito.mock(SendWishlistEventListener.class);
		final SendWishlistEvent sendWishlistEvent = Mockito.mock(SendWishlistEvent.class);
		Mockito.doNothing().when(sendEventListener).onEvent(sendWishlistEvent);
	}
}
