package com.keyrus.accelerator.kaccwishlist.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Class Test for KaccCustomerPopulator
 * 
 */

@UnitTest
public class KaccCustomerPopulatorTest
{
	private static final String CONTACT_EMAIL = "email";

	@InjectMocks
	private final KaccCustomerPopulator kaccCustomerPopulator = new KaccCustomerPopulator();


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void populateTest()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		Mockito.when(customerModel.getContactEmail()).thenReturn(CONTACT_EMAIL);
		final CustomerData productData = new CustomerData();
		kaccCustomerPopulator.populate(customerModel, productData);
		Assert.assertEquals("test ContactEmail", CONTACT_EMAIL, productData.getContactEmail());
	}
}
