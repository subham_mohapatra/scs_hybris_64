package com.keyrus.accelerator.kaccwishlist.core.services.impl.test;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.keyrus.accelerator.kaccwishlist.core.exceptions.KaccWishlistException;
import com.keyrus.accelerator.kaccwishlist.core.services.KaccWishlist2Service;
import com.keyrus.accelerator.kaccwishlist.core.services.impl.DefaultKaccWishlist2Service;


/**
 * Class Test for DefaultKaccWishlist2Service
 *
 * @author Islam.Elghaoui
 */

@UnitTest
public class DefaultKaccWishlist2ServiceTest
{
	@InjectMocks
	private final DefaultKaccWishlist2Service defaultKaccWishlist2Service = new DefaultKaccWishlist2Service();

	@Mock
	private DefaultWishlist2Service defaultWishlist2Service;

	@Mock
	private KaccWishlist2Service kaccWishlist2Service;

	@Mock
	private Wishlist2Model wishlist2Model;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Mock
	private UserModel user;

	@Mock
	private ModelService modelService;

	@Mock
	private ProductService productService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

	}


	@Test
	public void getWishlistForCodeTest()
	{
		final List<Wishlist2Model> listWishlistModel = new ArrayList<>();
		listWishlistModel.add(new Wishlist2Model());

		final SearchResult<Wishlist2Model> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(searchResult.getResult()).thenReturn(listWishlistModel);
		Mockito.when(flexibleSearchService.<Wishlist2Model> search(Mockito.any(FlexibleSearchQuery.class)))
				.thenReturn(searchResult);
		Assert.assertNotNull(searchResult);
	}

	@Test
	public void removeWishlistEntryTest() throws KaccWishlistException
	{
		final DefaultKaccWishlist2Service kaccWishlist2Service = Mockito.mock(DefaultKaccWishlist2Service.class);
		final ProductModel product = Mockito.mock(ProductModel.class);
		Mockito.when(productService.getProductForCode(Mockito.anyString())).thenReturn(product);
		final Wishlist2Model wishlist2Model = Mockito.mock(Wishlist2Model.class);
		Mockito.when(kaccWishlist2Service.getWishlistForCode(Mockito.anyString(), Mockito.anyString())).thenReturn(wishlist2Model);
		Mockito.doThrow(new KaccWishlistException("Subscription not found.")).when(kaccWishlist2Service)
				.removeWishlistEntry(Mockito.anyString(), Mockito.anyString(), Mockito.anyString());
	}


	@Test
	public void removeWishlistTest() throws KaccWishlistException
	{
		final Wishlist2Model wishlist2Model = Mockito.mock(Wishlist2Model.class);
		final DefaultKaccWishlist2Service kaccWishlist2Service = Mockito.mock(DefaultKaccWishlist2Service.class);
		Mockito.when(kaccWishlist2Service.getWishlistForCode(Mockito.anyString(), Mockito.anyString())).thenReturn(wishlist2Model);
		final Wishlist2EntryModel wishlist2EntryModel = Mockito.mock(Wishlist2EntryModel.class);
		final List<Wishlist2EntryModel> wishlistEntries = new ArrayList<>();
		wishlistEntries.add(wishlist2EntryModel);
		Mockito.doThrow(new KaccWishlistException("Subscription not found.")).when(kaccWishlist2Service)
				.removeWishlist(Mockito.anyString(), Mockito.anyString());
		Assert.assertNotNull(wishlistEntries);
	}
}
