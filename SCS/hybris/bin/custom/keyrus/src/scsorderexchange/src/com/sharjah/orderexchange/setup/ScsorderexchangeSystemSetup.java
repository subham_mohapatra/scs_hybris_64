/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.orderexchange.setup;

import static com.sharjah.orderexchange.constants.ScsorderexchangeColumns.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.sharjah.orderexchange.constants.ScsorderexchangeColumns;
import com.sharjah.orderexchange.service.ScsorderexchangeService;


@SystemSetup(extension = ScsorderexchangeColumns.EXTENSIONNAME)
public class ScsorderexchangeSystemSetup
{
	private final ScsorderexchangeService scsorderexchangeService;

	public ScsorderexchangeSystemSetup(final ScsorderexchangeService scsorderexchangeService)
	{
		this.scsorderexchangeService = scsorderexchangeService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		scsorderexchangeService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return ScsorderexchangeSystemSetup.class.getResourceAsStream("/scsorderexchange/sap-hybris-platform.png");
	}
}
