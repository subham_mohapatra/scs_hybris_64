/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sharjah.orderexchange.outbound.impl;

import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
// import de.hybris.platform.sap.saporderexchangeoms.outbound.SapVendorService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultOrderEntryContributor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.sharjah.orderexchange.constants.ScsOrderEntryCsvColumns;
import com.sharjah.orderexchange.constants.ScsorderexchangeColumns;


/**
 *
 */
public class ScsOrderEntryContributer extends DefaultOrderEntryContributor
{

	private final static Logger LOG = Logger.getLogger(ScsOrderEntryContributer.class);

	//	private SapVendorService sapVendorService;

	@Override
	public Set<String> getColumns()
	{
		final Set<String> columns = super.getColumns();
		columns.addAll(Arrays.asList(ScsOrderEntryCsvColumns.WAREHOUSE, ScsOrderEntryCsvColumns.EXPECTED_SHIPPING_DATE,
				ScsOrderEntryCsvColumns.ITEM_CATEGORY, ScsOrderEntryCsvColumns.EAN_UPC));
		return columns;
	}

	@Override
	public List<Map<String, Object>> createRows(final OrderModel order)
	{

		final List<Map<String, Object>> result = new ArrayList<>();
		if (order.getConsignments() != null && order.getConsignments().size() > 0)
		{
			for (final ConsignmentModel consignment : order.getConsignments())
			{
				for (final ConsignmentEntryModel consignmentEntry : consignment.getConsignmentEntries())
				{
					final Map<String, Object> row = new HashMap<>();

					row.put(ScsorderexchangeColumns.ORDER_ID, order.getCode());
					row.put(ScsOrderEntryCsvColumns.ENTRY_NUMBER, Integer.valueOf(consignmentEntry.getSapOrderEntryRowNumber() - 1));
					row.put(ScsOrderEntryCsvColumns.QUANTITY, consignmentEntry.getQuantity());
					row.put(ScsOrderEntryCsvColumns.PRODUCT_CODE, consignmentEntry.getOrderEntry().getProduct().getCode());
					row.put(ScsOrderEntryCsvColumns.WAREHOUSE, consignmentEntry.getConsignment().getWarehouse().getCode());
					row.put(ScsOrderEntryCsvColumns.EXPECTED_SHIPPING_DATE, consignmentEntry.getConsignment().getShippingDate());
					row.put(ScsOrderEntryCsvColumns.EXTERNAL_PRODUCT_CONFIGURATION,
							getProductConfigurationData(consignmentEntry.getOrderEntry()));

					row.put(ScsOrderEntryCsvColumns.EAN_UPC, consignmentEntry.getOrderEntry().getProduct().getEan());

					System.out.println("###### ScsOrderEntryContributer Consignment EAN : "
							+ consignmentEntry.getOrderEntry().getProduct().getEan());

					final UnitModel unit = consignmentEntry.getOrderEntry().getUnit();
					if (unit != null)
					{
						row.put(ScsOrderEntryCsvColumns.ENTRY_UNIT_CODE, unit.getCode());
					}
					else
					{
						LOG.warn(String.format("Could not determine unit code for product %s as entry %d of order %s",
								consignmentEntry.getOrderEntry().getProduct().getCode(),
								consignmentEntry.getOrderEntry().getEntryNumber(), order.getCode()));
					}

					String shortText = determineItemShortText(consignmentEntry.getOrderEntry(), order.getLanguage().getIsocode());

					if (shortText.isEmpty())
					{
						final List<LanguageModel> fallbackLanguages = order.getLanguage().getFallbackLanguages();
						if (!fallbackLanguages.isEmpty())
						{
							shortText = determineItemShortText(consignmentEntry.getOrderEntry(), fallbackLanguages.get(0).getIsocode());
						}
					}

					row.put(ScsOrderEntryCsvColumns.PRODUCT_NAME, shortText);

					/*
					 * if
					 * (getSapVendorService().isVendorExternal(consignmentEntry.getConsignment().getWarehouse().getVendor().getCode()))
					 * { row.put(ScsOrderEntryCsvColumns.ITEM_CATEGORY, getSapVendorService().getVendorItemCategory()); }
					 */

					result.add(row);

				}
			}
		}
		else
		{
			final List<AbstractOrderEntryModel> entries = order.getEntries();

			for (final AbstractOrderEntryModel entry : entries)
			{
				final Map<String, Object> row = new HashMap<>();
				row.put(ScsorderexchangeColumns.ORDER_ID, order.getCode());
				row.put(ScsOrderEntryCsvColumns.ENTRY_NUMBER, entry.getEntryNumber());
				row.put(ScsOrderEntryCsvColumns.QUANTITY, entry.getQuantity());
				row.put(ScsOrderEntryCsvColumns.PRODUCT_CODE, entry.getProduct().getCode());
				row.put(ScsOrderEntryCsvColumns.EAN_UPC, entry.getProduct().getEan());

				System.out.println("###### ScsOrderEntryContributer Entries EAN : " + entry.getProduct().getEan());
				final UnitModel unit = entry.getUnit();
				if (unit != null)
				{
					row.put(ScsOrderEntryCsvColumns.ENTRY_UNIT_CODE, unit.getCode());
				}
				else
				{
					LOG.warn("Could not determine unit code for product " + entry.getProduct().getCode() + "as entry "
							+ entry.getEntryNumber() + "of order " + order.getCode());
				}
				row.put(ScsOrderEntryCsvColumns.EXTERNAL_PRODUCT_CONFIGURATION, getProductConfigurationData(entry));
				String language = order.getLanguage().getIsocode();
				String shortText = determineItemShortText(entry, language);

				if (shortText.isEmpty())
				{
					final List<LanguageModel> fallbackLanguages = order.getLanguage().getFallbackLanguages();
					if (!fallbackLanguages.isEmpty())
					{
						language = fallbackLanguages.get(0).getIsocode();
						shortText = determineItemShortText(entry, language);
					}
				}
				row.put(ScsOrderEntryCsvColumns.PRODUCT_NAME, shortText);

				result.add(row);
			}
		}
		return result;

	}


	/*
	 * protected SapVendorService getSapVendorService() { return sapVendorService; }
	 *
	 * @Required public void setSapVendorService(final SapVendorService sapVendorService) { this.sapVendorService =
	 * sapVendorService; }
	 */


}
