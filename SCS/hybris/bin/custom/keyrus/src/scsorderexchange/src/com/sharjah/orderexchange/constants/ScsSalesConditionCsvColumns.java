/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sharjah.orderexchange.constants;

/**
 *
 */
public class ScsSalesConditionCsvColumns
{

	@SuppressWarnings("javadoc")
	public static final String CONDITION_COUNTER = "conditionCounter";

	@SuppressWarnings("javadoc")
	public static final String CONDITION_PRICE_QUANTITY = "conditionPriceQuantity";

	@SuppressWarnings("javadoc")
	public static final String CONDITION_UNIT_CODE = "conditionUnitCode";

	@SuppressWarnings("javadoc")
	public static final String ABSOLUTE = "absolute";

	@SuppressWarnings("javadoc")
	public static final String CONDITION_VALUE = "conditionValue";

	@SuppressWarnings("javadoc")
	public static final String CONDITION_CURRENCY_ISO_CODE = "conditionCurrencyIsoCode";

	@SuppressWarnings("javadoc")
	public static final String CONDITION_CODE = "conditionCode";

	@SuppressWarnings("javadoc")
	public static final String CONDITION_ENTRY_NUMBER = "conditionEntryNumber";

}
