/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.orderexchange.constants;

/**
 * Global class for all Scsorderexchange constants. You can add global constants for your extension into this class.
 */
public final class ScsorderexchangeColumns
{
	public static final String EXTENSIONNAME = "scsorderexchange";
	public static final String PLATFORM_LOGO_CODE = "scsorderexchangePlatformLogo";
	//	order
	@SuppressWarnings("javadoc")
	public static final String DATE = "date";

	@SuppressWarnings("javadoc")
	public static final String DELIVERY_MODE = "deliveryMode";

	@SuppressWarnings("javadoc")
	public static final String PAYMENT_MODE = "paymentMode";

	@SuppressWarnings("javadoc")
	public static final String ORDER_CURRENCY_ISO_CODE = "orderCurrencyIsoCode";

	@SuppressWarnings("javadoc")
	public static final String LOGICAL_SYSTEM = "logicalSystem";

	@SuppressWarnings("javadoc")
	public static final String SALES_ORGANIZATION = "salesOrganization";

	@SuppressWarnings("javadoc")
	public static final String DISTRIBUTION_CHANNEL = "distributionChannel";

	@SuppressWarnings("javadoc")
	public static final String DIVISION = "division";


	//	general columns
	@SuppressWarnings("javadoc")
	public static final String ORDER_ID = "orderId";

	@SuppressWarnings("javadoc")
	public static final String PURCHASE_ORDER_NUMBER = "purchaseOrderNumber";

	@SuppressWarnings("javadoc")
	public static final String BASE_STORE = "baseStore";

	@SuppressWarnings("javadoc")
	public static final String CHANNEL = "channel";

	@SuppressWarnings("javadoc")
	public static final String REQ_DATE_H = "requestedDate";

	@SuppressWarnings("javadoc")
	public static final String PURCH_NO_S = "deliverySlot";

	@SuppressWarnings("javadoc")
	public static final String DOC_TYPE = "docType";

	@SuppressWarnings("javadoc")
	public static final String DEFAULT_MESSAGE_TYPE = "SALESORDER_CREATEFROMDAT2";

	@SuppressWarnings("javadoc")
	public static final String TRUE = Boolean.TRUE.toString();

	@SuppressWarnings("javadoc")
	public static final String FALSE = Boolean.FALSE.toString();

	@SuppressWarnings("javadoc")
	public static final String HEADER_ENTRY = "-1";

	//partner
	@SuppressWarnings("javadoc")
	public static final String ADDRESS_ONE = "1";

	@SuppressWarnings("javadoc")
	public static final String ADDRESS_TWO = "2";

}
