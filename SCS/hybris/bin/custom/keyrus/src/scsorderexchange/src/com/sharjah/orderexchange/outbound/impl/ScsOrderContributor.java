package com.sharjah.orderexchange.outbound.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.sap.orderexchange.outbound.impl.DefaultOrderContributor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sharjah.orderexchange.constants.ScsorderexchangeColumns;


public class ScsOrderContributor extends DefaultOrderContributor
{


	@Override
	public Set<String> getColumns()
	{
		final Set<String> columns = super.getColumns();
		columns.addAll(Arrays.asList(ScsorderexchangeColumns.LOGICAL_SYSTEM, ScsorderexchangeColumns.SALES_ORGANIZATION,
				ScsorderexchangeColumns.DISTRIBUTION_CHANNEL, ScsorderexchangeColumns.DIVISION, ScsorderexchangeColumns.REQ_DATE_H,
				ScsorderexchangeColumns.PURCH_NO_S, ScsorderexchangeColumns.DOC_TYPE));
		return columns;
	}

	@Override
	public List<Map<String, Object>> createRows(final OrderModel order)
	{

		final Map<String, Object> row = new HashMap<>();

		row.put(ScsorderexchangeColumns.ORDER_ID, order.getCode());
		row.put(ScsorderexchangeColumns.DATE, order.getDate());
		row.put(ScsorderexchangeColumns.ORDER_CURRENCY_ISO_CODE, order.getCurrency().getIsocode());
		row.put(ScsorderexchangeColumns.BASE_STORE, order.getStore().getUid());

		final DeliveryModeModel deliveryMode = order.getDeliveryMode();
		row.put(ScsorderexchangeColumns.DELIVERY_MODE, deliveryMode != null ? deliveryMode.getCode() : "");

		row.put(ScsorderexchangeColumns.LOGICAL_SYSTEM, "");

		final String orderStatus = order.getStatus().toString();

		System.out.println("### Order status : " + orderStatus);

		if (orderStatus != null)
		{
			if (orderStatus.toLowerCase().contains("cancel"))
			{
				row.put(ScsorderexchangeColumns.DOC_TYPE, "ZERE");
			}
			else
			{
				row.put(ScsorderexchangeColumns.DOC_TYPE, "ZEOR");
			}
		}

		String paymentMode = "";
		if (order != null)
		{
			final String paymentModeName = order.getPaymentMode().getName();
			if (paymentModeName != null)
			{
				if (paymentModeName.equalsIgnoreCase("Cash On Delivery"))
				{
					paymentMode = "7";
				}
				else if (paymentModeName.equalsIgnoreCase("Credit Card On Delivery"))
				{
					paymentMode = "8";
				}
				else
				{
					paymentMode = "9";
				}
			}
		}

		row.put(ScsorderexchangeColumns.PAYMENT_MODE, paymentMode);
		row.put(ScsorderexchangeColumns.REQ_DATE_H,
				(order.getSelectedDeliverySlot() != null) ? order.getSelectedDeliverySlot().getDate() : "");
		if (order.getSelectedDeliverySlot() != null)
		{
			String beginTime = order.getSelectedDeliverySlot().getDeliverySlot().getBeginTime();
			if (beginTime != null)
			{
				if (beginTime.contains("9") || beginTime.contains("11"))
				{
					beginTime = beginTime + " am";
				}
				else
				{
					beginTime = beginTime + " pm";
				}
			}
			String endTime = order.getSelectedDeliverySlot().getDeliverySlot().getEndTime();
			if (endTime != null)
			{
				if (endTime.contains("11"))
				{
					endTime = endTime + " am";
				}
				else
				{
					endTime = endTime + " pm";
				}
			}
			row.put(ScsorderexchangeColumns.PURCH_NO_S, beginTime + " to " + endTime);
		}
		System.out.println("### SCS Order exchange : PAYMENT_MODE : " + paymentMode);
		System.out.println("### SCS Order exchange : REQ_DATE_H : "
				+ ((order.getSelectedDeliverySlot() != null) ? order.getSelectedDeliverySlot().getDate() : ""));
		System.out.println("### SCS Order exchange : PURCH_NO_S : "
				+ ((order.getSelectedDeliverySlot() != null) ? order.getSelectedDeliverySlot().getDeliverySlot().getBeginTime()
						+ " to " + order.getSelectedDeliverySlot().getDeliverySlot().getEndTime() : ""));
		return Arrays.asList(row);

	}


}
