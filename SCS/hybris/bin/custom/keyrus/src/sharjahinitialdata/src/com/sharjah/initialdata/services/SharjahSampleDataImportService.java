package com.sharjah.initialdata.services;

import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;


/**
 * The sample data import service extending {@link SampleDataImportService}
 */
public class SharjahSampleDataImportService extends SampleDataImportService {

	@Override
	protected void importContentCatalog(final String extensionName, final String contentCatalogName) {
		super.importContentCatalog(extensionName, contentCatalogName);

		// Load customer nationalities
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/stores/scs/nationality.impex", extensionName, contentCatalogName),
				false);
	}
}
