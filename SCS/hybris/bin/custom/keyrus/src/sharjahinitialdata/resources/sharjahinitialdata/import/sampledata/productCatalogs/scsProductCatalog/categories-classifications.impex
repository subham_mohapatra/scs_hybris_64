# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# ImpEx for Importing Category Classifications into scs Store

# Macros / Replacement Parameter definitions
$version=Staged
#$version=Online
$productCatalog=scsProductCatalog
$productCatalogName=scs Product Catalog
$catalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default=$version])[unique=true,default=$productCatalog:$version]
$classCatalogVersion=catalogversion(catalog(id[default='scsClassification']),version[default='1.0'])[unique=true,default='scsClassification:1.0']
$classSystemVersion=systemVersion(catalog(id[default='scsClassification']),version[default='1.0'])[unique=true]
$attributeValue=value(code, $classSystemVersion)[unique = true, forceWrite = true, allownull = true]
$class=classificationClass(ClassificationClass.code,$classCatalogVersion)[unique=true]
$supercategories=source(code, $classCatalogVersion)[unique=true]
$categories=target(code, $catalogVersion)[unique=true]
$products=target(code, $productCV)[unique=true]
$attribute=classificationAttribute(code,$classSystemVersion)[unique=true]
$unit=unit(code,$classSystemVersion)
$class2=classificationClass(code)[unique=true]
$classAttribute2 = classificationAttribute(code)[unique = true]
$attributeAssignment = attributeAssignment($classAttribute2, $class2, $classSystemVersion)[unique = true, forceWrite = true]


# BEGIN SCS categories-classifications
INSERT_UPDATE ClassificationClass ; $classCatalogVersion ; code[unique=true] ; allowedPrincipals(uid)[default='customergroup']
                                  ;                      ; nutrition         ;                                                
                                  ;                      ; informations      ;
                                  ;						 ; configuration	  ;
                                  	 
INSERT_UPDATE ClassificationAttribute ; $classSystemVersion ; code[unique=true]
                                      ;                     ; energy    
                                      ;                     ; fat              
                                      ;                     ; saturates        
                                      ;                     ; sugar            
                                      ;                     ; salt          
									  
                                      ;                     ; weight           
                                      ;                     ; countryoforigin              
                                      ;                     ; ingredients        
                                      ;                     ; storage
                                      
                                      ;                     ; cut
                                      ;                     ; pack            

INSERT_UPDATE ClassAttributeAssignment ; $class    ; $attribute ; position[unique=true] ; $unit ; attributeType(code[default=string]) ; multiValued[default=false] ; range[default=false] ; localized[default=true]
                                       ; nutrition ; energy     			; 1                     ;    ; string                             ;true                            ;                      ;                        
                                       ; nutrition ; fat        			; 2                     ;    ; string                             ;true                            ;                      ;                        
                                       ; nutrition ; saturates  			; 3                     ;    ; string                             ;true                            ;                      ;                        
                                       ; nutrition ; sugar      			; 4                     ;    ; string                             ;true                            ;                      ;                        
                                       ; nutrition ; salt       			; 5                     ;    ; string                             ;true                            ;                      ;
 
 
                                       ; informations ; weight     		; 1                     ; 38  ; string                              ;                            ;                      ;                        
                                       ; informations ; countryoforigin ; 2                     ;     ; string                              ;                            ;                      ;                        
                                       ; informations ; ingredients  	; 3                     ;     ; string                              ;                            ;                      ;                        
                                       ; informations ; storage      	; 4                     ;     ; string                              ;                            ;                      ;
                                       
                                       ; configuration; cut		      	; 1                     ;     ; enum     	                         ;   true                    ;                      ;
                                       ; configuration; pack	      	; 2                     ;     ; enum     	                         ;   true                    ;                      ;                        
                                                          
INSERT_UPDATE ClassificationAttributeValue; code[unique = true, forceWrite = true, allownull = false]; externalID; $classSystemVersion
; "NO_CUT"	; ;
; "CUT_2" 	; ;
; "CUT_4" 	; ;
; "CUT_8" 	; ;
; "PACK_X8"	; ; 
; "PACK_X16"; ;
; "PACK_X24"; ;	

INSERT_UPDATE AttributeValueAssignment; $attributeAssignment; externalID; position; $attributeValue; $classSystemVersion
; cut:configuration 	; ; ; "NO_CUT"
; cut:configuration 	; ; ; "CUT_2"
; cut:configuration 	; ; ; "CUT_4"
; cut:configuration 	; ; ; "CUT_8"
; pack:configuration ; ; ; "PACK_X8"
; pack:configuration ; ; ; "PACK_X16"
; pack:configuration ; ; ; "PACK_X24"	

# Links ClassificationClasses to Categories
INSERT_UPDATE CategoryCategoryRelation;$categories;$supercategories
#;576;nutrition

#INSERT_UPDATE CategoryProductRelation 	; $products 	; $supercategories
#													; 592506       ; configuration
#													; 289540       ; configuration

#$feature1=@cut[$clAttrModifiers];         
#INSERT_UPDATE Product ; code[unique=true] ; $feature1     ; $catalogVersion ;
#;300938; CUT_8,CUT_2;
 
 
# END SCS categories-classifications

