package com.sharjah.facades.cronjobs;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sharjah.core.deliveryslots.service.impl.DefaultDeliverySlotService;
import com.sharjah.facades.cart.SharjahCartFacade;
import com.sharjah.facades.constants.SharjahFacadesConstants;


/**
 * Releases delivering capacity in slots for carts that have been inactive for too long.
 *
 * @author Pablo François
 */
public class DeliverySlotReleaseJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOGGER = Logger.getLogger(DefaultDeliverySlotService.class);

	@Resource(name = "defaultSharjahCartFacade")
	private SharjahCartFacade cartFacade;

	private List<CartModel> searchExpiredCarts()
	{
		final int timeLimit = Config.getInt(SharjahFacadesConstants.DELIVERY_SLOTS_TIME_RESTRICTION, 30);
		final Date threshold = Date.from(Instant.now().minus(timeLimit, ChronoUnit.MINUTES));

		final Map<String, Object> params = new HashMap<>();
		params.put("threshold", threshold);

		final SearchResult<CartModel> searchResult = flexibleSearchService.search(SharjahFacadesConstants.QUERY_EXPIRED_CARTS, params);
		return searchResult.getResult();
	}

	/**
	 * Perform the job.
	 */
	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		if (LOGGER.isDebugEnabled())
		{
			LOGGER.debug("Performing DeliverySlotReleaseJob");
		}
		final List<CartModel> carts = searchExpiredCarts();

		if (LOGGER.isInfoEnabled())
		{
			LOGGER.info(String.format("Found %d inavtive cart(s), releasing slot capacity", carts.size()));
		}
		carts.forEach(cart -> cartFacade.updateSelectedDeliverySlot(cart, null));

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}
