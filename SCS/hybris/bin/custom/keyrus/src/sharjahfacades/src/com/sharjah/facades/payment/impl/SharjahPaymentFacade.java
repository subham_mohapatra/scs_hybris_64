package com.sharjah.facades.payment.impl;

import de.hybris.platform.acceleratorfacades.payment.impl.DefaultPaymentFacade;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CashOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.UUID;

import javax.annotation.Resource;

import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;


/**
 * @author Pablo François
 */
public class SharjahPaymentFacade extends DefaultPaymentFacade
{

	private static final String CASH_ON_DELIVERY = "Cash On Delivery";
	private static final String CREDIT_CARD_ON_DELIVERY = "Credit Card On Delivery";

	@Resource
	private ModelService modelService;
	@Resource
	private CartService cartService;
	@Resource
	private Populator<AddressData, AddressModel> addressReversePopulator;

	/**
	 * Add the option to save the card that will be entered in CyberSource to the native method.
	 *
	 * @param responseUrl
	 *           The response URL
	 * @param merchantCallbackUrl
	 *           The merchant callback URL
	 * @param saveCard
	 *           If the card should be saved
	 * @return The payment data
	 */
	public PaymentData beginHopCreateSubscription(final String responseUrl, final String merchantCallbackUrl,
			final boolean saveCard)
	{
		final String fullResponseUrl = getFullResponseUrl(responseUrl, true);
		final String fullMerchantCallbackUrl = getFullResponseUrl(merchantCallbackUrl, true);
		final String siteName = getCurrentSiteName();

		final CustomerModel customerModel = getCurrentUserForCheckout();
		final AddressModel paymentAddress = getDefaultPaymentAddress(customerModel);

		final CreditCardPaymentInfoModel cardInfo = new CreditCardPaymentInfoModel();
		cardInfo.setSaved(saveCard);
		// Set any value so the native populator does not crash
		cardInfo.setType(CreditCardType.VISA);

		return getPaymentService().beginHopCreatePaymentSubscription(siteName, fullResponseUrl, fullMerchantCallbackUrl,
				customerModel, cardInfo, paymentAddress);
	}

	/**
	 * Save the transaction entry for a cart that will be paid on delivery.
	 *
	 * @param cart
	 *           The cart
	 */
	public void saveOnDeliveryPaymentTransactionEntry(final CartModel cart)
	{
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();

		PaymentInfoModel paymentInfo = modelService.create(CashOnDeliveryPaymentInfoModel.class);
		if (cart.getPaymentMode() != null && CyberSourceConstants.PAYMENT_CREDITCARD_OD.equals(cart.getPaymentMode().getCode()))
		{
			paymentInfo = modelService.create(CreditCardOnDeliveryPaymentInfoModel.class);
		}
		paymentInfo.setCode(UUID.randomUUID().toString());
		paymentInfo.setUser(cart.getUser());

		paymentInfo.setBillingAddress(cart.getPaymentAddress());

		final CurrencyModel currency = cart.getCurrency();
		final PaymentTransactionModel paymentTransaction = modelService.create(PaymentTransactionModel.class);
		paymentTransaction.setInfo(paymentInfo);
		paymentTransaction.setOrder(cart);
		paymentTransaction.setCode(currentUser.getCustomerID() + "_" + UUID.randomUUID());
		paymentTransaction.setCurrency(currency);
		final PaymentTransactionEntryModel transactionEntry = modelService.create(PaymentTransactionEntryModel.class);
		transactionEntry.setCode(paymentTransaction.getCode() + "-" + System.currentTimeMillis());
		transactionEntry.setPaymentTransaction(paymentTransaction);
		transactionEntry.setTime(new java.util.Date());
		transactionEntry.setAmount(BigDecimal.valueOf(cart.getTotalPrice()));

		transactionEntry.setCurrency(currency);
		transactionEntry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());
		if (cart.getPaymentMode() != null && CyberSourceConstants.PAYMENT_COD.equals(cart.getPaymentMode().getCode()))
		{
			paymentTransaction.setPaymentProvider(CASH_ON_DELIVERY);

		}
		if (cart.getPaymentMode() != null && CyberSourceConstants.PAYMENT_CREDITCARD_OD.equals(cart.getPaymentMode().getCode()))
		{
			paymentTransaction.setPaymentProvider(CREDIT_CARD_ON_DELIVERY);

		}
		transactionEntry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
		transactionEntry.setType(PaymentTransactionType.CAPTURE);

		cart.setPaymentStatus(PaymentStatus.PAID);
		cart.setPaymentInfo(paymentInfo);
		modelService.saveAll(cart, paymentTransaction, transactionEntry);
	}

	/**
	 * Set a defaultPaymentAddress for the customer that owns the given cart.
	 *
	 * @param cart
	 *           The cart
	 */
	public void setCustomerPaymentAddress(final CartModel cart)
	{
		final CustomerModel customer = (CustomerModel) cart.getUser();
		// Make sure customer.defaultPaymentAddress is set because DefaultPaymentFacade uses it to generate the CyberSource request
		if (customer.getDefaultPaymentAddress() == null)
		{
			final AddressModel address = cart.getPaymentAddress() != null ? cart.getPaymentAddress() : cart.getDeliveryAddress();
			customer.setDefaultPaymentAddress(address);
			modelService.save(customer);
		}
	}

	/**
	 * Save the payment address in the cart.
	 *
	 * @param addressData
	 *           The address to persist
	 */
	public void savePaymentAddress(final AddressData addressData)
	{
		final CartModel cartModel = cartService.getSessionCart();
		if (cartModel != null)
		{
			AddressModel addressModel = null;
			if (addressData != null)
			{
				addressModel = addressData.getId() == null ? createDeliveryAddressModel(addressData, cartModel)
						: findAddressForCustomer(cartModel.getUser(), addressData.getId());
			}
			if (addressModel != null) {
				cartModel.setPaymentAddress(addressModel);
				modelService.save(cartModel);
			}
		}
	}

	/**
	 * 
	 * @param customer
	 * @param addressId
	 * @return
	 */
	private AddressModel findAddressForCustomer(final UserModel customer, final String addressId)
	{
		for (final AddressModel address : customer.getAddresses())
		{
			if (addressId.equals(address.getPk().toString()))
			{
				return address;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param addressData
	 * @param cartModel
	 * @return
	 */
	private AddressModel createDeliveryAddressModel(final AddressData addressData, final CartModel cartModel)
	{
		final AddressModel addressModel = modelService.create(AddressModel.class);
		addressReversePopulator.populate(addressData, addressModel);
		addressModel.setOwner(cartModel);
		modelService.save(addressModel);
		return addressModel;
	}

}
