package com.sharjah.facades.deliveryarea;

import java.util.List;

import com.sharjah.facades.user.data.DeliveryAreaData;
import com.sharjah.facades.user.data.DeliveryCityData;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public interface SharjahDeliveryAreaFacade
{

	/**
	 * retrieve DeliveryCityData from the model
	 * 
	 * @param cityCode
	 * @return
	 */
	DeliveryCityData getDeliveryCityByCode(final String cityCode);

	/**
	 * retrieves all DeliveryCityData from the model
	 *
	 * @return
	 */
	List<DeliveryCityData> getDeliveryCities();

	/**
	 * Return list of {@link DeliveryAreaData} from City
	 *
	 * @param cityCode
	 * @return
	 */
	List<DeliveryAreaData> getDeliveryAreasForCityCode(final String cityCode);
}
