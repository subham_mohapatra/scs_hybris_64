package com.sharjah.facades.converters;

import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import com.sharjah.core.data.DeliverySlotData;
import com.sharjah.core.deliveryslots.service.DeliverySlotService;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.util.SharjahDateUtils;


/**
 * @author juanpablo.francois@keyrus.com
 */
@Component
public class DeliverySlotConverter extends AbstractDeliverySlotConverter
		implements Converter<CalendarDeliverySlotModel, DeliverySlotData>
{

	private static final String SPACE = " ";

	@Resource(name = "defaultDeliverySlotService")
	private DeliverySlotService deliverySlotService;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotData convert(final CalendarDeliverySlotModel source)
	{
		return convert(source, new DeliverySlotData());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotData convert(final CalendarDeliverySlotModel source, final DeliverySlotData target)
	{
		target.setPos(source.getPos().getName());
		target.setDeliverySlotCode(source.getDeliverySlot().getCode());
		target.setBeginTime(convertTime(source.getDeliverySlot().getBeginTime(), true));
		target.setEndTime(convertTime(source.getDeliverySlot().getEndTime(), true));
		target.setDayOfWeek(getDayOfWeekFromDate(source.getDate()));
		target.setCapacity(source.getCapacity().intValue());
		target.setSelected(deliverySlotService.isSlotSelected(source));
		target.setExpired(deliverySlotService.isSlotExpired(source, calculateFirstDeliveryDate(target)));
		target.setDeliveryDate(getDeliveryDate(source.getDate()));
		return target;
	}

	/**
	 * Calculate the first possible available date for delivery.
	 */
	private LocalDate calculateFirstDeliveryDate(final DeliverySlotData target)
	{
		return LocalDate.now().plus(target.getDeliveryDateOffset(), ChronoUnit.DAYS);
	}

	/**
	 * Format Date to DD Month
	 */
	private String getDeliveryDate(final Date date)
	{
		final DateTime dateTime = new DateTime(date.getTime());
		return dateTime.toString("dd") + SPACE + dateTime.monthOfYear().getAsText(i18nService.getCurrentLocale());
	}

	/**
	 * Parse the day of the week from a date object.
	 */
	private String getDayOfWeekFromDate(final Date date)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE");
		return formatter.format(SharjahDateUtils.toLocalDate(date));
	}

}
