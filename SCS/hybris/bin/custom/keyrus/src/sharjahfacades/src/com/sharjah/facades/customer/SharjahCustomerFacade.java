package com.sharjah.facades.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;

import java.util.List;

import com.sharjah.facades.user.data.NationalityData;


/**
 * Interface of Sharjah customer facade extending {@link CustomerFacade} interface
 */
public interface SharjahCustomerFacade extends CustomerFacade{

	/**
	 * Get nationality items
	 * 
	 * @return {@NationalityModel} List
	 */
	List<NationalityData> getAllNationality();

	/**
	 * Get nationality by code
	 * 
	 * @param code
	 * @return
	 */
	NationalityData getNationalityByCode(String code);
	
}
