package com.sharjah.facades.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.enums.PhoneContactInfoType;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.PhoneContactInfoModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.daos.LanguageDao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.sharjah.core.model.NationalityModel;
import com.sharjah.core.customer.service.SharjahCustomerAccountService;
import com.sharjah.core.model.ShareholderModel;
import com.sharjah.facades.customer.SharjahCustomerFacade;
import com.sharjah.facades.user.data.NationalityData;
import com.sharjah.shareholder.service.impl.DefaultSharjahShareholderService;


/**
 * Sharjah customer facade extending {@link DefaultCustomerFacade} and implementing {@link SharjahCustomerFacade}
 */
public class DefaultSharjahCustomerFacade extends DefaultCustomerFacade implements SharjahCustomerFacade
{

	@Resource(name = "languageDao")
	private LanguageDao languageDao;

	@Resource(name = "sharjahShareholderService")
	private DefaultSharjahShareholderService defaultSharjahShareholderService;

	@Resource(name = "nationalityConverter")
	private Converter<NationalityModel, NationalityData> nationalityConverter;

	
	public DefaultSharjahShareholderService getDefaultSharjahShareholderService()
	{
		return defaultSharjahShareholderService;
	}

	@Override
	public void register(final RegisterData registerData) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		Assert.hasText(registerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(registerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(registerData.getLogin(), "The field [Login] cannot be empty");

		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		
		newCustomer.setFirstName(registerData.getFirstName());
		newCustomer.setLastName(registerData.getLastName());
		
		if (StringUtils.isNotBlank(registerData.getFirstName()) && StringUtils.isNotBlank(registerData.getLastName()))
		{
			newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		}

		setUidForRegister(registerData, newCustomer);

		LanguageModel language = new LanguageModel();
		if (!CollectionUtils.isEmpty(languageDao.findLanguagesByCode(registerData.getDefaultLanguage())))
		{
			language = languageDao.findLanguagesByCode(registerData.getDefaultLanguage()).get(0);
		}
		else
		{
			language = getCommonI18NService().getCurrentLanguage();
		}
		newCustomer.setGender(Gender.MALE.toString().equals(registerData.getGenderCode()) ? Gender.MALE : Gender.FEMALE);
		newCustomer.setSessionLanguage(language);
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		final PhoneContactInfoModel phoneInfo = getModelService().create(PhoneContactInfoModel.class);
		phoneInfo.setPhoneNumber(registerData.getMobileNumber());
		phoneInfo.setPrefix(registerData.getMobileprefixNumber());
		newCustomer.setPhoneContactInfo(phoneInfo);
		phoneInfo.setUser(newCustomer);
		phoneInfo.setType(PhoneContactInfoType.MOBILE);

		final String membershipNumber = registerData.getMembershipNumber();

		if (StringUtils.isNotEmpty(membershipNumber))
		{
			final ShareholderModel shareholder = getDefaultSharjahShareholderService().findShareholderByNumber(membershipNumber);
			if (shareholder != null)
			{
				newCustomer.setShareholder(shareholder);
			}
		}

		getCustomerAccountService().register(newCustomer, registerData.getPassword());
	}
	
	@Override
	public void updateProfile(final CustomerData customerData) throws DuplicateUidException {
		validateDataBeforeUpdate(customerData);

		final CustomerModel customer = getCurrentSessionCustomer();
		customer.setOriginalUid(customerData.getDisplayUid());

		if (getCustomerAccountService() instanceof SharjahCustomerAccountService) {
			((SharjahCustomerAccountService) getCustomerAccountService()).updateProfile(customer, customerData);
		}

	}
	
	@Override
	protected void validateDataBeforeUpdate(final CustomerData customerData)
	{
		validateParameterNotNullStandardMessage("customerData", customerData);
		Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(customerData.getUid(), "The field [Uid] cannot be empty");
	}
	
	@Override
	public List<NationalityData> getAllNationality()
	{
	
		return nationalityConverter.convertAll(((SharjahCustomerAccountService) getCustomerAccountService()).getAllNationality());
	}

	@Override
	public NationalityData getNationalityByCode(final String code)
	{

		return nationalityConverter.convert(((SharjahCustomerAccountService) getCustomerAccountService()).getNationalityByCode(code));
	}
}
