package com.sharjah.facades.product.populator;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.sharjah.core.model.StickerModel;
import com.sharjah.facades.product.data.StickerData;


/**
 * 
 * @author Issam Maiza
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahStickerPopulator<SOURCE extends StickerModel, TARGET extends StickerData>
		implements Populator<SOURCE, TARGET> {

	private Converter<MediaModel, ImageData> imageConverter;

	@Override
	public void populate(final SOURCE stickerModel, final TARGET stickerData) throws ConversionException {

		if (stickerModel.getImage() != null)
		{
			stickerData.setImage(getImageConverter().convert(stickerModel.getImage()));
		}
		stickerData.setCode(stickerModel.getCode());
		stickerData.setDescription(stickerModel.getDescription());
		stickerData.setName(stickerModel.getName());
		if (stickerModel.getType() != null) {
			stickerData.setStickerType(stickerModel.getType().toString());
		}
		stickerData.setStartDate(stickerModel.getStartDate());
		stickerData.setEndDate(stickerModel.getEndDate());
	}

	/**
	 * @return the imageConverter
	 */
	public Converter<MediaModel, ImageData> getImageConverter() {
		return imageConverter;
	}

	/**
	 * @param imageConverter
	 *            the imageConverter to set
	 */
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter) {
		this.imageConverter = imageConverter;
	}

}
