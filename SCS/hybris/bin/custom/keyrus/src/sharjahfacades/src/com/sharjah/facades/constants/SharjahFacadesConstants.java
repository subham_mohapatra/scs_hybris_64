/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.facades.constants;

/**
 * Global class for all SharjahFacades constants.
 */
@SuppressWarnings("PMD")
public class SharjahFacadesConstants extends GeneratedSharjahFacadesConstants
{
	public static final String EXTENSIONNAME = "sharjahfacades";
	public static final String DELIVERY_SLOTS_TIME_RESTRICTION = "deliveryslots.time.restriction";
	public static final String QUERY_EXPIRED_CARTS = "SELECT {pk} FROM {Cart} WHERE {modifiedTime} < ?threshold"
			+ " AND {selectedDeliverySlot} IS NOT NULL";

	public static final String TRANSACTION_TEXT = "Transaction: ";

	private SharjahFacadesConstants()
	{
		//empty
	}
}
