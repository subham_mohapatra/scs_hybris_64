package com.sharjah.facades.populators;

import java.math.BigDecimal;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
/**
 * 
 * @author Ben Amor Bassem
 * 
 * Converter implementation for {@link de.hybris.platform.core.model.order.OrderModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.OrderHistoryData} as target type.
 *
 */
public class SharjahOrderHistoryPopulator  extends OrderHistoryPopulator {
	
	@Override
	public void populate(final OrderModel source, final OrderHistoryData target)	{
		

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setGuid(source.getGuid());
		target.setPlaced(source.getDate());
		target.setStatus(source.getStatus());
		target.setStatusDisplay(getStatusDisplay(source));
		if (source.getTotalPrice() != null)
		{
			BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());
			if (Boolean.TRUE.equals(source.getNet()))
			{
				totalPrice = totalPrice.add(BigDecimal.valueOf(source.getTotalTax().doubleValue()));
			}
			target.setTotal(getPriceDataFactory().create(PriceDataType.BUY, totalPrice, source.getCurrency()));
		}
	
	}
	
	/**
	 * 
	 * @param orderModel
	 * @return
	 */
	private String getStatusDisplay(OrderModel orderModel){
		Set<ConsignmentModel> consignments = orderModel.getConsignments();
		if(CollectionUtils.isNotEmpty(consignments)){
			ConsignmentModel consignmentModel = consignments.iterator().next();
			 if (ConsignmentStatus.SHIPPED.equals(consignmentModel.getStatus())){
				 return "Shipped";
			 }
		}
		return orderModel.getStatusDisplay();
	}
}
