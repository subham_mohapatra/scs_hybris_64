package com.sharjah.facades.populators;

import org.apache.commons.lang.StringUtils;

import de.hybris.platform.commercefacades.storelocator.converters.populator.PointOfServicePopulator;
import de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

/**
 * Converter implementation for {@link de.hybris.platform.storelocator.model.PointOfServiceModel} as source and
 * {@link de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData} as target type.
 */
public class DefaultSharjahPointOfServicePopulator extends PointOfServicePopulator{

	/**
	 * Populate the target instance with values from the source instance.
	 *
	 * @param source the PointOfServiceModel object
	 * @param PointOfServiceData the target to fill
	 */
	@Override
	public void populate(final PointOfServiceModel source, final PointOfServiceData target)
	{
		super.populate(source, target);
		if(StringUtils.isNotEmpty(source.getPosDisplayName())){
			target.setDisplayName(source.getPosDisplayName());
		}
	}

}
