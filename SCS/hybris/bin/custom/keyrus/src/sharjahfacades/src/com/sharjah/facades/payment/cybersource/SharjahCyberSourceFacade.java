package com.sharjah.facades.payment.cybersource;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import javax.servlet.http.HttpServletRequest;


/**
 * Interface of the Sharjah cyber source facade
 * 
 * @author Pablo François
 */
public interface SharjahCyberSourceFacade
{

	/**
	 * Save a payment transaction entry.
	 *
	 * @param cart
	 *           The processed cart
	 * @param requestId
	 *           The CyberSource request ID
	 * @param requestToken
	 *           The CyberSource request token
	 * @param paymentInfoModel
	 *           The credit card payment info
	 * @return The saved entry
	 */
	PaymentTransactionEntryModel savePaymentTransactionEntry(CartModel cart, String requestId, String requestToken,
			CreditCardPaymentInfoModel paymentInfoModel);

	/**
	 * Save a payment token.
	 *
	 * @param request
	 *           The request coming as a response from the CyberSource server
	 * @param paymentInfoData
	 *           The credit card payment info
	 * @param cartModel
	 *           The cart model
	 * @return The saved credit card payment info
	 */
	CreditCardPaymentInfoModel saveToken(HttpServletRequest request, CCPaymentInfoData paymentInfoData, CartModel cartModel);

	/**
	 * Set the given payment details on the cart
	 * 
	 * @param paymentInfoId
	 * @return true if successful
	 */
	boolean setPaymentDetails(final String paymentInfoId);

	/**
	 * Place an order.
	 *
	 * @param status
	 *           The status of the order
	 * @param paymentInfoModel
	 *           The credit card payment info
	 * @return The order data object
	 * @throws InvalidCartException
	 *            If the cart is invalid
	 */
	OrderData placeOrder(String status, CreditCardPaymentInfoModel paymentInfoModel) throws InvalidCartException;

	/**
	 * Cancel a transaction.
	 *
	 * @param newEntryCode
	 *           The code for the cancellation
	 * @param requestId
	 *           The request ID
	 * @param requestToken
	 *           The request token
	 * @param paymentProvider
	 *           The payment provider
	 */
	void cancel(String newEntryCode, String requestId, String requestToken, String paymentProvider);

	/**
	 * Set the default payment for the current user.
	 *
	 * @param paymentInfoData
	 *           The payment info
	 */
	void setDefaultPaymentInfo(CCPaymentInfoData paymentInfoData);

}
