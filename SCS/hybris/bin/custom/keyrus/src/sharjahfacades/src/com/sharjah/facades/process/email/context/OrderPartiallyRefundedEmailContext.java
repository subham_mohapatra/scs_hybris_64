/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.orderprocessing.model.OrderModificationProcessModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;



/**
 * Velocity context for email about partially order refund
 */
public class OrderPartiallyRefundedEmailContext extends OrderPartiallyModifiedEmailContext
{
	private PriceData refundAmount;
	private List<OrderEntryData> modifiedEntries;

	@Override
	public void init(final OrderModificationProcessModel orderProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(orderProcessModel, emailPageModel);
		fillModifiedEntries(orderProcessModel);
		calculateRefundAmount();
	}

	/**
	 * 
	 */
	protected void calculateRefundAmount()
	{
		BigDecimal refundAmountValue = BigDecimal.ZERO;
		PriceData totalPrice = null;
		for (final OrderEntryData entryData : getRefundedEntries())
		{
			totalPrice = entryData.getTotalPrice();
			refundAmountValue = refundAmountValue.add(totalPrice.getValue());
		}

		if (totalPrice != null)
		{
			refundAmount = getPriceDataFactory().create(totalPrice.getPriceType(), refundAmountValue, totalPrice.getCurrencyIso());
		}
	}
	
	@Override
	protected void fillModifiedEntries(final OrderModificationProcessModel orderProcessModel)
	{
		modifiedEntries = new ArrayList<OrderEntryData>();
		final OrderModel order = orderProcessModel.getOrder();
		final List<ReturnRequestModel> returnRequests = order.getReturnRequests();

		if (CollectionUtils.isNotEmpty(returnRequests))
		{
			final ReturnRequestModel returnRequest = returnRequests.get(returnRequests.size() - 1);
			final List<ReturnEntryModel> returnEntries = returnRequest.getReturnEntries();
			for (final ReturnEntryModel returnEntry : returnEntries)
			{
				if (returnEntry instanceof RefundEntryModel)
				{
					final OrderEntryModel orderEntryModel = (OrderEntryModel) returnEntry.getOrderEntry();
					final OrderEntryData orderEntryData = getOrderEntryConverter().convert(orderEntryModel);

					orderEntryData.setQuantity(returnEntry.getReceivedQuantity());

					final BigDecimal refundAmountValue = calculateRefundAmount((RefundEntryModel) returnEntry);

					orderEntryData.setTotalPrice(getPriceDataFactory().create(orderEntryData.getTotalPrice().getPriceType(),
							refundAmountValue, orderEntryData.getTotalPrice().getCurrencyIso()));
					modifiedEntries.add(orderEntryData);
				}
			}
		}
	}
	/**
	 * Calculate the refund amount
	 * @param  RefundEntryModel
	 */
	public  BigDecimal calculateRefundAmount(final RefundEntryModel refundEntryModel)
	{
		return BigDecimal
				.valueOf((refundEntryModel.getOrderEntry().getTotalPrice() / refundEntryModel.getOrderEntry().getQuantity())
						* (refundEntryModel.getExpectedQuantity()));
	}
	/**
	 * 
	 * @return
	 */
	public List<OrderEntryData> getRefundedEntries()
	{
		return getModifiedEntries();
	}
	
	@Override
	public List<OrderEntryData> getModifiedEntries()
	{
		return modifiedEntries;
	}
	/**
	 * 
	 * @return
	 */
	public PriceData getRefundAmount()
	{
		return refundAmount;
	}
}
