package com.sharjah.facades.product.populator;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * The real overriding of ProductBasicPopulator since the name has been taken for a class that does something else.
 *
 * @author Pablo François
 *
 * @param <SOURCE>
 *           A ProductModel
 * @param <TARGET>
 *           A ProductData
 */
public class RealSharjahProductBasicPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
		ProductBasicPopulator<SOURCE, TARGET>
{

	@Override
	public void populate(final SOURCE productModel, final TARGET productData)
	{
		super.populate(productModel, productData);
		productData.setDelayDays(productModel.getDelayDays());
	}

}
