package com.sharjah.facades.cart;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;

import org.springframework.ui.Model;

import com.sharjah.core.model.CalendarDeliverySlotModel;


/**
 * Interface of Sharjah cart facade extending {@link CartFacade} interface
 * 
 * @author Pablo François
 */
public interface SharjahCartFacade extends CartFacade
{

	/**
	 * Set the given selected Delivery Slot in the cart.
	 *
	 * @param cart
	 *           The cart to modify
	 * @param selectedSlot
	 *           The selected delivery slot
	 */
	void updateSelectedDeliverySlot(CartModel cart, CalendarDeliverySlotModel selectedSlot);

	/**
	 * Update the Entry Config of the cart's entry
	 * 
	 * @param entryNumber
	 * @param entryConf
	 */
	void updateCartEntryConfig(final long entryNumber, final String entryConf);

	/**
	 * Set the given selected Delivery Slot in the current session cart.
	 *
	 * @param selectedSlot
	 *           The selected delivery slot
	 */
	void updateSelectedDeliverySlot(CalendarDeliverySlotModel selectedSlot);

	/**
	 * Checks if the cart has an already selected slot.
	 *
	 * @return If the cart has a selected slot
	 */
	CalendarDeliverySlotModel getSelectedDeliverySlot();

	/**
	 * Set the given preferred Delivery Slot for the customer.
	 *
	 * @param preferredSlot
	 *           The customer's preferred delivery slot
	 */
	void updatePreferredDeliverySlot(CalendarDeliverySlotModel preferredSlot);

	/**
	 * Get the preferred delivery slot for the current customer.
	 *
	 * @return The current customer's preferred delivery slot
	 */
	CalendarDeliverySlotModel getPreferredDeliverySlot();

	/**
	 * Check cart entries total price against configured cart threshold
	 *
	 * @return Object
	 */
	Object[] checkCartThreshold(Model model);

	/**
	 * Method for adding a product to cart.
	 *
	 * @param code
	 *           code of product to add
	 * @param prodConf
	 *           The product's configuration option. If null {@see CartFacade#addToCart(String, long)} is used.
	 * @param quantity
	 *           the quantity of the product
	 * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
	 * @throws CommerceCartModificationException
	 *            if the cart cannot be modified
	 */
	CartModificationData addToCart(String code, String prodConf, long quantity) throws CommerceCartModificationException;

}
