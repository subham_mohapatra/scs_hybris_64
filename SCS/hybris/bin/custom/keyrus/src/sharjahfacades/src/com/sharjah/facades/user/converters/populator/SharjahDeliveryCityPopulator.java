package com.sharjah.facades.user.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.model.DeliveryAreaModel;
import com.sharjah.core.model.DeliveryCityModel;
import com.sharjah.facades.user.data.DeliveryAreaData;
import com.sharjah.facades.user.data.DeliveryCityData;


/**
 * 
 * @author Abderrazzak.Blej
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahDeliveryCityPopulator<SOURCE extends DeliveryCityModel, TARGET extends DeliveryCityData>
		implements Populator<SOURCE, TARGET>
{

	private Converter<DeliveryAreaModel, DeliveryAreaData> sharjahDeliveryAreaConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		
		if(source != null && target != null){
			target.setCode(source.getCode());
			target.setName(source.getName());
			if (CollectionUtils.isNotEmpty(source.getDeliveryAreas()))
			{
				target.setAreas(sharjahDeliveryAreaConverter.convertAll(source.getDeliveryAreas()));
			}
		}
	}

	/**
	 * 
	 * @return the deliveryAreaConverter
	 */
	public Converter<DeliveryAreaModel, DeliveryAreaData> getSharjahDeliveryAreaConverter()
	{
		return sharjahDeliveryAreaConverter;
	}

	/**
	 * 
	 * @param deliveryAreaConverter
	 *           the deliveryAreaConverter to set
	 */
	@Required
	public void setSharjahDeliveryAreaConverter(final Converter<DeliveryAreaModel, DeliveryAreaData> sharjahDeliveryAreaConverter)
	{
		this.sharjahDeliveryAreaConverter = sharjahDeliveryAreaConverter;
	}
}
