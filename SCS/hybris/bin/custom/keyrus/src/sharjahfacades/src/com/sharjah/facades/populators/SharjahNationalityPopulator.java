package com.sharjah.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;

import com.sharjah.core.model.NationalityModel;
import com.sharjah.facades.user.data.NationalityData;

/**
 * 
 * @author Wael Sakhri
 *
 */
public class SharjahNationalityPopulator implements Populator<NationalityModel, NationalityData> {

	@Override
	public void populate(final NationalityModel source, final NationalityData target) throws ConversionException {

		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setCode(source.getCode());
		}
		if (StringUtils.isNotEmpty(source.getName()))
		{
			target.setName(source.getName());
		}
	}
}
