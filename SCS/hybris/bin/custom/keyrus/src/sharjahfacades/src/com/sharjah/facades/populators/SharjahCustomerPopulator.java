package com.sharjah.facades.populators;

import javax.annotation.Resource;

import com.sharjah.core.model.ShareholderModel;
import com.sharjah.facades.product.data.GenderData;
import com.sharjah.facades.shareholder.data.ShareholderData;
import com.sharjah.facades.shareholder.populator.SharjahShareholderPopulator;

import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;

/**
 * Converter implementation for {@link de.hybris.platform.core.model.user.UserModel} as source and
 * {@link de.hybris.platform.commercefacades.user.data.CustomerData} as target type.
 */
public class SharjahCustomerPopulator extends CustomerPopulator {

	@Resource
	private SharjahShareholderPopulator<ShareholderModel, ShareholderData> sharjahShareholderPopulator;

	@Override
	public void populate(final CustomerModel source, final CustomerData target) {
		super.populate(source, target);

		ShareholderData shareholderData = new ShareholderData();

		if (source.getShareholder() != null) {

			sharjahShareholderPopulator.populate(source.getShareholder(), shareholderData);
		}

		GenderData genderData = new GenderData();
		if (source.getGender() != null) {
			genderData.setCode(source.getGender().getCode());
			genderData.setName(source.getGender().name());
		}

		LanguageData languageData = new LanguageData();
		if (source.getSessionLanguage() != null) {
			languageData.setIsocode(source.getSessionLanguage().getIsocode());
			languageData.setName(source.getSessionLanguage().getName());
		}
		
		if(source.getPhoneContactInfo() != null){
			target.setMobileprefixNumber(source.getPhoneContactInfo().getPrefix());
			target.setMobileNumber(source.getPhoneContactInfo().getPhoneNumber());
		}

		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setLandline(source.getLandline());
		target.setNationality(source.getNationality());
		target.setLanguage(languageData);
		target.setDateOfBirth(source.getDateOfBirth());
		target.setGender(genderData);
		target.setMaritalStatus(source.getMaritalStatus());
		target.setHowDidYouFindUs(source.getHowDidYouKnow());
		target.setNumberOfDependants(source.getNumberOfDependants());
		target.setHouseHoldSize(source.getHouseholdSize());
		target.setShareholderData(shareholderData);
	}
}
