package com.sharjah.facades.user.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 * Class of the Sharjah user facade extending {@link DefaultUserFacade}
 */
public class DefaultSharjahUserFacade extends DefaultUserFacade
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AddressData> getAddressBook()
	{
		// Get the current customer's addresses
		final CustomerModel currentUser = (CustomerModel) getUserService().getCurrentUser();
		final Collection<AddressModel> addresses = getCustomerAccountService().getAddressBookEntries(currentUser);

		if (CollectionUtils.isNotEmpty(addresses))
		{
			return filterAddress(addresses);
		}
		return Collections.emptyList();
	}

	/**
	 * filter the address and return valids address
	 * 
	 * @param addresses
	 * @return
	 */
	private List<AddressData> filterAddress(final Collection<AddressModel> addresses)
	{
		final List<AddressData> result = new ArrayList<>();
		final Collection<CountryModel> deliveryCountries = getCommerceCommonI18NService().getAllCountries();
		final AddressData defaultAddress = getDefaultAddress();

		for (final AddressModel address : addresses)
		{
			// Filter out invalid addresses for the site
			if (address.getCountry() != null && deliveryCountries != null && deliveryCountries.contains(address.getCountry()))
			{
				final AddressData addressData = getAddressConverter().convert(address);

				if (defaultAddress != null && defaultAddress.getId() != null && defaultAddress.getId().equals(addressData.getId()))
				{
					addressData.setDefaultAddress(true);
					result.add(0, addressData);
				}
				else
				{
					result.add(addressData);
				}
			}
		}
		return result;
	}

	@Override
	public void removeCCPaymentInfo(final String id)
	{
		validateParameterNotNullStandardMessage("id", id);
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		for (final CreditCardPaymentInfoModel creditCardPaymentInfo : getCustomerAccountService()
				.getCreditCardPaymentInfos(currentCustomer, false))
		{
			if (creditCardPaymentInfo.getCode() != null && creditCardPaymentInfo.getCode().equals(id))
			{
				getCustomerAccountService().deleteCCPaymentInfo(currentCustomer, creditCardPaymentInfo);
				break;
			}
		}
		updateDefaultPaymentInfo(currentCustomer);
	}

	@Override
	public void unlinkCCPaymentInfo(final String id)
	{
		validateParameterNotNullStandardMessage("id", id);
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		for (final CreditCardPaymentInfoModel creditCardPaymentInfo : getCustomerAccountService()
				.getCreditCardPaymentInfos(currentCustomer, false))
		{
			if (creditCardPaymentInfo.getCode() != null && creditCardPaymentInfo.getCode().equals(id))
			{
				getCustomerAccountService().unlinkCCPaymentInfo(currentCustomer, creditCardPaymentInfo);
				break;
			}
		}
		updateDefaultPaymentInfo(currentCustomer);
	}

}
