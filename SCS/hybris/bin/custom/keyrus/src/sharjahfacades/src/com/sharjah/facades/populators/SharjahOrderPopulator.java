package com.sharjah.facades.populators;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CashOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.CreditCardOnDeliveryPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;

import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.Assert;

import com.sharjah.core.data.DeliverySlotData;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.facades.converters.DeliverySlotConverter;
import com.sharjah.facades.order.data.PaymentModeData;

/**
 * Converter implementation for {@link de.hybris.platform.core.model.order.OrderModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.OrderData} as target type.
 */

public class SharjahOrderPopulator  extends OrderPopulator {
	private static final String COD ="COD";
    private static final String CREDITCARDONDELIVRY = "CreditCardOnDelivery";
    
	@Resource(name = "deliverySlotConverter")
	private DeliverySlotConverter deliverySlotConverter;
	
	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		
		super.populate(source,  target);
		addTotals(source, target);
		addDeliverySlot(source, target);
		addPaymentMode(source, target);
		addBillingAdressForPaymentMode(source, target);
		addPromotions(source, target);

		final CustomerModel customerModel = (CustomerModel) source.getOwner();
		if(customerModel != null){
			if (customerModel.getPhoneContactInfo() != null)
			{
				target.setShareholderPhoneNumber(customerModel.getPhoneContactInfo().getPhoneNumber());
			}
			else if (customerModel.getPhoneContactInfo() == null && customerModel.getShareholder() != null)
			{
				target.setShareholderPhoneNumber(customerModel.getShareholder().getMobilePhone());
			}
		}
		if (source.getMoneyChange() != null)
		{
			target.setMoneyChange(source.getMoneyChange().getCode());
		}
		
		target.setStatusDisplay(getStatusDisplay(source));
	}
	

	/*
	 * Adds applied and potential promotions.
	 */
	@Override
	protected void addPromotions(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		addPromotions(source, getPromotionsService().getPromotionResults(source), prototype);
	}

	@Override
	protected void addPromotions(final AbstractOrderModel source, final PromotionOrderResults promoOrderResults,
			final AbstractOrderData prototype)
	{
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);
		prototype.setQuoteDiscounts(createPrice(source, Double.valueOf(quoteDiscountsAmount)));

		if (promoOrderResults != null)
		{
			final double productsDiscountsAmount = getProductsDiscountsAmount(source);
			final double orderDiscountsAmount = getOrderDiscountsAmount(source);

			prototype.setProductDiscounts(createPrice(source, Double.valueOf(productsDiscountsAmount)));
			prototype.setOrderDiscounts(createPrice(source, Double.valueOf(orderDiscountsAmount)));
			prototype.setTotalDiscounts(createPrice(source, Double.valueOf(orderDiscountsAmount)));
			prototype.setTotalDiscountsWithQuoteDiscounts(
					createPrice(source, Double.valueOf(productsDiscountsAmount + orderDiscountsAmount + quoteDiscountsAmount)));
			prototype.setAppliedOrderPromotions(getPromotions(promoOrderResults.getAppliedOrderPromotions()));
			prototype.setAppliedProductPromotions(getPromotions(promoOrderResults.getAppliedProductPromotions()));
		}
	}

	@Override
	protected void addTotals(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		final double orderDiscountsAmount = getOrderDiscountsAmount(source);
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);

		prototype.setTotalPrice(createPrice(source, source.getTotalPrice()));
		prototype.setTotalTax(createPrice(source, source.getTotalTax()));
		final double subTotal = source.getSubtotal().doubleValue();
		final PriceData subTotalPriceData = createPrice(source, Double.valueOf(subTotal));
		prototype.setSubTotal(subTotalPriceData);
		prototype.setSubTotalWithDiscounts(subTotalPriceData);
		prototype.setSubTotalWithoutQuoteDiscounts(createPrice(source, Double.valueOf(subTotal + quoteDiscountsAmount)));
		prototype.setDeliveryCost(source.getDeliveryMode() != null ? createPrice(source, source.getDeliveryCost()) : null);
		prototype.setTotalPriceWithTax((createPrice(source, calcTotalWithTax(source))));
	}
	/**
	 * 
	 * @param source
	 * @param target
	 */
	protected void addDeliverySlot(final OrderModel source, final OrderData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		final CalendarDeliverySlotModel deliverySlotModel = source.getSelectedDeliverySlot();

		DeliverySlotData deliverySlotData = null;
		if (deliverySlotModel != null)
		{
			deliverySlotData = getDeliverySlotConverter().convert(deliverySlotModel);
		}
		target.setDeliverySlot(deliverySlotData);
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	protected void addBillingAdressForPaymentMode (final OrderModel source, final OrderData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		
		if (source.getPaymentInfo() != null && source.getPaymentInfo() instanceof CashOnDeliveryPaymentInfoModel)
		{
			if (source.getPaymentInfo().getBillingAddress() != null)
			{
				final CCPaymentInfoData paymentInfo = new CCPaymentInfoData();
				paymentInfo.setBillingAddress(getAddressConverter().convert(source.getPaymentInfo().getBillingAddress()));
				paymentInfo.setId(COD);
				target.setPaymentInfo(paymentInfo);
			}
		}
		if (source.getPaymentInfo() != null && source.getPaymentInfo() instanceof CreditCardOnDeliveryPaymentInfoModel)
		{
			if (source.getPaymentInfo().getBillingAddress() != null)
			{
				final CCPaymentInfoData paymentInfo = new CCPaymentInfoData();
				paymentInfo.setBillingAddress(getAddressConverter().convert(source.getPaymentInfo().getBillingAddress()));
				paymentInfo.setId(CREDITCARDONDELIVRY);
				target.setPaymentInfo(paymentInfo);
			}
		}
	}
	
	/**
	 * 
	 * @param source
	 * @param target
	 */
	protected void addPaymentMode (final OrderModel source, final OrderData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		
		if (source.getPaymentMode() != null)
		{
			final PaymentModeData paymentMode = new PaymentModeData();
			paymentMode.setCode(source.getPaymentMode().getCode());
			paymentMode.setDescription(source.getPaymentMode().getDescription());
			paymentMode.setName(source.getPaymentMode().getName());
			target.setPaymentMode(paymentMode);
		}
	
	}
	
	/**
	 * 
	 * @param orderModel
	 * @return
	 */
	private String getStatusDisplay(final OrderModel orderModel){
		final Set<ConsignmentModel> consignments = orderModel.getConsignments();
		if(CollectionUtils.isNotEmpty(consignments)){
			final ConsignmentModel consignmentModel = consignments.iterator().next();
			 if (ConsignmentStatus.SHIPPED.equals(consignmentModel.getStatus())){
				 return "Shipped";
			 }
		}
		return orderModel.getStatusDisplay();
	}
	
	/**
	 * 
	 * @return
	 */
	public DeliverySlotConverter getDeliverySlotConverter()
	{
		return deliverySlotConverter;
	}
}
