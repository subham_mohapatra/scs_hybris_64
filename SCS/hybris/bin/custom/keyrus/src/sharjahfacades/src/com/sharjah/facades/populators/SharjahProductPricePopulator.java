package com.sharjah.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPricePopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.product.service.SharjahProductDiscountService;
import com.sharjah.core.util.SharjahDoubleUtils;
import com.sharjah.facades.product.impl.SharjahDefaultPriceDataFactory;

/**
 * Populate the product data with the price information
 */
public class SharjahProductPricePopulator extends ProductPricePopulator<ProductModel, ProductData>
{
	
	private SharjahProductDiscountService productDiscountService;
	
	private SharjahDefaultPriceDataFactory priceDataFactory;

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException {
		
		final PriceDataType priceType;
		final PriceInformation info;
		if (CollectionUtils.isEmpty(productModel.getVariants()))
		{
			priceType = PriceDataType.BUY;
			info = getCommercePriceService().getWebPriceForProduct(productModel);
		}
		else
		{
			priceType = PriceDataType.FROM;
			info = getCommercePriceService().getFromPriceForProduct(productModel);
		}

		if (info != null)
		{
			final PriceData priceData = getPriceDataFactory().create(priceType, BigDecimal.valueOf(info.getPriceValue().getValue()),
					info.getPriceValue().getCurrencyIso());
			productData.setPrice(priceData);
		}
		else
		{
			productData.setPurchasable(Boolean.FALSE);
		}

		if(productData.getPrice() != null && productData.getPrice().getValue() != null)
		{
			
			final Double discount = productDiscountService.calculateDiscount(productData.getPrice().getValue().doubleValue(), productModel);
			if(SharjahDoubleUtils.checkDouble(discount))
			{
				final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(discount),
						productData.getPrice().getCurrencyIso());
				productData.setDiscountPrice(priceData);
				
				final int percentage = productDiscountService.calculatePercentageDiscount(productData.getPrice().getValue().doubleValue(), discount);
				productData.setPercentageDiscount(percentage);
			}
		}
	}
	
	public void setProductDiscountService(final SharjahProductDiscountService productDiscountService)
	{
		this.productDiscountService = productDiscountService;
	}

	@Override
	protected SharjahDefaultPriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	@Required
	public void setPriceDataFactory(final SharjahDefaultPriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

}
