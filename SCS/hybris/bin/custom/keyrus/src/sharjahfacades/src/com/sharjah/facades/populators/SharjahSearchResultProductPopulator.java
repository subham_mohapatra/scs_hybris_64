package com.sharjah.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.product.service.SharjahProductDiscountService;
import com.sharjah.core.search.solrfacetsearch.provider.impl.SharjahProductStickersValueResolver;
import com.sharjah.facades.product.data.StickerData;

/**
 * Populates data from PRoduto to show in the Search/Category page
 * @author Luiz.Henriques
 *
 */
public class SharjahSearchResultProductPopulator extends SearchResultVariantProductPopulator
{
	private static final Logger LOG = LogManager.getLogger(SharjahSearchResultProductPopulator.class);
	private SharjahProductDiscountService productDiscountService;
	
	@Override
	public void populate(final SearchResultValueData source, final ProductData target) {
		
		super.populate(source, target);
		populateDiscount(source, target);
		target.setStickers(getStickersData(source));
		
	}
	
	protected void populateDiscount(final SearchResultValueData source, final ProductData target)
	{
		// Pull the discount price value for the current currency
		final Double discountValue = this.<Double> getValue(source, "discountValue");
		if (discountValue != null)
		{
			final PriceData priceData = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(discountValue.doubleValue()),
					getCommonI18NService().getCurrentCurrency());
			target.setDiscountPrice(priceData);
			
			final int percentage = productDiscountService.calculatePercentageDiscount(target.getPrice().getValue().doubleValue(), discountValue);
			target.setPercentageDiscount(percentage);
		}
	}
	
	/**
	 * Populate Stickers information
	 *
	 * @param source
	 * @param target
	 */
	private List<StickerData> getStickersData(final SearchResultValueData source)
	{
		final List<String> stickers = this.<List<String>> getValue(source, SharjahCoreConstants.INDEXEDED_PROPERTY_STICKER);
		if (CollectionUtils.isEmpty(stickers))
		{
			return Collections.emptyList();
		}
		final Map<String, StickerData> stickersDatas = stickers.stream()
				.collect(Collectors.toMap(x -> x, x -> new StickerData()));
		for (final Map.Entry<String, StickerData> entry : stickersDatas.entrySet())
		{
			if (entry.getKey().contains(SharjahProductStickersValueResolver.SEPARATOR))
			{
				entry.getValue()
						.setName(entry.getKey().substring(0, entry.getKey().lastIndexOf(SharjahProductStickersValueResolver.SEPARATOR)));
				final ImageData image = new ImageData();
				image.setUrl(entry.getKey().substring(entry.getKey().lastIndexOf(SharjahProductStickersValueResolver.SEPARATOR) + 1,
						entry.getKey().length()));
				entry.getValue().setImage(image);
			}

		}

		prepareStickerDates(this.<List<String>> getValue(source, SharjahCoreConstants.INDEXEDED_PROPERTY_STICKER_START_DATE),
				stickersDatas, true);
		prepareStickerDates(this.<List<String>> getValue(source, SharjahCoreConstants.INDEXEDED_PROPERTY_STICKER_END_DATE),
				stickersDatas, false);

		return new ArrayList<>(stickersDatas.values());

	}
	
	private void prepareStickerDates(final List<String> dates, final Map<String, StickerData> stickersData,
			final boolean startDate)
	{
		if (CollectionUtils.isNotEmpty(dates))
		{
			for (final String date : dates)
			{
				final int indexOfSeparator = date.lastIndexOf(SharjahProductStickersValueResolver.SEPARATOR);
				final String stickerName = date.substring(0, indexOfSeparator);
				final String dateString = date.substring(indexOfSeparator + 1);

				final StickerData sticker = stickersData.get(stickerName);
				if (sticker != null)
				{
					setStickerDate(sticker, dateString, startDate);
				}
				//date.substring(0, date.lastIndexOf(SharjahProductStickersValueResolver.SEPARATOR));
			}
		}
	}
	
	/**
	 * Set the date for a sticker
	 * 
	 * @param sticker
	 * @param dateString
	 * @param startDate
	 */
	private void setStickerDate(final StickerData sticker, final String dateString, final boolean startDate)
	{
		try
		{
			if (startDate)
			{
				sticker.setStartDate(SharjahProductStickersValueResolver.STICKER_DATE_FORMAT.get().parse(dateString));
			}
			else
			{
				sticker.setEndDate(SharjahProductStickersValueResolver.STICKER_DATE_FORMAT.get().parse(dateString));
			}
		}
		catch (final ParseException e)
		{
			LOG.error("Error during parsing sticker date indexed in solr", e);
		}
	}

	public void setProductDiscountService(final SharjahProductDiscountService productDiscountService)
	{
		this.productDiscountService = productDiscountService;
	}
}
