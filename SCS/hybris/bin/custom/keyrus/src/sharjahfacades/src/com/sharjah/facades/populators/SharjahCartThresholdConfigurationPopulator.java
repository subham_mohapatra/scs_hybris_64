package com.sharjah.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.DecimalFormat;

import com.sharjah.core.model.CartThresholdConfigurationModel;
import com.sharjah.facades.product.data.CartThresholdConfigurationData;


/**
 * 
 * @author Abderrazzak.Blej
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahCartThresholdConfigurationPopulator<SOURCE extends CartThresholdConfigurationModel, TARGET extends CartThresholdConfigurationData>
		implements Populator<SOURCE, TARGET>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (source.getThreshold() != null)
		{
			target.setThreshold(source.getThreshold());
		}
		if (source.getCurrency() != null)
		{
			target.setCurrency(source.getCurrency().getSymbol());
		}

		final DecimalFormat decimalFormat = new DecimalFormat("#.00");
		target.setFormattedValue(decimalFormat.format(source.getThreshold().doubleValue()));
	}

}
