package com.sharjah.facades.cart.impl;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.ModelValidationException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.sharjah.core.cartthresholdconfiguration.service.SharjahCartThresholdConfigurationService;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.model.CartThresholdConfigurationModel;
import com.sharjah.facades.cart.SharjahCartFacade;
import com.sharjah.facades.product.data.CartThresholdConfigurationData;


/**
 * @author juanpablo.francois@keyrus.com
 */
@Service
public class DefaultSharjahCartFacade extends DefaultCartFacade implements SharjahCartFacade
{
	protected static final Logger LOGGER = Logger.getLogger(DefaultSharjahCartFacade.class);

	private static final String CONTINUE_ATTRIBUT = "continueCheckout";
	private static final String CART_THRESHOLD_CODE = "cart_threshold_code";
	private static final String CART_THRESHOLDDATA_ATTRIBUT = "cartThresholdConfigurationData";
	private static final String THRESHOLD = "threshold";

	private ModelService modelService;
	private Populator<CartThresholdConfigurationModel, CartThresholdConfigurationData> sharjahCartThresholdConfigurationPopulator;
	private SharjahCartThresholdConfigurationService sharjahCartThresholdConfigurationService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSelectedDeliverySlot(final CartModel cart, final CalendarDeliverySlotModel selectedSlot)
	{
		// Release capacity if an old selected slot exists
		final CalendarDeliverySlotModel oldSlot = cart.getSelectedDeliverySlot();
		if (oldSlot != null)
		{
			// Do nothing if the selected slot was already chosen
			if (oldSlot.equals(selectedSlot))
			{
				return;
			}
			oldSlot.setCapacity(oldSlot.getCapacity() + 1);
			modelService.save(oldSlot);
		}

		if (selectedSlot != null)
		{
			final int capacity = selectedSlot.getCapacity();
			if (capacity > 0)
			{
				selectedSlot.setCapacity(capacity - 1);
				modelService.save(selectedSlot);
			}
			else
			{
				throw new ModelValidationException("The delivery slot has a capacity of 0");
			}
		}
		cart.setSelectedDeliverySlot(selectedSlot);
		modelService.save(cart);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSelectedDeliverySlot(final CalendarDeliverySlotModel selectedSlot)
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		updateSelectedDeliverySlot(sessionCart, selectedSlot);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CalendarDeliverySlotModel getSelectedDeliverySlot()
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		return sessionCart.getSelectedDeliverySlot();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updatePreferredDeliverySlot(final CalendarDeliverySlotModel preferredSlot)
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		sessionCart.setPreferredDeliverySlot(preferredSlot);
		modelService.save(sessionCart);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CalendarDeliverySlotModel getPreferredDeliverySlot()
	{
		final CartModel sessionCart = getCartService().getSessionCart();
		return sessionCart.getPreferredDeliverySlot();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object[] checkCartThreshold(final Model model)
	{
		final CartThresholdConfigurationModel cartThresholdConfiguration = sharjahCartThresholdConfigurationService
				.getCartThresholdConfigurationByCode(CART_THRESHOLD_CODE);
		final CartModel cartModel = getCartService().getSessionCart();

		double threshold = 0;
		if (cartThresholdConfiguration != null && cartThresholdConfiguration.getThreshold() != null)
		{
			threshold = cartThresholdConfiguration.getThreshold().doubleValue();
		}
		final double totalPrice = cartModel.getEntries().stream().mapToDouble(entry -> entry.getTotalPrice()).sum();

		if (cartThresholdConfiguration != null && totalPrice < threshold
				&& totalPrice > 0)
		{
			final CartThresholdConfigurationData cartThresholdConfigurationData = new CartThresholdConfigurationData();
			sharjahCartThresholdConfigurationPopulator.populate(cartThresholdConfiguration, cartThresholdConfigurationData);

			final double valueDiff = threshold - totalPrice;
			final DecimalFormat decimalFormat = new DecimalFormat("#.00");

			model.addAttribute(CONTINUE_ATTRIBUT, false);
			model.addAttribute(CART_THRESHOLDDATA_ATTRIBUT, cartThresholdConfigurationData);
			model.addAttribute(THRESHOLD, new DecimalFormat("0.00").format(threshold));

			return new Object[]
			{ cartThresholdConfigurationData.getFormattedValue(), cartThresholdConfigurationData.getCurrency(),
					decimalFormat.format(valueDiff) };
		}
		else
		{
			model.addAttribute(CONTINUE_ATTRIBUT, true);
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModificationData addToCart(final String code, final String prodConf, final long quantity)
			throws CommerceCartModificationException
	{
		final ProductModel product = getProductService().getProductForCode(code);
		final CartModel cartModel = getCartService().getSessionCart();
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setQuantity(quantity);
		parameter.setProduct(product);
		parameter.setUnit(product.getUnit());
		parameter.setCreateNewEntry(false);
		parameter.setEntryConf(prodConf);

		final CommerceCartModification modification = getCommerceCartService().addToCart(parameter);

		return getCartModificationConverter().convert(modification);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCartEntryConfig(final long entryNumber, final String entryConf)
	{
		try {
			final CartModel cartModel = getCartService().getSessionCart();
			final AbstractOrderEntryModel entry = cartModel.getEntries().get((int) entryNumber);
			if (entry != null)
			{
				entry.setEntryConf(entryConf);
				getModelService().save(entry);
			}
		}
		catch (final IndexOutOfBoundsException ex)
		{
			LOGGER.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
		}
	}
	
	/**
	 * 
	 * @return modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * to set modelService
	 * 
	 * @param modelService
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * 
	 * @return cartThresholdConfigurationPopulator
	 */
	public Populator<CartThresholdConfigurationModel, CartThresholdConfigurationData> getSharjahCartThresholdConfigurationPopulator()
	{
		return sharjahCartThresholdConfigurationPopulator;
	}

	/**
	 * to set sharjahCartThresholdConfigurationPopulator
	 * 
	 * @param sharjahCartThresholdConfigurationPopulator
	 */
	@Required
	public void setSharjahCartThresholdConfigurationPopulator(
			final Populator<CartThresholdConfigurationModel, CartThresholdConfigurationData> sharjahCartThresholdConfigurationPopulator)
	{
		this.sharjahCartThresholdConfigurationPopulator = sharjahCartThresholdConfigurationPopulator;
	}

	/**
	 * 
	 * @return cartThresholdConfigurationService
	 */
	public SharjahCartThresholdConfigurationService getSharjahCartThresholdConfigurationService()
	{
		return sharjahCartThresholdConfigurationService;
	}

	/**
	 * to set cartThresholdConfigurationService
	 * 
	 * @param cartThresholdConfigurationService
	 */
	@Required
	public void setSharjahCartThresholdConfigurationService(
			final SharjahCartThresholdConfigurationService sharjahCartThresholdConfigurationService)
	{
		this.sharjahCartThresholdConfigurationService = sharjahCartThresholdConfigurationService;
	}

}
