package com.sharjah.facades.deliveryarea.impl;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.deliveryarea.service.SharjahDeliveryAreaService;
import com.sharjah.core.model.DeliveryAreaModel;
import com.sharjah.core.model.DeliveryCityModel;
import com.sharjah.facades.deliveryarea.SharjahDeliveryAreaFacade;
import com.sharjah.facades.user.data.DeliveryAreaData;
import com.sharjah.facades.user.data.DeliveryCityData;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahDeliveryAreaFacade implements SharjahDeliveryAreaFacade
{

	protected static final Logger LOG = LogManager.getLogger(DefaultSharjahDeliveryAreaFacade.class);

	private SharjahDeliveryAreaService sharjahDeliveryAreaService;

	private Converter<DeliveryCityModel, DeliveryCityData> sharjahDeliveryCityConverter;

	private Converter<DeliveryAreaModel, DeliveryAreaData> sharjahDeliveryAreaConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryCityData getDeliveryCityByCode(final String cityCode)
	{
		final DeliveryCityModel deliveryCityModel = sharjahDeliveryAreaService.findDeliveryCityByCode(cityCode);
		return sharjahDeliveryCityConverter.convert(deliveryCityModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryCityData> getDeliveryCities()
	{
		final List<DeliveryCityModel> deliveryCities = sharjahDeliveryAreaService.findAllDeliveryCities();
		if (CollectionUtils.isNotEmpty(deliveryCities))
		{
			return Converters.convertAll(deliveryCities, sharjahDeliveryCityConverter);
		}
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryAreaData> getDeliveryAreasForCityCode(final String cityCode)
	{
		if (StringUtils.isNotBlank(cityCode))
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Getting areas for city: " + cityCode);
			}

			final List<DeliveryAreaModel> deliveryAreas = sharjahDeliveryAreaService.findAreasForCityCode(cityCode);
			if (CollectionUtils.isNotEmpty(deliveryAreas))
			{
				return Converters.convertAll(deliveryAreas, sharjahDeliveryAreaConverter);
			}
		}

		return Collections.emptyList();
	}

	/**
	 * 
	 * @return the deliveryAreaService
	 */
	public SharjahDeliveryAreaService getSharjahDeliveryAreaService()
	{
		return sharjahDeliveryAreaService;
	}

	/**
	 * 
	 * @param deliveryAreaService
	 *           the deliveryAreaService to set
	 */
	@Required
	public void setSharjahDeliveryAreaService(final SharjahDeliveryAreaService sharjahDeliveryAreaService)
	{
		this.sharjahDeliveryAreaService = sharjahDeliveryAreaService;
	}

	/**
	 * @return the sharjahDeliveryCityConverter
	 */
	public Converter<DeliveryCityModel, DeliveryCityData> getSharjahDeliveryCityConverter()
	{
		return sharjahDeliveryCityConverter;
	}

	/**
	 * @param sharjahDeliveryCityConverter
	 *           the sharjahDeliveryCityConverter to set
	 */
	@Required
	public void setSharjahDeliveryCityConverter(final Converter<DeliveryCityModel, DeliveryCityData> sharjahDeliveryCityConverter)
	{
		this.sharjahDeliveryCityConverter = sharjahDeliveryCityConverter;
	}

	/**
	 * 
	 * @return the sharjahDeliveryAreaConverter
	 */
	public Converter<DeliveryAreaModel, DeliveryAreaData> getSharjahDeliveryAreaConverter()
	{
		return sharjahDeliveryAreaConverter;
	}

	/**
	 * 
	 * @param sharjahDeliveryAreaConverter
	 *           the sharjahDeliveryAreaConverter to set
	 */
	@Required
	public void setSharjahDeliveryAreaConverter(final Converter<DeliveryAreaModel, DeliveryAreaData> sharjahDeliveryAreaConverter)
	{
		this.sharjahDeliveryAreaConverter = sharjahDeliveryAreaConverter;
	}

}
