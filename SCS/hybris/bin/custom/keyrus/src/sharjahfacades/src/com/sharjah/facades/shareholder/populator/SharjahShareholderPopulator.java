package com.sharjah.facades.shareholder.populator;

import de.hybris.platform.constants.GeneratedCoreConstants.Enumerations.Gender;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang3.StringUtils;

import com.sharjah.core.model.ShareholderModel;
import com.sharjah.facades.shareholder.data.ShareholderData;


/**
 * 
 * @author Issam Maiza
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahShareholderPopulator<SOURCE extends ShareholderModel, TARGET extends ShareholderData>
		implements Populator<SOURCE, TARGET> {

	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException {
		
		if (StringUtils.isNotEmpty(source.getFirstName()))
		{
			target.setFirstName(source.getFirstName());
		}
		if (StringUtils.isNotEmpty(source.getLastName()))
		{
			target.setLastName(source.getLastName());
		}
		if (StringUtils.isNotEmpty(source.getEmail()))
		{
			target.setEmail(source.getEmail());
		}
		if (source.getGender() != null)
		{
			if(Gender.MALE.equals(source.getGender().getCode())){
				target.setGender(Gender.MALE.toString());
			}
			else
			{
				target.setGender(Gender.FEMALE.toString());	
			}
		}
		target.setLandLine(source.getLandline());
		target.setShareholderNumber(source.getShareholderNumber());
		target.setNationality(source.getNationality());
		target.setPhoneNumber(source.getMobilePhone());

	}

}
