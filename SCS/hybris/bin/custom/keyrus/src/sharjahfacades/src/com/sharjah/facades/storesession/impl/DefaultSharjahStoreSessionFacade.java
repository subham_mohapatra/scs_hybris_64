package com.sharjah.facades.storesession.impl;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import de.hybris.platform.commercefacades.storesession.data.LanguageData;
import de.hybris.platform.commercefacades.storesession.impl.DefaultStoreSessionFacade;

/**
 * Store session facade implementation. The main purpose is to load language data from existing services.
 */
public class DefaultSharjahStoreSessionFacade extends DefaultStoreSessionFacade{
    @Override
	protected void initializeSessionLanguage(final List<Locale> preferredLocales) {
		// Try to use the default language for the site
		final LanguageData defaultLanguage = getDefaultLanguage();
		if (defaultLanguage != null) {
			setCurrentLanguage(defaultLanguage.getIsocode());
			return;
		}
		if (CollectionUtils.isNotEmpty(preferredLocales)) {
			// Find the preferred locale that is in our set of supported
			// languages
			final Collection<LanguageData> storeLanguages = getAllLanguages();
			if (CollectionUtils.isNotEmpty(storeLanguages)) {
				final LanguageData bestLanguage = findBestLanguage(storeLanguages, preferredLocales);
				if (bestLanguage != null) {
					setCurrentLanguage(StringUtils.defaultString(bestLanguage.getIsocode()));
				}
			}
		}
	}
}
