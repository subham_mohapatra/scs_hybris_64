package com.sharjah.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CreditCardPaymentInfoPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

import org.apache.commons.lang.StringUtils;


/**
 * Set the code of the CreditCardPaymentInfoModel as the Id of CCPaymentInfoData because it is used when searching the
 * payment info and other manipulations since the PK is not always accessible in the service layer.
 *
 * @author Pablo François
 */
public class SharjahCreditCardPaymentInfoPopulator extends CreditCardPaymentInfoPopulator
{

	@Override
	public void populate(final CreditCardPaymentInfoModel source, final CCPaymentInfoData target)
	{
		super.populate(source, target);
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setId(source.getCode());
		}
	}

}
