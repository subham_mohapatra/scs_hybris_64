package com.sharjah.facades.product.populator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.sharjah.core.model.BrandCategoryModel;
import com.sharjah.core.model.StickerModel;
import com.sharjah.core.sticker.SharjahStickerUtils;
import com.sharjah.facades.product.data.StickerData;


/**
 * 
 * @author Issam Maiza
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahProductBasicPopulator<SOURCE extends ProductModel, TARGET extends ProductData> extends
                    AbstractProductPopulator<SOURCE, TARGET>
{
	private SharjahStickerPopulator<StickerModel, StickerData> sharjahStickerPopulator;
    private Converter<MediaModel, ImageData> imageConverter;


	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		
		populateStickers(productModel, productData);
		populateBrandLogo(productModel, productData);
		
    }
	private void populateBrandLogo(final SOURCE productModel, final TARGET productData)
	{
		final Collection<CategoryModel> supercategories = productModel.getSupercategories();
		if (CollectionUtils.isNotEmpty(supercategories))
		{
			supercategories.forEach(category -> {
				if (category instanceof BrandCategoryModel)
				{
					final Collection<MediaModel> images = category.getLogo();
					if (CollectionUtils.isNotEmpty(images))
					{
						productData.setBrandLogo(getImageConverter().convert(images.iterator().next()));
					}
					if (StringUtils.isNotEmpty(category.getName()))
					{
						productData.setBrandName(category.getName());
					}
					if (StringUtils.isNotEmpty(category.getCode()))
					{
						productData.setBrandCode(category.getCode());
					}

				}
			});
		}
		}

	/**
	 * 
	 * @param productModel
	 * @param productData
	 */
	private void populateStickers(final SOURCE productModel, final TARGET productData)
	{
		if (productModel.getStickers() != null && CollectionUtils.isNotEmpty(productModel.getStickers()))
		{

			final List<StickerModel> stickerModels = new ArrayList<StickerModel>(productModel.getStickers());

			stickerModels.removeIf(stickerModel -> (!SharjahStickerUtils.stickerValidation(stickerModel)));

			if (CollectionUtils.isNotEmpty(stickerModels))
			{

				final StickerModel stickerModel = stickerModels.get(0);

				if (stickerModel.getImage() != null && SharjahStickerUtils.stickerValidation(stickerModel))
				{
					productData.setStickerUrl(stickerModel.getImage().getURL());
				}
			}
		}

		final List<StickerData> stickerDatas = new ArrayList<StickerData>();
		if (CollectionUtils.isNotEmpty(productModel.getStickers()))
		{

			final Collection<StickerModel> stickerModels = productModel.getStickers();
			for (final StickerModel stickerModel : stickerModels)
			{
				if(SharjahStickerUtils.stickerValidation(stickerModel)){
					final StickerData stickerData = new StickerData();
					sharjahStickerPopulator.populate(stickerModel, stickerData);
					stickerDatas.add(stickerData);
				}
				
				
			}
			productData.setStickers(stickerDatas);
		}

	}
	
	/**
	 * 
	 * @return
	 */
	public Converter<MediaModel, ImageData> getImageConverter() {
		return imageConverter;
	}

	/**
	 * 
	 * @param imageConverter
	 */
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter) {
		this.imageConverter = imageConverter;
	}

	/**
	 * 
	 * @return
	 */
	public SharjahStickerPopulator<StickerModel, StickerData> getSharjahStickerPopulator() {
		return sharjahStickerPopulator;
	}

	/**
	 * 
	 * @param sharjahStickerPopulator
	 */
	public void setSharjahStickerPopulator(final SharjahStickerPopulator<StickerModel, StickerData> sharjahStickerPopulator) {
		this.sharjahStickerPopulator = sharjahStickerPopulator;
	}
	
}
