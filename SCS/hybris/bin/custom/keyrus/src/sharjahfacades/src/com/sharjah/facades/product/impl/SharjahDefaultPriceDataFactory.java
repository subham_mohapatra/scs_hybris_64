package com.sharjah.facades.product.impl;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.impl.DefaultPriceDataFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.LanguageModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.springframework.util.Assert;


public class SharjahDefaultPriceDataFactory extends DefaultPriceDataFactory
{
	private static final String arabCurrency = "درهم";

	private static final String AR = "ar";
	
	@Override
	public PriceData create(final PriceDataType priceType, final BigDecimal value, final CurrencyModel currency)
	{
		Assert.notNull(priceType, "Parameter priceType cannot be null.");
		Assert.notNull(value, "Parameter value cannot be null.");
		Assert.notNull(currency, "Parameter currency cannot be null.");
		final DecimalFormat df = new DecimalFormat("#0.00");
		  
		final PriceData priceData = createPriceData();

		priceData.setPriceType(priceType);
		priceData.setValue(new BigDecimal(df.format(value)));
		priceData.setCurrencyIso(currency.getIsocode());
		priceData.setFormattedValue(formatPrice(value, currency));

		return priceData;
	}

	@Override
	protected String formatPrice(final BigDecimal value, final CurrencyModel currency)
	{
		String formattedValue = "";
		final LanguageModel currentLanguage = getCommonI18NService().getCurrentLanguage();
		Locale locale = getCommerceCommonI18NService().getLocaleForLanguage(currentLanguage);
		if (locale == null)
		{
			// Fallback to session locale
			locale = getI18NService().getCurrentLocale();
		}

		final NumberFormat currencyFormat = createCurrencyFormat(locale, currency);
		if (AR.equals(currentLanguage.getIsocode()))
		{
			formattedValue = new DecimalFormat("#0.00", new DecimalFormatSymbols(locale)).format(value) + " " + arabCurrency;
		}
		else
		{
			formattedValue = currencyFormat.getCurrency().getCurrencyCode() + " "
					+ new DecimalFormat("#0.00", new DecimalFormatSymbols(locale)).format(value);
		}

		return formattedValue;
	}
}
