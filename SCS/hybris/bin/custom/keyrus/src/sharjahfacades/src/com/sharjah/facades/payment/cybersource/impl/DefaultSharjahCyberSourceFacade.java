package com.sharjah.facades.payment.cybersource.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.payment.dao.CreditCardPaymentSubscriptionDao;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.commands.request.VoidRequest;
import de.hybris.platform.payment.commands.result.VoidResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.methods.CardPaymentService;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;
import com.sharjah.facades.constants.SharjahFacadesConstants;
import com.sharjah.facades.payment.cybersource.SharjahCyberSourceFacade;
import com.sharjah.webservices.axon.services.AxonSMSService;


/**
 * @author Pablo François
 */
public class DefaultSharjahCyberSourceFacade extends DefaultAcceleratorCheckoutFacade implements SharjahCyberSourceFacade
{

	private static final Logger LOG = LogManager.getLogger(DefaultSharjahCyberSourceFacade.class);

	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.en')}")
	private String orderConfirmationMessageEN;

	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.ar.1')}")
	private String orderConfirmationMessageAR1;
	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.ar.2')}")
	private String orderConfirmationMessageAR2;
	@Value("#{configurationService.configuration.getProperty('order.confirmation.message.ar.3')}")
	private String orderConfirmationMessageAR3;

	@Resource(name = "axonSMSService")
	private AxonSMSService axonSMSService;

	@Resource(name = "i18nService")
	private I18NService i18nService;
	@Resource
	private ModelService modelService;
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private UserService userService;
	@Resource
	private CreditCardPaymentSubscriptionDao creditCardPaymentSubscriptionDao;
	@Resource
	private CardPaymentService cardPaymentService;
	@Resource
	private PaymentService paymentService;
	@Resource
	private CustomerAccountService customerAccountService;

	@Override
	public PaymentTransactionEntryModel savePaymentTransactionEntry(final CartModel cart, final String requestId,
			final String requestToken, final CreditCardPaymentInfoModel paymentInfoModel)
	{
		List<PaymentTransactionModel> transactions = new ArrayList<>();

		if (cart.getPaymentTransactions() != null)
		{
			transactions = new ArrayList<>(cart.getPaymentTransactions());
		}

		final PaymentTransactionModel transaction = savePaymentTransactionModel(cart, requestId, requestToken, paymentInfoModel, transactions);

		transactions.add(transaction);

		final PaymentTransactionEntryModel entry = savePaymentTransactionEntryModel(cart, requestId, requestToken, transaction) ;

		final List<PaymentTransactionEntryModel> entries = new ArrayList<>();
		entries.add(entry);
		transaction.setEntries(entries);
		cart.setPaymentTransactions(transactions);

		// Preserve calculated status before set PaymentMode
		final Boolean memoCalculated = cart.getCalculated();
		PaymentModeModel paymentMode = new PaymentModeModel();
		paymentMode.setCode(CyberSourceConstants.PAYMENT_CREDITCARD);
		paymentMode = flexibleSearchService.getModelByExample(paymentMode);
		cart.setPaymentMode(paymentMode);
		cart.setPaymentStatus(PaymentStatus.NOTPAID);
		modelService.save(cart);
		// reset calculated status after set PaymentMode
		cart.setCalculated(memoCalculated);

		modelService.save(cart);
		modelService.refresh(cart);

		return entry;
	}
	
	/**
	 * 
	 * @param cart
	 * @param requestId
	 * @param requestToken
	 * @param paymentInfoModel
	 * @param transactions
	 * @return
	 */
	private PaymentTransactionModel savePaymentTransactionModel(final CartModel cart, final String requestId,
			final String requestToken,
			final CreditCardPaymentInfoModel paymentInfoModel, final List<PaymentTransactionModel> transactions){
		final PaymentTransactionModel transaction = modelService.create(PaymentTransactionModel.class);
		transaction.setCode(cart.getUser().getUid() + "_" + UUID.randomUUID());
		transaction.setRequestId(requestId);
		transaction.setRequestToken(requestToken);
		transaction.setPaymentProvider(CyberSourceConstants.PAYMENT_PROVIDER);
		transaction.setCurrency(cart.getCurrency());
		transaction.setOrder(cart);
		transaction.setInfo(paymentInfoModel);
		transaction.setPlannedAmount(BigDecimal.valueOf(cart.getTotalPrice()));
		modelService.save(transaction);
		return transaction;
	}
	/**
	 * 
	 * @param cart
	 * @param requestId
	 * @param requestToken
	 * @param transaction
	 * @return
	 */
	private PaymentTransactionEntryModel savePaymentTransactionEntryModel(final CartModel cart,final String requestId,
			final String requestToken, final PaymentTransactionModel transaction){
		final PaymentTransactionEntryModel entry = modelService.create(PaymentTransactionEntryModel.class);
		final PaymentTransactionType paymentTransactionType = PaymentTransactionType.AUTHORIZATION;
		entry.setType(paymentTransactionType);
		entry.setRequestId(requestId);
		entry.setRequestToken(requestToken);
		entry.setTime(new Date());
		entry.setPaymentTransaction(transaction);
		entry.setTransactionStatus(TransactionStatus.ACCEPTED.name());
		entry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.name());
		entry.setCurrency(cart.getCurrency());
		entry.setCode(paymentService.getNewPaymentTransactionEntryCode(transaction, paymentTransactionType));
		entry.setAmount(BigDecimal.valueOf(cart.getTotalPrice()));
		modelService.save(entry);
		return entry;
	}

	@Override
	public CreditCardPaymentInfoModel saveToken(final HttpServletRequest request, final CCPaymentInfoData paymentInfoData,
			final CartModel cartModel)
	{
		if (StringUtils.isNotEmpty(paymentInfoData.getSubscriptionId()))
		{
			final CreditCardPaymentInfoModel existingPaymentInfo = creditCardPaymentSubscriptionDao
					.findCreditCartPaymentBySubscription(paymentInfoData.getSubscriptionId());
			if (existingPaymentInfo != null)
			{
				return existingPaymentInfo;
			}
		}

		final CreditCardPaymentInfoModel paymentInfoModel = modelService.create(CreditCardPaymentInfoModel.class);
		paymentInfoModel.setCode(paymentInfoData.getId());
		paymentInfoModel.setCcOwner(paymentInfoData.getAccountHolderName());
		paymentInfoModel.setNumber(paymentInfoData.getCardNumber());
		paymentInfoModel.setType(getCreditCardType(paymentInfoData.getCardType()));
		paymentInfoModel.setSubscriptionId(paymentInfoData.getSubscriptionId());
		paymentInfoModel.setValidToMonth(paymentInfoData.getExpiryMonth());
		paymentInfoModel.setValidToYear(paymentInfoData.getExpiryYear());
		paymentInfoModel.setUser(userService.getCurrentUser());
		paymentInfoModel.setSaved(paymentInfoData.isSaved());

		final AddressModel addressModel = modelService.clone(cartModel.getPaymentAddress());
		addressModel.setOwner(paymentInfoModel);
		paymentInfoModel.setBillingAddress(addressModel);

		modelService.save(paymentInfoModel);
		return paymentInfoModel;
	}

	@Override
	public boolean setPaymentDetails(final String paymentInfoId)
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);

		if (checkIfCurrentUserIsTheCartUser() && StringUtils.isNotBlank(paymentInfoId))
		{
			final CustomerModel currentUserForCheckout = getCurrentUserForCheckout();
			final CreditCardPaymentInfoModel ccPaymentInfoModel = customerAccountService.getCreditCardPaymentInfoForCode(
					currentUserForCheckout, paymentInfoId);
			final CartModel cart = getCart();
			if (ccPaymentInfoModel != null)
			{
				if (cart.getPaymentAddress() == null)
				{
					cart.setPaymentAddress(ccPaymentInfoModel.getBillingAddress());
					modelService.save(cart);
				}

				final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
				parameter.setEnableHooks(true);
				parameter.setCart(cart);
				parameter.setPaymentInfo(ccPaymentInfoModel);
				return getCommerceCheckoutService().setPaymentInfo(parameter);
			}
			else
			{
				if (LOG.isWarnEnabled())
				{
					LOG.warn(String.format(
							"Did not find CreditCardPaymentInfoModel for user: %s, cart: %s &  paymentInfoId: %s. PaymentInfo will not get set.",
							currentUserForCheckout, cart, paymentInfoId));
				}
			}
		}

		return false;
	}

	@Override
	public OrderData placeOrder(final String status, final CreditCardPaymentInfoModel paymentInfoModel) throws InvalidCartException
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final UserModel currentUser = getCurrentUserForCheckout();
			if (cartModel.getUser().equals(currentUser) || getCheckoutCustomerStrategy().isAnonymousCheckout())
			{
				beforePlaceOrder(cartModel);
				cartModel.setPaymentInfo(paymentInfoModel);
				this.getModelService().save(cartModel);

				// The placeOrder call triggers the process; everything done after it will not be visible in the process
				final OrderModel orderModel = placeOrder(cartModel);

				if (CyberSourceConstants.DECISION_REVIEW.equals(status))
				{
					orderModel.setStatus(OrderStatus.WAIT_FRAUD_MANUAL_CHECK);
				}
				else if (CyberSourceConstants.DECISION_REJECT.equals(status))
				{
					orderModel.setStatus(OrderStatus.PAYMENT_NOT_AUTHORIZED);
				}
				orderModel.setPaymentInfo(paymentInfoModel);

				PaymentModeModel paymentMode = new PaymentModeModel();
				paymentMode.setCode(CyberSourceConstants.PAYMENT_CREDITCARD);
				paymentMode = flexibleSearchService.getModelByExample(paymentMode);
				cartModel.setPaymentMode(paymentMode);
				cartModel.setPaymentStatus(PaymentStatus.PAID);

				this.getModelService().save(orderModel);

				afterPlaceOrder(cartModel, orderModel);

				this.getModelService().refresh(orderModel);

				//Send SMS Axon

				final OrderData orderData = getOrderConverter().convert(orderModel);

				String phoneNumber = orderData.getShareholderPhoneNumber();
				if (StringUtils.isEmpty(phoneNumber) && orderData.getDeliveryAddress() != null)
				{
					phoneNumber = orderData.getDeliveryAddress().getPhone();
				}
				String smsLang = "english";
				if ("ar".equals(i18nService.getCurrentLocale().getLanguage()))
				{
					smsLang = "arabic";
				}
				axonSMSService.sendSMS(phoneNumber, getSMSOrderConfirmationMessage(orderData), smsLang);

				return orderData;
			}
		}

		return null;
	}

	/**
	 * 
	 * @param orderData
	 * @return
	 */
	private String getSMSOrderConfirmationMessage(final OrderData orderData)
	{
		final String orderConfirmationMessageAR = orderConfirmationMessageAR1 + " " + orderData.getCode() + " "
				+ orderConfirmationMessageAR2 + " " + orderData.getDeliverySlot().getDeliveryDate() + " "
				+ orderConfirmationMessageAR3 + " " + orderData.getDeliverySlot().getBeginTime() + " - "
				+ orderData.getDeliverySlot().getEndTime();

		final StringBuilder confirmationText = new StringBuilder(
				Locale.ENGLISH.equals(i18nService.getCurrentLocale()) ? orderConfirmationMessageEN : orderConfirmationMessageAR);
		if (Locale.ENGLISH.equals(i18nService.getCurrentLocale()))
		{
			confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1, orderData.getCode());
			confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1,
					orderData.getDeliverySlot().getDeliveryDate());
			confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1,
					orderData.getDeliverySlot().getBeginTime() + " - " + orderData.getDeliverySlot().getEndTime());
		}

		return confirmationText.toString();
	}

	@Override
	public void cancel(final String newEntryCode, final String requestId, final String requestToken, final String paymentProvider)
	{
		try
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info(SharjahFacadesConstants.TRANSACTION_TEXT + requestId + " is rejected. EntryCode=" + newEntryCode);
			}
			final VoidResult result = cardPaymentService
					.voidCreditOrCapture(new VoidRequest(newEntryCode, requestId, requestToken, paymentProvider));
			if (LOG.isInfoEnabled())
			{
				LOG.info(
						SharjahFacadesConstants.TRANSACTION_TEXT + requestId + " status is " + result.getTransactionStatus().toString());
			}
		}
		catch (final Exception e)
		{
			LOG.error(SharjahFacadesConstants.TRANSACTION_TEXT + requestId + " was not canceled due to this error", e);
		}
	}


	@Override
	public void setDefaultPaymentInfo(final CCPaymentInfoData paymentInfoData)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final CreditCardPaymentInfoModel ccPaymentInfoModel = customerAccountService
				.getCreditCardPaymentInfoForCode(currentCustomer, paymentInfoData.getId());
		if (ccPaymentInfoModel != null)
		{
			getCustomerAccountService().setDefaultPaymentInfo(currentCustomer, ccPaymentInfoModel);
		}
	}

	/**
	 * 
	 * @param type
	 * @return
	 */
	private CreditCardType getCreditCardType(final String type)
	{
		for (final CreditCardType cardType : CreditCardType.values())
		{
			if (cardType.getCode().equals(type))
			{
				return cardType;
			}
		}
		return null;
	}
}
