package com.sharjah.facades.user.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.sharjah.core.deliveryarea.service.SharjahDeliveryAreaService;


/**
 *
 * @author Abderrazzak.Blej
 *
 */
public class SharjahAddressReversePopulator extends AddressReversePopulator
{

	@Resource
	private SharjahDeliveryAreaService sharjahDeliveryAreaService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException
	{
		super.populate(addressData, addressModel);
		if (addressData.getDeliveryArea() != null)
		{
			addressModel.setDeliveryArea(sharjahDeliveryAreaService.findDeliveryAreaByCode(addressData.getDeliveryArea().getCode()));
		}
		addressModel.setBuilding(addressData.getBuilding());
		addressModel.setAppartment(addressData.getApartment());
		addressModel.setLandmark(addressData.getLandmark());
		if (StringUtils.isNotEmpty(addressData.getGrl()))
		{
			addressModel.setGrl(addressData.getGrl());
		}
		if (StringUtils.isNotEmpty(addressData.getLongitude()))
		{
			addressModel.setLongitude(Double.valueOf(addressData.getLongitude()));
		}
		if (StringUtils.isNotEmpty(addressData.getLatitude()))
		{
			addressModel.setLatitude(Double.valueOf(addressData.getLatitude()));
		}
		if (StringUtils.isNotEmpty(addressData.getLine1()))
		{
			addressModel.setStreetname(addressData.getLine1());
		}
		if (StringUtils.isNotEmpty(addressData.getName()))
		{
			addressModel.setName(addressData.getName());
		}
	}
}
