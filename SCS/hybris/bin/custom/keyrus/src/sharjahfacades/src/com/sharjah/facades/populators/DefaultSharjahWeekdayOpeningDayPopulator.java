package com.sharjah.facades.populators;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.hybris.platform.commercefacades.storelocator.converters.populator.WeekdayOpeningDayPopulator;
import de.hybris.platform.commercefacades.storelocator.data.WeekdayOpeningDayData;
import de.hybris.platform.storelocator.model.WeekdayOpeningDayModel;

/**
 * Base converter for both of {@link de.hybris.platform.storelocator.model.WeekdayOpeningDayModel} , {@link de.hybris.platform.storelocator.model.SpecialOpeningDayModel} types.
 */
public class DefaultSharjahWeekdayOpeningDayPopulator extends WeekdayOpeningDayPopulator {

	@Override
	public void populate(final WeekdayOpeningDayModel source, final WeekdayOpeningDayData target)
	{
		populateBase(source, target);
		target.setWeekDay(getWeekDaySymbols().get(source.getDayOfWeek().ordinal()));
		
	}
	
	/**
	 * getWeekDaySymbols for english or arabic day name 
	 */
	protected List<String> getWeekDaySymbols()
	{
		final List<String> notEmptyWeekDay = new ArrayList<String>();
		final DateFormatSymbols weekDaySymbols = new DateFormatSymbols(getCurrentLocale());
		if("EN".equals(commerceCommonI18NService.getCurrentLanguage().getIsocode())){
			for (final String anyWeekDay : weekDaySymbols.getShortWeekdays())
			{
				if (StringUtils.isNotBlank(anyWeekDay))
				{
					notEmptyWeekDay.add(anyWeekDay);
				}
			}
		}
		else
		{
			for (final String anyWeekDay : weekDaySymbols.getWeekdays())
			{
				if (StringUtils.isNotBlank(anyWeekDay))
				{
					notEmptyWeekDay.add(anyWeekDay);
				}
			}
			
		}


		return notEmptyWeekDay;
	}
}
