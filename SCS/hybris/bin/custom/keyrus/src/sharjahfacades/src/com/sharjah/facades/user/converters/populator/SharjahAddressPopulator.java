package com.sharjah.facades.user.converters.populator;

import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang3.StringUtils;

import com.sharjah.facades.user.data.DeliveryAreaData;
import com.sharjah.facades.user.data.DeliveryCityData;


/**
 *
 * @author Abderrazzak.Blej
 *
 */
public class SharjahAddressPopulator extends AddressPopulator
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		super.populate(source, target);
		if (source.getDeliveryArea() != null)
		{
			final DeliveryAreaData deliveryAreaData = new DeliveryAreaData();
			deliveryAreaData.setCode(source.getDeliveryArea().getCode());
			deliveryAreaData.setName(source.getDeliveryArea().getName());
			target.setDeliveryArea(deliveryAreaData);
			final DeliveryCityData deliveryCityData = new DeliveryCityData();
			if (source.getDeliveryArea().getDeliveryCity() != null)
			{
				deliveryCityData.setCode(source.getDeliveryArea().getDeliveryCity().getCode());
				deliveryCityData.setName(source.getDeliveryArea().getDeliveryCity().getName());
			}

			target.setDeliveryCity(deliveryCityData);
		}
		target.setBuilding(source.getBuilding());
		target.setApartment(source.getAppartment());
		target.setLandmark(source.getLandmark());

		if (StringUtils.isNotEmpty(source.getStreetname()))
		{
			target.setLine1(source.getStreetname());
		}
		if (StringUtils.isNotEmpty(source.getName()))
		{
			target.setName(source.getName());
		}
		if (StringUtils.isNotEmpty(source.getGrl()))
		{
			target.setGrl(source.getGrl());
		}
		if (source.getLongitude() != null)
		{
			target.setLongitude(source.getLongitude().toString());
		}
		if (source.getLatitude() != null)
		{
			target.setLatitude(source.getLatitude().toString());
		}
	}
}
