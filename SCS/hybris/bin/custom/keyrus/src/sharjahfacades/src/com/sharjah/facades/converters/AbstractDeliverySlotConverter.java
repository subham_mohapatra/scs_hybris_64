package com.sharjah.facades.converters;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.StringUtils;

/**
 * @author juanpablo.francois@keyrus.com
 */
public abstract class AbstractDeliverySlotConverter
{

	/**
	 * Convert a time to the format of the site.
	 *
	 * @param timeText
	 *           Text with the time
	 * @param addAmPm
	 *           If am/pm should be added at the end
	 * @return The formatted time
	 */
	protected String convertTime(final String timeText, final boolean addAmPm)
	{
		if (StringUtils.isBlank(timeText))
		{
			return null;
		}

		final LocalTime time = LocalTime.parse(timeText);
		final StringBuilder result = new StringBuilder();

		final String hour = time.format(DateTimeFormatter.ofPattern("hh"));
		result.append(hour);

		if (time.getMinute() > 0)
		{
			final String minute = time.format(DateTimeFormatter.ofPattern("mm"));
			result.append(":").append(minute);
		}

		if (addAmPm)
		{
			final String amPm = time.format(DateTimeFormatter.ofPattern("a"));
			result.append(" ").append(amPm.toLowerCase());
		}

		return result.toString();
	}

}
