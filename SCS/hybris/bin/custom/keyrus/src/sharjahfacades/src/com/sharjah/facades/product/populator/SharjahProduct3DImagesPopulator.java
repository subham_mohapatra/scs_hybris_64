package com.sharjah.facades.product.populator;

import de.hybris.platform.commercefacades.product.converters.populator.AbstractProductPopulator;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * 
 * @author Abderrazzak.Blej
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahProduct3DImagesPopulator<SOURCE extends ProductModel, TARGET extends ProductData>
		extends AbstractProductPopulator<SOURCE, TARGET>
{

	private Converter<MediaModel, ImageData> imageConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final SOURCE productModel, final TARGET productData) throws ConversionException
	{
		final List<MediaModel> images3DList = productModel.getImages3D();
		final List<MediaModel> images3DModelList = new ArrayList<>(images3DList);
		Collections.sort(images3DModelList, new SharjahImage3DComparator());

		if (CollectionUtils.isNotEmpty(images3DModelList))
		{
			final List<ImageData> images3DDataList = new ArrayList<ImageData>();

			final Iterator<MediaModel> iterator = images3DModelList.iterator();
			while (iterator.hasNext())
			{
				final ImageData image3DData = getImageConverter().convert(iterator.next());
				image3DData.setFormat("projection3D");
				image3DData.setImageType(ImageDataType.GALLERY_3D);
				if (image3DData.getAltText() == null)
				{
					image3DData.setAltText(productModel.getName());
				}
				images3DDataList.add(image3DData);
			}
			productData.setImages3D(images3DDataList);
		}
	}

	/**
	 * 
	 * @return imageConverter
	 */
	public Converter<MediaModel, ImageData> getImageConverter()
	{
		return imageConverter;
	}

	/**
	 * set imageConverter attribute
	 * 
	 * @param imageConverter
	 */
	@Required
	public void setImageConverter(final Converter<MediaModel, ImageData> imageConverter)
	{
		this.imageConverter = imageConverter;
	}

}
