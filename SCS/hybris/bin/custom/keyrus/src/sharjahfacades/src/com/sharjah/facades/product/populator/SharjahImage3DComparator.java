package com.sharjah.facades.product.populator;

import de.hybris.platform.core.model.media.MediaModel;

import java.util.Comparator;


/**
 * 
 * @author Ben Amor Bassem
 *
 */
public class SharjahImage3DComparator implements Comparator<MediaModel>
{

	@Override
	public int compare(final MediaModel media1, final MediaModel media2)
	{
		return media1.getCode().compareTo(media2.getCode());
   }

}
