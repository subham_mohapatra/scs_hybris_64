package com.sharjah.facades.converters;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.sharjah.core.data.DeliverySlotData;
import com.sharjah.core.model.DeliverySlotByPOSModel;

/**
 * @author juanpablo.francois@keyrus.com
 */
@Component
public class PreferredDeliverySlotConverter extends AbstractDeliverySlotConverter
		implements Converter<DeliverySlotByPOSModel, DeliverySlotData>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotData convert(final DeliverySlotByPOSModel source)
	{
		return convert(source, new DeliverySlotData());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotData convert(final DeliverySlotByPOSModel source, final DeliverySlotData target)
	{
		target.setPos(source.getPos().getName());
		target.setDeliverySlotCode(source.getDeliverySlot().getCode());
		target.setBeginTime(convertTime(source.getDeliverySlot().getBeginTime(), false));
		target.setEndTime(convertTime(source.getDeliverySlot().getEndTime(), true));
		target.setDayOfWeek(StringUtils.capitalize(source.getDayOfWeek().getCode().toLowerCase()));
		target.setCapacity(source.getCapacity().intValue());
		return target;
	}

}
