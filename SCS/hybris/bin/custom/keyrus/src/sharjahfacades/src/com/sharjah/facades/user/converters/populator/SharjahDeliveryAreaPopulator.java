package com.sharjah.facades.user.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;

import com.sharjah.core.model.DeliveryAreaModel;
import com.sharjah.facades.user.data.DeliveryAreaData;


/**
 * 
 * @author Abderrazzak.Blej
 *
 * @param <SOURCE>
 * @param <TARGET>
 */
public class SharjahDeliveryAreaPopulator<SOURCE extends DeliveryAreaModel, TARGET extends DeliveryAreaData>
		implements Populator<SOURCE, TARGET>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final SOURCE source, final TARGET target) throws ConversionException
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setCode(source.getCode());
		}
		if (StringUtils.isNotEmpty(source.getName()))
		{
			target.setName(source.getName());
		}
	}

}
