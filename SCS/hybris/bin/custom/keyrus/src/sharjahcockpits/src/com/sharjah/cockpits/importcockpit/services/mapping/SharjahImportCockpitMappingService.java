package com.sharjah.cockpits.importcockpit.services.mapping;

import de.hybris.platform.importcockpit.jalo.ImportCockpitMapping;
import de.hybris.platform.importcockpit.model.ImportCockpitCronJobModel;
import de.hybris.platform.importcockpit.model.ImportCockpitMappingModel;
import de.hybris.platform.importcockpit.model.mappingview.MappingModel;
import de.hybris.platform.importcockpit.services.mapping.impl.DefaultImportCockpitMappingService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.io.InputStreamReader;
import java.io.PushbackReader;


/**
 * Class of the Import Cockpit Mapping Service
 */
public class SharjahImportCockpitMappingService extends DefaultImportCockpitMappingService
{

	@Override
	public MappingModel loadMapping(final ImportCockpitCronJobModel jobModel)
	{
		MappingModel ret = null;
		if (jobModel.getMapping() != null)
		{

			final ImportCockpitMappingModel mappingModel = jobModel.getMapping();
			final ImportCockpitMapping mappingSourceItem = this.getModelService().getSource(mappingModel);
			if (mappingSourceItem.hasData())
			{
				ret = this.loadMappingFromXml(this.getMappingXMLData(mappingModel,
						new PushbackReader(new InputStreamReader(this.getMediaService().getStreamFromMedia(mappingModel)))));
			}

			if (ret != null && mappingModel != null && ret.getCatalogVersion() == null)
			{
				ret.setCatalogVersion(mappingModel.getCatalogVersion());
			}
		}

		return ret;
	}

	@Override
	protected void saveMappingToProcessModel(final MappingModel mapping, final String mappingName,
			final ImportCockpitCronJobModel processModel, final boolean saveAs) throws ModelSavingException
	{

		ImportCockpitMappingModel importMapping = this.createMapping(mappingName);

		if (!(processModel.getMapping() != null && !saveAs))
		{
			try
			{
				this.getModelService().save(importMapping);

			}
			catch (final ModelSavingException arg8)
			{
				this.getModelService().refresh(processModel);
				throw arg8;
			}
			processModel.setMapping(importMapping);
			importMapping = processModel.getMapping();
			final String e = processModel.getCode() == null ? "Unnamed_Mapping" : (processModel.getCode() + "_Mapping");
			final String code = mappingName == null ? e : mappingName;
			importMapping.setCode(code);
		}

		this.getMediaService().setDataForMedia(importMapping, this.createMappingXML(mapping).getBytes());

		try
		{
			this.getModelService().save(importMapping);
		}
		catch (final ModelSavingException arg7)
		{

			this.getModelService().refresh(processModel);
			throw arg7;
		}
	}

}
