package com.sharjah.importcockpit.services.impex.impl;

import de.hybris.platform.importcockpit.model.ImportCockpitCronJobModel;
import de.hybris.platform.importcockpit.model.mappingview.MappingModel;
import de.hybris.platform.importcockpit.model.mappingview.mappingline.AttributeCollectionMappingLine;
import de.hybris.platform.importcockpit.model.mappingview.mappingline.MappingLineModel;
import de.hybris.platform.importcockpit.services.impex.impl.DefaultImpExMediaGenerationService;
import de.hybris.platform.util.CSVWriter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;


/**
 * Class of the Default ImpEx Media Generation Service
 */
public class DefaultSharjahImpExMediaGenerationService extends DefaultImpExMediaGenerationService {

	private static final Logger LOG = Logger.getLogger(DefaultSharjahImpExMediaGenerationService.class);

	
	@Override
	protected void generateImportScriptAsZipEntry(final ZipOutputStream zip, final ImportCockpitCronJobModel cronJobModel,
			final MappingModel mappingModel) {

		ZipEntry entry = null;
		final StringWriter stringWriter = new StringWriter();
		CSVWriter writer = null;

		try {
			writer = new CSVWriter(stringWriter);
			writeLocale(writer);
			if (mappingModel.getCatalogVersion() != null) {
				writeCatSysVersionMacro(mappingModel, writer);
			}

			writer.write(this.getHeaderGeneratorOperation().generateImpexHeaderLibrary(cronJobModel, mappingModel));
			entry = new ZipEntry("importscript.impex");
		} catch (final IOException arg22) {
			LOG.error("Import script file could not be created as a zip entry!", arg22);
		} finally {
			if (writer != null) {
				writer.closeQuietly();
			}

		}

		if (entry != null) {
			try {
				zip.putNextEntry(entry);
				zip.write(stringWriter.toString().getBytes("UTF-8"));
			} catch (final IOException arg21) {
				LOG.error("Import script file could not be created as a zip entry!", arg21);
			} finally {
				try {
					zip.closeEntry();
				} catch (final IOException arg24) {
					if (LOG.isDebugEnabled()) {
						LOG.debug(arg24.getMessage(), arg24);
					}
				}

			}
		}
	}
	
	private void writeLocale(final CSVWriter writer) throws IOException
	{
		writer.writeSrcLine("#%impex.setLocale(Locale.US)");
	}
	
	private void writeCatSysVersionMacro(final MappingModel mappingModel, final CSVWriter writer) throws IOException {
		if (mappingModel.getBaseTypeModel().getCatalogVersionAttribute() != null){
			final String catalogVersionBaseType = mappingModel.getBaseTypeModel().getCatalogVersionAttribute().getQualifier();
			final Set allCatalogVersions = this.getHeaderGeneratorOperation().getCatalogVersionsForMapping(catalogVersionBaseType,
				mappingModel);
		
			final Iterator arg5 = allCatalogVersions.iterator();

			while (arg5.hasNext())
			{
				final String prodAttributeLine = (String) arg5.next();
				writer.writeSrcLine(this.getHeaderGeneratorOperation().generateCatSysVersionMacro(mappingModel, prodAttributeLine));
			}

			final AttributeCollectionMappingLine prodAttributeLine1 = getProdAttributeLine(mappingModel);
			if (prodAttributeLine1 != null)
			{
				writer.writeSrcLine(this.getHeaderGeneratorOperation().generateMacro4ProductAttributes(prodAttributeLine1));
			}

			writer.writeSrcLine("");
		}
	}
	
	private AttributeCollectionMappingLine getProdAttributeLine(final MappingModel mapping) {
		AttributeCollectionMappingLine ret = null;
		final Iterator arg3 = mapping.getEntries().iterator();

		while (arg3.hasNext()) {
			final MappingLineModel line = (MappingLineModel) arg3.next();
			if (line instanceof AttributeCollectionMappingLine) {
				ret = (AttributeCollectionMappingLine) line;
			}
		}

		return ret;
	}
}
