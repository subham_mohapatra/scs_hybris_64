package com.sharjah.dataimport.batch.converter.translator;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.jalo.Category;
import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.translators.SingleValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.servicelayer.exceptions.SystemException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sharjah.core.jalo.BrandCategory;

public class ProductBrandTranslator extends SingleValueTranslator {
	private static final Logger LOG = LogManager.getLogger(ProductBrandTranslator.class);

	@SuppressWarnings("unchecked")
	@Override
	protected Object convertToJalo(final String cellValue, final Item processedItem) {

		BrandCategory brandCategory = null;
		Category category = null;
		final String cellValueSplit = cellValue;
		final String[] parts = cellValueSplit.split(",");
		final String brandCode = parts[0];
		final String categoryCode = parts[1];

		try {
		final CatalogVersionModel catalogVersionModel = getCatalogVersionService().getCatalogVersion("scsProductCatalog", "Staged");
			if (catalogVersionModel != null) {
				brandCategory = (BrandCategory) getCategoryService().getCategoryForCode(catalogVersionModel, brandCode)
						.getItemModelContext().getSource();
				category = (Category) getCategoryService().getCategoryForCode(catalogVersionModel,categoryCode).getItemModelContext()
						.getSource();
			}
		} catch (final Exception e) {
			LOG.warn("Cannot resolve catalogVersion for item '" + processedItem.getPK() + "'", e);
			setError();
			return null;
		}

		Collection<Category> superCategories = null;
		if (processedItem != null) {
			try {
				superCategories = (Collection<Category>) processedItem.getAttribute("superCategories");
			} catch (final JaloSecurityException e) {
				throw new SystemException("attribute superCategories unreadable for item " + processedItem.getPK(), e);
			}
		}

		final List<Category> newSuperCategories = superCategories == null ? new ArrayList<>()
				: new ArrayList<>(superCategories);
		for (final Iterator<Category> it = newSuperCategories.iterator(); it.hasNext();) {
			final Category categorycheck = it.next();
			if ((categorycheck instanceof BrandCategory && brandCategory != null)|| (categorycheck instanceof Category && category !=null) ) {
				it.remove();
			}
		}
		if (brandCategory != null) {
			newSuperCategories.add(brandCategory);
		}
		if (category != null) {
			newSuperCategories.add(category);
		}

		return newSuperCategories;
	}

	@Override
	protected String convertToString(final Object arg0) {
		return null;
	}

	private CategoryService getCategoryService() {
		return Registry.getApplicationContext().getBean("categoryService", CategoryService.class);
	}
	
	private CatalogVersionService getCatalogVersionService() {
		return Registry.getApplicationContext().getBean("catalogVersionService", CatalogVersionService.class);
	}
}
