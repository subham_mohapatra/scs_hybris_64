package com.sharjah.facades.order.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.List;

import javax.annotation.Resource;

import com.sharjah.facades.order.SharjahOrderDao;

/**
 * @author issam maiza
 * 
 * DefaultSharjahOrderServiceImpl to implement findOrdersWithFailedCapture() method
 * 
 */

public class DefaultSharjahOrderDao implements SharjahOrderDao{

	@Resource (name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;
	
	/**
	 * 
	 */
	@Override
	public List<OrderModel> findOrdersWithFailedCapture() {
		final OrderModel order = new OrderModel();
		order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
		order.setPaymentStatus(PaymentStatus.NOTPAID);
		return getFlexibleSearchService().getModelsByExample(order);
	}
	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService() {
		return flexibleSearchService;
	}
	/**
	 * @param flexibleSearchService the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
		this.flexibleSearchService = flexibleSearchService;
	}

}
