package com.sharjah.core.interceptor.order;

import de.hybris.platform.constants.GeneratedCoreConstants.Enumerations.DeliveryStatus;
import de.hybris.platform.constants.GeneratedCoreConstants.Enumerations.PaymentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sharjah.service.event.SharjahCaptureOrderEvent;

/**
 * @author issam maiza
 * 
 * SharjahCaptureOrderValidateInterceptor to override onValidate() method
 */

public class SharjahCaptureOrderValidateInterceptor implements ValidateInterceptor<OrderModel>{


	
	@Resource(name = "eventService")
	private EventService eventService;

	private static final Logger LOG = Logger.getLogger(SharjahCaptureOrderValidateInterceptor.class);

	@Override
	public void onValidate(final OrderModel order, final InterceptorContext ctx) throws InterceptorException {
		if (order.getPaymentStatus() != null && PaymentStatus.NOTPAID.equals(order.getPaymentStatus().getCode())
				&& ((order.getStatus() != null && (OrderStatus.COMPLETED.equals(order.getStatus())))
						|| (order.getDeliveryStatus() != null && DeliveryStatus.SHIPPED.equals(order.getDeliveryStatus().getCode()))))
		{
			final SharjahCaptureOrderEvent sharjahCaptureOrderEvent = new SharjahCaptureOrderEvent();
			sharjahCaptureOrderEvent.setOrder(order);
			getEventService().publishEvent(sharjahCaptureOrderEvent);
			 LOG.info("order with code: "+ sharjahCaptureOrderEvent.getOrder().getCode()+" will be captured.");
		}
	}
	
	/**
	 * @return the eventService
	 */
	public EventService getEventService() {
		return eventService;
	}

	/**
	 * @param eventService the eventService to set
	 */
	public void setEventService(final EventService eventService) {
		this.eventService = eventService;
	}
	

}
