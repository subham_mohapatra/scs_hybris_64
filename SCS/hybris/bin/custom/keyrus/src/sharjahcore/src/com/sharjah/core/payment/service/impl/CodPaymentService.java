package com.sharjah.core.payment.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.impl.DefaultPaymentServiceImpl;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.refund.enums.RefundsEnum;


/**
 * PaymentService for Cash On Delivery.
 *
 * @author Pablo François
 */
public class CodPaymentService extends DefaultPaymentServiceImpl
{
	private static final Logger LOG = LogManager.getLogger(CodPaymentService.class);

	@Resource
	private ModelService modelService;

	/**
	 * Create a new paymentTransactionEntry only if the refundsEnum is not equals to returnByCsAgent
	 *
	 * @param refundEnum
	 *           the refundEnum
	 * @param transaction
	 *           the paymentTransaction
	 * @param refundAmount
	 *           the refundAmount
	 * @return a new paymentTransactionEntry
	 */
	public PaymentTransactionEntryModel refundFollowOn(final RefundsEnum refundEnum, final PaymentTransactionModel transaction,
			final BigDecimal refundAmount)
	{
		if (RefundsEnum.RETURN_BY_CS_AGENT.equals(refundEnum))
		{
			LOG.debug("Automatic refund is not allowed for returnRequest");
			return null;
		}

		PaymentTransactionEntryModel capture = null;
		for (final PaymentTransactionEntryModel paymentTransactionEntry : transaction.getEntries())
		{
			final PaymentTransactionType transactionType = paymentTransactionEntry.getType();
			if (transactionType.equals(PaymentTransactionType.CAPTURE))
			{
				capture = paymentTransactionEntry;
				break;
			}
		}

		if (capture == null)
		{
			throw new AdapterException("Could not refund follow-on without capture");
		}
		else
		{
			final PaymentTransactionType transactionType = PaymentTransactionType.REFUND_FOLLOW_ON;
			final String newEntryCode = getNewPaymentTransactionEntryCode(transaction, transactionType);
			final PaymentTransactionEntryModel entry = modelService.create(PaymentTransactionEntryModel.class);
			entry.setType(transactionType);
			entry.setAmount(refundAmount);
			entry.setPaymentTransaction(transaction);
			entry.setTime(new Date());
			entry.setCurrency(transaction.getCurrency());
			entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
			entry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());
			entry.setCode(newEntryCode);
			modelService.save(entry);
			modelService.refresh(transaction);
			return entry;
		}
	}

	/**
	 * Get the total amount paid by the order by COD. The calculation takes the paymentTransaction with paymentProviders
	 * = CashOnDelivery and add the paymentTransactionEntry CAPTURE minus the paymentTransactionEntry REFUND
	 *
	 * @param order
	 *           the order
	 * @param currency
	 *           ISO code of the amount return using {@link CurrencyUtil}
	 * @return the total amount (Capture amount minus Refund amount of the paymentTransaction CashOnDelivery)
	 */
	public BigDecimal getTotalAmountPaid(final AbstractOrderModel order, final String currency)
	{
		BigDecimal totalAmountPaid = BigDecimal.ZERO;
		for (final PaymentTransactionModel paymentTransaction : order.getPaymentTransactions())
		{
			if (SharjahCoreConstants.PAYMENT_PROVIDER_COD.equalsIgnoreCase(paymentTransaction.getPaymentProvider()))
			{
				for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransaction.getEntries())
				{
					final PaymentTransactionType paymentTransactionType = paymentTransactionEntry.getType();
					final BigDecimal transactionEntryAmount = paymentTransactionEntry.getAmount();
					if (PaymentTransactionType.CAPTURE.equals(paymentTransactionType))
					{
						totalAmountPaid = totalAmountPaid.add(transactionEntryAmount);
					}
					else if (PaymentTransactionType.REFUND_FOLLOW_ON.equals(paymentTransactionType))
					{
						totalAmountPaid = totalAmountPaid.subtract(transactionEntryAmount);
					}
				}
			}
		}
		return totalAmountPaid;
	}

}
