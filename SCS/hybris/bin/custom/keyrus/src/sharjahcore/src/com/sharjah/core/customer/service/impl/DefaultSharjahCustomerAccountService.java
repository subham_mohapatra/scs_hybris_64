/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sharjah.core.customer.service.impl;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.enums.PhoneContactInfoType;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.PhoneContactInfoModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.daos.LanguageDao;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.impl.UniqueAttributesInterceptor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.sharjah.core.customer.dao.SharjahNationalityDao;
import com.sharjah.core.customer.service.SharjahCustomerAccountService;
import com.sharjah.core.model.NationalityModel;
import com.sharjah.core.model.ShareholderModel;
import com.sharjah.shareholder.service.impl.DefaultSharjahShareholderService;


/**
 * Customer Account Service extending {@link DefaultCustomerAccountService} and implements the
 * {@link SharjahCustomerAccountService} interface
 */
public class DefaultSharjahCustomerAccountService extends DefaultCustomerAccountService implements SharjahCustomerAccountService
{
	@Resource(name = "languageDao")
	private LanguageDao languageDao;
	
	@Resource
	private CustomerNameStrategy customerNameStrategy;
	
	@Resource
	private SharjahNationalityDao sharjahNationalityDao;

	@Resource(name = "sharjahShareholderService")
	private DefaultSharjahShareholderService defaultSharjahShareholderService;
	
	@Override
	public void updateProfile(final CustomerModel customerModel, final CustomerData customerData) throws DuplicateUidException
	{


		customerModel.setHowDidYouKnow(customerData.getHowDidYouFindUs());
		customerModel.setMaritalStatus(customerData.getMaritalStatus());
		customerModel.setNumberOfDependants(customerData.getNumberOfDependants());
		customerModel.setDateOfBirth(customerData.getDateOfBirth());
		customerModel.setHouseholdSize(customerData.getHouseHoldSize());
		customerModel.setGender(Gender.MALE.toString().equals(customerData.getGender().getCode()) ? Gender.MALE : Gender.FEMALE);
		customerModel.setNationality(customerData.getNationality());
		customerModel.setLandline(customerData.getLandline());
		customerModel.setFirstName(customerData.getFirstName());
		customerModel.setLastName(customerData.getLastName());
		
		final List<LanguageModel> languages = languageDao.findLanguagesByCode(customerData.getLanguage().getIsocode());
		
		LanguageModel language = new LanguageModel();
		
		if (!CollectionUtils.isEmpty(languages))
		{
			language = languages.get(0);
		}
		else
		{
			language = getCommonI18NService().getCurrentLanguage();
		}
		
		if (StringUtils.isNotBlank(customerData.getFirstName()) && StringUtils.isNotBlank(customerData.getLastName()))
		{
			customerModel.setName(getCustomerNameStrategy().getName(customerData.getFirstName(), customerData.getLastName()));
		}
		
		customerModel.setSessionLanguage(language);
		
		PhoneContactInfoModel phoneInfo = customerModel.getPhoneContactInfo();
		
		if(phoneInfo == null){
			phoneInfo = getModelService().create(PhoneContactInfoModel.class);
			phoneInfo.setUser(customerModel);
			phoneInfo.setType(PhoneContactInfoType.MOBILE);
		}
		
		phoneInfo.setPhoneNumber(customerData.getMobileNumber());
		phoneInfo.setPrefix(customerData.getMobileprefixNumber());

		customerModel.setPhoneContactInfo(phoneInfo);
		
		final String membershipNumber = customerData.getShareholderData() != null ? customerData.getShareholderData().getShareholderNumber() : null;

		if (membershipNumber != null && !membershipNumber.isEmpty())
		{
			final ShareholderModel shareholder = defaultSharjahShareholderService.findShareholderByNumber(membershipNumber);
			if (shareholder != null)
			{
				customerModel.setShareholder(shareholder);
			}
		}
		else
		{
			customerModel.setShareholder(null);
		}
		
		internalSaveCustomer(customerModel);
		internalSavePhoneContactInfo(phoneInfo);
	}
	
	@Override
	public List<NationalityModel> getAllNationality()
	{
		final NationalityModel emiratesNationality = sharjahNationalityDao.getNationalityByCode("971");
		final List<NationalityModel> allNationalities = sharjahNationalityDao.getAllNationality();
		final List<NationalityModel> unmodifiableAllNationalities = new ArrayList<NationalityModel>(allNationalities);
		unmodifiableAllNationalities.add(0, emiratesNationality);
		return unmodifiableAllNationalities;
	}

	@Override
	public NationalityModel getNationalityByCode(final String code)
	{
		return sharjahNationalityDao.getNationalityByCode(code);
	}
	/**
	 * 
	 * @param phoneContactInfo
	 * @throws DuplicateUidException
	 */
	protected void internalSavePhoneContactInfo(final PhoneContactInfoModel phoneContactInfo) throws DuplicateUidException
	{
		try
		{
			getModelService().save(phoneContactInfo);
		}
		catch (final ModelSavingException e)
		{
			if (e.getCause() instanceof InterceptorException
					&& ((InterceptorException) e.getCause()).getInterceptor().getClass().equals(UniqueAttributesInterceptor.class))
			{
				throw new DuplicateUidException(phoneContactInfo.getCode(), e);
			}
			else
			{
				throw e;
			}
		}
		catch (final AmbiguousIdentifierException e)
		{
			throw new DuplicateUidException(phoneContactInfo.getCode(), e);
		}
	}
	
}
