package com.sharjah.core.customer.service.impl;

import static com.sharjah.core.util.SharjahStringUtils.safeFormatDate;
import static com.sharjah.core.util.SharjahStringUtils.safeFormatEnum;
import static com.sharjah.core.util.SharjahStringUtils.safeFormatNumber;
import static org.apache.commons.lang3.StringUtils.defaultString;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.sharjah.core.customer.service.SharjahCustomerExportService;
import com.sharjah.core.model.ShareholderModel;

/**
 * @author Pablo François
 */
@Service
public class DefaultSharjahCustomerExportService implements SharjahCustomerExportService {

	private static final Logger LOGGER = Logger.getLogger(DefaultSharjahCustomerExportService.class);

	private static final String CUSTOMER_EXPORT_FOLDER = "customer.export.folder";
	private static final String CUSTOMER_EXPORT_HEADER = "customer.export.header";
	private static final String FILE_PREFIX = "customer_";
	private static final String FILE_EXTENSION = ".csv";
	private static final String SEPARATOR = ";";
	private static final String DATE_FORMAT = "dd.MM.yyyy";

	private static final char SEPARATOR_CHAR = ';';

	@Override
	public void exportCustomers(final List<CustomerModel> customers) throws IOException {
		final List<String[]> lines = new ArrayList<>();
		lines.add(Config.getParameter(CUSTOMER_EXPORT_HEADER).split(SEPARATOR));

		LOGGER.debug("Start generating CSV line for " + customers.size() + " customer(s) ");

		for (final CustomerModel customer : customers) {
			lines.add(generateCustomerLine(customer));
		}

		LOGGER.debug("Finish generating CSV line for " + customers.size() + " customer(s) ");

		final String exportCustomerDir = Config.getParameter(CUSTOMER_EXPORT_FOLDER);
		FileUtils.forceMkdir(new File(exportCustomerDir));

		final File file = new File(exportCustomerDir, FILE_PREFIX + new Date().getTime() + FILE_EXTENSION);
		try (final Writer writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
				final CSVWriter csvWriter = new CSVWriter(writer, SEPARATOR_CHAR);) {
			csvWriter.writeAll(lines, false);
			LOGGER.debug("File created and content exported to : " + exportCustomerDir);
		}
	}

	/**
	 * 
	 * @param customer
	 * @return
	 */
	private String[] generateCustomerLine(final CustomerModel customer) {
		final ShareholderModel shareholder = customer.getShareholder();
		final AddressModel deliveryAddress = findDeliveryAddress(customer);
		final AddressModel paymentAddress = findPaymentAddress(customer);

		final List<String> customerFields = new ArrayList<>();
		customerFields.add(customer.getUid());
		customerFields.add(defaultString(shareholder != null ? shareholder.getShareholderNumber() : ""));
		customerFields.add(defaultString(customer.getName()));
		customerFields.add(defaultString(customer.getContactEmail()));
		customerFields.add(defaultString(deliveryAddress != null ? deliveryAddress.getCellphone() : ""));
		customerFields.add(defaultString(deliveryAddress != null ? deliveryAddress.getPhone1() : ""));
		customerFields.add(safeFormatEnum(customer.getGender()));
		customerFields.add(safeFormatDate(customer.getDateOfBirth(), DATE_FORMAT));
		customerFields.add(safeFormatEnum(customer.getMaritalStatus()));
		customerFields.add(safeFormatNumber(customer.getNumberOfDependants()));
		customerFields.add(defaultString(customer.getNationality()));
		customerFields.add(safeFormatEnum(shareholder != null ? shareholder.getLoyaltyLevel() : null));

		addAddressElements(customerFields, deliveryAddress);
		addAddressElements(customerFields, paymentAddress);

		return customerFields.toArray(new String[0]);
	}

	/**
	 * 
	 * @param customerFields
	 * @param address
	 */
	private void addAddressElements(final List<String> customerFields, final AddressModel address)
	{
		customerFields.add(defaultString(address != null ? address.getAppartment() : null));
		customerFields.add(defaultString(address != null ? address.getBuilding() : null));
		customerFields.add(defaultString(address != null ? address.getStreetname() : null));
		// District = Area
		customerFields.add(defaultString(address != null ? address.getDistrict() : null));
		// Town = City
		customerFields.add(defaultString(address != null ? address.getTown() : null));
	}

	/**
	 * 
	 * @param customer
	 * @return
	 */
	private AddressModel findDeliveryAddress(final CustomerModel customer)
	{
		AddressModel deliveryAddress = customer.getDefaultShipmentAddress();
		if (deliveryAddress == null && CollectionUtils.isNotEmpty(customer.getAddresses()))
		{
			deliveryAddress = customer.getAddresses().stream()
					.filter(address -> address.getShippingAddress())
					.findFirst()
					.orElse(null);
		}
		return deliveryAddress;
	}

	/**
	 * 
	 * @param customer
	 * @return
	 */
	private AddressModel findPaymentAddress(final CustomerModel customer)
	{
		AddressModel paymentAddress = customer.getDefaultPaymentAddress();
		if (paymentAddress == null && CollectionUtils.isNotEmpty(customer.getAddresses()))
		{
			paymentAddress = customer.getAddresses().stream()
					.filter(address -> address.getBillingAddress())
					.findFirst()
					.orElse(null);
		}
		return paymentAddress;
	}
}
