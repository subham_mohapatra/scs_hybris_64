package com.sharjah.core.payment.cybersource.impl;

import de.hybris.platform.acceleratorservices.model.payment.CCPaySubValidationModel;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionResult;
import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.acceleratorservices.payment.enums.DecisionsEnum;
import de.hybris.platform.acceleratorservices.payment.impl.DefaultAcceleratorPaymentService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sharjah.core.customer.dao.impl.SharjahCustomerAccountDao;
import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;
import com.sharjah.core.util.CyberSourceUtils;


/**
 * @author Pablo François
 */
public class SharjahCyberSourcePaymentService extends DefaultAcceleratorPaymentService
{
	private static final Logger LOG = Logger.getLogger(DefaultAcceleratorPaymentService.class);

	@Resource
	private CustomerAccountService customerAccountService;
	@Resource
	private SharjahCustomerAccountDao sharjahCustomerAccountDao;

	@Override
	public void handleCreateSubscriptionCallback(final Map<String, String> parameters)
	{
		parameters.put("VerifyTransactionSignature()", "true");

		final String decision = parameters.get(CyberSourceConstants.DECISION);
		final String signature = parameters.get(CyberSourceConstants.SIGNATURE);
		final String signedFieldNames = parameters.get(CyberSourceConstants.SIGNED_FIELD_NAMES);

		if (decision != null)
		{
			if (DecisionsEnum.ACCEPT.name().equalsIgnoreCase(decision))
			{
				final SubscriptionInfoData subscriptionInfoData = new SubscriptionInfoData();
				subscriptionInfoData.setSubscriptionIDPublicSignature(signature);
				subscriptionInfoData.setSubscriptionSignedValue(CyberSourceUtils.buildDataToSign(parameters));
				final CreateSubscriptionResult response = new CreateSubscriptionResult();
				response.setSubscriptionInfoData(subscriptionInfoData);

				if (getSignatureValidationStrategy().validateSignature(subscriptionInfoData))
				{
					markPaymentValidated(parameters);
				}
				else
				{
					LOG.error(String.format(
							"Cannot create subscription. Subscription signature does not match signed_field_names=%s signature=%s",
							signedFieldNames, signature));
				}
			}
		}
		else
		{
			LOG.warn("Received CyberSource callback without the necessary elements: " + parameters);
		}
	}

	protected void markPaymentValidated(final Map<String, String> params)
	{
		String paymentId = params.get(CyberSourceConstants.PAYMENT_TOKEN);
		CreditCardPaymentInfoModel paymentInfo = null;
		if (paymentId != null)
		{
			paymentInfo = getCreditCardPaymentSubscriptionDao().findCreditCartPaymentBySubscription(paymentId);
		}
		if (paymentInfo == null)
		{
			paymentId = params.get(CyberSourceConstants.REQ_TRANSACTION_UUID);
			paymentInfo = sharjahCustomerAccountDao.findCreditCardPaymentInfoByCode(paymentId);
		}

		if (paymentInfo != null)
		{
			// Mark the CreditCardPaymentInfoModel as validated
			paymentInfo.setSubscriptionValidated(true);
			getModelService().save(paymentInfo);
			LOG.info("Payment marked as validated: " + paymentId);
		}
		else
		{
			// No CreditCardPaymentInfoModel exists yet, create a CCPaySubValidationModel to store the validated subscription ID
			final CCPaySubValidationModel subscriptionInfo = getModelService().create(CCPaySubValidationModel.class);
			subscriptionInfo.setSubscriptionId(paymentId);
			getModelService().save(subscriptionInfo);
		}
	}

}
