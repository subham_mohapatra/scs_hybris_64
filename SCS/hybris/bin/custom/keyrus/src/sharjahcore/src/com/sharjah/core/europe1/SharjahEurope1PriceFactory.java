package com.sharjah.core.europe1;

import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.jalo.TaxRow;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.Tax;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.StandardDateRange;
import de.hybris.platform.util.TaxValue;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.apache.log4j.Logger;


/**
 * Define the logic to get the right Price.
 * SCR-1295
 *
 * @author Ben Amor Bassem
 *
 */
public class SharjahEurope1PriceFactory extends Europe1PriceFactory
{
	private static final Logger LOG = Logger.getLogger(SharjahEurope1PriceFactory.class);
	private RetrieveChannelStrategy channelStrategy;


	@Override
	public List getDiscountValues(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		final SessionContext ctx = this.getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		return Europe1Tools.createDiscountValueList(this.matchDiscountRows(entry.getProduct(ctx), this.getPDG(ctx, entry),
				order.getUser(ctx), this.getUDG(ctx, entry), order.getCurrency(ctx), new Date(), -1));
	}

	@Override
	public List getDiscountValues(final AbstractOrder order) throws JaloPriceFactoryException
	{
		final SessionContext ctx = this.getSession().getSessionContext();
		return Europe1Tools.createDiscountValueList(this.matchDiscountRows((Product) null, (EnumerationValue) null,
				order.getUser(ctx), this.getUDG(ctx, order), order.getCurrency(ctx), new Date(), -1));
	}

	@Override
	public Collection getTaxValues(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		SessionContext ctx;
		AbstractOrder order;
		ArrayList ret;
		Currency reqCurr;
		String reqIsoCode;
		Iterator arg8;
		boolean hasValue;
		if (this.isCachingTaxes())
		{
			ctx = this.getSession().getSessionContext();
			order = entry.getOrder(ctx);
			final Collection rows1 = this.getCachedTaxes(entry.getProduct(ctx), this.getPTG(ctx, entry), order.getUser(ctx),
					this.getUTG(ctx, entry), new Date());
			if (rows1.isEmpty())
			{
				return Collections.EMPTY_LIST;
			}
			else
			{
				ret = new ArrayList(rows1.size());
				reqCurr = order.getCurrency(ctx);
				reqIsoCode = reqCurr.getIsoCode();

				TaxValue tax1;
				for (arg8 = rows1.iterator(); arg8.hasNext(); ret.add(tax1))
				{
					final TaxValue tr1 = (TaxValue) arg8.next();
					tax1 = tr1;
					hasValue = tr1.isAbsolute();
					if (hasValue)
					{
						final String abs1 = tr1.getCurrencyIsoCode();
						if (abs1 != null && !abs1.equals(reqIsoCode))
						{
							tax1 = new TaxValue(tr1.getCode(),
									C2LManager.getInstance().getCurrencyByIsoCode(abs1).convertAndRound(reqCurr, tr1.getValue()), true,
									reqIsoCode);
						}
					}
				}

				return ret;
			}
		}
		else
		{
			ctx = this.getSession().getSessionContext();
			order = entry.getOrder(ctx);
			final List rows = this.matchTaxRows(entry.getProduct(ctx), this.getPTG(ctx, entry), order.getUser(ctx),
					this.getUTG(ctx, entry), new Date(), -1);
			if (rows.isEmpty())
			{
				return Collections.EMPTY_LIST;
			}
			else
			{
				ret = new ArrayList(rows.size());
				reqCurr = order.getCurrency(ctx);
				reqIsoCode = reqCurr.getIsoCode();

				Tax tax;
				boolean abs;
				double value;
				String isoCode;
				for (arg8 = rows.iterator(); arg8.hasNext(); ret.add(new TaxValue(tax.getCode(), value, abs, isoCode)))
				{
					final TaxRow tr = (TaxRow) arg8.next();
					tax = tr.getTax(ctx);
					hasValue = tr.hasValue(ctx);
					abs = hasValue ? tr.isAbsoluteAsPrimitive() : tax.isAbsolute().booleanValue();
					if (abs)
					{
						final Currency rowCurr = abs ? (hasValue ? tr.getCurrency(ctx) : tax.getCurrency(ctx)) : null;
						if (rowCurr != null && !rowCurr.equals(reqCurr))
						{
							final double taxDoubleValue = hasValue ? tr.getValue(ctx).doubleValue() : tax.getValue().doubleValue();
							value = rowCurr.convertAndRound(reqCurr, taxDoubleValue);
						}
						else
						{
							value = hasValue ? tr.getValue(ctx).doubleValue() : tax.getValue().doubleValue();
						}

						isoCode = reqIsoCode;
					}
					else
					{
						value = hasValue ? tr.getValue(ctx).doubleValue() : tax.getValue().doubleValue();
						isoCode = null;
					}
				}

				return ret;
			}
		}
	}

	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		final SessionContext ctx = this.getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		Currency currency = null;
		EnumerationValue productGroup = null;
		User user = null;
		EnumerationValue userGroup = null;
		Unit unit = null;
		long quantity = 0L;
		boolean net = false;
		Date date = null;
		final Product product = entry.getProduct();
		final boolean giveAwayMode = entry.isGiveAway(ctx).booleanValue();
		final boolean entryIsRejected = entry.isRejected(ctx).booleanValue();
		PriceRow row;
		if (giveAwayMode && entryIsRejected)
		{
			row = null;
		}
		else
		{
			row = matchPriceRowForPrice(ctx, product, productGroup = this.getPPG(ctx, product), user = order.getUser(),
					userGroup = this.getUPG(ctx, user), quantity = entry.getQuantity(ctx).longValue(), unit = entry.getUnit(ctx),
					currency = order.getCurrency(ctx), date = new Date(), net = order.isNet().booleanValue(), giveAwayMode);
		}

		if (row != null)
		{
			final Currency msg1 = row.getCurrency();
			double price;
			if (currency.equals(msg1))
			{
				price = row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive();
			}
			else
			{
				price = msg1.convert(currency, row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive());
			}

			final Unit priceUnit = entry.getUnit();
			final Unit entryUnit = entry.getUnit();
			final double convertedPrice = priceUnit.convertExact(entryUnit, price);
			return new PriceValue(currency.getIsoCode(), convertedPrice, row.isNetAsPrimitive());
		}
		else if (giveAwayMode)
		{
			return new PriceValue(order.getCurrency(ctx).getIsoCode(), 0.0D, order.isNet().booleanValue());
		}
		else
		{
			final String msg = Localization
					.getLocalizedString("exception.europe1pricefactory.getbaseprice.jalopricefactoryexception1", new Object[]
			{ product, productGroup, user, userGroup, Long.toString(quantity), unit, currency, date, Boolean.toString(net) });
			throw new JaloPriceFactoryException(msg, 0);
		}
	}

	@Override
	public PriceRow matchPriceRowForPrice(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final long qtd, final Unit unit, final Currency currency,
			final Date date, final boolean net, final boolean giveAwayMode) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price without product and product group - at least one must be present", 0);
		}
		else if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException("cannot match price without user and user group - at least one must be present", 0);
		}
		else if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price without currency", 0);
		}
		else if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price without date", 0);
		}
		else if (unit == null)
		{
			throw new JaloPriceFactoryException("cannot match price without unit", 0);
		}
		else
		{
			final Collection rows = this.queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
			if (!rows.isEmpty())
			{
				final PriceRowChannel channel = this.channelStrategy.getChannel(ctx);
				final List list = this.filterPriceRows4Price(rows, qtd, unit, currency, date, giveAwayMode, channel);
				if (list.isEmpty())
				{
					return null;
				}
				else if (list.size() == 1)
				{
					return (PriceRow) list.get(0);
				}
				else
				{
					Collections.sort(list, new PriceRowMatchComparator(currency, net, unit));
					return (PriceRow) list.get(0);
				}
			}
			else
			{
				return null;
			}
		}
	}

	@Override
	protected List<PriceRow> filterPriceRows4Price(final Collection<PriceRow> rows, final long _quantity, final Unit unit,
			final Currency curr, final Date date, final boolean giveAwayMode, final PriceRowChannel channel)
	{
		if (rows.isEmpty())
		{
			return Collections.EMPTY_LIST;
		}
		else
		{
			final Currency base = curr.isBase().booleanValue() ? null : C2LManager.getInstance().getBaseCurrency();
			final Set convertible = unit.getConvertibleUnits();
			final ArrayList ret = new ArrayList(rows);
			final long quantity = _quantity == 0L ? 1L : _quantity;
			boolean hasChannelRowMatching = false;
			ListIterator it = ret.listIterator();

			while (true)
			{
				PriceRow priceRow;
				while (it.hasNext())
				{
					priceRow = (PriceRow) it.next();
					if (quantity < priceRow.getMinqtdAsPrimitive())
					{
						it.remove();
					}
					else
					{
						final Currency currency = priceRow.getCurrency();
						if (!curr.equals(currency) && (base == null || !base.equals(currency)))
						{
							it.remove();
						}
						else
						{
							//							Unit user = priceRow.getUnit();
							//							if (!unit.equals(user) && !convertible.contains(user)) {
							//								it.remove();
							//							} else {
							final StandardDateRange dateRange = priceRow.getDateRange();
							if (dateRange != null && !dateRange.encloses(date))
							{
								it.remove();
							}
							else if (giveAwayMode != priceRow.isGiveAwayPriceAsPrimitive())
							{
								it.remove();
							}
							else if (channel != null && priceRow.getChannel() != null
									&& !priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
							{
								it.remove();
							}
							else if (channel != null && priceRow.getChannel() != null
									&& priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
							{
								hasChannelRowMatching = true;
							}
							//}
						}
					}
				}

				if (hasChannelRowMatching && ret.size() > 1)
				{
					it = ret.listIterator();

					while (it.hasNext())
					{
						priceRow = (PriceRow) it.next();
						if (priceRow.getChannel() == null)
						{
							it.remove();
						}
					}
				}

				return ret;
			}
		}
	}

	protected class PriceRowMatchComparator implements Comparator<PriceRow>
	{
		private final Currency curr;
		private final boolean net;
		//	private final Unit unit;

		protected PriceRowMatchComparator(final Currency curr, final boolean net, final Unit unit)
		{
			this.curr = curr;
			this.net = net;
			//this.unit = unit;
		}

		@Override
		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final int matchValue1 = row1.getMatchValueAsPrimitive();
			final int matchValue2 = row2.getMatchValueAsPrimitive();
			if (matchValue1 != matchValue2)
			{
				return matchValue2 - matchValue1;
			}
			else
			{
				final boolean c1Match = this.curr.equals(row1.getCurrency());
				final boolean c2Match = this.curr.equals(row2.getCurrency());
				if (c1Match != c2Match)
				{
					return c1Match ? -1 : 1;
				}
				else
				{
					final boolean n1Match = this.net == row1.isNetAsPrimitive();
					final boolean n2Match = this.net == row2.isNetAsPrimitive();
					if (n1Match != n2Match)
					{
						return n1Match ? -1 : 1;
					}
					else
					{
						//final boolean u1Match = this.unit.equals(row1.getUnit());
						//final boolean u2Match = this.unit.equals(row2.getUnit());
						//						if (u1Match != u2Match)
						//						{
						//							return u1Match ? -1 : 1;
						//						}
						//else
						//{
						final long min1 = row1.getMinqtdAsPrimitive();
						final long min2 = row2.getMinqtdAsPrimitive();
						if (min1 != min2)
						{
							return min1 > min2 ? -1 : 1;
						}
						else
						{
							final boolean row1Range = row1.getStartTime() != null;
							final boolean row2Range = row2.getStartTime() != null;
							if (row1Range != row2Range)
							{
								return row1Range ? -1 : 1;
							}
							else
							{
								LOG.warn("found ambigous price rows " + row1 + " and " + row2 + " - using PK to distinguish");
								return row1.getPK().compareTo(row2.getPK());
							}
						}
						//}
					}
				}
			}
		}
	}

	public RetrieveChannelStrategy getChannelStrategy()
	{
		return channelStrategy;
	}

	public void setChannelStrategy(final RetrieveChannelStrategy channelStrategy)
	{
		this.channelStrategy = channelStrategy;
	}

}
