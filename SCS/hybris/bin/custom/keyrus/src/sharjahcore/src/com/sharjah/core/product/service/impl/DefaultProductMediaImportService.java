package com.sharjah.core.product.service.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.media.service.SharjahMediaContainerService;
import com.sharjah.core.product.service.MediaImportException;
import com.sharjah.core.product.service.ProductMediaImportService;
import com.sharjah.core.product.service.SharjahProductService;


/**
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultProductMediaImportService implements ProductMediaImportService
{

	private static final Logger LOG = LogManager.getLogger(DefaultProductMediaImportService.class);

	private static final String SEPARATOR = "\\|";
	private static final String UNDERSCORE = "_";

	private static final Pattern IMAGE_FILE_PATTERN = Pattern.compile("^([\\w]+)(_)([\\w]+)(_)([0-9]{3})\\.(pdf|jpg)$");
	private static final String[] MEDIA_FORMAT_CODES = Config.getString("media.import.cronjob.formats", "").split(SEPARATOR);
	private static final boolean KEEP_FILES = Config.getBoolean("media.import.cronjob.storeprocessedmedias", true);
	private static final String MIME_TYPE_JPG = Config.getParameter("media.import.mimetype.jpg");
	private static final String MIME_TYPE_PDF = Config.getParameter("media.import.mimetype.pdf");
	
	private static final String PICTURE_FORMAT = Config.getString("media.import.cronjob.formats.principal.picture", "medium");
	private static final String THUMBNAIL_FORMAT = Config.getString("media.import.cronjob.formats.principal.thumbnail", "small");
	private static final String DETAIL_FORMAT = Config.getString("media.import.cronjob.formats.detail", "xxl");
	private static final String NORMAL_FORMAT = Config.getString("media.import.cronjob.formats.normal", "large");
	private static final String MAIN_MEDIA_SUFIX = Config.getString("media.import.cronjob.main", "001");

	private CatalogVersionService catalogVersionService;
	private MediaService mediaService;
	private SharjahProductService productService;
	private ModelService modelService;
	private SharjahMediaContainerService mediaContainerService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sharjah.core.product.service.ProductMediaImportService#importMedia(java.lang.String)
	 */
	@Override
	public void importMedia(final String brandName)
	{
		//read all the files in the import folder
		final Map<String, List<File>> mediasPathMap = createMediaMap(brandName);

		if (MapUtils.isNotEmpty(mediasPathMap))
		{
			final CatalogVersionModel sharjahCatalogVersion = catalogVersionService
					.getCatalogVersion(SharjahCoreConstants.SHARJAH_PRODUCT_CATALOG_CODE, SharjahCoreConstants.CATALOG_STAGED_CODE);
			final MediaFolderModel sharjahMediaFolder = mediaService.getFolder(SharjahCoreConstants.SHARJAH_MEDIA_FOLDER_CODE);

			final MediaFormatModel[] mediaFormats = new MediaFormatModel[MEDIA_FORMAT_CODES.length];
			for (int i = 0; i < MEDIA_FORMAT_CODES.length; i++)
			{
				mediaFormats[i] = mediaService.getFormat(MEDIA_FORMAT_CODES[i]);
			}

			final int totalRecords = mediasPathMap.size();
			int currentRecord = 0;
			for (final Map.Entry<String, List<File>> entry : mediasPathMap.entrySet())
			{
				currentRecord = currentRecord + 1;
				importAndCreateMedia(entry, currentRecord, totalRecords, sharjahCatalogVersion, mediaFormats, sharjahMediaFolder,
						brandName);
			}
		}
	}

	/**
	 * 
	 * @param entry
	 * @param currentRecord
	 * @param totalRecords
	 * @param sharjahCatalogVersion
	 * @param mediaFormats
	 * @param sharjahMediaFolder
	 * @param brandName
	 */
	private void importAndCreateMedia(final Map.Entry<String, List<File>> entry, final int currentRecord, final int totalRecords,
			final CatalogVersionModel sharjahCatalogVersion, final MediaFormatModel[] mediaFormats,
			final MediaFolderModel sharjahMediaFolder, final String brandName)
	{
		final String productBarcode = entry.getKey();
		final List<File> mediaFiles = entry.getValue();

		Collections.sort(mediaFiles, new SharjahFileComparator());

		String productLabel = "[" + productBarcode + "]";
		if (LOG.isInfoEnabled())
		{
			LOG.info("Importing medias for product " + productLabel + " (" + currentRecord + "/" + totalRecords + ").");
		}
		try
		{
			ProductModel product = productService.findProductByEAN(sharjahCatalogVersion, productBarcode);
			if (product == null)
			{
				product = productService.getProductForCode(sharjahCatalogVersion, productBarcode);
			}
			productLabel = product.getCode() + " " + productLabel;
			createProductMedia(product, mediaFiles, mediaFormats, sharjahMediaFolder);

			handleSuccess(productLabel, mediaFiles, SharjahCoreConstants.getCronJobMediaSuccessFolder(brandName));
		}
		catch (final MediaImportException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Error [" + productBarcode + "]");
			}
			handleError(productLabel, mediaFiles, e, SharjahCoreConstants.getCronJobMediaErrorFolder(brandName));
		}
		catch (final UnknownIdentifierException e)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("There is no product with code [" + productBarcode + "]");
			}
			handleError(productLabel, mediaFiles, e, SharjahCoreConstants.getCronJobMediaErrorFolder(brandName));
		}
	}

	/**
	 * Creates the media to the product
	 * 
	 * @param product
	 * @param mediaFiles
	 * @param mediaFormats
	 * @param mediaFolder
	 * @throws MediaImportException
	 */
	private void createProductMedia(final ProductModel product, final List<File> mediaFiles, final MediaFormatModel[] mediaFormats,
			final MediaFolderModel mediaFolder) throws MediaImportException
	{

		for (final File mediaFile : mediaFiles)
		{
			final String fileName = mediaFile.getName();
			final Matcher matcher = IMAGE_FILE_PATTERN.matcher(fileName);
			if (matcher.matches())
			{
				createMediaContainerAndModel(product, mediaFormats, mediaFolder, matcher, mediaFile);
			}
		}
		product.setModifiedtime(new Date());
		modelService.save(product);
	}

	/**
	 * Create or update the Media container and model
	 * 
	 * @param product
	 * @param mediaFormats
	 * @param mediaFolder
	 * @param matcher
	 * @param mediaFile
	 * @throws MediaImportException
	 */
	private void createMediaContainerAndModel(final ProductModel product,
			final MediaFormatModel[] mediaFormats, final MediaFolderModel mediaFolder, final Matcher matcher, final File mediaFile)
			throws MediaImportException
	{
		final String fileName = mediaFile.getName();
		final StringBuilder sb = new StringBuilder();
		sb.append(product.getCode());
		sb.append(UNDERSCORE);
		sb.append(matcher.group(5));

		final String baseName = sb.toString();
		final String mediaFormat = matcher.group(3);
		final String mediaSufix = matcher.group(5);

		try
		{
			final MediaContainerModel container = getOrCreateMediaContainer(baseName, product.getCatalogVersion());
			final MediaFormatModel format = getFormat(mediaFormat, mediaFormats);

			if (format != null)
			{
				final MediaModel mediaModel = updateOrCreateMediaModel(baseName, mediaFile, container, mediaFolder, format,
						isPDF(fileName));

				setPictureAndThumbnail(product, mediaModel, mediaSufix, format);
				updateProductMediaListList(product, mediaModel);
				updateGalleryList(container, product);

			}
		}
		catch (final Exception e)
		{
			throw new MediaImportException("Error when try to create media with qualifier '" + baseName + "'.", e);
		}
	}

	/**
	 * Is the file a pdf file
	 *
	 * @param fileName
	 * @return
	 */
	public boolean isPDF(final String fileName)
	{
		return fileName.endsWith("pdf");
	}

	/**
	 * Return the format as MediaFormatModel
	 * 
	 * @param formatCode
	 * @param mediaFormats
	 * @return MediaFormatModel
	 */
	public MediaFormatModel getFormat(final String formatCode, final MediaFormatModel[] mediaFormats)
	{
		for (final MediaFormatModel format : mediaFormats)
		{
			if (format.getQualifier().equals(formatCode))
			{
				return format;
			}
		}
		LOG.warn("Format [" + formatCode + "] is not known in the system");
		return null;
	}

	/**
	 * Updates the media in the right container
	 * 
	 * @param mediaContainer
	 * @param product
	 */
	private void updateGalleryList(final MediaContainerModel mediaContainer, final ProductModel product)
	{
		boolean updateGalleryImage = false;
		final List<MediaContainerModel> galleryImageList = new ArrayList<>();
		galleryImageList.addAll(product.getGalleryImages());

		for (final MediaContainerModel galleryImage : product.getGalleryImages())
		{
			if (galleryImage.getQualifier().equals(mediaContainer.getQualifier()))
			{
				updateGalleryImage = true;
				break;
			}
		}
		if (!updateGalleryImage)
		{
			galleryImageList.add(mediaContainer);
			product.setGalleryImages(galleryImageList);
		}
	}

	/**
	 * Sets the primary and thumbnai by format Should be the MAIN_MEDIA_SUFIX
	 * 
	 * @param product
	 * @param picture
	 * @param mediaSufix
	 * @param mediaFormat
	 */
	private void setPictureAndThumbnail(final ProductModel product, final MediaModel picture, final String mediaSufix,
			final MediaFormatModel mediaFormat)
	{
		Validate.notEmpty(PICTURE_FORMAT);
		Validate.notEmpty(THUMBNAIL_FORMAT);
		if (mediaSufix.equals(MAIN_MEDIA_SUFIX))
		{
			if (PICTURE_FORMAT.equals(mediaFormat.getQualifier()))
			{
				product.setPicture(picture);
			}
			else if (THUMBNAIL_FORMAT.equals(mediaFormat.getQualifier()))
			{
				product.setThumbnail(picture);
			}

		}
	}

	/**
	 * Organize the media in the product, depending of the format
	 * 
	 * @param product
	 * @param media
	 */
	private void updateProductMediaListList(final ProductModel product, final MediaModel media)
	{
		boolean updateMedia = false;
		final List<MediaModel> mediaList = new ArrayList<>();

		if (THUMBNAIL_FORMAT.equals(media.getMediaFormat().getQualifier()))
		{
			mediaList.addAll(product.getThumbnails());

			updateMedia = checkMedia(product.getThumbnails(), media);
			if (!updateMedia)
			{
				mediaList.add(media);
				product.setThumbnails(mediaList);
			}
		}
		else if (DETAIL_FORMAT.equals(media.getMediaFormat().getQualifier()))
		{
			mediaList.addAll(product.getDetail());

			updateMedia = checkMedia(product.getDetail(), media);
			if (!updateMedia)
			{
				mediaList.add(media);
				product.setDetail(mediaList);
			}
		}
		else if (NORMAL_FORMAT.equals(media.getMediaFormat().getQualifier()))
		{
			mediaList.addAll(product.getNormal());

			updateMedia = checkMedia(product.getNormal(), media);
			if (!updateMedia)
			{
				mediaList.add(media);
				product.setNormal(mediaList);
			}
		}
		else
		{
			//Gets picture image and the rest of all
			mediaList.addAll(product.getOthers());

			updateMedia = checkMedia(product.getOthers(), media);
			if (!updateMedia)
			{
				mediaList.add(media);
				product.setOthers(mediaList);
			}
		}

	}

	/**
	 * Check if the media already exists in the product, otherwise it will return as update
	 * 
	 * @param medias
	 * @param currentMedia
	 * @return boolean
	 */
	private boolean checkMedia(final Collection<MediaModel> medias, final MediaModel currentMedia)
	{
		boolean updateMedia = false;
		for (final MediaModel mediaModel : medias)
		{
			if (mediaModel.getCode().equals(currentMedia.getCode()))
			{
				updateMedia = true;
				break;
			}
		}
		return updateMedia;
	}

	/**
	 * 
	 * @param baseName
	 * @param mediaFile
	 * @param container
	 * @param mediaFolder
	 * @param format
	 * @return
	 * @throws FileNotFoundException
	 */
	private MediaModel updateOrCreateMediaModel(final String baseName, final File mediaFile, final MediaContainerModel container,
			final MediaFolderModel mediaFolder, final MediaFormatModel format, final boolean isPDF) throws FileNotFoundException
	{
		
		final StringBuilder sbMediaCode = new StringBuilder();
		sbMediaCode.append(baseName);
		sbMediaCode.append(UNDERSCORE);
		sbMediaCode.append(format.getQualifier());
		
		String mime = null;
		if(isPDF)
		{
			mime = MIME_TYPE_PDF;
			sbMediaCode.append("/BASEPDF");
		}
		else
		{
			mime = MIME_TYPE_JPG;
			sbMediaCode.append("/BASEIMAGE");
		}

		MediaModel mediaModel = null;
		InputStream mediaInputStream = null;
		try
		{
			mediaModel = mediaService.getMedia(container.getCatalogVersion(), sbMediaCode.toString());
			if (mediaModel != null)
			{
				mediaInputStream = new BufferedInputStream(new FileInputStream(mediaFile));
				mediaService.setStreamForMedia(mediaModel, mediaInputStream, mediaFile.getName(), mime);
				modelService.refresh(mediaModel);
			}
		}
		catch (final UnknownIdentifierException uie)
		{
			mediaInputStream = new BufferedInputStream(new FileInputStream(mediaFile));
			mediaModel = modelService.create(MediaModel.class);
			mediaModel.setCode(sbMediaCode.toString());
			mediaModel.setMediaContainer(container);
			mediaModel.setCatalogVersion(container.getCatalogVersion());
			mediaModel.setFolder(mediaFolder);
			mediaModel.setMediaFormat(format);
			modelService.save(mediaModel);
			mediaService.setStreamForMedia(mediaModel, mediaInputStream, mediaFile.getName(), mime);
			modelService.refresh(mediaModel);
		}
		finally
		{
			if (mediaInputStream != null)
			{
				try
				{
					mediaInputStream.close();
				}
				catch (final IOException e)
				{
					LOG.info("error in close mediaInputStream",e);
				}
			}
		}
		return mediaModel;
	}

	/**
	 * Gets the media container by image/product Each container contains the list of image in different formats
	 * 
	 * @param qualifier
	 * @param catalogVersion
	 * @return MediaContainerModel
	 */
	private MediaContainerModel getOrCreateMediaContainer(final String qualifier, final CatalogVersionModel catalogVersion)
	{
		MediaContainerModel container = mediaContainerService.findMediaContainerForQualifier(qualifier, catalogVersion);

		//if it does not find, creates it
		if (container == null)
		{
			container = (MediaContainerModel) modelService.create(MediaContainerModel.class);
			container.setQualifier(qualifier);
			container.setCatalogVersion(catalogVersion);
			modelService.save(container);
		}
		return container;
	}

	/**
	 * Reads the files in the folder and group it using the product code, ean
	 * 
	 * @param brand
	 * @return Map<String, List<File>> map grouping the files into product codes
	 */
	private Map<String, List<File>> createMediaMap(final String brand)
	{
		final File[] mediaFiles = getMediaFiles(brand);
		final Map<String, List<File>> mediaMap = new HashMap<>();

		for (final File mediaFile : mediaFiles)
		{
			final String mediaFileName = mediaFile.getName();
			final Matcher matcher = IMAGE_FILE_PATTERN.matcher(mediaFileName);
			if (matcher.matches())
			{
				final String ean = StringUtils.upperCase(matcher.group(1));
				if (!mediaMap.containsKey(ean))
				{
					mediaMap.put(ean, new LinkedList<File>());
				}
				final List<File> eanMedias = mediaMap.get(ean);
				eanMedias.add(mediaFile);
			}
		}
		return mediaMap;
	}

	/**
	 * Reads the files in the media import folder
	 * 
	 * @param brand
	 * @return File[]
	 */
	private File[] getMediaFiles(final String brand)
	{
		final String mediaFolderFormat = "media.%s.import.cronjob.folder.new";
		final String mediaFolder = Config.getString(String.format(mediaFolderFormat, brand), null);
		Validate.notEmpty(mediaFolder, "Parameter " + String.format(mediaFolderFormat, brand) + " cannot be empty!");
		final File file = new File(mediaFolder);

		//creates the folder if it does not exist
		if (!file.exists())
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Folder [" + mediaFolder + "] does not exist. Creating a brand new one !");
			}
			file.mkdirs();
		}

		//check the file pattern to recognize if it is a media to be read
		final FilenameFilter filterByName = new FilenameFilter()
		{
			@Override
			public boolean accept(final File dir, final String name)
			{
				final Matcher matcher = IMAGE_FILE_PATTERN.matcher(name);
				return matcher.matches();
			}
		};
		return file.listFiles(filterByName);
	}

	private void handleSuccess(final String legacySKU, final List<File> mediaFiles, final String successFolder)
	{
		if (KEEP_FILES)
		{
			Validate.notEmpty(successFolder);
			final File destDir = new File(successFolder);
			if (!destDir.exists())
			{
				if (LOG.isInfoEnabled())
				{
					LOG.info("Folder success does not exist [" + successFolder + "], creating a brand new one !");
				}
				destDir.mkdirs();
			}
			moveFilesToFolder(legacySKU, mediaFiles, destDir);
		}
		else
		{
			try
			{
				for (final File srcFile : mediaFiles)
				{
					FileUtils.forceDelete(srcFile);
					LOG.debug("File '" + srcFile.getName() + "' successfully removed.");
				}
			}
			catch (final IOException ioe)
			{
				if (LOG.isWarnEnabled())
				{
					LOG.warn("Error trying to remove product media files. Legacy SKU [" + legacySKU + "].", ioe);
				}
			}
		}
	}

	/**
	 * Move the files to the destination directory
	 * 
	 * @param legacySKU
	 * @param mediaFiles
	 * @param destDir
	 */
	private void moveFilesToFolder(final String legacySKU, final List<File> mediaFiles, final File destDir)
	{
		try
		{
			for (final File srcFile : mediaFiles)
			{
				final File destFile = new File(destDir, srcFile.getName());
				if (destFile.exists())
				{
					destFile.delete();
				}
				FileUtils.moveFile(srcFile, destFile);
				LOG.debug("File '" + srcFile.getName() + "', moved to success folder.");
			}
		}
		catch (final IOException e)
		{
			if (LOG.isWarnEnabled())
			{
				LOG.warn("Error trying to move product media files to processed folder. Legacy SKU [" + legacySKU + "].", e);
			}
		}
	}

	/**
	 * Handle the error medias
	 * 
	 * @param productLabel
	 * @param mediaFiles
	 * @param mce
	 * @param errorFolder
	 */
	private void handleError(final String productLabel, final List<File> mediaFiles, final Exception mce, final String errorFolder)
	{
		LOG.error("Error when try to import product media files. Product '" + productLabel + "'.", mce);
		Validate.notEmpty(errorFolder);
		final File destDir = new File(errorFolder);

		if (!destDir.exists())
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Folder error does not exist [" + errorFolder + "], creating a brand new one !");
			}
			destDir.mkdirs();
		}
		try
		{
			for (final File srcFile : mediaFiles)
			{
				FileUtils.moveFileToDirectory(srcFile, destDir, true);
				if (LOG.isInfoEnabled())
				{
					LOG.info("File '" + srcFile.getName() + "' successfully moved to error folder.");
				}
			}
		}
		catch (final IOException e)
		{
			if (LOG.isWarnEnabled())
			{
				LOG.warn("Error trying to move product media files to error folder Product '" + productLabel + "'.", e);
			}
		}
	}

	/**
	 * 
	 * @param catalogVersionService
	 */
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}

	/**
	 * 
	 * @param productService
	 */
	public void setProductService(final SharjahProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * 
	 * @param mediaService
	 */
	public void setMediaService(final MediaService mediaService)
	{
		this.mediaService = mediaService;
	}

	/**
	 * 
	 * @param mediaContainerService
	 */
	public void setMediaContainerService(final SharjahMediaContainerService mediaContainerService)
	{
		this.mediaContainerService = mediaContainerService;
	}

	/**
	 * 
	 * @param modelService
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
