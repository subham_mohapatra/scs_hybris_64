package com.sharjah.core.shareholder.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sharjah.core.jalo.Shareholder;
import com.sharjah.core.model.ShareholderModel;
import com.sharjah.core.shareholder.dao.ShareholderDao;

/**
 * Sharjah Shareholder Service that implements the {@link ShareholderDao} interface
 */

public class DefaultSharjahShareholderDao extends DefaultGenericDao<ShareholderModel> implements ShareholderDao{

	public DefaultSharjahShareholderDao(final String typecode) {
		super(ShareholderModel._TYPECODE);
	}

	@Override
	public List<ShareholderModel> findShareholderByNumber(final String number) {
		validateParameterNotNull(number, "Shareholder number must not be null!");

		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(Shareholder.SHAREHOLDERNUMBER, number);
		
		return find(parameters);
	}

}
