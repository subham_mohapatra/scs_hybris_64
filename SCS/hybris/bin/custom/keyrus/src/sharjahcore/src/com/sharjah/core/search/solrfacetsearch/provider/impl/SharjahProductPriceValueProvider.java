package com.sharjah.core.search.solrfacetsearch.provider.impl;


import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


/**
 * Resolvers for Price Value Provider
 * 
 * @author Ben Amor Bassem
 * 
 */
public class SharjahProductPriceValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;
	private PriceService priceService;

	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model)
	{
		final ArrayList<FieldValue> fieldValues = new ArrayList<FieldValue>();

		try
		{
			List<String> e = null;
			ProductModel product = null;
			if (!(model instanceof ProductModel))
			{
				throw new FieldValueProviderException("Cannot evaluate price of non-product item");
			}
			else
			{
				product = (ProductModel) model;
				String fieldName;
				Iterator<String> arg13;
				if (indexConfig.getCurrencies().isEmpty())
				{
					final List<PriceInformation> currency = this.priceService.getPriceInformationsForProduct(product);
					if (currency != null && !currency.isEmpty())
					{
						final PriceInformation price = currency.get(0);
						final Double sessionCurrency = Double.valueOf(price.getPriceValue().getValue());
						e = this.getRangeNameList(indexedProperty, sessionCurrency);
						final Collection<String> prices = this.fieldNameProvider.getFieldNames(indexedProperty,
								price.getPriceValue().getCurrencyIso());
						final Iterator<String> fieldNames = prices.iterator();

						while (true)
						{
							while (fieldNames.hasNext())
							{
								final String value = fieldNames.next();
								if (e.isEmpty())
								{
									fieldValues.add(new FieldValue(value, sessionCurrency));
								}
								else
								{
									arg13 = e.iterator();

									while (arg13.hasNext())
									{
										fieldName = arg13.next();
										fieldValues.add(new FieldValue(value, fieldName == null ? sessionCurrency : fieldName));
									}
								}
							}

							return fieldValues;
						}
					}
				}
				else
				{
					final Iterator<CurrencyModel> price1 = indexConfig.getCurrencies().iterator();

					label170: while (price1.hasNext())
					{
						final CurrencyModel currency1 = price1.next();
						final CurrencyModel sessionCurrency1 = this.i18nService.getCurrentCurrency();

						try
						{
							this.i18nService.setCurrentCurrency(currency1);
							final List<PriceInformation> prices1 = this.priceService.getPriceInformationsForProduct(product);
							if (prices1 != null && !prices1.isEmpty())
							{
								final Double value1 = Double.valueOf(prices1.get(0).getPriceValue().getValue());
								e = this.getRangeNameList(indexedProperty, value1, currency1.getIsocode());
								final Collection<String> fieldNames1 = this.fieldNameProvider.getFieldNames(indexedProperty,
										currency1.getIsocode().toLowerCase());
								arg13 = fieldNames1.iterator();

								while (true)
								{
									while (true)
									{
										if (!arg13.hasNext())
										{
											continue label170;
										}

										fieldName = arg13.next();
										if (e.isEmpty())
										{
											fieldValues.add(new FieldValue(fieldName, value1));
										}
										else
										{
											final Iterator<String> arg15 = e.iterator();

											while (arg15.hasNext())
											{
												final String rangeName = arg15.next();
												fieldValues.add(new FieldValue(fieldName, rangeName == null ? value1 : rangeName));
											}
										}
									}
								}
							}
						}
						finally
						{
							this.i18nService.setCurrentCurrency(sessionCurrency1);
						}
					}
				}

				return fieldValues;
			}
		}
		catch (final Exception arg20)
		{
			LOG.error("Cannot evaluate " + indexedProperty.getName() + " using " + this.getClass().getName(),arg20);
		}
		return fieldValues;
	}

	/**
	 * 
	 * @param fieldNameProvider
	 */
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * 
	 * @param priceService
	 */
	public void setPriceService(final PriceService priceService)
	{
		this.priceService = priceService;
	}
}
