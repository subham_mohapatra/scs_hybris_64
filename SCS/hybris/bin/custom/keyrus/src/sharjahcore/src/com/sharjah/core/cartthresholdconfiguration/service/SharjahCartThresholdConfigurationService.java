package com.sharjah.core.cartthresholdconfiguration.service;

import com.sharjah.core.model.CartThresholdConfigurationModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public interface SharjahCartThresholdConfigurationService
{

	/**
	 * Get cart configuration by code.
	 * 
	 * @param code
	 *           The code to find
	 * @return The cart threshold configuration
	 */
	CartThresholdConfigurationModel getCartThresholdConfigurationByCode(String code);
}
