package com.sharjah.core.media.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

import java.util.List;


/**
 * Interface of the DAO for the Sharjah Media Container
 * 
 * @author Luiz.Henriques
 *
 */
public interface SharjahMediaContainerDao
{

	/**
	 * Finds the container by catalog (for products)
	 * 
	 * @param qualifier
	 * @param catalogVersion
	 * @return List<MediaContainerModel>
	 */
	List<MediaContainerModel> findMediaContainersByQualifier(final String qualifier, final CatalogVersionModel catalogVersion);
	
}
