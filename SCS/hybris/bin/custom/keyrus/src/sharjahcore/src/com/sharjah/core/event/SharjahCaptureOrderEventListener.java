package com.sharjah.core.event;

import de.hybris.platform.commerceservices.event.AbstractSiteEventListener;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sharjah.service.event.SharjahCaptureOrderEvent;

/**
 * @author issam maiza
 * 
 * SharjahCaptureOrderEventListener to override onSiteEvent() and shouldHandleEvent() methods
 * 
 */

public class SharjahCaptureOrderEventListener extends  AbstractSiteEventListener<SharjahCaptureOrderEvent>
{

	@Resource(name = "paymentService")
	private PaymentService paymentService;
	
	@Resource(name = "modelService")
	private ModelService modelService;
	

	private static final String SUCCESFULL = "SUCCESFULL";
	private static final Logger LOG = Logger.getLogger(SharjahCaptureOrderEventListener.class);

	
	@Override
	protected void onSiteEvent(final SharjahCaptureOrderEvent event) {
		final OrderModel order = event.getOrder();

		try {
            
			if (order != null && CollectionUtils.isNotEmpty(order.getPaymentTransactions())
					&& PaymentStatus.NOTPAID.equals(order.getPaymentStatus()))
			{
					final PaymentTransactionEntryModel PaymentTransactionEntryModel = paymentService
						.capture(order.getPaymentTransactions().get(0));
					if (SUCCESFULL.equals(PaymentTransactionEntryModel.getTransactionStatusDetails())
						&& PaymentTransactionType.CAPTURE.equals(PaymentTransactionEntryModel.getType()))
					{
						order.setPaymentStatus(PaymentStatus.PAID);
						order.setStatus(OrderStatus.COMPLETED);
						getModelService().save(order);
						LOG.info("order with code : " + order.getCode() + " was success captured and payment status change to PAID");
					}
					else
					{
						LOG.info("order with code : " + order.getCode() + " was failed to captured");
						order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
						getModelService().save(order);
					}
			}
		} catch (final Exception e) {
			LOG.info("order with code : " + order.getCode() + " was failed to captured, message : " + e.getMessage(), e);
			if (e.getCause() != null)
			{
				LOG.info("order with code : " + order.getCode() + " was failed to captured, cause : " + e.getCause().getMessage(), e);
			}
			    order.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
				getModelService().save(order);
		}
	}

	@Override
	protected boolean shouldHandleEvent(final SharjahCaptureOrderEvent event) {
		// TODO Auto-generated method stub
		return true;
	}
	
	/**
	 * @return the paymentService
	 */
	public PaymentService getPaymentService() {
		return paymentService;
	}

	/**
	 * @param paymentService the paymentService to set
	 */
	public void setPaymentService(final PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService() {
		return modelService;
	}

	/**
	 * @param modelService the modelService to set
	 */
	public void setModelService(final ModelService modelService) {
		this.modelService = modelService;
	}


}
