package com.sharjah.core.payment.cybersource.strategies.impl;

import de.hybris.platform.acceleratorservices.payment.cybersource.strategies.impl.DefaultCreateSubscriptionRequestStrategy;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import org.apache.log4j.Logger;


/**
 *
 * @author Pablo François
 */
public class SharjahCreateSubscriptionRequestStrategy extends DefaultCreateSubscriptionRequestStrategy
{

	private static final Logger LOG = Logger.getLogger(SharjahCreateSubscriptionRequestStrategy.class);

	@Override
	public CreateSubscriptionRequest createSubscriptionRequest(final String siteName, final String requestUrl,
			final String responseUrl, final String merchantCallbackUrl, final CustomerModel customerModel,
			final CreditCardPaymentInfoModel cardInfo, final AddressModel paymentAddress)
	{
		final CreateSubscriptionRequest request = super.createSubscriptionRequest(siteName, requestUrl, responseUrl,
				merchantCallbackUrl, customerModel, cardInfo, paymentAddress);

		boolean saveCard = false;
		if (cardInfo != null)
		{
			saveCard = cardInfo.isSaved();
			if (LOG.isDebugEnabled())
			{
				LOG.debug(String.format("saveCard set to %s", saveCard));
			}
		}
		request.setSaveCard(saveCard);

		return request;
	}

}
