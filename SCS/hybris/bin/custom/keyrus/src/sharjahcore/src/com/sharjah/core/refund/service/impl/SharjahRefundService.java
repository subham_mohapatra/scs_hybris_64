package com.sharjah.core.refund.service.impl;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.refund.impl.DefaultRefundService;
import de.hybris.platform.returns.OrderReturnRecordsHandlerException;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.model.CsTicketModel;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.payment.service.impl.CodPaymentService;
import com.sharjah.core.refund.enums.RefundsEnum;
import com.sharjah.core.ticket.service.impl.SharjahTicketBusinessService;


/**
 * Used for refunding by CyberSource and notifying CSAgent for manual refund.
 *
 * @author Pablo François
 */
public class SharjahRefundService extends DefaultRefundService
{

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LogManager.getLogger(SharjahRefundService.class);

	private static final String PAYMENT_PROVIDER_CYBERSOURCE = "CyberSource";
	private static final String PAYMENT_PROVIDER_MANUAL_REFUND_VOUCHER = "Manual_Refund_Voucher";
	private static final String CS_TICKET_SUBJECT_REFUND = "REFUND";

	// CyberSource payments
	@Resource
	private PaymentService paymentService;
	@Resource
	private CodPaymentService codPaymentService;
	@Resource
	private SharjahTicketBusinessService ticketBusinessService;

	/**
	 * Execute the automatic (CyberSource) refund and notify csAgent for manual refund if an error occurred.
	 *
	 * @param refundsEnum
	 *           the RefundsEnum use for notify the supportBackoffice if a manual refund is required
	 * @param order
	 *           the order that contains the paymentTransaction
	 * @param refundAmount
	 *           the amount to refund
	 * @return true if the automatic refund works or false otherwise
	 */
	public boolean executeRefund(final RefundsEnum refundsEnum, final OrderModel order, final BigDecimal refundAmount)
	{
		final BigDecimal automaticRefundAmount = refundAmount;
		final List<PaymentTransactionModel> transactions = order.getPaymentTransactions();

		// Get the payment transaction with automatic payment providers like cybersource (CS) or paypal (PP) transaction
		final List<String> automaticPaymentProviders = Arrays.asList(PAYMENT_PROVIDER_CYBERSOURCE,
				SharjahCoreConstants.PAYMENT_PROVIDER_COD);
		final PaymentTransactionModel automaticPaymentTransaction = getTransactionByPaymentProviders(transactions,
				automaticPaymentProviders);

		// Get the maximum amount refundable by CS or PP
		final BigDecimal maximumAmountAutomaticallyRefundable = getMaximumAmountAutomaticallyRefundable(
				automaticPaymentTransaction);

		boolean automaticRefundSuccessful = true;

		//Refund the maximum amount by CS or PP
		final CsTicketModel csTicket = executeAutomaticRefund(refundsEnum, automaticPaymentTransaction, automaticRefundAmount,
				maximumAmountAutomaticallyRefundable);

		//Get the remaining amount to refund manually
		BigDecimal remainingAmountForManualRefund = BigDecimal.ZERO;
		if (csTicket != null)
		{
			automaticRefundSuccessful = false;
			remainingAmountForManualRefund = automaticRefundAmount;
		}
		else if (automaticRefundAmount.compareTo(maximumAmountAutomaticallyRefundable) > 0)
		{
			remainingAmountForManualRefund = automaticRefundAmount.subtract(maximumAmountAutomaticallyRefundable);
		}

		//Notify the csAgent for a manual refund if the remaining amount is greater than 0
		if (remainingAmountForManualRefund.compareTo(BigDecimal.ZERO) > 0)
		{
			//reload the manualPaymentTransaction
			final PaymentTransactionModel manualPaymentTransaction = getManualPaymentTransaction(order.getPaymentTransactions());

			//Refund the remaining amount by creating the paymentTransaction and the paymentTransactionEntry and notify the csAgent
			executeManualRefund(refundsEnum, order, manualPaymentTransaction, remainingAmountForManualRefund, csTicket);
		}

		return automaticRefundSuccessful;
	}

	/**
	 * Calculate the manual amount to refund manually, then execute the automatic (CyberSource) refund and notify csAgent
	 * for manual refund if an error occurred.
	 *
	 * @param refundEnum
	 *           the RefundsEnum use for notify the supportBackoffice if a manual refund is required
	 * @param order
	 *           the order that contains the paymentTransaction
	 * @param refundAmount
	 *           the amount to refund
	 * @param manualRefundAmount
	 *           the amount to refund manually
	 * @return true if the automatic refund works or false otherwise
	 */
	public boolean executeRefund(final RefundsEnum refundEnum, final OrderModel order, final BigDecimal refundAmount,
			final BigDecimal manualRefundAmount)
	{
		final List<PaymentTransactionModel> transactions = order.getPaymentTransactions();
		BigDecimal automaticRefundAmount = refundAmount;

		//Get the manual paymentTransaction
		final PaymentTransactionModel manualPaymentTransaction = getManualPaymentTransaction(transactions);
		//handle manual amount defined by the csAgent in the customerbackoffice
		if (BigDecimal.ZERO.compareTo(manualRefundAmount) < 0)
		{
			if (manualRefundAmount.compareTo(refundAmount) == 0)
			{
				//Everything need to be manually refund => just create a paymentTransactionEntry (and his paymentTransaction if needed) and return true
				executeManualRefund(refundEnum, order, manualPaymentTransaction, manualRefundAmount, null);
				return true;
			}
			else if (manualRefundAmount.compareTo(refundAmount) < 0)
			{
				//Recalculate the automaticRefundAmount with the manualRefundAmount set by the csAgent
				automaticRefundAmount = refundAmount.subtract(manualRefundAmount);
				//And create the paymentTransaction for the manualRefund
				executeManualRefund(refundEnum, order, manualPaymentTransaction, manualRefundAmount, null);
				getModelService().refresh(order);
			}
		}

		return executeRefund(refundEnum, order, automaticRefundAmount);
	}

	/**
	 * Get the paymentTransaction by paymentProviders names
	 *
	 * @param paymentTransactions
	 *           the list of paymentTransaction
	 * @param paymentProviders
	 *           the list of paymentProvider
	 * @return the first paymentTransaction that match the paymentProvider name
	 */
	private PaymentTransactionModel getTransactionByPaymentProviders(final List<PaymentTransactionModel> paymentTransactions,
			final List<String> paymentProviders)
	{
		for (final PaymentTransactionModel paymentTransaction : paymentTransactions)
		{
			if (paymentProviders.contains(paymentTransaction.getPaymentProvider()))
			{
				return checkPaymentTransactionEntry(paymentTransaction);
			}
		}
		return null;
	}

	/**
	 * Check the PaymentTransactionEntry and return the first paymentTransaction that match the paymentProvider name
	 * 
	 * @param paymentTransaction
	 * @return
	 */
	private PaymentTransactionModel checkPaymentTransactionEntry(final PaymentTransactionModel paymentTransaction)
	{
		for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransaction.getEntries())
		{
			if (PaymentTransactionType.CAPTURE.equals(paymentTransactionEntry.getType())
					&& TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(paymentTransactionEntry.getTransactionStatus()))
			{
				return paymentTransaction;
			}
		}
		return null;
	}

	/**
	 * Get the manual PaymentTransaction with paymentProvider = Manual_Refund_Voucher
	 *
	 * @param paymentTransactions
	 *           the list of paymentTransaction of the order
	 * @return the manual PaymentTransaction with paymentProvider = Manual_Refund_Voucher
	 */
	private PaymentTransactionModel getManualPaymentTransaction(final List<PaymentTransactionModel> paymentTransactions)
	{
		for (final PaymentTransactionModel paymentTransaction : paymentTransactions)
		{
			if (PAYMENT_PROVIDER_MANUAL_REFUND_VOUCHER.equalsIgnoreCase(paymentTransaction.getPaymentProvider()))
			{
				return paymentTransaction;
			}
		}
		return null;
	}

	/**
	 * Get the maximum amount that can be automatically refundable for the paymentTransaction given in parameter. If the
	 * paymentPaymentTransaction is equals to PayPal, convert the maximum amount refundable into USD.
	 *
	 * @param paymentTransaction
	 *           the paymentTransaction
	 * @return the maximum amount that can be automatically refundable
	 */
	private BigDecimal getMaximumAmountAutomaticallyRefundable(final PaymentTransactionModel paymentTransaction)
	{
		BigDecimal totalPaid = BigDecimal.ZERO;
		BigDecimal totalRefunded = BigDecimal.ZERO;

		if (paymentTransaction == null || CollectionUtils.isEmpty(paymentTransaction.getEntries()))
		{
			return BigDecimal.ZERO;
		}

		for (final PaymentTransactionEntryModel paymentTransactionEntry : paymentTransaction.getEntries())
		{
			final PaymentTransactionType paymentTransactionType = paymentTransactionEntry.getType();
			final String paymentTransactionStatus = paymentTransactionEntry.getTransactionStatus();
			final BigDecimal amount = paymentTransactionEntry.getAmount();
			/**
			 * total transaction paid
			 */
			if (PaymentTransactionType.CAPTURE.equals(paymentTransactionType))
			{
				if (TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(paymentTransactionStatus))
				{
					totalPaid = totalPaid.add(amount);
					if(LOG.isDebugEnabled()){
					LOG.debug("Add this amount " + amount + " to the totalPaid . TotalPaid = " + totalPaid);
					}
				}
			}
			else if ((PaymentTransactionType.REFUND_FOLLOW_ON.equals(paymentTransactionType)
					|| PaymentTransactionType.REFUND_STANDALONE.equals(paymentTransactionType))
					&& TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(paymentTransactionStatus))
			{
				totalRefunded = totalRefunded.add(amount);
				if(LOG.isDebugEnabled()){
				LOG.debug("Subtract this amount " + amount + " to the totalRefund . TotalRefund = " + totalRefunded);
				}
			}
		}
		final BigDecimal totalAmountRefundable = totalPaid.subtract(totalRefunded);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Total amount refundable(" + totalAmountRefundable + ")  = totalPaid (" + totalPaid + ") - totalRefunded ("
					+ totalRefunded + ")");
		}

		return totalAmountRefundable;
	}

	/**
	 * Execute the automatic refund and notify the csAgent if an error occurred
	 *
	 * @param refundsEnum
	 *           the refundsEnum used for the notification
	 * @param paymentTransaction
	 *           the paymentTransaction
	 * @param amountToRefund
	 *           the totalAmountToRefund
	 * @param maximumAmountRefundable
	 *           the maximum amount refundable
	 * @return a CsTicketModel if the automatic refund is unsuccessful otherwise null
	 */
	private CsTicketModel executeAutomaticRefund(final RefundsEnum refundsEnum, final PaymentTransactionModel paymentTransaction,
			final BigDecimal amountToRefund, final BigDecimal maximumAmountRefundable)
	{
		if (BigDecimal.ZERO.equals(amountToRefund) || BigDecimal.ZERO.equals(maximumAmountRefundable))
		{
			if (LOG.isWarnEnabled())
			{
				LOG.warn("the amount use for automatic refund or the maximum amount refundable equals to zero, nothing to do");
			}
			return null;
		}

		BigDecimal amountUseForAutomaticRefund = amountToRefund;
		if (amountToRefund.compareTo(maximumAmountRefundable) > 0)
		{
			LOG.warn(
					"The amount to refund is greater than the maximum amount that can be automatically refund, reset the amount to refund to the maximum amount refundable : "
							+ maximumAmountRefundable);
			amountUseForAutomaticRefund = maximumAmountRefundable;
		}

		final String paymentProvider = paymentTransaction.getPaymentProvider();
		PaymentTransactionEntryModel paymentTransactionEntry = null;

		switch (paymentTransaction.getPaymentProvider())
		{
			case PAYMENT_PROVIDER_CYBERSOURCE:
				paymentTransactionEntry = paymentService.refundFollowOn(paymentTransaction, amountUseForAutomaticRefund);
				break;
			case SharjahCoreConstants.PAYMENT_PROVIDER_COD:
				paymentTransactionEntry = codPaymentService.refundFollowOn(refundsEnum, paymentTransaction,
						amountUseForAutomaticRefund);
				break;
			default:
				LOG.error("Unable to refund paymentProvider " + paymentProvider + ", no paymentProvider found");
		}

		if (paymentTransactionEntry == null)
		{
			final String errorMsg = "REASON : PSP didn't return any response to hybris";
			return generateCsErrorTicketForRefund(refundsEnum, paymentTransaction, errorMsg);
		}
		else if (!TransactionStatus.ACCEPTED.toString().equalsIgnoreCase(paymentTransactionEntry.getTransactionStatus()))
		{
			final String errorMsg = "REASON : PSP return an response with an error to hybris : "
					+ paymentTransactionEntry.getTransactionStatusDetails();
			return generateCsErrorTicketForRefund(refundsEnum, paymentTransaction, errorMsg);
		}
		return null;
	}

	/**
	 * Generate a CsTicket for the csAgentGroup with the errorMsg
	 *
	 * @param refundsEnum
	 *           the refundsEnum used for build the note
	 * @param paymentTransaction
	 *           the paymentTransaction used for build the note
	 * @param errorMsg
	 *           the errorMsg used for build the note
	 * @return a new CsTicket
	 */
	private CsTicketModel generateCsErrorTicketForRefund(final RefundsEnum refundsEnum,
			final PaymentTransactionModel paymentTransaction, final String errorMsg)
	{
		final StringBuilder errorMessage = new StringBuilder();
		errorMessage.append(refundsEnum.getType()).append(" - ").append(refundsEnum.getUser()).append(" | ");
		errorMessage.append("Refund Failed for the paymentTransaction ");
		errorMessage.append(paymentTransaction.getPaymentProvider()).append(" : ").append(paymentTransaction.getCode())
				.append(". ");
		errorMessage.append(errorMsg);
		return ticketBusinessService.createTicketForManualRefund(CsTicketCategory.INCIDENT,
				(OrderModel) paymentTransaction.getOrder(), CS_TICKET_SUBJECT_REFUND, errorMessage.toString());
	}

	/**
	 * Execute a manual refund by creating a new paymentTransactionEntry (and a paymentTransaction if needed)
	 *
	 * @param refundsEnum
	 *           the refundsEnum used for the notification
	 * @param order
	 *           the order
	 * @param manualPaymentTransaction
	 *           the paymentTransaction where the new paymentTransactionEntry is attached
	 * @param amountUseForManualRefund
	 *           the amount use for manual refund
	 * @param csTicket
	 *           the ticket already created when an error occurred during the payment with Cybersource or Paypal. If the
	 *           payment is OK, the csTicket is null
	 */
	private void executeManualRefund(final RefundsEnum refundsEnum, final OrderModel order,
			final PaymentTransactionModel manualPaymentTransaction, final BigDecimal amountUseForManualRefund,
			final CsTicketModel csTicket)
	{
		if (amountUseForManualRefund.compareTo(BigDecimal.ZERO) > 0)
		{
			PaymentTransactionModel paymentTransaction = manualPaymentTransaction;
			if (paymentTransaction == null)
			{
				paymentTransaction = createPaymentTransaction(order, PAYMENT_PROVIDER_MANUAL_REFUND_VOUCHER);
			}

			final PaymentTransactionEntryModel paymentTransactionEntry = createManualPaymentTransactionEntry(paymentTransaction,
					amountUseForManualRefund);
			if (paymentTransactionEntry != null)
			{
				CsTicketModel currentCsTicket = csTicket;
				if (currentCsTicket == null)
				{
					final String initialNote = refundsEnum.getType() + " - " + refundsEnum.getUser()
							+ " | A manual refund is required";
					currentCsTicket = ticketBusinessService.createTicketForManualRefund(CsTicketCategory.NOTE, order,
							CS_TICKET_SUBJECT_REFUND, initialNote);
				}
			}
		}
	}

	/**
	 * Create the paymentTransaction for the order given in parameter
	 *
	 * @param order
	 *           the order
	 * @param paymentProvider
	 *           the paymentProvider of the paymentTransaction
	 * @return a new paymentTransaction
	 */
	private PaymentTransactionModel createPaymentTransaction(final OrderModel order, final String paymentProvider)
	{
		final PaymentTransactionModel paymentTransaction = getModelService().create(PaymentTransactionModel.class);
		paymentTransaction.setPaymentProvider(paymentProvider);
		paymentTransaction.setOrder(order);
		paymentTransaction.setCode(order.getCode() + "_" + System.currentTimeMillis());
		paymentTransaction.setCurrency(order.getCurrency());
		getModelService().save(paymentTransaction);
		return paymentTransaction;
	}

	/**
	 *
	 * Create a successful paymentTransactionEntry for a manual refund
	 *
	 * @param paymentTransaction
	 *           the paymentTransaction that will contains the paymentTransactionEntry created
	 * @param amountUseForManualRefund
	 *           the amount use for manual refund
	 * @return the new paymentTransactionEntry
	 */
	private PaymentTransactionEntryModel createManualPaymentTransactionEntry(final PaymentTransactionModel paymentTransaction,
			final BigDecimal amountUseForManualRefund)
	{
		getModelService().refresh(paymentTransaction);
		final String newEntryCode = paymentService.getNewPaymentTransactionEntryCode(paymentTransaction,
				PaymentTransactionType.REFUND_STANDALONE);
		final PaymentTransactionEntryModel entry = (PaymentTransactionEntryModel) getModelService()
				.create(PaymentTransactionEntryModel.class);
		entry.setType(PaymentTransactionType.REFUND_STANDALONE);
		entry.setAmount(amountUseForManualRefund);
		entry.setPaymentTransaction(paymentTransaction);
		entry.setTime(new Date());
		entry.setCurrency(paymentTransaction.getCurrency());
		entry.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
		entry.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL.toString());
		entry.setCode(newEntryCode);
		getModelService().save(entry);
		return entry;
	}

	/**
	 * Same method as hybris but set calculated to true for the order and his order entries + remove the recalculation of
	 * the order.
	 */
	@Override
	public void apply(final OrderModel previewOrder, final ReturnRequestModel request) throws OrderReturnRecordsHandlerException
	{
		final OrderModel finalOrder = request.getOrder();
		getModificationHandler().createRefundEntry(finalOrder, getRefunds(request),
				"Refund request for order : " + finalOrder.getCode());

		AbstractOrderEntryModel originalEntry;
		for (final Iterator<AbstractOrderEntryModel> arg4 = previewOrder.getEntries().iterator(); arg4.hasNext(); getModelService()
				.save(originalEntry))
		{
			final AbstractOrderEntryModel previewEntry = arg4.next();
			originalEntry = this.getEntry(finalOrder, previewEntry.getEntryNumber());
			final Long newQuantity = previewEntry.getQuantity();
			originalEntry.setQuantity(newQuantity);
			originalEntry.setCalculated(Boolean.TRUE);
			if (newQuantity.longValue() <= 0L)
			{
				originalEntry.setQuantityStatus(OrderEntryStatus.DEAD);
			}
		}

		finalOrder.setCalculated(Boolean.TRUE);
		getModelService().save(finalOrder);
	}

}
