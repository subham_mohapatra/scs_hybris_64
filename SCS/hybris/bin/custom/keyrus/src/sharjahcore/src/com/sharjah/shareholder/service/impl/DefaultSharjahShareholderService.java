package com.sharjah.shareholder.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.sharjah.core.model.ShareholderModel;
import com.sharjah.core.shareholder.dao.ShareholderDao;
import com.sharjah.shareholder.service.ShareholderService;


/**
 * Sharjah Shareholder Service that implements the {@link ShareholderService} interface
 */
public class DefaultSharjahShareholderService implements ShareholderService {

	private ShareholderDao sharjahShareholderDao;

	@Override
	public ShareholderModel findShareholderByNumber(final String number) {

		validateParameterNotNull(number, "Parameter shareholder number must not be null");
		final List<ShareholderModel> shareholders = new ArrayList<ShareholderModel>(
				sharjahShareholderDao.findShareholderByNumber(number));
		
		if (CollectionUtils.isNotEmpty(shareholders))
		{
			return shareholders.get(0);
		}
		return null;

	}
	/**
	 * 
	 * @return
	 */
	public ShareholderDao getSharjahShareholderDao() {
		return sharjahShareholderDao;
	}
	/**
	 * 
	 * @param sharjahShareholderDao
	 */
	public void setSharjahShareholderDao(final ShareholderDao sharjahShareholderDao) {
		this.sharjahShareholderDao = sharjahShareholderDao;
	}

}
