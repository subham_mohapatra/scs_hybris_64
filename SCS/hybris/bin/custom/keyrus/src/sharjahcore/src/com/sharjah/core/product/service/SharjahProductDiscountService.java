package com.sharjah.core.product.service;

import de.hybris.platform.core.model.product.ProductModel;


/**
 * Interface of the Sharjah Product Discount Service
 */
public interface SharjahProductDiscountService
{
	/**
	 * Calculate the discount percentage from the original price and the price with the discount
	 * 
	 * @param originalPrice
	 * @param discountPrice
	 * @return int
	 */
	int calculatePercentageDiscount(Double originalPrice, Double discountPrice);
	
	/**
	 * Checks if the product has any DiscountRowEntry in order to display strike price in PDP or PLP/Search page
	 * 
	 * @param regularPrice
	 * @param product
	 * @return Double
	 */
	Double calculateDiscount(Double regularPrice, ProductModel product);
	
}
