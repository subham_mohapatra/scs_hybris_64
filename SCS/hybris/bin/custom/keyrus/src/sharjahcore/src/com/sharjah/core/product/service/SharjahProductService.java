package com.sharjah.core.product.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.Date;
import java.util.List;


/**
 * Interface of the Sharjah Product Service
 * 
 * @author Luiz.Henriques
 *
 */
public interface SharjahProductService extends ProductService
{

	/**
	 * Return all products from staged catalog and checks the eligibility of these
	 * 
	 * @param lastExecutedTime
	 * @return List<ProductModel>
	 */
	List<ProductModel> getProductsToEligibilityForPublication(Date lastExecutedTime);
	
	/**
	 * Checks the product eligibility
	 * 
	 * @param product
	 */
	void checkProductEligibility(ProductModel product);

	/**
	 * Finds one product with the given EAN
	 * @param ean
	 * @return ProductModel
	 */
	ProductModel findProductByEAN(String ean);
	
	/**
	 * Finds one product with the given EAN
	 * @param ean
	 * @param catalogVersion
	 * @return ProductModel
	 */ 
	ProductModel findProductByEAN(CatalogVersionModel catalogVersion, String ean);
	
	/**
	 * Finds one product with the given code and catalogVersion
	 * @param code
	 * @param catalogVersion
	 * @return ProductModel
	 */
	ProductModel findProductByCodeForMedia(CatalogVersionModel catalogVersion, String code);
	
	
}
