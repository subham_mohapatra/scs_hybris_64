/**
 *
 */
package com.sharjah.core.cronjobs;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.TenantAwareThreadFactory;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.tx.Transaction;
import de.hybris.platform.tx.TransactionBody;
import de.hybris.platform.util.Config;
import de.hybris.platform.warehousing.model.InventoryEventModel;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sharjah.core.model.cronjob.SharjahCronjobModel;


/**
 * @author Ben Amor Bassem
 *
 */
public class SharjahReleaseStockReservationCronJob extends AbstractJobPerformable<SharjahCronjobModel>
{

	private static final Logger LOG = Logger.getLogger(SharjahReleaseStockReservationCronJob.class);
	private static final String EVENT_DELIVERED_OR_CANCELLED_QUERY_BY_UPDATE_DATE = "select {event.pk} from {InventoryEvent as event "
			+ "join ConsignmentEntry as ce on {event.consignmentEntry} = {ce.pk} "
			+ "join Consignment as c on {ce.consignment} = {c.pk} "
			+ "join Stocklevel as stock on {event.stockLevel} = {stock.pk} join ConsignmentStatus as cs on {cs.pk} = {c.status} } "
			+ "where {stock.modifiedtime} >= {event.creationtime} and ({cs.code} in "
			+ "('SHIPPED','REFUNDED','DELIVERED','CANCELLED'))";

	@Override
	public PerformResult perform(final SharjahCronjobModel cronJob)
	{
		final Date launchDate = cronJob.getLastSuccessfulRun() != null ? cronJob.getLastSuccessfulRun() : new Date();

		if (LOG.isInfoEnabled())
		{
			LOG.info("Release Stock Reservation CronJob with code <" + cronJob.getCode() + "> for locale <"
					+ cronJob.getSessionLanguage().getIsocode() + "> started. Last successful run: " + launchDate);
		}

		removeInventoryEvents();

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 */
	private void removeInventoryEvents()
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(EVENT_DELIVERED_OR_CANCELLED_QUERY_BY_UPDATE_DATE);
		final SearchResult<InventoryEventModel> results = flexibleSearchService.search(query);

		final Process<InventoryEventModel> processUpdateEvent = new Process<InventoryEventModel>() {
			@Override
			void processtask(final List<InventoryEventModel> listModels) {
				LOG.info("removing inventory events...");
			}
		};
		processUpdateEvent.execute(results.getResult());
	}

	/**
	 * 
	 * @author ASUS
	 *
	 * @param <T>
	 */
	private abstract class Process<T> {
		abstract void processtask(List<T> listModels);

		void execute(final List<T> listModels) {
			if (CollectionUtils.isNotEmpty(listModels)) {
				final ExecutorService threadPool = Executors.newFixedThreadPool(
						Config.getInt("impex.import.workers", 4),
						new TenantAwareThreadFactory(Registry.getCurrentTenant(), JaloSession.getCurrentSession()));

				int indexStart = 0;
				while (indexStart < listModels.size()) {
					final int indexEnd = Math.min(indexStart + 1000, listModels.size());
					final List<T> stocks = listModels.subList(indexStart, indexEnd);
					indexStart = indexEnd;

					threadPool.execute(() -> {
						processtask(listModels);
						try {
							Transaction.current().execute(new TransactionBody() {
								@Override
								public Void execute() {
									modelService.removeAll(stocks);
									return null;
								}
							});
						} catch (final Exception e) {
							throw new RuntimeException(e);
						}
					});
				}

				threadPool.shutdown();
				try {
					threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
				} catch (final InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
}
