package com.sharjah.core.payment.cybersource.converters.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.cybersource.converters.populators.request.AbstractRequestPopulator;
import de.hybris.platform.acceleratorservices.payment.data.CreateSubscriptionRequest;
import de.hybris.platform.acceleratorservices.payment.data.CustomerBillToData;
import de.hybris.platform.acceleratorservices.payment.data.OrderPageConfirmationData;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorservices.payment.data.SignatureData;
import de.hybris.platform.acceleratorservices.payment.utils.AcceleratorDigestUtils;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;
import com.sharjah.core.util.CyberSourceUtils;


/**
 * @author Pablo François
 */
public class SharjahCyberSourcePopulator extends AbstractRequestPopulator<CreateSubscriptionRequest, PaymentData>
{

	private static final Logger LOG = Logger.getLogger(SharjahCyberSourcePopulator.class);

	private static final String PROFILE_ID = "hop.cybersource.profile-id";
	private static final String ACCESS_KEY = "hop.cybersource.access-key";
	private static final String SECRET_KEY = "hop.cybersource.secret-key";

	// This String must include the names of all the fields sent in the HOP form, except for the signature
	private static final String SIGNED_FIELDS = "access_key,profile_id,transaction_uuid,signed_field_names,signed_date_time,"
			+ "locale,transaction_type,reference_number,amount,currency,bill_to_address_line1,bill_to_address_city,"
			+ "bill_to_address_country,bill_to_email,bill_to_forename,bill_to_surname,override_custom_receipt_page,"
			+ "override_custom_cancel_page,override_backoffice_post_url";

	@Resource
	private ConfigurationService configurationService;
	@Resource
	private AcceleratorDigestUtils sharjahDigestUtils;
	@Resource
	private CartService cartService;
	@Resource
	private CommonI18NService commonI18NService;

	@Override
	public void populate(final CreateSubscriptionRequest source, final PaymentData target)
	{
		validateParameterNotNull(source, "Parameter [CreateSubscriptionRequest] source cannot be null");
		validateParameterNotNull(target, "Parameter [PaymentData] target cannot be null");

		try
		{
			populateTransactionData(source, target);
			populateBillingData(source, target);

			final String dataToSign = CyberSourceUtils.buildDataToSign(target.getParameters());
			addRequestQueryParam(target, "signature", sharjahDigestUtils.getPublicDigest(dataToSign, getProperty(SECRET_KEY)));
		}
		catch (final Exception e)
		{
			LOG.error("Error inserting CyberSource Hosted Order Page signature", e);
			throw new ConversionException("Error inserting CyberSource Hosted Order Page signature", e);
		}
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	private void populateTransactionData(final CreateSubscriptionRequest source, final PaymentData target)
	{
		final CartModel sessionCart = cartService.getSessionCart();
		Assert.notNull(sessionCart, "sessionCart cannot be null");
		final SignatureData signatureData = source.getSignatureData();
		Assert.notNull(signatureData, "signatureData cannot be null");

		final String currency = signatureData.getCurrency();
		final String amount = String.valueOf(sessionCart.getTotalPrice());
		validateParameterNotNull(currency, "Currency cannot be null");
		validateParameterNotNull(amount, "Amount cannot be null");

		final OrderPageConfirmationData confirmationData = source.getOrderPageConfirmationData();
		addRequestQueryParam(target, "override_custom_receipt_page", confirmationData.getReceiptResponseUrl());
		addRequestQueryParam(target, "override_custom_cancel_page", confirmationData.getCancelResponseUrl());
		addRequestQueryParam(target, "override_backoffice_post_url", confirmationData.getMerchantUrlPostAddress());
		addRequestQueryParam(target, "access_key", getProperty(ACCESS_KEY));
		addRequestQueryParam(target, "profile_id", getProperty(PROFILE_ID));
		addRequestQueryParam(target, "transaction_uuid", source.getRequestId());
		addRequestQueryParam(target, "signed_field_names", SIGNED_FIELDS);
		addRequestQueryParam(target, "signed_date_time", getCurrentUtcTime());
		addRequestQueryParam(target, "locale", commonI18NService.getCurrentLanguage().getIsocode().toLowerCase());
		addRequestQueryParam(target, "reference_number", sessionCart.getCode());
		addRequestQueryParam(target, "amount", amount);
		addRequestQueryParam(target, "currency", currency);

		final CreditCardPaymentInfoModel paymentInfo = (CreditCardPaymentInfoModel) sessionCart.getPaymentInfo();
		if (source.isSaveCard())
		{
			addRequestQueryParam(target, "transaction_type", CyberSourceConstants.SALE_AND_CREATE_PAYMENT_TOKEN);
		}
		else
		{
			addRequestQueryParam(target, "transaction_type", CyberSourceConstants.PAYMENT_SALE);
			if (paymentInfo != null && StringUtils.isNotEmpty(paymentInfo.getSubscriptionId()))
			{
				addRequestQueryParam(target, CyberSourceConstants.PAYMENT_TOKEN, paymentInfo.getSubscriptionId());
				addRequestQueryParam(target, "signed_field_names", SIGNED_FIELDS + "," + CyberSourceConstants.PAYMENT_TOKEN);
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @param target
	 */
	private void populateBillingData(final CreateSubscriptionRequest source, final PaymentData target)
	{
		final CustomerBillToData customerBillToData = source.getCustomerBillToData();
		addRequestQueryParam(target, "bill_to_address_line1", customerBillToData.getBillToStreet1());
		addRequestQueryParam(target, "bill_to_address_city", customerBillToData.getBillToCity());
		addRequestQueryParam(target, "bill_to_address_country", customerBillToData.getBillToCountry());
		addRequestQueryParam(target, "bill_to_email", customerBillToData.getBillToEmail());
		addRequestQueryParam(target, "bill_to_forename", customerBillToData.getBillToFirstName());
		addRequestQueryParam(target, "bill_to_surname", customerBillToData.getBillToLastName());
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	private String getProperty(final String key)
	{
		final String property = configurationService.getConfiguration().getString(key, null);
		validateParameterNotNull(property, key + " cannot be null");
		return property;
	}

	/**
	 * 
	 * @return
	 */
	private String getCurrentUtcTime()
	{
		final ZonedDateTime utcTime = ZonedDateTime.now(ZoneOffset.UTC);
		return utcTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"));
	}

}
