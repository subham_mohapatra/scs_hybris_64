package com.sharjah.core.stock;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.stock.StockService;

/**
 * override  StockService to add new method releaseStockAfterCancelOrder
 * 
 */
public interface SharjahStockService extends StockService{

	
	/**
	 * releaseStockAfterCancelOrder method to release stock after cancel
	 * @param orderCancelRequest
	 */
	public void releaseStockAfterCancelOrder(OrderCancelRequest orderCancelRequest) throws OrderCancelException;
}
