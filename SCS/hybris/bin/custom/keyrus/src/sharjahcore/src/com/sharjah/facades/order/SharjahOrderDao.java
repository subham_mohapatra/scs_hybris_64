package com.sharjah.facades.order;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

/**
 * @author issam maiza
 * 
 * SharjahOrderDao interface for Sharjah Order Dao
 * 
 */
public interface SharjahOrderDao {

	/**
	 * Returns orders with status equal to PAYMENT_NOT_CAPTURED.
	 */
	List<OrderModel> findOrdersWithFailedCapture();

}
