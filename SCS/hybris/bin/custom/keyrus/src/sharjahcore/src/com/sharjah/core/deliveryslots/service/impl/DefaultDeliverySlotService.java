package com.sharjah.core.deliveryslots.service.impl;

import static com.sharjah.core.util.SharjahDateUtils.toLocalDate;

import de.hybris.platform.order.CartService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sharjah.core.deliveryslots.dao.DeliverySlotDao;
import com.sharjah.core.deliveryslots.service.DeliverySlotService;
import com.sharjah.core.enums.DeliveryDayEnum;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.model.DeliverySlotByPOSModel;
import com.sharjah.core.model.DeliverySlotMappingModel;
import com.sharjah.core.model.DeliverySlotScheduleModel;


/**
 * @author juanpablo.francois@keyrus.com
 */
@Service
public class DefaultDeliverySlotService implements DeliverySlotService
{

	@Resource(name = "defaultDeliverySlotDao")
	private DeliverySlotDao deliverySlotDao;
	@Resource
	private CartService cartService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CalendarDeliverySlotModel> findSlotsForPos(final PointOfServiceModel pos, final Date beginDate, final Date endDate)
	{
		return deliverySlotDao.findSlotsForPos(pos, beginDate, endDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CalendarDeliverySlotModel> findSlotsWithCapacityForPos(final PointOfServiceModel pos, final Date beginDate,
			final Date endDate)
	{
		return deliverySlotDao.findSlotsWithCapacityForPos(pos, beginDate, endDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSlotExpired(final CalendarDeliverySlotModel source, final LocalDate firstDeliveryDate)
	{
		final DeliverySlotScheduleModel slot = source.getDeliverySlot();
		if (slot == null || slot.getEndTime() == null || source.getDate() == null)
		{
			return true;
		}

		final LocalDate slotDate = toLocalDate(source.getDate());
		final LocalTime endTime = LocalTime.parse(slot.getEndTime());
		return hasTimePassed(slotDate, endTime.getHour(), endTime.getMinute()) || isSlotOutOfRange(source, firstDeliveryDate);
	}

	/**
	 * Check if a slot respects the rules defining the range for delivering the same or next day, depending on the time
	 * of the order.
	 */
	private boolean isSlotOutOfRange(final CalendarDeliverySlotModel source, final LocalDate firstDeliveryDate)
	{
		final DeliverySlotMappingModel mapping = deliverySlotDao.findCurrentMappingForPos(source.getPos());
		if (mapping == null)
		{
			return true;
		}

		final LocalDate slotDate = toLocalDate(source.getDate());
		final LocalTime slotTime = LocalTime.parse(source.getDeliverySlot().getBeginTime());
		final LocalDateTime slotDateTime = LocalDateTime.of(slotDate.getYear(), slotDate.getMonth(), slotDate.getDayOfMonth(),
				slotTime.getHour(), slotTime.getMinute());

		LocalDate firstSlotDate = firstDeliveryDate;
		if (mapping.getDeliveryDay() != null && mapping.getDeliveryDay().getCode().equals(DeliveryDayEnum.TOMORROW.getCode()))
		{
			firstSlotDate = firstSlotDate.plusDays(1);
		}
		final LocalTime firstSlotTime = LocalTime.parse(mapping.getFirstDeliverySlot().getBeginTime());
		final LocalDateTime firstSlotDateTime = LocalDateTime.of(firstSlotDate.getYear(), firstSlotDate.getMonth(),
				firstSlotDate.getDayOfMonth(), firstSlotTime.getHour(), firstSlotTime.getMinute());

		return slotDateTime.isBefore(firstSlotDateTime);
	}

	/**
	 * Check if a time has passed.
	 */
	private boolean hasTimePassed(final LocalDate date, final int hour, final int minute)
	{
		final LocalDateTime slotDate = LocalDateTime.of(date.getYear(), date.getMonth(), date.getDayOfMonth(), hour, minute);
		return LocalDateTime.now().isAfter(slotDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSlotSelected(final CalendarDeliverySlotModel source)
	{
		final CalendarDeliverySlotModel cartDeliverySlot = cartService.getSessionCart().getSelectedDeliverySlot();
		if (cartDeliverySlot != null)
		{
			final DeliverySlotScheduleModel cartSelectedSlot = cartDeliverySlot.getDeliverySlot();

			final LocalDate sourceDate = toLocalDate(source.getDate());
			final LocalDate cartDate = toLocalDate(cartDeliverySlot.getDate());

			if (cartSelectedSlot != null && sourceDate != null)
			{
				return cartSelectedSlot.equals(source.getDeliverySlot()) && sourceDate.equals(cartDate);
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CalendarDeliverySlotModel findSlotWithDate(final String pos, final Date date, final String deliverySlot)
	{
		return deliverySlotDao.findSlotWithDate(pos, date, deliverySlot);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotByPOSModel findSlotWithDayOfWeek(final String pos, final String dayOfWeek, final String deliverySlot)
	{
		return deliverySlotDao.findSlotWithDayOfWeek(pos, dayOfWeek, deliverySlot);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteSlotsAfter(final LocalDate startDate)
	{
		deliverySlotDao.deleteSlotsAfter(startDate);
	}

}
