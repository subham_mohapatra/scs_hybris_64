package com.sharjah.dataimport.batch.converter.decorator;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;

import java.util.Map;


public class ProductUnitDecorator extends AbstractImpExCSVCellDecorator
{
	private static final String PIECES = "pieces";
	private static final String PCS = "PCS";

	@Override
	public String decorate(final int position, final Map<Integer, String> srcLine)
	{
		final String cellValue = srcLine.get(Integer.valueOf(position));

		if (cellValue.equalsIgnoreCase(PCS))
		{
			return PIECES;
		}
		return cellValue;
	}
}
