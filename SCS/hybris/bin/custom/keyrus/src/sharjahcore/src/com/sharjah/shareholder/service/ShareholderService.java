package com.sharjah.shareholder.service;

import com.sharjah.core.model.ShareholderModel;


/**
 * Interface for the Shareholder service
 */
public interface ShareholderService {

	
	/**
	 * Finds one shareholder with the given SHAREHOLDER NUMBER
	 * 
	 * @param number
	 * @return ShareholderModel
	 */
	ShareholderModel findShareholderByNumber(String number);
}
