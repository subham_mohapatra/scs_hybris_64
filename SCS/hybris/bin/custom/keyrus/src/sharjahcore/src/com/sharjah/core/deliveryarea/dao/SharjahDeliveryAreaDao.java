package com.sharjah.core.deliveryarea.dao;

import com.sharjah.core.model.DeliveryAreaModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public interface SharjahDeliveryAreaDao
{

	/**
	 * Find a delivery area by code.
	 *
	 * @param code
	 *           the code of the delivery area
	 * @return DeliveryAreaModel
	 */
	DeliveryAreaModel findDeliveryAreaByCode(String code);

}
