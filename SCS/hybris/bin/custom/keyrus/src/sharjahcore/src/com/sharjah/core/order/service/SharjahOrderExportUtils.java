package com.sharjah.core.order.service;

import java.io.File;
import java.io.PrintWriter;

import com.opencsv.CSVWriter;


/**
 * Util for order export file
 * 
 * @author Luiz.Henriques
 *
 */
final public class SharjahOrderExportUtils
{
	private SharjahOrderExportUtils()
	{
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Checks if the folder exists, if not, it creates the new folder
	 * 
	 * @param folderPath
	 */
	public static void checkFolder(final String folderPath)
	{
		final File folder = new File(folderPath);
		if (!folder.exists())
		{
			folder.mkdirs();
		}
	}

	public static void addHeader(final CSVWriter csvWriter, final String[] header)
	{
		csvWriter.writeNext(header, false);
	}
	
	public static void addHeader(final PrintWriter exportWriter, final String header)
	{
		exportWriter.println(header);
	}

}
