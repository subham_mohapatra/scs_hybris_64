package com.sharjah.core.product.service.impl;

import java.io.File;
import java.util.Comparator;


/**
 * 
 * @author Ben Amor Bassem
 *
 */
public class SharjahFileComparator implements Comparator<File>
{

	@Override
	public int compare(final File media1, final File media2)
	{
		return media1.getName().compareTo(media2.getName());
   }

}
