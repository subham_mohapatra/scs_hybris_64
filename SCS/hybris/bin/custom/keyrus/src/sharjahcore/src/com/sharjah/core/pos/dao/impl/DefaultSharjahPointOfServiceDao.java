package com.sharjah.core.pos.dao.impl;

import de.hybris.platform.acceleratorservices.store.impl.AcceleratorPointOfServiceDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;
import java.util.Collections;

import com.sharjah.core.pos.dao.SharjahPointOfServiceDao;


/**
 * DAO for PointOfServiceModel
 * 
 * @author Ben Amor Bassem
 *
 */
public class DefaultSharjahPointOfServiceDao extends AcceleratorPointOfServiceDao implements SharjahPointOfServiceDao
{
	private static final String PERCENTAGE = "%";

	private static final String QUERY_POS = "SELECT {ps:pk} FROM {PointOfService as ps } WHERE (lower({ps.name}) NOT LIKE 'default') AND (lower({ps.posDisplayName}) LIKE ?queryText1 OR	upper({ps.posDisplayName}) LIKE ?queryText2 OR  {ps.posDisplayName} LIKE ?queryText3)";

	private static final String QUERY_ALL_POS = "SELECT {ps:pk} FROM {PointOfService as ps } WHERE lower({ps.name}) NOT LIKE 'default' ";

	
	@Override
	public Collection<PointOfServiceModel> getPointsOfServicesByDisplayName(final String queryText)
	{
		final FlexibleSearchQuery searchQueryFormat = new FlexibleSearchQuery(QUERY_POS);
		searchQueryFormat.setResultClassList(Collections.singletonList(PointOfServiceModel.class));

		searchQueryFormat.addQueryParameter("queryText3", PERCENTAGE + queryText + PERCENTAGE);
		searchQueryFormat.addQueryParameter("queryText2", PERCENTAGE + queryText.toUpperCase() + PERCENTAGE);
		searchQueryFormat.addQueryParameter("queryText1", PERCENTAGE + queryText.toLowerCase() + PERCENTAGE);
		final SearchResult<PointOfServiceModel> searchResultFormat = getFlexibleSearchService().search(searchQueryFormat);
		return searchResultFormat.getResult();

	}
	
	

	@Override
	public Collection<PointOfServiceModel> getPointsOfServices()
	{
		final FlexibleSearchQuery searchQueryFormat = new FlexibleSearchQuery(QUERY_ALL_POS);
		searchQueryFormat.setResultClassList(Collections.singletonList(PointOfServiceModel.class));

		final SearchResult<PointOfServiceModel> searchResultFormat = getFlexibleSearchService().search(searchQueryFormat);
		return searchResultFormat.getResult();

	}
}
