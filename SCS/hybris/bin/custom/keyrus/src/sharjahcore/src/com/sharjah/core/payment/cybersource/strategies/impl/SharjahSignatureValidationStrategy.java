package com.sharjah.core.payment.cybersource.strategies.impl;

import de.hybris.platform.acceleratorservices.payment.data.SubscriptionInfoData;
import de.hybris.platform.acceleratorservices.payment.strategies.SignatureValidationStrategy;
import de.hybris.platform.acceleratorservices.payment.utils.AcceleratorDigestUtils;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * @author Pablo François
 */
public class SharjahSignatureValidationStrategy implements SignatureValidationStrategy
{

	private static final Logger LOG = Logger.getLogger(SharjahSignatureValidationStrategy.class);

	private static final String SECRET_KEY = "hop.cybersource.secret-key";

	@Resource
	private AcceleratorDigestUtils sharjahDigestUtils;
	@Resource
	private ConfigurationService configurationService;

	@Override
	public boolean validateSignature(final SubscriptionInfoData subscriptionInfoData)
	{
		final String dataToSign = subscriptionInfoData.getSubscriptionSignedValue();
		final String remoteSignature = subscriptionInfoData.getSubscriptionIDPublicSignature();
		if (StringUtils.isEmpty(dataToSign) || StringUtils.isEmpty(remoteSignature))
		{
			return false;
		}

		try
		{
			final String secretKey = configurationService.getConfiguration().getString(SECRET_KEY, null);
			final String localSignature = sharjahDigestUtils.getPublicDigest(dataToSign, secretKey);
			return remoteSignature.equals(localSignature);
		}
		catch (final InvalidKeyException | NoSuchAlgorithmException e)
		{
			LOG.error("Error validating signature", e);
		}
		return false;
	}

}
