package com.sharjah.core.util;

import de.hybris.platform.core.HybrisEnumValue;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Utility class for String object
 * 
 * @author Pablo François
 */
public class SharjahStringUtils
{
	private SharjahStringUtils()
	{
		// Hide constructor for utility class
	}

	/**
	 * Format a date. Null safe.
	 *
	 * @param date
	 * @param format
	 * @return The formatted value
	 */
	public static String safeFormatDate(final Date date, final String format)
	{
		if (date == null)
		{
			return "";
		}
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	/**
	 * Format a number. Null safe.
	 *
	 * @param number
	 * @return The formatted value
	 */
	public static String safeFormatNumber(final Number number)
	{
		if (number == null)
		{
			return "";
		}
		return String.valueOf(number.intValue());
	}

	/**
	 * Format a hybris enum. Null safe.
	 *
	 * @param hybrisEnum
	 * @return The formatted value
	 */
	public static String safeFormatEnum(final HybrisEnumValue hybrisEnum)
	{
		if (hybrisEnum == null)
		{
			return "";
		}
		return hybrisEnum.getCode();
	}
}
