package com.sharjah.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ProductPageSiteMapGenerator;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author Issam Maiza
 *
 */
public class SharjahProductPageSiteMapGenerator extends ProductPageSiteMapGenerator{

	
	@Override
	protected List<ProductModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final String query = "SELECT {p.pk} FROM {Product AS p JOIN CatalogVersion AS cv ON {p.catalogVersion}={cv.pk} "
				+ " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
				+ " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}}  WHERE {site.pk} = ?site"
				+ " AND {p.approvalStatus} = ?approvalStatus"
				+ " AND ({{SELECT {pa.pk} FROM {ProductPage AS pa JOIN CatalogVersion AS cv ON {pa.catalogVersion}={cv.pk}} WHERE {pa.siteMapEligiblePage} = ?siteMapEligiblePage and {cv.version} = 'Staged'}})";

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("site", siteModel);
		params.put("approvalStatus", ArticleApprovalStatus.APPROVED);
		params.put("siteMapEligiblePage", Boolean.TRUE);
		return doSearch(query, params, ProductModel.class);
	}
}
