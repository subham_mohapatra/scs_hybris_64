package com.sharjah.core.fraud.service.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import com.sharjah.core.model.FraudScoreModel;
import com.sharjah.core.model.FraudSymptomModel;


/**
 * Service for the fraud configuration parameters stored in hybris' model.
 *
 * @author Pablo François
 */
public class FraudConfigService
{

	private static final String NAME = "name";
	private static final String SCORE_BY_NAME = "SELECT {PK} FROM {" + FraudScoreModel._TYPECODE + "} WHERE {name} = ?" + NAME;
	private static final String SYMPTOM_BY_NAME = "SELECT {PK} FROM {" + FraudSymptomModel._TYPECODE + "} WHERE {name} = ?" + NAME;

	@Resource
	FlexibleSearchService flexibleSearchService;

	/**
	 * Find a type of FraudScore by name.
	 *
	 * @param scoreName
	 *           The name of the score
	 * @return The FraudScore
	 */
	public FraudScoreModel findFraudScoreByName(final String scoreName)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SCORE_BY_NAME);
		query.addQueryParameter(NAME, scoreName);
		return flexibleSearchService.searchUnique(query);
	}

	/**
	 * Find a FraudSymptom by name.
	 *
	 * @param symptomName
	 *           The name of the symptom
	 * @return The FraudSymptom
	 */
	public FraudSymptomModel findFraudSymptomByName(final String symptomName)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SYMPTOM_BY_NAME);
		query.addQueryParameter(NAME, symptomName);
		return flexibleSearchService.searchUnique(query);
	}

}
