package com.sharjah.core.deliveryarea.service;

import java.util.List;

import com.sharjah.core.model.DeliveryAreaModel;
import com.sharjah.core.model.DeliveryCityModel;


/**
 * Interface of Sharjah Delivery Area Service
 * 
 * @author Abderrazzak.Blej
 *
 */
public interface SharjahDeliveryAreaService
{

	/**
	 * Find all {@link DeliveryCityModel}s.
	 *
	 * @return
	 */
	List<DeliveryCityModel> findAllDeliveryCities();

	/**
	 * Find a delivery area model by code.
	 *
	 * @param code
	 * @return the deliveryAreaModel
	 */
	DeliveryAreaModel findDeliveryAreaByCode(String code);

	/**
	 * Get all Areas for the given City code.
	 *
	 * @param cityCode
	 * @return List of the delivery areas
	 */
	List<DeliveryAreaModel> findAreasForCityCode(final String cityCode);

	/**
	 * Finds a delivery city by name.
	 *
	 * @param cityCode
	 *           the name of the city
	 * @return The city
	 */
	DeliveryCityModel findDeliveryCityByCode(String cityCode);
}
