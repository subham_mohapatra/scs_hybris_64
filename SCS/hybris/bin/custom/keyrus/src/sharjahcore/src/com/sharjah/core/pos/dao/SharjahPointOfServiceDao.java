package com.sharjah.core.pos.dao;

import de.hybris.platform.storelocator.PointOfServiceDao;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;


/**
 *
 * @author Ben Amor Bassem
 *
 */
public interface SharjahPointOfServiceDao extends PointOfServiceDao
{


	/**
	 * Retrieves Points Of Services By DisplayName
	 *
	 * @param queryText
	 * @return Collection<PointOfServiceModel>
	 */
	Collection<PointOfServiceModel> getPointsOfServicesByDisplayName(String queryText);

	/**
	 * Retrieves Points Of Services despite default
	 *
	 * @return Collection<PointOfServiceModel>
	 */
	Collection<PointOfServiceModel> getPointsOfServices();

}
