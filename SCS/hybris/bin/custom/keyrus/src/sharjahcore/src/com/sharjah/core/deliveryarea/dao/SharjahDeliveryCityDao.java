package com.sharjah.core.deliveryarea.dao;

import java.util.List;

import com.sharjah.core.model.DeliveryCityModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public interface SharjahDeliveryCityDao
{

	/**
	 * Find all {@link DeliveryCityModel}s.
	 *
	 * @return
	 */
	List<DeliveryCityModel> findAll();

	/**
	 * Finds a delivery city by name.
	 *
	 * @param cityName
	 *           the name of the city
	 * @return The city
	 */
	DeliveryCityModel findDeliveryCityByCode(String cityCode);
}
