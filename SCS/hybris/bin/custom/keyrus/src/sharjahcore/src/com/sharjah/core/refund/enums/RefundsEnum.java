package com.sharjah.core.refund.enums;

/**
 * Enumeration used for the refund notifications.
 *
 * @author Pablo François
 */
public enum RefundsEnum
{
	CANCEL_BY_WAREHOUSE_AGENT("CancelRequest", "WarehouseAgent"), CANCEL_BY_CS_AGENT("CancelRequest",
			"CsAgent"), RETURN_BY_CS_AGENT("ReturnRequest", "CsAgent");

	private final String type;
	private final String user;

	/**
	 * Constructor
	 *
	 * @param type
	 *           the type of refund
	 * @param user
	 *           the user who asked for the refund
	 */
	private RefundsEnum(final String type, final String user)
	{
		this.type = type;
		this.user = user;
	}

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @return the user
	 */
	public String getUser()
	{
		return user;
	}

}
