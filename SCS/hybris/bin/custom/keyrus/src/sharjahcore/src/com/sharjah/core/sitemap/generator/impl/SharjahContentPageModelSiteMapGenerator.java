package com.sharjah.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ContentPageModelSiteMapGenerator;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Issam Maiza
 *
 */
public class SharjahContentPageModelSiteMapGenerator extends ContentPageModelSiteMapGenerator {

	@Override
	protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel) {
		final Map<String, Object> params = new HashMap<String, Object>();
		params.put(ContentPageModel.DEFAULTPAGE, Boolean.TRUE);
		params.put("siteMapEligiblePage", Boolean.TRUE);

		final StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT {pa.pk} FROM {ContentPage AS pa JOIN CatalogVersion AS cv ON {pa.catalogVersion}={cv.pk}} "
				+ "WHERE {pa.siteMapEligiblePage} = ?siteMapEligiblePage and {cv.version} = 'Staged' ");
		queryBuilder.append(" AND {pa." + ContentPageModel.DEFAULTPAGE + "}  = ?defaultPage");
		queryBuilder.append(" AND {pa.siteMapEligiblePage} = ?siteMapEligiblePage");

		return doSearch(queryBuilder.toString(), params, ContentPageModel.class);

	}
}
