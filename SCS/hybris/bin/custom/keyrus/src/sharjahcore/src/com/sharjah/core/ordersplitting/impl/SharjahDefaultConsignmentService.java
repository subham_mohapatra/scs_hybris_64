package com.sharjah.core.ordersplitting.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.ConsignmentCreationException;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.impl.DefaultConsignmentService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Ben Amor Bassem
 *
 */
public class SharjahDefaultConsignmentService extends DefaultConsignmentService
{

	private KeyGenerator sharjahConsignmentGuidKeyGenerator;
	private ModelService modelService;
	private WarehouseService warehouseService;

	@Override
	public ConsignmentModel createConsignment(final AbstractOrderModel order, final String code,
			final List<AbstractOrderEntryModel> orderEntries) throws ConsignmentCreationException
	{
		final ConsignmentModel cons = (ConsignmentModel) this.modelService.create(ConsignmentModel.class);
		cons.setStatus(ConsignmentStatus.READY);
		cons.setConsignmentEntries(new HashSet<ConsignmentEntryModel>());
		cons.setCode(sharjahConsignmentGuidKeyGenerator.generate().toString());
		if (order != null)
		{
			cons.setShippingAddress(order.getDeliveryAddress());
		}

		final Iterator<AbstractOrderEntryModel> warehouse = orderEntries.iterator();

		while (warehouse.hasNext())
		{
			final AbstractOrderEntryModel warehouses = warehouse.next();
			final ConsignmentEntryModel entry = (ConsignmentEntryModel) this.modelService.create(ConsignmentEntryModel.class);
			entry.setOrderEntry(warehouses);
			entry.setQuantity(warehouses.getQuantity());
			entry.setConsignment(cons);
			cons.getConsignmentEntries().add(entry);
			cons.setDeliveryMode(warehouses.getDeliveryMode());
		}

		final List<WarehouseModel> warehouses1 = warehouseService.getWarehouses(orderEntries);
		if (warehouses1.isEmpty())
		{
			throw new ConsignmentCreationException("No default warehouse found for consignment");
		}
		else
		{
			final WarehouseModel warehouse1 = warehouses1.iterator().next();
			cons.setWarehouse(warehouse1);
			cons.setOrder(order);
			return cons;
		}
	}

	/**
	 * 
	 * @return
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	@Override
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * 
	 * @return
	 */
	public WarehouseService getWarehouseService()
	{
		return warehouseService;
	}

	@Override
	public void setWarehouseService(final WarehouseService warehouseService)
	{
		this.warehouseService = warehouseService;
	}

	/**
	 * 
	 * @return
	 */
	public KeyGenerator getSharjahConsignmentGuidKeyGenerator()
	{
		return sharjahConsignmentGuidKeyGenerator;
	}

	/**
	 * 
	 * @param sharjahConsignmentGuidKeyGenerator
	 */
	public void setSharjahConsignmentGuidKeyGenerator(final KeyGenerator sharjahConsignmentGuidKeyGenerator)
	{
		this.sharjahConsignmentGuidKeyGenerator = sharjahConsignmentGuidKeyGenerator;
	}

}
