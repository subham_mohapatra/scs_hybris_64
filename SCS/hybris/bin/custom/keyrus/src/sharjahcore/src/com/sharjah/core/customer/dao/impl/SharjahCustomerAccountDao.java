package com.sharjah.core.customer.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Pablo François
 */
public class SharjahCustomerAccountDao extends DefaultCustomerAccountDao
{
	private static final String FIND_PAYMENT_INFO_BY_CUSTOMER_QUERY = "SELECT {" + CreditCardPaymentInfoModel.PK + "} FROM {"
			+ CreditCardPaymentInfoModel._TYPECODE + "} WHERE {" + CreditCardPaymentInfoModel.USER + "} = ?customer AND {"
			+ CreditCardPaymentInfoModel.CODE + "} = ?code AND {" + CreditCardPaymentInfoModel.DUPLICATE + "} = ?duplicate";

	private static final String FIND_PAYMENT_INFO_BY_CODE_QUERY = "SELECT {" + CreditCardPaymentInfoModel.PK + "} FROM {"
			+ CreditCardPaymentInfoModel._TYPECODE + "} WHERE {" + CreditCardPaymentInfoModel.CODE + "} = ?code AND {"
			+ CreditCardPaymentInfoModel.DUPLICATE + "} = ?duplicate";

	/**
	 * Override to look by code instead of PK, as the native method does.
	 */
	@Override
	public CreditCardPaymentInfoModel findCreditCardPaymentInfoByCustomer(final CustomerModel customerModel, final String code)
	{
		validateParameterNotNull(customerModel, "Customer must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("customer", customerModel);
		queryParams.put("duplicate", Boolean.FALSE);
		queryParams.put("code", code);
		final SearchResult<CreditCardPaymentInfoModel> result = getFlexibleSearchService().search(
				FIND_PAYMENT_INFO_BY_CUSTOMER_QUERY, queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	/**
	 * Look for a CreditCardPaymentInfo by code.
	 *
	 * @param code
	 *           The payment code
	 * @return A credit card payment info
	 */
	public CreditCardPaymentInfoModel findCreditCardPaymentInfoByCode(final String code)
	{
		validateParameterNotNull(code, "Code must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("code", code);
		queryParams.put("duplicate", Boolean.FALSE);
		final SearchResult<CreditCardPaymentInfoModel> result = getFlexibleSearchService().search(
				FIND_PAYMENT_INFO_BY_CODE_QUERY, queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

}
