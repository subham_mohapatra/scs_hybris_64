package com.sharjah.core.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sharjah.core.orders.service.SharjahOrderService;

/**
 * @author issam maiza
 * 
 * CreditCardRecaptureOrderCronjob call sharjahOrderService to recapture all commands that wasn't captured
 * 
 */
public class CreditCardRecaptureOrderCronjob extends AbstractJobPerformable<CronJobModel> {

	@Resource(name = "sharjahOrderService")
	private SharjahOrderService sharjahOrderService;

	private static final Logger LOG = LogManager.getLogger(CreditCardRecaptureOrderCronjob.class);

	@Override
	public PerformResult perform(final CronJobModel arg0) {
		if (LOG.isInfoEnabled()) {
			LOG.info("Start -> recapture order with faild capture");
		}

		sharjahOrderService.recaptureOrderswithFailedCapture();

		if (LOG.isInfoEnabled()) {
			LOG.info("Finish -> recapture order with faild capture");
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * @return the sharjahOrderService
	 */
	public SharjahOrderService getSharjahOrderService() {
		return sharjahOrderService;
	}

	/**
	 * @param sharjahOrderService
	 *            the sharjahOrderService to set
	 */
	public void setSharjahOrderService(final SharjahOrderService sharjahOrderService) {
		this.sharjahOrderService = sharjahOrderService;
	}
}
