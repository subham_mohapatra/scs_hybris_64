package com.sharjah.core.util;

/**
 * Utility class for int object
 *
 */
public class SharjahIntUtils
{
	private SharjahIntUtils()
	{
		//empty
	}

	/**
	 * Check if an int is superior to 0
	 * 
	 * @param intToCheck
	 * @return boolean
	 */
	public static boolean checkInt(final int intToCheck)
	{
		if (intToCheck > 0)
		{
			return true;
		}
		return false;
	}
}
