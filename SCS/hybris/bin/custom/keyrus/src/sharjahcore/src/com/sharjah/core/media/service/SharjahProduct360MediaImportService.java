package com.sharjah.core.media.service;

/**
 * 
 * @author Wael Sakhri
 *
 */
public interface SharjahProduct360MediaImportService {

	/**
	 * Import 360 product media according to the configuration
	 * (media folder to import and archive or delete data options)
	 */
	public void importProduct360Media();
	
}
