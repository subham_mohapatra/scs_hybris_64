package com.sharjah.core.order.strategies.calculation.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.strategies.calculation.impl.DefaultFindDeliveryCostStrategy;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.PriceValue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.cartthresholdconfiguration.service.SharjahCartThresholdConfigurationService;
import com.sharjah.core.model.CartThresholdConfigurationModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class SharjahFindDeliveryCostStrategy extends DefaultFindDeliveryCostStrategy
{
	private static final long serialVersionUID = 1L;
	
	protected static final Logger LOG = LogManager.getLogger(SharjahFindDeliveryCostStrategy.class);
	
	private static final String CART_THRESHOLD_CODE = "cart_threshold_code";

	private CartService cartService;
	private SharjahCartThresholdConfigurationService sharjahCartThresholdConfigurationService;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PriceValue getDeliveryCost(final AbstractOrderModel order)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("order", order);
		try
		{
			final DeliveryModeModel deliveryMode = order.getDeliveryMode();

			final CartModel cartModel = getCartService().getSessionCart();
			final CartThresholdConfigurationModel cartThresholdConfiguration = sharjahCartThresholdConfigurationService
					.getCartThresholdConfigurationByCode(CART_THRESHOLD_CODE);

			double threshold = 0;
			if (cartThresholdConfiguration != null && cartThresholdConfiguration.getThreshold() != null)
			{
				threshold = cartThresholdConfiguration.getThreshold().doubleValue();
			}
			final double totalPrice = cartModel.getEntries().stream().mapToDouble(entry -> entry.getTotalPrice()).sum();

			if (deliveryMode != null)
			{
				if (cartThresholdConfiguration != null && totalPrice >= threshold && totalPrice > 0)
				{
					return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
				}

				getModelService().save(order);
				
				final AbstractOrder orderItem = getModelService().getSource(order);
				final DeliveryMode dModeJalo = getModelService().getSource(deliveryMode);
				return dModeJalo.getCost(orderItem);
			}
			else
			{
				return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
			}
		}
		catch (final Exception exception)
		{
			LOG.warn("Could not find deliveryCost for current order", exception);
			return new PriceValue(order.getCurrency().getIsocode(), 0.0, order.getNet().booleanValue());
		}
	}
	
	/**
	 * 
	 * @return cartService
	 */
	protected CartService getCartService()
	{
		return cartService;
	}

	/**
	 * to set cartService
	 * 
	 * @param cartService
	 */
	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}
	
	/**
	 * 
	 * @return cartThresholdConfigurationService
	 */
	public SharjahCartThresholdConfigurationService getSharjahCartThresholdConfigurationService()
	{
		return sharjahCartThresholdConfigurationService;
	}

	/**
	 * to set cartThresholdConfigurationService
	 * 
	 * @param cartThresholdConfigurationService
	 */
	@Required
	public void setSharjahCartThresholdConfigurationService(
			final SharjahCartThresholdConfigurationService sharjahCartThresholdConfigurationService)
	{
		this.sharjahCartThresholdConfigurationService = sharjahCartThresholdConfigurationService;
	}
}
