package com.sharjah.core.orders.service;

/**
 * @author issam maiza
 * 
 * SharjahOrderService interface for Sharjah Order Service
 * 
 */
public interface SharjahOrderService {

	
	/**
	 * recarpture orders with status equal to PAYMENT_NOT_CAPTURED.
	 */
	void recaptureOrderswithFailedCapture();

}
