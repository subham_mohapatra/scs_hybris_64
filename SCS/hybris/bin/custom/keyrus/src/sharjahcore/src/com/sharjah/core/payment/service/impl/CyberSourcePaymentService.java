package com.sharjah.core.payment.service.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.FollowOnRefundRequest;
import de.hybris.platform.payment.commands.result.RefundResult;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.impl.DefaultPaymentServiceImpl;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @author Pablo François
 */
public class CyberSourcePaymentService extends DefaultPaymentServiceImpl
{
	private static Logger LOG = LogManager.getLogger(CyberSourcePaymentService.class);

	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private ModelService modelService;

	/**
	 * Override the native method to validate that an entry of type PaymentTransactionType.CAPTURE exists, instead of
	 * PaymentTransactionType.AUTHORIZATION. Also, the native code has been refactored to be more readable.
	 */
	@Override
	public PaymentTransactionEntryModel refundFollowOn(final PaymentTransactionModel transaction, final BigDecimal amount)
	{
		final PaymentTransactionEntryModel auth = CollectionUtils.emptyIfNull(transaction.getEntries()).stream()
				.filter(entry -> entry.getType().equals(PaymentTransactionType.CAPTURE))
				.findFirst()
				.orElse(null);

		if (auth == null)
		{
			throw new AdapterException("Could not refund follow-on without authorization");
		}

		final PaymentTransactionType transactionType = PaymentTransactionType.REFUND_FOLLOW_ON;
		final String newEntryCode = this.getNewPaymentTransactionEntryCode(transaction, transactionType);
		final Currency currency = Currency.getInstance(auth.getCurrency().getIsocode());
		final FollowOnRefundRequest followOnRefundRequest = new FollowOnRefundRequest(newEntryCode, transaction.getRequestId(),
				transaction.getRequestToken(), currency, amount, transaction.getPaymentProvider());
		final RefundResult result = getCardPaymentService().refundFollowOn(followOnRefundRequest);

		final PaymentTransactionEntryModel entry = (PaymentTransactionEntryModel) modelService
				.create(PaymentTransactionEntryModel.class);
		if (result.getCurrency() != null)
		{
			entry.setCurrency(commonI18NService.getCurrency(result.getCurrency().getCurrencyCode()));
		}

		entry.setType(transactionType);
		entry.setTime(result.getRequestTime() == null ? new Date() : result.getRequestTime());
		entry.setPaymentTransaction(transaction);
		entry.setAmount(result.getTotalAmount());
		entry.setRequestId(result.getRequestId());
		entry.setRequestToken(result.getRequestToken());
		entry.setTransactionStatus(result.getTransactionStatus().toString());
		entry.setTransactionStatusDetails(result.getTransactionStatusDetails().toString());
		entry.setCode(newEntryCode);
		modelService.save(entry);
		if (LOG.isInfoEnabled())
		{
			LOG.info("Refund PaymentTransactionEntry saved: {}", newEntryCode);
		}
		return entry;
	}

}
