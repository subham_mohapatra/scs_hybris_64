package com.sharjah.core.product.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author Luiz.Henriques
 *
 */
public interface SharjahProductDao extends ProductDao
{

	/**
	 * Search products to check eligibility, based on the difference between dates/modified time
	 * 
	 * @param lastExecutedTime
	 * @return List<ProductModel>
	 */
	List<ProductModel> getProductsToEligibilityForPublication(Date lastExecutedTime);

	/**
	 * Find products by EAN and catalog version
	 * 
	 * @param catalogVersion
	 * @param ean
	 * @return List<ProductModel>
	 */
	List<ProductModel> findProductsByEAN(final CatalogVersionModel catalogVersion, final String ean);
	
	/**
	 * Find products by EAN
	 * 
	 * @param ean
	 * @return List<ProductModel>
	 */
	List<ProductModel> findProductsByEAN(final String ean);
	
	/**
	 * 
	 * @param productCode
	 * @return
	 */
	List<StockLevelModel> getStockLevelForProductCode(String productCode);

}
