package com.sharjah.core.util;

/**
 * Utility class for Double object
 *
 */
public class SharjahDoubleUtils
{
	private SharjahDoubleUtils()
	{
		//empty
	}

	/**
	 * Check if a double is not null and superior to 0
	 * 
	 * @param doubleToCheck
	 * @return boolean
	 */
	public static boolean checkDouble(final Double doubleToCheck)
	{
		if (doubleToCheck != null && doubleToCheck.doubleValue() > 0)
		{
			return true;
		}
		return false;
	}
}
