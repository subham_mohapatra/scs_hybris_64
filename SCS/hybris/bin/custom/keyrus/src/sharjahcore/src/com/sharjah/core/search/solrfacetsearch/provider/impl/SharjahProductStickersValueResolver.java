package com.sharjah.core.search.solrfacetsearch.provider.impl;


import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.model.StickerModel;
import com.sharjah.core.sticker.SharjahStickerUtils;


/**
 * Resolvers for stickers
 * 
 * @author issam.maiza
 * 
 */
public class SharjahProductStickersValueResolver extends AbstractValueResolver<ProductModel, List<StickerModel>, Object>{
	public static final String SEPARATOR = "#";
	private static final String OPTIONAL_PARAM = "optional";
	private static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	private static final String STICKER_DATE_PATTERN = "yyyyMMddHH:mm:ss";
	
	public static final ThreadLocal<DateFormat> STICKER_DATE_FORMAT = new ThreadLocal<DateFormat>()
	{
		@Override
		protected DateFormat initialValue()
		{
			final DateFormat df = new SimpleDateFormat(STICKER_DATE_PATTERN, Locale.US);
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			return df;
		}
	};
	
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final ProductModel product,
			final ValueResolverContext<List<StickerModel>, Object> resolverContext) throws FieldValueProviderException
	{

		boolean hasValue = false;
		final List<StickerModel> stickers = resolverContext.getData();

		if (CollectionUtils.isNotEmpty(stickers))
		{
			hasValue = addFieldDocument(document, stickers, indexedProperty, resolverContext);
		}

		if (!hasValue)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}

	}

	/**
	 * 
	 * add field to document and return boolean
	 * 
	 * @param InputDocument
	 * @param List<StickerModel>
	 * @param IndexedProperty
	 * @param ValueResolverContext<List<StickerModel>,
	 *           Object>
	 * @return boolean
	 */
	private boolean addFieldDocument(final InputDocument document, final List<StickerModel> stickers,
			final IndexedProperty indexedProperty, final ValueResolverContext<List<StickerModel>, Object> resolverContext)
			throws FieldValueProviderException
	{
		boolean hasValue = false;
		final String indexedPropertyName = indexedProperty.getName();
		for (final StickerModel sticker : stickers)
		{
			final String name = sticker.getName();
			if (SharjahStickerUtils.stickerValidation(sticker) && StringUtils.isNotEmpty(name))
			{
				if (SharjahCoreConstants.INDEXEDED_PROPERTY_STICKER.equals(indexedPropertyName))
				{
					hasValue = true;
					if (sticker.getImage() != null)
					{
						document.addField(indexedProperty, name + SEPARATOR + sticker.getImage().getURL(),
								resolverContext.getFieldQualifier());
					}
					else
					{
						document.addField(indexedProperty, name, resolverContext.getFieldQualifier());
					}
				}
				else if (SharjahCoreConstants.INDEXEDED_PROPERTY_STICKER_START_DATE.equals(indexedPropertyName))
				{
					final Date date = sticker.getStartDate();
					hasValue = addStickerDateField(document, indexedProperty, resolverContext, name, date);
				}
				else if (SharjahCoreConstants.INDEXEDED_PROPERTY_STICKER_END_DATE.equals(indexedPropertyName))
				{
					final Date date = sticker.getEndDate();
					hasValue = addStickerDateField(document, indexedProperty, resolverContext, name, date);
				}
			}
		}

		return hasValue;
	}

	@Override
	protected List<StickerModel> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final ProductModel product) throws FieldValueProviderException
	{
		return (List<StickerModel>) product.getStickers();
	}

	/**
	 * Adds in the document field value
	 * 
	 * @param document
	 *           the solr document
	 * @param indexedProperty
	 * @param resolverContext
	 * @param name
	 * @param date
	 * @return
	 * @throws FieldValueProviderException
	 */
	private boolean addStickerDateField(final InputDocument document, final IndexedProperty indexedProperty,
			final ValueResolverContext<List<StickerModel>, Object> resolverContext, final String name, final Date date)
			throws FieldValueProviderException
	{
		boolean hasValue = false;
		if (date != null)
		{
			hasValue = true;
			document.addField(indexedProperty, name + SEPARATOR + STICKER_DATE_FORMAT.get().format(date),
					resolverContext.getFieldQualifier());
		}
		return hasValue;
	}
}
