package com.sharjah.core.deliveryslots.service;

import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.model.DeliverySlotByPOSModel;


/**
 * Service for operations with the Delivery Slot elements.
 *
 * @author juanpablo.francois@keyrus.com
 */
public interface DeliverySlotService
{

	/**
	 * Get all the slots for a defined PoS, regardless of time and capacity.
	 *
	 * @param pos
	 *           The PoS to look for
	 * @param beginDate
	 *           The beginning date for the range
	 * @param endDate
	 *           The ending date for the range
	 * @return The calendar
	 */
	List<CalendarDeliverySlotModel> findSlotsForPos(PointOfServiceModel pos, Date beginDate, Date endDate);

	/**
	 * Get all the slots for a defined PoS with available capacity, regardless of time.
	 *
	 * @param pos
	 *           The PoS to look for
	 * @param beginDate
	 *           The beginning date for the range
	 * @param endDate
	 *           The ending date for the range
	 * @return The calendar
	 */
	List<CalendarDeliverySlotModel> findSlotsWithCapacityForPos(PointOfServiceModel pos, Date beginDate, Date endDate);

	/**
	 * Check if a slot has expired for today.
	 *
	 * @param source
	 *           The slot to check
	 * @param firstDeliveryDate
	 *           The first possible available date for delivery
	 * @return If the slot time has already passed
	 */
	boolean isSlotExpired(CalendarDeliverySlotModel source, LocalDate firstDeliveryDate);

	/**
	 * Check if a slot is selected in the current cart.
	 *
	 * @param source
	 *           The slot to check
	 * @return If the slot is selected
	 */
	boolean isSlotSelected(CalendarDeliverySlotModel source);

	/**
	 * Find a slot with the given parameters.
	 *
	 * @param pos
	 *           A Point of Service name
	 * @param date
	 *           A date
	 * @param deliverySlot
	 *           A delivery slot code
	 * @return
	 */
	CalendarDeliverySlotModel findSlotWithDate(String pos, Date date, String deliverySlot);

	/**
	 * Find a slot with the given parameters.
	 *
	 * @param pos
	 *           A Point of Service name
	 * @param dayOfWeek
	 *           A day of the week code
	 * @param deliverySlot
	 *           A delivery slot code
	 * @return
	 */
	DeliverySlotByPOSModel findSlotWithDayOfWeek(String pos, String dayOfWeek, String deliverySlot);

	/**
	 * Delete all the calendar delivery slots that correspond to a date after the parameter.
	 *
	 * @param startDate
	 *           All slots after this date will be deleted
	 */
	void deleteSlotsAfter(LocalDate startDate);

}
