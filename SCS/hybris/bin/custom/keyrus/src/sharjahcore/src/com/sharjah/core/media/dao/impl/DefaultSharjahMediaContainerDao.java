package com.sharjah.core.media.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.impl.DefaultMediaContainerDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sharjah.core.media.dao.SharjahMediaContainerDao;


/**
 * Implementation of the Default DAO for the Sharjah Media Container
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultSharjahMediaContainerDao extends DefaultMediaContainerDao implements SharjahMediaContainerDao
{
	@Override
    public List<MediaContainerModel> findMediaContainersByQualifier(final String qualifier,
                                                                    final CatalogVersionModel catalogVersion)
    {
        final Map<String, Object> params = new HashMap<>();
        params.put("qualifier", qualifier);
        params.put("catalogVersion", catalogVersion);

        final StringBuilder builder = new StringBuilder();
        builder.append("SELECT {").append("pk").append("} ");
        builder.append("FROM {").append("MediaContainer").append("} ");
        builder.append("WHERE {").append("qualifier").append("}=?qualifier ");
        builder.append("and {").append("catalogVersion").append("}=?catalogVersion ");
        builder.append("ORDER BY {").append("pk").append("} ASC");
        final SearchResult<MediaContainerModel> result = getFlexibleSearchService().search(builder.toString(), params);
        return result.getResult();
    }
}
