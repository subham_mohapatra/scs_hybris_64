package com.sharjah.dataimport.batch.converter.decorator;

import org.apache.commons.lang3.StringUtils;


public class DiscountCurrencyDecorator extends AbstractDiscountDecorator
{
	@Override
	protected String decorate(final String discountValue, final String percentageValue)
	{
		if (StringUtils.isNotEmpty(discountValue))
		{
			return "AED";
		}
		else
		{
			return null;
		}
	}
}
