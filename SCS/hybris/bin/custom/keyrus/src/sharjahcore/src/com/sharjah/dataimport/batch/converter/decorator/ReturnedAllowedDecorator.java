package com.sharjah.dataimport.batch.converter.decorator;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;

import java.util.Map;


/**
 * 
 * @author Issam Maiza
 *
 */
public class ReturnedAllowedDecorator extends AbstractImpExCSVCellDecorator {

	private static final String TRUE = "true";
	private static final String YES = "yes";
	private static final String FALSE = "false";
	private static final String NO = "no";

	@Override
	public String decorate(final int position, final Map<Integer, String> srcLine) {
		final String cellValue = srcLine.get(Integer.valueOf(position));

		if (YES.equalsIgnoreCase(cellValue)) {
			return TRUE;
		}
		if (NO.equalsIgnoreCase(cellValue)) {
			return FALSE;
		}
		return cellValue;
	}
}
