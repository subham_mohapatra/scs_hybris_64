package com.sharjah.core.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


/**
 * Utility class for Date object
 * 
 * @author Pablo François
 */
final public class SharjahDateUtils
{

	private SharjahDateUtils()
	{
		// Hide constructor for utility class
	}

	/**
	 * Create a LocalDate from a date.
	 *
	 * @param date
	 *           The date to convert
	 * @return A LocalDate
	 */
	public static LocalDate toLocalDate(final Date date)
	{
		return new java.sql.Date(date.getTime()).toLocalDate();
	}

	/**
	 * Create a Date from a LocalDate.
	 *
	 * @param localDate
	 *           The LocalDate to convert
	 * @return A Date
	 */
	public static Date toDate(final LocalDate localDate)
	{
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

}
