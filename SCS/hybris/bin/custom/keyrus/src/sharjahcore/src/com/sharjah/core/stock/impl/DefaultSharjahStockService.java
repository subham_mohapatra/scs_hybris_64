package com.sharjah.core.stock.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.impl.DefaultStockService;
import de.hybris.platform.warehousing.inventoryevent.service.InventoryEventService;
import de.hybris.platform.warehousing.model.CancellationEventModel;

import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.sharjah.core.stock.SharjahStockService;

/**
 * @author issam.maiza
 * 
 *         DefaultSharjahStockService Class for implementing interface
 *         SharjahStockService
 *
 */

public class DefaultSharjahStockService extends DefaultStockService implements SharjahStockService {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(DefaultSharjahStockService.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "inventoryEventService")
	private InventoryEventService inventoryEventService;

	/**
	 * 
	 * implementing releaseStockAfterCancelOrder method to release stock after
	 * cancel
	 *
	 */
	@Override
	public void releaseStockAfterCancelOrder(final OrderCancelRequest orderCancelRequest) throws OrderCancelException {
		for (final OrderCancelEntry orderCancelEntry : orderCancelRequest.getEntriesToCancel()) {
			final AbstractOrderEntryModel orderEntry = orderCancelEntry.getOrderEntry();

			final Set<ConsignmentEntryModel> consignmentEntries = orderEntry.getConsignmentEntries();
			if (consignmentEntries.isEmpty()) {
				LOG.error("Unable to update the quantity of the consignmentEntry for this orderEntry "
						+ orderEntry.getPk()
						+ " : the orderEntry doesn't have a consignmentEntry linked to this orderEntry");
			} else {
				for (ConsignmentEntryModel consignmentEntry : consignmentEntries) {

					final Optional<OrderCancelEntry> myEntry = orderCancelRequest.getEntriesToCancel().stream()
							.filter(entry -> entry.getOrderEntry().equals(consignmentEntry.getOrderEntry()))
							.findFirst();

					final CancelReason myReason = myEntry.isPresent() ? myEntry.get().getCancelReason()
							: orderCancelRequest.getCancelReason();
					createCancellationEvent(orderEntry, orderCancelEntry.getCancelQuantity(), consignmentEntry,
							myReason);
				}
			}

		}
	}

	/**
	 * 
	 * @param orderEntry
	 * @param orderCancelQuantity
	 * @param newConsignmentEntry
	 * @param myReason
	 */
	private void createCancellationEvent(final AbstractOrderEntryModel orderEntry, final long orderCancelQuantity,
			final ConsignmentEntryModel newConsignmentEntry, final CancelReason myReason) {
		final CancellationEventModel event = new CancellationEventModel();
		event.setConsignmentEntry(newConsignmentEntry);
		event.setOrderEntry((OrderEntryModel) orderEntry);
		event.setReason(myReason.getCode());
		event.setQuantity(orderCancelQuantity);
		inventoryEventService.createCancellationEvent(event);
		getModelService().save(newConsignmentEntry);
	}

	@Override
	public ModelService getModelService() {
		return modelService;
	}

	@Override
	public void setModelService(final ModelService modelService) {
		this.modelService = modelService;
	}

	/**
	 * 
	 * @return
	 */
	public InventoryEventService getInventoryEventService() {
		return inventoryEventService;
	}

	/**
	 * 
	 * @param inventoryEventService
	 */
	public void setInventoryEventService(final InventoryEventService inventoryEventService) {
		this.inventoryEventService = inventoryEventService;
	}
	 
}
