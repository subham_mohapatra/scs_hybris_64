package com.sharjah.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.PointOfServicePageSiteMapGenerator;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author Issam Maiza
 *
 */
public class SharjahPointOfServicePageSiteMapGenerator extends PointOfServicePageSiteMapGenerator {

	@Override
	protected List<PointOfServiceModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final List<BaseStoreModel> stores = siteModel.getStores();
		final String query = "SELECT {ps.pk} FROM {PointOfService as ps} WHERE {ps.BaseStore} in (?stores)"
				+ " AND ({{SELECT {pa.pk} FROM {ContentPage AS pa JOIN CatalogVersion AS cv ON {pa.catalogVersion}={cv.pk}} "
				+ "WHERE {pa.uid} = 'storefinderPage' and {pa.siteMapEligiblePage} = ?siteMapEligiblePage and {cv.version} = 'Staged'}})";

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("stores", stores);
		params.put("siteMapEligiblePage", Boolean.TRUE);
		return doSearch(query, params, PointOfServiceModel.class);
	}
}