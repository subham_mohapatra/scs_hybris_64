package com.sharjah.core.customer.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.sharjah.core.customer.dao.SharjahNationalityDao;
import com.sharjah.core.model.NationalityModel;

/**
 * 
 * @author Wael Sakhri
 *
 */
public class DefaultSharjahNationalityDao extends DefaultGenericDao<NationalityModel> implements SharjahNationalityDao
{
	private static final String CODE = "code";
	
	private static final String QUERY_NAT = "SELECT {nt:pk} FROM {Nationality as nt } WHERE {nt:code} != '971' ORDER BY {nt:name}";


	public DefaultSharjahNationalityDao()
	{
		super(NationalityModel._TYPECODE);
	}

	@Override
	public List<NationalityModel> getAllNationality()
	{
		//return find();

		final FlexibleSearchQuery searchQueryFormat = new FlexibleSearchQuery(QUERY_NAT);
		searchQueryFormat.setResultClassList(Collections.singletonList(PointOfServiceModel.class));

		final SearchResult<NationalityModel> searchResultFormat = getFlexibleSearchService().search(searchQueryFormat);
		return searchResultFormat.getResult();
	}

	@Override
	public NationalityModel getNationalityByCode(final String code)
	{

		ServicesUtil.validateParameterNotNull(code, "code must not be null");

		final Map<String, Object> params = new HashMap<>();
		params.put(CODE, code);

		final List<NationalityModel> natios = find(params);
		if (CollectionUtils.isEmpty(natios))
		{
			return null;
		}

		return natios.get(0);

	}

}
