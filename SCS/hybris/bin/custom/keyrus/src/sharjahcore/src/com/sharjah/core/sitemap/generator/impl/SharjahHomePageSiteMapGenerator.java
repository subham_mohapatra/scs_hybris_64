package com.sharjah.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.HomePageSiteMapGenerator;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 * Override of HomePageSiteMapGenerator to look on the attribute SITEMAPELIGIBLEPAGE
 * @author Issam Maiza
 *
 */
public class SharjahHomePageSiteMapGenerator extends HomePageSiteMapGenerator {

	private static final Logger LOG = LogManager.getLogger(SharjahHomePageSiteMapGenerator.class);
	private static final String CATALOGID  = "scsContentCatalog";
	private static final String CATALOGVERSIONNAME  = "Staged" ;

	@Override
	protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final ContentPageModel homepage = getCmsPageService().getHomepage();
		if (homepage !=  null)
		{
			try {
				final CatalogVersionModel catalogVersionModel = getCatalogVersionService().getCatalogVersion(CATALOGID, CATALOGVERSIONNAME);
				final ContentPageModel homepagetomap = getCmsPageService().getDefaultPageForLabel(homepage.getLabel(), catalogVersionModel);
				if (Boolean.TRUE.equals(homepagetomap.getSiteMapEligiblePage()))
				{
				return Collections.singletonList(homepage);
				}
			} catch (final CMSItemNotFoundException e) {
				if (LOG.isDebugEnabled())
				{
					LOG.debug("error when getting home page from site : " , e);
				}
			}

		}
		return Collections.emptyList();
	}
}
