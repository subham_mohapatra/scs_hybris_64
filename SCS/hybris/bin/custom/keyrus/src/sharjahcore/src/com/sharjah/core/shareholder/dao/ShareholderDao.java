package com.sharjah.core.shareholder.dao;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.sharjah.core.model.ShareholderModel;


/**
 * The DAO for Shareholder Model
 *
 */
public interface ShareholderDao extends Dao {

	/**
	 * 
	 * @param number
	 * @return
	 */
	List<ShareholderModel> findShareholderByNumber(final String number);
}
