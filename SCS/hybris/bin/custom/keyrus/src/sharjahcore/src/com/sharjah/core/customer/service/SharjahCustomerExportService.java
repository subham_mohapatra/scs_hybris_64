package com.sharjah.core.customer.service;

import de.hybris.platform.core.model.user.CustomerModel;

import java.io.IOException;
import java.util.List;


/**
 * @author Pablo François
 */
public interface SharjahCustomerExportService
{
	/**
	 * Export new or updated customers to a file.
	 *
	 * @param customers
	 *           The customers to export
	 * @throws IOException
	 */
	void exportCustomers(List<CustomerModel> customers) throws IOException;
}
