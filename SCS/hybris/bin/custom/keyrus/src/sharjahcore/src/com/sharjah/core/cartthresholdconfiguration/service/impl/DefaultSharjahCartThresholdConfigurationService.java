package com.sharjah.core.cartthresholdconfiguration.service.impl;

import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.cartthresholdconfiguration.dao.SharjahCartThresholdConfigurationDao;
import com.sharjah.core.cartthresholdconfiguration.service.SharjahCartThresholdConfigurationService;
import com.sharjah.core.model.CartThresholdConfigurationModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahCartThresholdConfigurationService implements SharjahCartThresholdConfigurationService
{

	private SharjahCartThresholdConfigurationDao sharjahCartThresholdConfigurationDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartThresholdConfigurationModel getCartThresholdConfigurationByCode(final String code)
	{
		return sharjahCartThresholdConfigurationDao.getCartThresholdConfigurationByCode(code);
	}

	/**
	 * 
	 * @return sharjahCartThresholdConfigurationDao
	 */
	public SharjahCartThresholdConfigurationDao getSharjahCartThresholdConfigurationDao()
	{
		return sharjahCartThresholdConfigurationDao;
	}

	/**
	 * set sharjahCartThresholdConfigurationDao
	 * 
	 * @param sharjahCartThresholdConfigurationDao
	 */
	@Required
	public void setSharjahCartThresholdConfigurationDao(
			final SharjahCartThresholdConfigurationDao sharjahCartThresholdConfigurationDao)
	{
		this.sharjahCartThresholdConfigurationDao = sharjahCartThresholdConfigurationDao;
	}

}
