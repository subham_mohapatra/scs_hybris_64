package com.sharjah.core.deliveryslots.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sharjah.core.deliveryslots.dao.DeliverySlotDao;
import com.sharjah.core.enums.DaysOfDeliveryEnum;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.model.DeliverySlotByPOSModel;
import com.sharjah.core.model.DeliverySlotMappingModel;
import com.sharjah.core.model.DeliverySlotScheduleModel;
import com.sharjah.core.util.SharjahDateUtils;

import reactor.util.CollectionUtils;


/**
 * @author juanpablo.francois@keyrus.com
 */
@Repository
public class DefaultDeliverySlotDao extends AbstractItemDao implements DeliverySlotDao
{

	private static final Logger LOG = LogManager.getLogger(DefaultDeliverySlotDao.class);

	private static final String PARAM_POS = "pos";
	private static final String PARAM_BEGIN_DATE = "beginDate";
	private static final String PARAM_END_DATE = "endDate";
	private static final String PARAM_DATE = "date";
	private static final String PARAM_DELIVERY_SLOT = "deliverySlot";
	private static final String PARAM_DAY = "day";
	private static final String PARAM_START_DATE = "startDate";
	private static final String SELECT_FROM = "SELECT {PK} FROM { ";

	private static final String QUERYS_SLOTS = SELECT_FROM + CalendarDeliverySlotModel._TYPECODE + " AS c JOIN "
			+ DeliverySlotScheduleModel._TYPECODE + " AS s ON {c.deliverySlot} = {s.pk} } WHERE {c.pos} = ?" + PARAM_POS
			+ " AND {c.date} BETWEEN ?" + PARAM_BEGIN_DATE + " AND ?" + PARAM_END_DATE;
	private static final String ORDER_BY = " ORDER BY {c.date}, {s.beginTime}";

	private static final String QUERY_SLOTS_ALL = QUERYS_SLOTS + ORDER_BY;
	private static final String QUERY_SLOTS_CAPACITY = QUERYS_SLOTS + " AND {c.capacity} > 0" + ORDER_BY;

	private static final String QUERY_SLOT_WITH_DATE = SELECT_FROM + CalendarDeliverySlotModel._TYPECODE + " AS s JOIN "
			+ PointOfServiceModel._TYPECODE + " AS p ON {s.pos} = {p.pk} JOIN " + DeliverySlotScheduleModel._TYPECODE
			+ " AS e ON {s.deliverySlot} = {e.pk} }" + " WHERE {p.name} = ?" + PARAM_POS + " AND {s.date} = ?" + PARAM_DATE
			+ " AND {e.code} = ?" + PARAM_DELIVERY_SLOT;

	private static final String QUERY_SLOT_WITH_DAY = SELECT_FROM + DeliverySlotByPOSModel._TYPECODE + " AS s JOIN "
			+ PointOfServiceModel._TYPECODE + " AS p ON {s.pos} = {p.pk} JOIN " + DeliverySlotScheduleModel._TYPECODE
			+ " AS e ON {s.deliverySlot} = {e.pk}" + " JOIN " + DaysOfDeliveryEnum._TYPECODE + " AS d ON {s.dayOfWeek} = {d.pk} }"
			+ " WHERE {p.name} = ?" + PARAM_POS + " AND {d.code} = ?" + PARAM_DAY + " AND {e.code} = ?" + PARAM_DELIVERY_SLOT;

	private static final String QUERY_ALL_MAPPINGS = SELECT_FROM + DeliverySlotMappingModel._TYPECODE + "}";

	private static final String QUERY_CALENDAR_SLOTS_AFTER = SELECT_FROM + CalendarDeliverySlotModel._TYPECODE + "}"
			+ " WHERE {date} >= ?" + PARAM_START_DATE;

	/**
	 * Perform a flexible search for delivery slots.
	 */
	private List<CalendarDeliverySlotModel> findSlots(final String query, final PointOfServiceModel pos, final Date beginDate,
			final Date endDate)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_POS, pos);
		params.put(PARAM_BEGIN_DATE, beginDate);
		params.put(PARAM_END_DATE, endDate);

		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(query);
		fsQuery.addQueryParameters(params);
		fsQuery.setNeedTotal(false);

		final SearchResult<CalendarDeliverySlotModel> result = getFlexibleSearchService().search(fsQuery);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CalendarDeliverySlotModel> findSlotsForPos(final PointOfServiceModel pos, final Date beginDate, final Date endDate)
	{
		return findSlots(QUERY_SLOTS_ALL, pos, beginDate, endDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CalendarDeliverySlotModel> findSlotsWithCapacityForPos(final PointOfServiceModel pos, final Date beginDate,
			final Date endDate)
	{
		return findSlots(QUERY_SLOTS_CAPACITY, pos, beginDate, endDate);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CalendarDeliverySlotModel findSlotWithDate(final String pos, final Date date, final String deliverySlot)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_POS, pos);
		params.put(PARAM_DATE, date);
		params.put(PARAM_DELIVERY_SLOT, deliverySlot);

		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(QUERY_SLOT_WITH_DATE);
		fsQuery.addQueryParameters(params);
		fsQuery.setNeedTotal(false);

		return getFlexibleSearchService().searchUnique(fsQuery);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotByPOSModel findSlotWithDayOfWeek(final String pos, final String dayOfWeek, final String deliverySlot)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_POS, pos);
		params.put(PARAM_DAY, dayOfWeek);
		params.put(PARAM_DELIVERY_SLOT, deliverySlot);

		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(QUERY_SLOT_WITH_DAY);
		fsQuery.addQueryParameters(params);
		fsQuery.setNeedTotal(false);

		return getFlexibleSearchService().searchUnique(fsQuery);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliverySlotMappingModel> findAllMappings()
	{
		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(QUERY_ALL_MAPPINGS);
		fsQuery.setNeedTotal(false);

		final SearchResult<DeliverySlotMappingModel> result = getFlexibleSearchService().search(fsQuery);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliverySlotMappingModel findCurrentMappingForPos(final PointOfServiceModel pos)
	{
		final LocalTime currentTime = LocalTime.now();

		final List<DeliverySlotMappingModel> slotMappings = findAllMappings().stream()
				.filter(mapping -> {
					final LocalTime beginTime = LocalTime.parse(mapping.getRule().getCheckoutTimeFrom());
					final LocalTime endTime = LocalTime.parse(mapping.getRule().getCheckoutTimeTo());
					return currentTime.compareTo(beginTime) >= 0 && currentTime.isBefore(endTime);
				})
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(slotMappings))
		{
			LOG.warn("No deliverySlot is found for the given hour");
			return null;
		}
		if (slotMappings.size() > 1)
		{
			LOG.warn("Several deliverySlots are found for the given hour, returning the first result");
		}
		return slotMappings.get(0);
	}

	@Override
	public void deleteSlotsAfter(final LocalDate startDate)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(PARAM_START_DATE, SharjahDateUtils.toDate(startDate));

		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(QUERY_CALENDAR_SLOTS_AFTER);
		fsQuery.addQueryParameters(params);
		fsQuery.setNeedTotal(false);

		final SearchResult<CalendarDeliverySlotModel> result = getFlexibleSearchService().search(fsQuery);
		getModelService().removeAll(result.getResult());
	}

	@Override
	@Autowired
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		super.setFlexibleSearchService(flexibleSearchService);
	}

	@Override
	@Autowired
	public void setModelService(final ModelService modelService)
	{
		super.setModelService(modelService);
	}

}
