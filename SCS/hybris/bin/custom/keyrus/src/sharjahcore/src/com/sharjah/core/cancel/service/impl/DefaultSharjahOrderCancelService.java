package com.sharjah.core.cancel.service.impl;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.ordercancel.CancelDecision;
import de.hybris.platform.ordercancel.OrderCancelDenialReason;
import de.hybris.platform.ordercancel.OrderCancelDenialStrategy;
import de.hybris.platform.ordercancel.impl.DefaultOrderCancelService;
import de.hybris.platform.ordercancel.model.OrderCancelConfigModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

/**
 * @author Ben Amor Bassem
 */
@Service
public class DefaultSharjahOrderCancelService extends DefaultOrderCancelService
{
	@Override
	public CancelDecision isCancelPossible(final OrderModel order, final PrincipalModel requestor, final boolean partialCancel,
			final boolean partialEntryCancel)
	{

		final OrderCancelConfigModel configuration = this.getConfiguration();
		final ArrayList<OrderCancelDenialReason> reasons = new ArrayList<OrderCancelDenialReason>();
		final Iterator<OrderCancelDenialStrategy> cancelDenialStrategy = getCancelDenialStrategies().iterator();

		while (cancelDenialStrategy.hasNext())
		{
			final OrderCancelDenialStrategy cancelAllowed = cancelDenialStrategy.next();
			final OrderCancelDenialReason result = cancelAllowed.getCancelDenialReason(configuration, order, requestor,
					partialCancel, partialEntryCancel);
			if (result != null)
			{
				reasons.add(result);
			}
		}

		boolean cancelAllowed1;
		if (reasons.isEmpty())
		{
			cancelAllowed1 = true;
		}
		else
		{
			cancelAllowed1 = false;
		}

		if (CollectionUtils.isNotEmpty(order.getConsignments()))
		{
			for (final ConsignmentModel consignmentModel : order.getConsignments())
			{
				if (ConsignmentStatus.SHIPPED.equals(consignmentModel.getStatus()))
				{
					cancelAllowed1 = false;
				}
			}
		}

		if (OrderStatus.COMPLETED.equals(order.getStatus()))
		{
			cancelAllowed1 = false;
		}

		return new CancelDecision(cancelAllowed1, reasons);
	}
}
