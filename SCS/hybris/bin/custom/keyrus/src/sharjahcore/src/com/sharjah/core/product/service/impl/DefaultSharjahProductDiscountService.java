package com.sharjah.core.product.service.impl;


import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.jalo.order.OrderManager;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sharjah.core.product.service.SharjahProductDiscountService;

/**
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultSharjahProductDiscountService implements SharjahProductDiscountService
{
	private static final Logger LOG = Logger.getLogger(DefaultSharjahProductDiscountService.class);
	
	private static final Double ONEHUNDRED = Double.valueOf(100);
	
	private ModelService modelService;
	
	private UserService userService;
	
	private CommonI18NService commonI18NService;
	
	@Override
	public Double calculateDiscount(final Double regularPrice, final ProductModel product) {
		final UserModel user = userService.getCurrentUser();
		final CurrencyModel currency = commonI18NService.getCurrentCurrency();
		final int digits = currency.getDigits().intValue();
		try
		{
			final List<DiscountValue> discounts = Europe1Tools
					.createDiscountValueList(getCurrentPriceFactory().matchDiscountRows(modelService.getSource(product), null,
					modelService.getSource(user), null, modelService.getSource(currency), new Date(), -1));
			
			//1 (one) is the quantity imposed to PDP
			final List<DiscountValue> appliedDiscounts = DiscountValue.apply(1, regularPrice.doubleValue(), digits,
					convertDiscountValues(currency, discounts), currency.getIsocode());
			
			if(CollectionUtils.isNotEmpty(appliedDiscounts))
			{
				double totalPrice = regularPrice.doubleValue();
				for (final Iterator<DiscountValue> it = appliedDiscounts.iterator(); it.hasNext();)
				{
					totalPrice -= it.next().getAppliedValue();
				}
				return new Double(totalPrice);
			}
		}
		catch(final JaloPriceFactoryException e)
		{
			LOG.error(String.format("There was an error trying to get discount for product %s", product.getCode()), e);
		}
		
		return new Double(0);
	}
	
	/**
	 * Convert Discount Values
	 * 
	 * @param curr
	 * @param dvs
	 * @return List
	 */
	protected List<DiscountValue> convertDiscountValues(final CurrencyModel curr, final List<DiscountValue> dvs)
	{
		if (dvs == null)
		{
			return Collections.emptyList();
		}
		if (CollectionUtils.isEmpty(dvs))
		{
			return dvs;
		}
		//

		final String iso = curr.getIsocode();
		final List<DiscountValue> tmp = new ArrayList<>(dvs);
		/*
		 * convert absolute discount values to order currency is needed
		 */
		// just don't search twice for an isocode
		final Map<String, CurrencyModel> currencyMap = new HashMap<String, CurrencyModel>();
		for (int i = 0; i < tmp.size(); i++)
		{
			final DiscountValue discountValue = tmp.get(i);
			if (discountValue.isAbsolute() && !iso.equals(discountValue.getCurrencyIsoCode()))
			{
				// get currency
				CurrencyModel dCurr = currencyMap.get(discountValue.getCurrencyIsoCode());
				if (dCurr == null)
				{
					dCurr = commonI18NService.getCurrency(discountValue.getCurrencyIsoCode());
					currencyMap.put(discountValue.getCurrencyIsoCode(), dCurr);
				}
				// replace old value in temp list
				tmp.set(
						i,
						new DiscountValue(discountValue.getCode(),
								commonI18NService.convertAndRoundCurrency(dCurr.getConversion().doubleValue(), curr.getConversion()
										.doubleValue(), curr.getDigits().intValue(), discountValue.getValue()), true, iso));
			}
		}
		return tmp;
	}
	
	
	@Override
	public int calculatePercentageDiscount(final Double originalPrice, final Double discountPrice)
	{
		Double percentage = Double.valueOf(0);
		
		percentage = ONEHUNDRED - ((discountPrice * ONEHUNDRED) / originalPrice) ;
		
		return BigDecimal.valueOf(percentage).setScale(0, RoundingMode.HALF_EVEN).intValue();
	}
	
	/**
	 * Get the current price factory
	 * 
	 * @return Europe1PriceFactory
	 */
	public Europe1PriceFactory getCurrentPriceFactory()
	{
		// Actually OrderManager.getPriceFactory() implements default / session specific price
		// factory fetching. So no need to do it twice.
		return (Europe1PriceFactory) OrderManager.getInstance().getPriceFactory();
	}
	
	/**
	 * 
	 * @param commonI18NService
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService) {
		this.commonI18NService = commonI18NService;
	}
	
	/**
	 * 
	 * @param modelService
	 */
	public void setModelService(final ModelService modelService) {
		this.modelService = modelService;
	}
	
	/**
	 * 
	 * @param userService
	 */
	public void setUserService(final UserService userService) {
		this.userService = userService;
	}
}
