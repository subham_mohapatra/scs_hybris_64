package com.sharjah.core.customer.dao;

import java.util.List;

import com.sharjah.core.model.NationalityModel;

/**
 * 
 * @author Wael Sakhri
 *
 */
public interface SharjahNationalityDao
{

	/**
	 * Get nationality items
	 * 
	 * @return {@NationalityModel} List
	 */
	List<NationalityModel> getAllNationality();

	/**
	 * 
	 * @param code
	 * @return
	 */
	NationalityModel getNationalityByCode(String code);
}
