package com.sharjah.core.deliveryslots.dao;

import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.model.DeliverySlotByPOSModel;
import com.sharjah.core.model.DeliverySlotMappingModel;


/**
 * @author juanpablo.francois@keyrus.com
 */
public interface DeliverySlotDao
{

	/**
	 * Get all the slots for a defined PoS, regardless of time and capacity.
	 *
	 * @param pos
	 *           The PoS to look for
	 * @param beginDate
	 *           The beginning date for the range
	 * @param endDate
	 *           The ending date for the range
	 * @return The calendar
	 */
	List<CalendarDeliverySlotModel> findSlotsForPos(PointOfServiceModel pos, Date beginDate, Date endDate);

	/**
	 * Get all the slots for a defined PoS with available capacity, regardless of time.
	 *
	 * @param pos
	 *           The PoS to look for
	 * @param beginDate
	 *           The beginning date for the range
	 * @param endDate
	 *           The ending date for the range
	 * @return The calendar
	 */
	List<CalendarDeliverySlotModel> findSlotsWithCapacityForPos(PointOfServiceModel pos, Date beginDate, Date endDate);

	/**
	 * Find a slot with the given parameters.
	 *
	 * @param pos
	 *           A Point of Service name
	 * @param date
	 *           A date
	 * @param deliverySlot
	 *           A delivery slot code
	 * @return
	 */
	CalendarDeliverySlotModel findSlotWithDate(String pos, Date date, String deliverySlot);

	/**
	 * Find a slot with the given parameters.
	 *
	 * @param pos
	 *           A Point of Service name
	 * @param dayOfWeek
	 *           A day of the week code
	 * @param deliverySlot
	 *           A delivery slot code
	 * @return
	 */
	DeliverySlotByPOSModel findSlotWithDayOfWeek(String pos, String dayOfWeek, String deliverySlot);

	/**
	 * Find the mapping for the current time and given Point of Service.
	 *
	 * @param pos
	 *           A Point of Service
	 * @return The mapping
	 */
	DeliverySlotMappingModel findCurrentMappingForPos(PointOfServiceModel pos);

	/**
	 * Find all the delivery slot mappings.
	 *
	 * @return The mappings
	 */
	List<DeliverySlotMappingModel> findAllMappings();

	/**
	 * Delete all the calendar delivery slots that correspond to a date after the parameter.
	 *
	 * @param startDate
	 *           All slots after this date will be deleted
	 */
	void deleteSlotsAfter(LocalDate startDate);

}
