package com.sharjah.core.ticket.service.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ticket.enums.CsInterventionType;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.events.model.CsCustomerEventModel;
import de.hybris.platform.ticket.model.CsAgentGroupModel;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.impl.DefaultTicketBusinessService;
import de.hybris.platform.ticket.strategies.TicketEventStrategy;

import javax.annotation.Resource;


/**
 * Extends TicketBusinessService to add a new method for creating a manual refund ticket for the csAgentGroup.
 *
 * @author Pablo François
 */
public class SharjahTicketBusinessService extends DefaultTicketBusinessService
{
	private static final long serialVersionUID = 1L;

	private static final String CUSTOMER_SUPPORT_AGENT_GROUP = "customersupportagentgroup";

	@Resource
	private TicketEventStrategy ticketEventStrategy;

	/**
	 * Create a manual refund ticket for the csAgentGroup.
	 *
	 * @param csTicketCategory
	 *           the csTicketCategory
	 * @param order
	 *           the order linked to the ticket
	 * @param headline
	 *           the subject of the ticket
	 * @param creationNotes
	 *           the notes of the ticket
	 * @return a new CsTicketModel
	 */
	public CsTicketModel createTicketForManualRefund(final CsTicketCategory csTicketCategory, final OrderModel order,
			final String headline, final String creationNotes)
	{
		final CsAgentGroupModel assignedGroup = getUserService().getUserGroupForUID(CUSTOMER_SUPPORT_AGENT_GROUP,
				CsAgentGroupModel.class);

		final CsTicketModel csTicket = super.createTicket(order.getUser(), order, csTicketCategory, CsTicketPriority.HIGH, null,
				assignedGroup, headline, CsInterventionType.TICKETMESSAGE, null, creationNotes);
		csTicket.setBaseSite(order.getSite());
		getModelService().save(csTicket);
		return csTicket;
	}

	/***
	 * Override the initial method for not sending an email.
	 */
	@Override
	protected CsTicketModel createTicketInternal(final CsTicketModel ticket, final CsCustomerEventModel creationEvent)
	{
		final BaseSiteModel currentBaseSite = this.getBaseSiteService().getCurrentBaseSite();
		if (currentBaseSite != null)
		{
			ticket.setBaseSite(currentBaseSite);
		}
		ticketEventStrategy.ensureTicketEventSetupForCreationEvent(ticket, creationEvent);
		getModelService().saveAll(ticket, creationEvent);
		getModelService().refresh(ticket);
		return ticket;
	}

}
