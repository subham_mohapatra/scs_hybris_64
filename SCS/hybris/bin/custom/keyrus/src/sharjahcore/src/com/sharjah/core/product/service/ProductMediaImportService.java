package com.sharjah.core.product.service;

/**
 * Interface of the Product Media Import Service
 * 
 * @author Luiz.Henriques
 *
 */
public interface ProductMediaImportService
{

	
	/**
	 * Triggers the import media job.
	 * Reads the media files in the specific folder and assign it to the products
	 * 
	 * @param brand
	 */
	void importMedia(String brand);
	
}
