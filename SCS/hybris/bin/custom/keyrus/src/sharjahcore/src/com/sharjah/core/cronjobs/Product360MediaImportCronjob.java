package com.sharjah.core.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import com.sharjah.core.media.service.SharjahProduct360MediaImportService;


/**
 * 
 * @author Wael Sakhri
 *
 */
public class Product360MediaImportCronjob extends AbstractJobPerformable<CronJobModel> {

	@Resource
	SharjahProduct360MediaImportService product360MediaImportService;
	
	@Override
	public PerformResult perform(final CronJobModel arg0) {

		product360MediaImportService.importProduct360Media ();

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
