package com.sharjah.core.payment.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.dao.impl.DefaultCreditCardPaymentSubscriptionDao;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;


/**
 * @author Pablo François
 */
public class SharjahCreditCardPaymentSubscriptionDao extends DefaultCreditCardPaymentSubscriptionDao
{

	private static final String PAYMENT_QUERY = "SELECT {p.pk} FROM {CreditCardPaymentInfo as p}"
			+ " WHERE {p.subscriptionId} = ?subscriptionId AND {p.duplicate} = ?duplicate"
			+ " ORDER BY {" + CreditCardPaymentInfoModel.MODIFIEDTIME + "} DESC";

	/**
	 * Make sure to get a PaymentInfo that is not a duplicate to avoid bugs like the paymentInfo getting removed from
	 * older orders when creating a new one.
	 */
	@Override
	public CreditCardPaymentInfoModel findCreditCartPaymentBySubscription(final String subscriptionId)
	{
		validateParameterNotNull(subscriptionId, "subscriptionId must not be null!");

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(PAYMENT_QUERY);
		fQuery.addQueryParameter("subscriptionId", subscriptionId);
		fQuery.addQueryParameter("duplicate", Boolean.FALSE);

		final SearchResult<CreditCardPaymentInfoModel> searchResult = getFlexibleSearchService().search(fQuery);
		final List<CreditCardPaymentInfoModel> results = searchResult.getResult();

		if (results != null && results.iterator().hasNext())
		{
			return results.iterator().next();
		}

		return null;
	}

}
