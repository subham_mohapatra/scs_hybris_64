package com.sharjah.core.payment.cybersource.constants;

import java.util.HashMap;
import java.util.Map;


/**
 * @author Pablo François
 */
public class CyberSourceConstants
{
	public static final String PAYMENT_SALE = "authorization";
	public static final String PAYMENT_PROVIDER = "CyberSource";
	public static final String REASON_CODE = "reason_code";
	public static final String CARD = "card";
	public static final String DEVICE_FINGERPRINT_ID = "device_fingerprint_id";
	public static final String REQ_DEVICE_FINGERPRINT_ID = "req_device_fingerprint_id";
	public static final String SIGNED_DATE_TIME = "signed_date_time";
	public static final String REQ_BILL_TO_ADDRESS_STATE = "req_bill_to_address_state";
	public static final String REQ_BILL_TO_ADDRESS_COUNTRY = "req_bill_to_address_country";
	public static final String REQ_BILL_TO_PHONE = "req_bill_to_phone";
	public static final String REQ_BILL_TO_EMAIL = "req_bill_to_email";
	public static final String REQ_BILL_TO_ADDRESS_POSTAL_CODE = "req_bill_to_address_postal_code";
	public static final String REQ_BILL_TO_ADDRESS_CITY = "req_bill_to_address_city";
	public static final String REQ_BILL_TO_ADDRESS_LINE2 = "req_bill_to_address_line2";
	public static final String REQ_BILL_TO_ADDRESS_LINE1 = "req_bill_to_address_line1";
	public static final String REQ_BILL_TO_SURNAME = "req_bill_to_surname";
	public static final String REQ_BILL_TO_FORENAME = "req_bill_to_forename";
	public static final String REQ_TRANSACTION_UUID = "req_transaction_uuid";
	public static final String REQ_LOCALE = "req_locale";
	public static final String PAYMENT_TOKEN = "payment_token";
	public static final String REQUEST_TOKEN = "request_token";
	public static final String REQUEST_ID = "transaction_id";
	public static final String REQ_CARD_EXPIRY_DATE = "req_card_expiry_date";
	public static final String REQ_CARD_NUMBER = "req_card_number";
	public static final String REQ_CARD_TYPE = "req_card_type";
	public static final String DECISION = "decision";
	public static final String SIGNATURE = "signature";
	public static final String PAYMENT_METHOD = "payment_method";
	public static final String CURRENCY = "currency";
	public static final String AMOUNT = "amount";
	public static final String REFERENCE_NUMBER = "reference_number";
	public static final String LOCALE = "locale";
	public static final String SALE_AND_CREATE_PAYMENT_TOKEN = "authorization,create_payment_token";
	public static final String TRANSACTION_TYPE = "transaction_type";
	public static final String TRANSACTION_UUID = "transaction_uuid";
	public static final String PROFILE_ID = "profile_id";
	public static final String ACCESS_KEY = "access_key";
	public static final String UNSIGNED_FIELD_NAMES = "unsigned_field_names";
	public static final String SIGNED_FIELD_NAMES = "signed_field_names";
	public static final String CVV_FIELD_NAME = "card_cvn";

	public static final String PAYER_AUTHENTICATION_ECI = "payer_authentication_eci";
	public static final String ECI01 = "01";
	public static final String ECI06 = "06";

	public static final String PAYMENT_CREDITCARD = "creditcard";
	public static final String PAYMENT_COD = "cod";
	public static final String PAYMENT_CREDITCARD_OD = "creditcardod";

	public static final String DECISION_ACCEPT = "ACCEPT";
	public static final String DECISION_REVIEW = "REVIEW";
	public static final String DECISION_REJECT = "REJECT";
	public static final String REASON_CODE_SUCCESS = "100";

	public static final Map<String, String> CARD_TYPES = new HashMap<>();

	static
	{
		CARD_TYPES.put("001", "visa");
		CARD_TYPES.put("002", "master");
		CARD_TYPES.put("003", "amex");
		CARD_TYPES.put("004", "discover");
	}
}
