package com.sharjah.core.comparators;

import java.util.Comparator;

import com.sharjah.core.model.DeliveryCityModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class SharjahDeliveryCityModelComparator implements Comparator<DeliveryCityModel>
{

	@Override
	public int compare(final DeliveryCityModel deliveryCityModel1, final DeliveryCityModel deliveryCityModel2)
	{
		return deliveryCityModel1.getName().compareTo(deliveryCityModel2.getName());
	}

}
