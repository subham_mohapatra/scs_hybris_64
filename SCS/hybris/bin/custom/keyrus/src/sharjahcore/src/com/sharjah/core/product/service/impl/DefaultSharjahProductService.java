package com.sharjah.core.product.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.product.impl.DefaultProductService;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.sharjah.core.product.dao.SharjahProductDao;
import com.sharjah.core.product.service.SharjahProductService;
import com.sharjah.core.util.SharjahDoubleUtils;
import com.sharjah.core.util.SharjahIntUtils;

/**
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultSharjahProductService extends DefaultProductService implements SharjahProductService
{

	private static final Locale LOCALE_EN = new Locale("en");
	private static final Locale LOCALE_AR = new Locale("ar");

	private SharjahProductDao productDao;
	
	/*
	 * (non-Javadoc)
	 * @see com.sharjah.core.product.service.SharjahProductService#getProductsToCheckEligibility(java.util.Date)
	 */
	@Override
	public List<ProductModel> getProductsToEligibilityForPublication(final Date lastExecutedTime)
	{
		return productDao.getProductsToEligibilityForPublication(lastExecutedTime);
	}
	
	@Override
	public void checkProductEligibility(final ProductModel productModel)
	{
		if (hasName(productModel) && hasSummary(productModel) && hasPrimaryAndGalleryImages(productModel) && hasStock(productModel)
				&& hasPrice(productModel))
		{
			productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
		}
		else
		{
			productModel.setApprovalStatus(ArticleApprovalStatus.CHECK);
		}
		getModelService().save(productModel);

	}

	/**
	 * 
	 * @param productModel
	 * @return
	 */
	private boolean hasName(final ProductModel productModel)
	{
		//i. Title (english and arabic)
		final String nameEn = productModel.getName(LOCALE_EN);
		final String nameAr = productModel.getName(LOCALE_AR);
		if (StringUtils.isEmpty(nameEn) || StringUtils.isEmpty(nameAr))
		{
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param productModel
	 * @return
	 */
	private boolean hasSummary(final ProductModel productModel)
	{
		//		ii. Description (english and arabic)
		final String descriptionEn = productModel.getDescription(LOCALE_EN);
		final String descriptionAr = productModel.getDescription(LOCALE_AR);
		if (StringUtils.isEmpty(descriptionEn) || StringUtils.isEmpty(descriptionAr))
		{
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param productModel
	 * @return
	 */
	private boolean hasPrimaryAndGalleryImages(final ProductModel productModel)
	{
		//		iii. Image (1 image mini)
		final Collection<MediaContainerModel> galleryImages = productModel.getGalleryImages();
		final MediaModel primaryImage = productModel.getPicture();
		
		if (CollectionUtils.isNotEmpty(galleryImages) && primaryImage != null)
		{
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param productModel
	 * @return
	 */
	private boolean hasPrice(final ProductModel productModel)
	{
		//		iv. Active price and > 0
		final Collection<PriceRowModel> priceRows = productModel.getEurope1Prices();

		if (CollectionUtils.isNotEmpty(priceRows))
		{
			final PriceRowModel priceRow = priceRows.iterator().next();
			if (priceRow != null)
			{
				return checkPrice(priceRow);
			}
		}
		return false;
	}

	/**
	 * Check the Price Row and its Double
	 * 
	 * @param priceRow
	 * @return
	 */
	private boolean checkPrice(final PriceRowModel priceRow)
	{
		final Double priceValue = priceRow.getPrice();

		if (SharjahDoubleUtils.checkDouble(priceValue))
		{
			if ((priceRow.getStartTime() == null && priceRow.getEndTime() == null)
					|| (priceRow.getStartTime() != null && priceRow.getEndTime() != null && new Date().after(priceRow.getStartTime())
							&& new Date().before(priceRow.getEndTime())))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param productModel
	 * @return
	 */
	private boolean hasStock(final ProductModel productModel)
	{
		//		v. Availability
		final List<StockLevelModel> stockLevels = productDao.getStockLevelForProductCode(productModel.getCode());

		if (CollectionUtils.isNotEmpty(stockLevels))
		{
			final int available = stockLevels.get(0).getAvailable();
			return SharjahIntUtils.checkInt(available);
		}
		return false;
	}
	
	@Override
	public ProductModel findProductByEAN(final String ean)
	{
		validateParameterNotNull(ean, "Parameter ean must not be null");
		final List<ProductModel> products = productDao.findProductsByEAN(ean);
		if (CollectionUtils.isNotEmpty(products))
		{
			return products.get(0);
		}
		else
		{
			return null;
		}
	}

	@Override
	public ProductModel findProductByEAN(final CatalogVersionModel catalogVersion, final String ean)
	{
		final List<ProductModel> products = productDao.findProductsByEAN(catalogVersion, ean);

		if (CollectionUtils.isNotEmpty(products))
		{
			return products.get(0);
		}
		else
		{
			return null;
		}
	}
	
	
	@Override
	public ProductModel findProductByCodeForMedia(final CatalogVersionModel catalogVersion, final String code) {
		final List<ProductModel> products = productDao.findProductsByCode(catalogVersion, code);
		if (CollectionUtils.isNotEmpty(products)) {
			return products.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public void setProductDao(final ProductDao productDao)
	{
		this.productDao = (SharjahProductDao) productDao;
        super.setProductDao(productDao);
	}

}
