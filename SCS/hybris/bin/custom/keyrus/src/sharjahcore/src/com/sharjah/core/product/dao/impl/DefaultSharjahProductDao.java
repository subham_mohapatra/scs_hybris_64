package com.sharjah.core.product.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sharjah.core.product.dao.SharjahProductDao;
/**
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultSharjahProductDao extends DefaultProductDao implements SharjahProductDao
{

	private static final String FIND_PRODUCTS_TO_ELIGIBILITY_FOR_PUBLICATION_QUERY = " select {p:pk} as pk from {product as p "
			+ " join  stocklevel as sl on {p:code} = {sl:productCode}} where {p:modifiedtime} >= ?lastExecutedTime OR  {sl:modifiedtime} >= ?lastExecutedTime";

	private static final String FIND_STOCK_LEVEL_BY_CODE_PRODUCT_QUERY = "SELECT {pk} FROM {StockLevel} WHERE {productCode} = ?productCode";

	/**
	 * 
	 * @param typecode
	 */
	public DefaultSharjahProductDao(final String typecode)
	{
		super(typecode);
	}
	
	@Override
	public List<ProductModel> getProductsToEligibilityForPublication(final Date lastExecutedTime)
	{
		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(FIND_PRODUCTS_TO_ELIGIBILITY_FOR_PUBLICATION_QUERY);
		final Map<String, Object> parameters = new HashMap<>();
		parameters.put("lastExecutedTime", lastExecutedTime);
		fsQuery.addQueryParameters(parameters);

		return getFlexibleSearchService().<ProductModel> search(fsQuery).getResult();
	}

	@Override
	public List<StockLevelModel> getStockLevelForProductCode(final String productCode)
	{
		final FlexibleSearchQuery fsQuery = new FlexibleSearchQuery(FIND_STOCK_LEVEL_BY_CODE_PRODUCT_QUERY);
		final Map<String, Object> parameters = new HashMap<>();
		parameters.put("productCode", productCode);
		fsQuery.addQueryParameters(parameters);

		return getFlexibleSearchService().<StockLevelModel> search(fsQuery).getResult();
	}

	@Override
	public List<ProductModel> findProductsByEAN(final CatalogVersionModel catalogVersion, final String ean)
	{
		validateParameterNotNull(catalogVersion, "CatalogVersion must not be null!");
		validateParameterNotNull(ean, "Product ean must not be null!");

		final Map parameters = new HashMap();
		parameters.put(ProductModel.EAN, ean);
		parameters.put(ProductModel.CATALOGVERSION, catalogVersion);

		return find(parameters);
	}
	
	@Override
	public List<ProductModel> findProductsByEAN(final String ean)
	{
		validateParameterNotNull(ean, "Product ean must not be null!");

		final Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(ProductModel.EAN, ean);
		
		return find(parameters);
	}

}
