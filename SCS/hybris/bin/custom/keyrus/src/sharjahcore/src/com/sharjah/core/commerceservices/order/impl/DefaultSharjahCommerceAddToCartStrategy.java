package com.sharjah.core.commerceservices.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.product.ConfiguratorSettingsService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahCommerceAddToCartStrategy extends DefaultCommerceAddToCartStrategy
{
	private ConfiguratorSettingsService configuratorSettingsService;
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameter.getCart();
		final ProductModel productModel = parameter.getProduct();
		final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();
		final UnitModel unit = parameter.getUnit();

		final boolean forceNewEntry = parameter.isCreateNewEntry()
				|| CollectionUtils.isNotEmpty(getConfiguratorSettingsService().getConfiguratorSettingsForProduct(productModel));

		final UnitModel orderableUnit = (unit != null ? unit : getUnit(parameter));

		// We are allowed to add items to the cart
		CartEntryModel cartEntryModel;

		if (deliveryPointOfService == null)
		{
			// Modify the cart
			cartEntryModel = getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange, orderableUnit,
					APPEND_AS_LAST, !forceNewEntry);
		}
		else
		{
			// Find the entry to modify
			final Integer entryNumber = getEntryForProductAndPointOfService(cartModel, productModel, deliveryPointOfService);

			// Modify the cart
			cartEntryModel = getCartService().addNewEntry(cartModel, productModel, actualAllowedQuantityChange, orderableUnit,
					entryNumber.intValue(), (entryNumber.intValue() < 0) ? false : !forceNewEntry);
		}

		if (cartEntryModel != null)
		{
			cartEntryModel.setDeliveryPointOfService(deliveryPointOfService);
			if (parameter.getEntryConf() != null)
			{
				cartEntryModel.setEntryConf(parameter.getEntryConf());
			}
			getModelService().save(cartEntryModel);
		}
		return cartEntryModel;
	}
	
	protected ConfiguratorSettingsService getConfiguratorSettingsService()
	{
		return configuratorSettingsService;
	}

	@Required
	public void setConfiguratorSettingsService(final ConfiguratorSettingsService configuratorSettingsService)
	{
		this.configuratorSettingsService = configuratorSettingsService;
	}
}
