package com.sharjah.core.sticker;
import java.util.Date;

import com.sharjah.core.model.StickerModel;


/**
 * Tools for the Sharjah's Stickers
 * 
 * @author issam.maiza
 *
 */
public class SharjahStickerUtils {

	/**
	 * To Validate a sticker
	 * 
	 * @param stickerModel
	 * @return boolean
	 */
	public static boolean stickerValidation(final StickerModel stickerModel) {
		if ((stickerModel.getStartDate() != null && stickerModel.getEndDate() != null
				&& stickerModel.getStartDate().before(new Date()) && stickerModel.getEndDate().after(new Date()))
				|| (stickerModel.getStartDate() == null) || (stickerModel.getEndDate() == null)) {
			return true;
		}
		return false;
	}
}
