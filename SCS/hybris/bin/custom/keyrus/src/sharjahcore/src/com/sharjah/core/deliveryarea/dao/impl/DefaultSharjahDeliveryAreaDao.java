package com.sharjah.core.deliveryarea.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.sharjah.core.deliveryarea.dao.SharjahDeliveryAreaDao;
import com.sharjah.core.model.DeliveryAreaModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahDeliveryAreaDao extends DefaultGenericDao<DeliveryAreaModel> implements SharjahDeliveryAreaDao
{

	/**
	 * Default constructor.
	 */
	public DefaultSharjahDeliveryAreaDao()
	{
		super(DeliveryAreaModel._TYPECODE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryAreaModel findDeliveryAreaByCode(final String code)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(DeliveryAreaModel.CODE, code);
		final List<DeliveryAreaModel> deliveryAreas = find(queryParams);
		if (CollectionUtils.isNotEmpty(deliveryAreas))
		{
			return deliveryAreas.get(0);
		}

		return null;
	}

}
