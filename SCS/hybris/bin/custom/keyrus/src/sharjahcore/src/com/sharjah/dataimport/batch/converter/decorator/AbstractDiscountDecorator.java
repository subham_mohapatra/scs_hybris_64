package com.sharjah.dataimport.batch.converter.decorator;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;


public abstract class AbstractDiscountDecorator extends AbstractImpExCSVCellDecorator
{
	@Override
	public String decorate(final int position, final Map<Integer, String> srcLine)
	{
		final String cellValue = srcLine.get(Integer.valueOf(position));
		if (StringUtils.isEmpty(cellValue))
		{
			return cellValue;
		}

		final int pipeIndex = cellValue.indexOf('|');
		if (pipeIndex < 0)
		{
			throw new IllegalStateException("Value should contains '|' character");
		}

		final String discountValue = cellValue.substring(0, pipeIndex);
		final String percentageValue = cellValue.substring(pipeIndex + 1);

		return decorate(discountValue, percentageValue);
	}

	protected abstract String decorate(String discountValue, String percentageValue);
}
