/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sharjah.core.customer.service;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.sharjah.core.model.NationalityModel;


/**
 * Interface for the Customer Account Service
 */
public interface SharjahCustomerAccountService extends CustomerAccountService
{

	/**
	 * Updates the current user with the given parameters
	 *
	 * @param customerModel
	 * @param customerData
	 * @throws DuplicateUidException
	 */
	void updateProfile(final CustomerModel customerModel, final CustomerData customerData) throws DuplicateUidException;

	/**
	 * Get nationality items
	 * 
	 * @return {@NationalityModel} List
	 */
	List<NationalityModel> getAllNationality();

	/**
	 * Get nationality by code
	 * 
	 * @param code
	 * @return
	 */
	NationalityModel getNationalityByCode(String code);
	
}
