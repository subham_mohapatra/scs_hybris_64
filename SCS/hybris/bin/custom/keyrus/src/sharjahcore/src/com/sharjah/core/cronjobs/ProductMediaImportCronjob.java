package com.sharjah.core.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sharjah.core.product.service.ProductMediaImportService;

public class ProductMediaImportCronjob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LogManager.getLogger(ProductMediaImportCronjob.class);

	private ProductMediaImportService productMediaImportService;
	
	private List<String> brandList;
	
	@Override
	public PerformResult perform(final CronJobModel job)
	{
		for(final String brandName : brandList)
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Start -> import medias for ["+brandName+"]");
			}
			productMediaImportService.importMedia(brandName);
			if (LOG.isInfoEnabled())
			{
				LOG.info("Finish -> import medias for ["+brandName+"]");
			}
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
	
	public void setProductMediaImportService(final ProductMediaImportService productMediaImportService) {
		this.productMediaImportService = productMediaImportService;
	}
	
	
	public void setBrandList(final List<String> brandList)
	{
		this.brandList = brandList;
	}
	
}
