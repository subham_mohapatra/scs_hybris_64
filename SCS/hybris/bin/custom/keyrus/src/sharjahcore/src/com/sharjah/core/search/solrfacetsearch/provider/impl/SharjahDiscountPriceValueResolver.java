package com.sharjah.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.Qualifier;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sharjah.core.product.service.SharjahProductDiscountService;
import com.sharjah.core.util.SharjahDoubleUtils;

/**
 * 
 * @author Luiz.Henriques
 *
 */
public class SharjahDiscountPriceValueResolver extends AbstractValueResolver<ProductModel, ProductModel, List<PriceInformation>>
{
	private static final Logger LOG = Logger.getLogger(SharjahDiscountPriceValueResolver.class);

	private PriceService priceService;
	
	private SharjahProductDiscountService productDiscountService;
	
	@Override
	protected List<PriceInformation> loadQualifierData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final ProductModel product, final Qualifier qualifier)
	{
		return loadPriceInformations( product);
	}
	
	protected List<PriceInformation> loadPriceInformations(
			final ProductModel product)
	{
		return priceService.getPriceInformationsForProduct(product);
	}
	
	/**
	 * 
	 * @param priceInformations
	 * @return
	 */
	protected Double getPriceValue(final List<PriceInformation> priceInformations)
	{
		Double value = null;

		if (priceInformations != null && !priceInformations.isEmpty())
		{
			value = Double.valueOf(priceInformations.get(0).getPriceValue().getValue());
		}

		return value;
	}
	
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext context,
			final IndexedProperty indexedProperty, final ProductModel product,
			final ValueResolverContext<ProductModel, List<PriceInformation>> resolverContext)
			throws FieldValueProviderException
	{

		
		final List<PriceInformation> priceInformations = resolverContext.getQualifierData();
		if (CollectionUtils.isNotEmpty(priceInformations))
		{
			final Double priceValue = getPriceValue(priceInformations);
			if (priceValue != null)
			{
				final Double discount = productDiscountService.calculateDiscount(priceValue, product);
				if (SharjahDoubleUtils.checkDouble(discount))
				{
					if(LOG.isDebugEnabled())
					{
						LOG.debug(String.format("Discount for [%s] is [%s]", product.getCode(), discount));
					}
					document.addField(indexedProperty, discount, resolverContext.getFieldQualifier());
				}
			}
		}

	}
	
	public void setPriceService(final PriceService priceService) {
		this.priceService = priceService;
	}
	
	public void setProductDiscountService(final SharjahProductDiscountService productDiscountService) {
		this.productDiscountService = productDiscountService;
	}

}
