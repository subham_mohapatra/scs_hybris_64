package com.sharjah.core.order.service.impl;

import static com.sharjah.core.util.SharjahStringUtils.safeFormatDate;
import static com.sharjah.core.util.SharjahStringUtils.safeFormatEnum;
import static org.apache.commons.lang3.StringUtils.defaultString;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.opencsv.CSVWriter;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.order.service.SharjahOrderExportService;
import com.sharjah.core.order.service.SharjahOrderExportUtils;


/**
 * Class for the Order Export Service that implements the SharjahOrderExportService interface. It exports order into a
 * file.
 */
public class DefaultSharjahOrderExportService implements SharjahOrderExportService
{

	private static final Logger LOG = LogManager.getLogger(DefaultSharjahOrderExportService.class);

	private static final String SEMICOLON = ";";
	private static final String SPACE = " ";
	private static final String ORDER_FILE_PREFIX = "order_";
	private static final String ORDERLINE_FILE_PREFIX = "orderLine_";
	private static final String FILE_PROPERTY_PREFIX = "order.export.";

	private static final String DATE_FORMAT = "dd.MM.yyyy hh:mm:ss";
	private static final String SIMPLE_DATE_FORMAT = "dd.MM.yyyy";
	private static final String FILE_EXTENSION = ".csv";
	private static final String VALUE_SEPARATOR = " | ";

	private static final char SEPARATOR_CHAR = ';';
	private static final int NUMBER_ADDRESS_FIELDS = 5;

	private static final String COMPLEMENT_TIME = ":00";

	private PromotionsService promotionsService;
	private ModelService modelService;

	@Override
	public boolean exportOrder(final ReturnRequestModel returnRequest, final String exportType)
	{
		final long time = System.currentTimeMillis();

		final String folderMain = Config.getString(FILE_PROPERTY_PREFIX + exportType + ".main.folder", "");
		final String folderLine = Config.getString(FILE_PROPERTY_PREFIX + exportType + ".line.folder", "");
		try
		{
			FileUtils.forceMkdir(new File(folderMain));
			FileUtils.forceMkdir(new File(folderLine));
			final boolean exportMain = exportOrderMain(returnRequest, time, folderMain);
			final boolean exportLine = exportOrderLine(returnRequest, time, folderLine);

			return (exportMain && exportLine);
		}
		catch(final IOException e)
		{
			LOG.error("There was an error creating folders for order export", e);
			return false;
		}
	}

	@Override
	public boolean exportOrder(final OrderModel order, final String exportType)
	{
		final long time = System.currentTimeMillis();

		final String folderMain = Config.getString(FILE_PROPERTY_PREFIX + exportType + ".main.folder", "");
		final String folderLine = Config.getString(FILE_PROPERTY_PREFIX + exportType + ".line.folder", "");

		try
		{
			FileUtils.forceMkdir(new File(folderMain));
			FileUtils.forceMkdir(new File(folderLine));
			final boolean exportMain = exportOrderMain(order, time, folderMain);
			final boolean exportLine = exportOrderLine(order, time, folderLine);

			return (exportMain && exportLine);
		}
		catch(final IOException e)
		{
			LOG.error("There was an error creating folders for order export", e);
			return false;
		}
	}

	/**
	 * Export the returnRequest header.
	 *
	 * @param returnRequest
	 *           The return request
	 * @param time
	 *           The time
	 * @param folder
	 *           The output folder
	 * @return boolean If the export was successful
	 * @throws IOException
	 *            When something goes wrong writing the file
	 */
	public boolean exportOrderMain(final ReturnRequestModel returnRequest, final long time, final String folder) throws IOException
	{
		final OrderModel order = returnRequest.getOrder();
		final File file = new File(folder, ORDER_FILE_PREFIX + time + FILE_EXTENSION);

		try (
				final Writer writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
				final CSVWriter csvWriter = new CSVWriter(writer, SEPARATOR_CHAR);)
		{
			final String[] headerFields = Config.getString("order.export.main.header", "").split(SEMICOLON);
			SharjahOrderExportUtils.addHeader(csvWriter, headerFields);

			addOrderExportFields(csvWriter, order, returnRequest);
			return true;
		}
	}

	/**
	 * Export the order header.
	 *
	 * @param order
	 *           The order
	 * @param time
	 *           The time
	 * @param folder
	 *           The output folder
	 * @return boolean If the export was successful
	 * @throws IOException
	 *            When something goes wrong writing the file
	 */
	public boolean exportOrderMain(final OrderModel order, final long time, final String folder) throws IOException
	{
		final File file = new File(folder, ORDER_FILE_PREFIX + time + FILE_EXTENSION);

		try (
				final Writer writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
				final CSVWriter csvWriter = new CSVWriter(writer, SEPARATOR_CHAR);)
		{
			final String[] headerFields = Config.getString("order.export.main.header", "").split(SEMICOLON);
			SharjahOrderExportUtils.addHeader(csvWriter, headerFields);

			addOrderExportFields(csvWriter, order, null);
			return true;
		}
	}

	/**
	 * Export a returnRequest line.
	 *
	 * @param returnRequest
	 *           The return request
	 * @param time
	 *           The time
	 * @param folder
	 *           The output folder
	 * @return boolean If the export was successful
	 * @throws IOException
	 *            When something goes wrong writing the file
	 */
	public boolean exportOrderLine(final ReturnRequestModel returnRequest, final long time, final String folder) throws IOException
	{
		final OrderModel order = returnRequest.getOrder();
		final File file = new File(folder, ORDERLINE_FILE_PREFIX + time + FILE_EXTENSION);

		try (
				final Writer writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
				final CSVWriter csvWriter = new CSVWriter(writer, SEPARATOR_CHAR);)
		{
			final String[] headerFields = Config.getString("order.export.line.header", "").split(SEMICOLON);
			SharjahOrderExportUtils.addHeader(csvWriter, headerFields);

			final List<PromotionResultModel> productPromotions = getProductPromotions(order);

			for(final ReturnEntryModel returnEntry : returnRequest.getReturnEntries())
			{
				addOrderLineExportFields(csvWriter, returnEntry.getOrderEntry(), returnEntry, productPromotions);
			}

			return true;
		}
	}

	/**
	 * Export an order line.
	 *
	 * @param order
	 *           The order
	 * @param time
	 *           The time
	 * @param folder
	 *           The output folder
	 * @return boolean If the export was successful
	 * @throws IOException
	 *            When something goes wrong writing the file
	 */
	public boolean exportOrderLine(final OrderModel order, final long time, final String folder) throws IOException
	{
		final File file = new File(folder, ORDERLINE_FILE_PREFIX + time + FILE_EXTENSION);

		try (
				final Writer writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
				final CSVWriter csvWriter = new CSVWriter(writer, SEPARATOR_CHAR);)
		{
			final String[] headerFields = Config.getString("order.export.line.header", "").split(SEMICOLON);
			SharjahOrderExportUtils.addHeader(csvWriter, headerFields);

			final List<PromotionResultModel> productPromotions = getProductPromotions(order);

			for(final AbstractOrderEntryModel entry : order.getEntries())
			{
				addOrderLineExportFields(csvWriter, entry, null, productPromotions);
			}

			return true;
		}
	}

	/**
	 * 
	 * @param csvWriter
	 * @param entry
	 * @param returnEntry
	 * @param promotionResultModels
	 */
	private void addOrderLineExportFields(final CSVWriter csvWriter, final AbstractOrderEntryModel entry,
			final ReturnEntryModel returnEntry, final List<PromotionResultModel> promotionResultModels)
	{
		final List<String> orderLineFields = new ArrayList<>();
		//order entry number
		orderLineFields.add(String.valueOf(entry.getEntryNumber()));

		//order code
		orderLineFields.add(entry.getOrder().getCode());

		//product code
		orderLineFields.add(entry.getProduct().getCode());

		//item number
		orderLineFields.add(entry.getProduct().getItemNumber());

		//order line update
		orderLineFields.add(entry.getEntryConf());

		//UOM (unit)
		orderLineFields.add(entry.getProduct().getUnit() != null ? entry.getProduct().getUnit().getCode() : StringUtils.EMPTY);

		//Base price
		orderLineFields.add(String.valueOf(entry.getBasePrice()));

		//Discount values
		orderLineFields.add(getDiscountValues(entry.getDiscountValues()));

		//Promotion code
		orderLineFields.add(getPromotionCodeIfExists(entry.getEntryNumber(), promotionResultModels));

		//Quantity
		//checks if there is a return entry, otherwise get the quantity from abstractOrderEntry
		orderLineFields.add(String.valueOf(returnEntry != null ? returnEntry.getExpectedQuantity() : entry.getQuantity()));

		//Rejected
		orderLineFields.add(String.valueOf(entry.getRejected()));

		//Tax Values
		orderLineFields.add(getTotalTaxValues(entry.getTaxValues()));

		//Total price
		orderLineFields.add(String.valueOf(entry.getTotalPrice()));

		csvWriter.writeNext(orderLineFields.toArray(new String[orderLineFields.size()]), false);
	}
	
	/**
	 * 
	 * @param csvWriter
	 * @param order
	 * @param returnRequest
	 */
	private void addOrderExportFields(final CSVWriter csvWriter, final OrderModel order, final ReturnRequestModel returnRequest)
	{
		final List<String> orderFields = new ArrayList<>();

		//order Code
		orderFields.add(returnRequest != null ? returnRequest.getCode() : order.getCode());

		//original order code
		orderFields.add(order.getCode());

		//modifiedBy
		orderFields.add(order.getPlacedBy() != null ? order.getPlacedBy().getUid() : order.getUser().getUid());

		//modifiedDate
		orderFields.add(safeFormatDate(order.getModifiedtime(), DATE_FORMAT));

		//shareholderNumber
		orderFields.add(defaultString(order.getShareholderNumber()));

		//money change
		orderFields.add(safeFormatEnum(order.getMoneyChange()));

		//tracking number
		orderFields.add(getTrackingNumber(order));

		//applied Coupon Code
		orderFields.add(getCouponCodes(order));

		//currency
		orderFields.add(order.getCurrency().getIsocode());

		//order date
		orderFields.add(safeFormatDate(order.getDate(), DATE_FORMAT));

		//delivery cost
		orderFields.add(String.valueOf(order.getDeliveryCost()));

		//delivery status
		orderFields.add(safeFormatEnum(order.getDeliveryStatus()));

		//Order description
		orderFields.add(defaultString(order.getDescription()));

		//Fraudulent
		orderFields.add(String.valueOf(order.getFraudulent()));

		//Potencially Fraudulent
		orderFields.add(String.valueOf(order.getPotentiallyFraudulent()));

		//Global Discount Values
		final List<DiscountValue> allDiscount = new ArrayList<>();
		allDiscount.addAll(order.getGlobalDiscountValues());
		order.getEntries().stream()
			.forEach(entry -> allDiscount.addAll(entry.getDiscountValues()));

		orderFields.add(getDiscountValues(allDiscount));

		//NET
		orderFields.add(String.valueOf(order.getNet()));

		//THIS NEXT Session regards the billing address information
		final AddressModel billingAddress = order.getPaymentAddress();
		fulfillAddressData(billingAddress, orderFields);

		//payment mode
		orderFields.add(order.getPaymentMode() != null ? order.getPaymentMode().getCode() : StringUtils.EMPTY);

		//payment status
		orderFields.add(safeFormatEnum(order.getPaymentStatus()));

		//placed by
		orderFields.add(order.getPlacedBy() != null ? order.getPlacedBy().getUid() : StringUtils.EMPTY);

		//payment status
		orderFields.add(safeFormatEnum(order.getStatus()));

		//payment info
		orderFields.add(order.getStatusInfo());

		//sub total
		orderFields.add(String.valueOf(order.getSubtotal()));

		//total discounts
		final double totalDiscounts = getSumDiscountValues(order.getEntries(), order.getTotalDiscounts());
		orderFields.add(String.valueOf(totalDiscounts));

		//total price
		orderFields.add(String.valueOf(order.getTotalPrice()));

		//total tax
		orderFields.add(String.valueOf(order.getTotalTax()));

		//tax values
		orderFields.add(getTotalTaxValues(order.getTotalTaxValues()));

		//User
		orderFields.add(order.getUser().getUid());

		//Delivery Slot Date Start
		//Delivery Slot Date End
		fulfillDeliverySlotData(order.getSelectedDeliverySlot(), orderFields);

		//shipping date
		orderFields.add(getShippingDate(order));

		//delivery date
		orderFields.add(getDeliveryDate(order));

		//THIS NEXT Session regards the billing address information
		final AddressModel deliveryAddress = order.getDeliveryAddress();
		fulfillAddressData(deliveryAddress, orderFields);

		csvWriter.writeNext(orderFields.toArray(new String[orderFields.size()]), false);
	}
	/**
	 * 
	 * @param entries
	 * @param globalDiscount
	 * @return
	 */
	private double getSumDiscountValues(final List<AbstractOrderEntryModel> entries, final double globalDiscount)
	{
		double result = globalDiscount;
		for(final AbstractOrderEntryModel entry : entries)
		{
			final List<DiscountValue> productDiscountValues = entry.getDiscountValues();
			for(final DiscountValue discount : productDiscountValues)
			{
				result = result + discount.getAppliedValue();
			}
		}
		return result;
	}
	/**
	 * 
	 * @param calendarDeliverySlot
	 * @param fieldList
	 */
	private void fulfillDeliverySlotData(final CalendarDeliverySlotModel  calendarDeliverySlot, final List<String> fieldList)
	{
		if(calendarDeliverySlot != null && calendarDeliverySlot.getDeliverySlot() != null)
		{
			final Date date = calendarDeliverySlot.getDate();
			final String dateStr = safeFormatDate(date, SIMPLE_DATE_FORMAT);
			final String beginTime = calendarDeliverySlot.getDeliverySlot().getBeginTime();
			final String endTime = calendarDeliverySlot.getDeliverySlot().getEndTime();

			final StringBuilder sbDate = new StringBuilder();
			sbDate.append(dateStr);
			sbDate.append(SPACE);
			sbDate.append(beginTime).append(COMPLEMENT_TIME);
			fieldList.add(sbDate.toString());

			sbDate.delete(0, sbDate.length());
			sbDate.append(dateStr);
			sbDate.append(SPACE);
			sbDate.append(endTime).append(COMPLEMENT_TIME);
			fieldList.add(sbDate.toString());
		}
		else
		{
			fieldList.add(StringUtils.EMPTY);
			fieldList.add(StringUtils.EMPTY);
		}
	}
	/**
	 * 
	 * @param address
	 * @param fieldList
	 */
	private void fulfillAddressData(final AddressModel address, final List<String> fieldList)
	{
		if (address != null)
		{
			fieldList.add(defaultString(address.getAppartment()));
			fieldList.add(defaultString(address.getBuilding()));
			fieldList.add(defaultString(address.getStreetname()));
			// Area
			fieldList.add(defaultString(address.getDistrict()));
			// City
			fieldList.add(defaultString(address.getTown()));
		}
		else
		{
			fieldList.addAll(Collections.nCopies(NUMBER_ADDRESS_FIELDS, StringUtils.EMPTY));
		}
	}
	/**
	 * 
	 * @param order
	 * @return
	 */
	private String getDeliveryDate(final OrderModel order)
	{
		String deliveryDate = StringUtils.EMPTY;

		for (final ConsignmentModel consigment : order.getConsignments())
		{
			deliveryDate = safeFormatDate(consigment.getNamedDeliveryDate(), DATE_FORMAT);
		}

		return deliveryDate;
	}
	/**
	 * 
	 * @param order
	 * @return
	 */
	private String getShippingDate(final OrderModel order)
	{
		String shippingDate = StringUtils.EMPTY;

		for (final ConsignmentModel consigment : order.getConsignments())
		{
			shippingDate = safeFormatDate(consigment.getShippingDate(), DATE_FORMAT);
		}

		return shippingDate;
	}
	/**
	 * 
	 * @param taxes
	 * @return
	 */
	private String getTotalTaxValues(final Collection<TaxValue> taxes)
	{
		final StringBuilder sbTotalTax = new StringBuilder();

		for (final TaxValue taxValue : taxes)
		{
			sbTotalTax.append(taxValue.getAppliedValue());
			sbTotalTax.append(VALUE_SEPARATOR);
		}

		return sbTotalTax.toString();
	}
	/**
	 * 
	 * @param discounts
	 * @return
	 */
	private String getDiscountValues(final List<DiscountValue> discounts)
	{
		final StringBuilder sbGDiscounts = new StringBuilder();

		for (final DiscountValue discount : discounts)
		{
			sbGDiscounts.append(discount.getAppliedValue());
			sbGDiscounts.append(VALUE_SEPARATOR);
		}
		return sbGDiscounts.toString();
	}
	/**
	 * 
	 * @param order
	 * @return
	 */
	private String getTrackingNumber(final OrderModel order)
	{
		String trackingNumber = StringUtils.EMPTY;

		for (final ConsignmentModel consignment : order.getConsignments())
		{
			trackingNumber = consignment.getTrackingID();
		}

		return trackingNumber;
	}

	/**
	 * Gets the coupon codes applied to the order
	 *
	 * @param order
	 * @return String
	 */
	private String getCouponCodes(final OrderModel order)
	{
		final StringBuilder sbCoupons = new StringBuilder();

		if (CollectionUtils.isNotEmpty(order.getAppliedCouponCodes()))
		{
			for (final String coupon : order.getAppliedCouponCodes())
			{
				sbCoupons.append(coupon);
				sbCoupons.append(SPACE);
			}
		}
		return sbCoupons.toString();
	}

	/**
	 * Returns the product promotions applied to the given order
	 *
	 * @param order
	 *           The order
	 * @return Product promotions
	 */
	public List<PromotionResultModel> getProductPromotions(final OrderModel order)
	{
		final PromotionOrderResults results = promotionsService.getPromotionResults(order);
		return modelService.getAll(results.getAppliedProductPromotions(),
				new ArrayList<PromotionResultModel>());
	}

	/**
	 * Returns the promotion code, if any.
	 *
	 * @param entryNumber
	 *           Entry number
	 * @param promotionResultModels
	 *           Product promotions applied to the order
	 * @return Promotion code
	 */
	public String getPromotionCodeIfExists(final int entryNumber, final List<PromotionResultModel> promotionResultModels)
	{
		final String code = StringUtils.EMPTY;

		for (final PromotionResultModel promotionResult : promotionResultModels)
		{
			for (final PromotionOrderEntryConsumedModel entry : promotionResult.getConsumedEntries())
			{
				if (entry.getOrderEntry().getEntryNumber().intValue() == entryNumber)
				{
					return promotionResult.getPromotion().getCode();
				}
			}
		}

		return code;
	}
	/**
	 * 
	 * @param promotionsService
	 */
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}
	/**
	 * 
	 * @param modelService
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
