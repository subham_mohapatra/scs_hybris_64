package com.sharjah.core.media.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;

/**
 * 
 * @author Luiz.Henriques
 *
 */
public interface SharjahMediaContainerService
{
	/**
	 * Media container
	 * 
	 * @param qualifier
	 * @param catalogVersion
	 * @return
	 */
	MediaContainerModel findMediaContainerForQualifier(final String qualifier, final CatalogVersionModel catalogVersion);
}
