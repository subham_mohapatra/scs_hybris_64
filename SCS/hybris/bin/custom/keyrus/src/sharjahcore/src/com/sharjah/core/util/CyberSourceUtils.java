package com.sharjah.core.util;

import java.util.ArrayList;
import java.util.Map;

import com.sharjah.core.payment.cybersource.constants.CyberSourceConstants;


/**
 * Utility class for Cyber Source
 * 
 * @author Pablo François
 */
public class CyberSourceUtils
{

	private static final String SEPARATOR = ",";

	private CyberSourceUtils()
	{
		// Hide constructor for utility class
	}

	/**
	 * Generate the data string to be signed for a CyberSource request.
	 *
	 * @param params
	 *           The parameters that will be sent in the request
	 * @return The data string
	 */
	public static String buildDataToSign(final Map<String, String> params)
	{
		final String[] signedFieldNames = String.valueOf(params.get(CyberSourceConstants.SIGNED_FIELD_NAMES)).split(SEPARATOR);
		final ArrayList<String> dataToSign = new ArrayList<>();
		for (final String signedFieldName : signedFieldNames)
		{
			dataToSign.add(signedFieldName + "=" + params.get(signedFieldName));
		}
		return String.join(SEPARATOR, dataToSign);
	}

}
