package com.sharjah.core.cartthresholdconfiguration.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.Map;

import com.sharjah.core.cartthresholdconfiguration.dao.SharjahCartThresholdConfigurationDao;
import com.sharjah.core.model.CartThresholdConfigurationModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahCartThresholdConfigurationDao extends AbstractItemDao implements SharjahCartThresholdConfigurationDao
{

	private static final String QUERY = "SELECT {" + CartThresholdConfigurationModel.PK + "} FROM {"
			+ CartThresholdConfigurationModel._TYPECODE + "} WHERE {" + CartThresholdConfigurationModel.CODE + "} = ?code";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartThresholdConfigurationModel getCartThresholdConfigurationByCode(final String code)
	{
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("code", code);
		final SearchResult<CartThresholdConfigurationModel> result = getFlexibleSearchService().search(QUERY, queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

}
