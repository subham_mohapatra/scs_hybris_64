package com.sharjah.core.fraud.symptom.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.symptom.impl.OrderThresholdSymptom;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.sharjah.core.model.FraudSymptomModel;
import com.sharjah.core.fraud.service.impl.FraudConfigService;


/**
 * Override the native symptom to read fraud configuration from a type in the model, instead of some Spring XML file.
 *
 * @author Pablo François
 */
public class SharjahOrderThresholdSymptom extends OrderThresholdSymptom
{

	private static final String THRESHOLD_SYMPTOM_NAME = "fraud.symptom.threshold";
	private static final String THRESHOLD_LIMIT = "thresholdLimit";
	private static final String THRESHOLD_DELTA = "thresholdDelta";

	@Resource
	ConfigurationService configurationService;
	@Resource
	FraudConfigService fraudConfigService;

	@Override
	public FraudServiceResponse recognizeSymptom(final FraudServiceResponse fraudResponse, final AbstractOrderModel order)
	{
		final String symptomName = configurationService.getConfiguration().getString(THRESHOLD_SYMPTOM_NAME);
		final FraudSymptomModel symptom = fraudConfigService.findFraudSymptomByName(symptomName);
		setIncrement(symptom.getIncrement());

		final Map<String, String> parameters = symptom.getParameters();
		if (MapUtils.isNotEmpty(parameters) && StringUtils.isNotBlank(parameters.get(THRESHOLD_LIMIT))
				&& StringUtils.isNotBlank(parameters.get(THRESHOLD_DELTA)))
		{
			final double limit = Double.parseDouble(parameters.get(THRESHOLD_LIMIT));
			final double delta = Double.parseDouble(parameters.get(THRESHOLD_DELTA));
			setThresholdLimit(limit);
			setThresholdDelta(delta);
		}

		return super.recognizeSymptom(fraudResponse, order);
	}

}
