package com.sharjah.core.orders.service.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.enums.PaymentStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.sharjah.core.orders.service.SharjahOrderService;
import com.sharjah.facades.order.SharjahOrderDao;

/**
 * @author issam maiza
 * 
 * DefaultSharjahOrderServiceImpl to implement recaptureOrderswithFailedCapture() method
 */

public class DefaultSharjahOrderServiceImpl implements SharjahOrderService {

	@Resource(name = "sharjahOrderDao")
	private SharjahOrderDao sharjahOrderDao;

	@Resource(name = "paymentService")
	private PaymentService paymentService;

	@Resource(name = "modelService")
	private ModelService modelService;

	private static final Logger LOG = Logger.getLogger(DefaultSharjahOrderServiceImpl.class);
	private static final String SUCCESFULL = "SUCCESFULL";
	@Override
	public void recaptureOrderswithFailedCapture() {
		final List<OrderModel> orderModels = sharjahOrderDao.findOrdersWithFailedCapture();
		if (!CollectionUtils.isEmpty(orderModels)) {
			for (final OrderModel orderModel : orderModels) {
				if (PaymentStatus.NOTPAID.equals(orderModel.getPaymentStatus())
						&& CollectionUtils.isNotEmpty(orderModel.getPaymentTransactions()))
				{
					LOG.info("order with code: " + orderModel.getCode() + " will be recaptured.");

					final PaymentTransactionEntryModel PaymentTransactionEntryModel = paymentService
							.capture(orderModel.getPaymentTransactions().get(0));

					if (SUCCESFULL.equals(PaymentTransactionEntryModel.getTransactionStatusDetails())
							&& PaymentTransactionType.CAPTURE.equals(PaymentTransactionEntryModel.getType()))
					{
						orderModel.setPaymentStatus(PaymentStatus.PAID);
						orderModel.setStatus(OrderStatus.COMPLETED);
						getModelService().save(orderModel);
						LOG.info("order with code : " + orderModel.getCode()
								+ " was success recaptured and payment status change to PAID and order status to COMPELED");
					} else {
						LOG.info("order with code : " + orderModel.getCode() + " was failed to recaptured");
						orderModel.setStatus(OrderStatus.PAYMENT_NOT_CAPTURED);
						getModelService().save(orderModel);
					}
				}

			}
		} else {
			LOG.info("there is no order to recapture");
		}
	}

	/**
	 * @return the paymentService
	 */
	public PaymentService getPaymentService() {
		return paymentService;
	}

	/**
	 * @param paymentService
	 *            the paymentService to set
	 */
	public void setPaymentService(final PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	/**
	 * @return the sharjahOrderDao
	 */
	public SharjahOrderDao getSharjahOrderDao() {
		return sharjahOrderDao;
	}

	/**
	 * @param sharjahOrderDao
	 *            the sharjahOrderDao to set
	 */
	public void setSharjahOrderDao(final SharjahOrderDao sharjahOrderDao) {
		this.sharjahOrderDao = sharjahOrderDao;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService() {
		return modelService;
	}

	/**
	 * @param modelService
	 *            the modelService to set
	 */
	public void setModelService(final ModelService modelService) {
		this.modelService = modelService;
	}
}
