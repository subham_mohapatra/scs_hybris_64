package com.sharjah.core.order.service.impl;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.OrderCancelRequest;
import de.hybris.platform.ordercancel.OrderCancelService;
import de.hybris.platform.orderhistory.model.OrderHistoryEntryModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.collect.Lists;
import com.opencsv.CSVReader;
import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.order.service.SharjahOrderExportUtils;
import com.sharjah.core.order.service.SharjahOrderImportService;
import com.sharjah.core.stock.SharjahStockService;
import com.sharjah.webservices.axon.services.AxonSMSService;


/**
 * Class that does the import order process
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultSharjahOrderImportService implements SharjahOrderImportService
{
	private static final Logger LOG = LogManager.getLogger(DefaultSharjahOrderImportService.class);

	private static final Pattern ORDER_FILE_PATTERN = Pattern.compile("^([order]+)(_)([0-9]+)\\.(csv)$");

	private static final char SEMICOLON_CHAR = ';';
	private static final char DOUBLE_QUOTES = '"';
	private static final int STARTING_LINE = 1;
	private static final String UTF8 = "UTF-8";
	
	private static final String UPDATE = "UPDATE";
	private static final String RETURN = "RETURN";
	private static final String CANCEL = "CANCEL";
	private static final String SHIPPED = "SHIPPED";

	
	@Value("#{configurationService.configuration.getProperty('order.shipping.message.en')}")
	private String orderShippingMessageEN;
	
	@Value("#{configurationService.configuration.getProperty('order.shipping.message.ar.1')}")
	private String orderShippingMessageAR1;
	@Value("#{configurationService.configuration.getProperty('order.shipping.message.ar.2')}")
	private String orderShippingMessageAR2;
	@Value("#{configurationService.configuration.getProperty('order.shipping.message.ar.3')}")
	private String orderShippingMessageAR3;
	@Value("#{configurationService.configuration.getProperty('order.shipping.message.ar.4')}")
	private String orderShippingMessageAR4;
	
	private String orderUpdateFolder;
	private String orderUpdateLineFolder;

	private String orderCancelFolder;
	private String orderCancelLineFolder;

	private String orderReturnFolder;
	private String orderReturnLineFolder;

	private String archiveFolder;

	private String errorFolder;

	private CustomerAccountService customerAccountService;
	
	private UserService userService;

	private BaseStoreService baseStoreService;

	private ModelService modelService;
	
	@Resource(name = "axonSMSService")
	private AxonSMSService axonSMSService;
	
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;
	
	@Resource(name = "i18nService")
	private I18NService i18nService;
	
	@Resource(name = "orderCancelService")
	private OrderCancelService orderCancelService;
	

	@Resource
	private BusinessProcessService businessProcessService;
	
	@Resource
	private SharjahStockService stockService;


	@Override
	public boolean readCancelOrders()
	{
		//read all order files placed in the cancel folder
		final File[] orders = readOrderFiles(orderCancelFolder);
		return doUpdateOrder(orders, orderCancelFolder, orderCancelLineFolder, CANCEL);
	}

	@Override
	public boolean readReturnOrders()
	{
		//read all order files placed in the return folder
		final File[] orders = readOrderFiles(orderReturnFolder);
		return doUpdateOrder(orders, orderReturnFolder, orderReturnLineFolder, RETURN);
	}

	@Override
	public boolean readUpdateOrders()
	{
		//read all order files placed in the order update folder
		final File[] orders = readOrderFiles(orderUpdateFolder);
		return doUpdateOrder(orders, orderUpdateFolder, orderUpdateLineFolder, UPDATE);

	}

	/**
	 * Perform the order update, depending of the operation type (operationType parameter)
	 * Reads the order_xxxxx.csv file and gets the fields need by each operation
	 * @param orders
	 * @param orderFolder
	 * @param orderLineFolder
	 * @param operationType
	 * @return
	 */
	private boolean doUpdateOrder(final File[] orders, final String orderFolder, final String orderLineFolder, final String operationType)
	{
		boolean isAllOK = true;

		//checks if the orderLine folders have been created already, otherwise it will create
		SharjahOrderExportUtils.checkFolder(orderLineFolder);
		
		for (final File orderFile : orders)
		{
			final String fileName = orderFile.getName();
		
			//checks if the orderLine file is also placed for the same order_ file that is being read
			final File orderLineFile = findOrderLine(fileName, orderLineFolder);

			if (orderLineFile != null)
			{
				isAllOK = readAndMoveOrderFile(fileName, orderFile, orderLineFile, operationType, orderFolder, orderLineFolder);
			}
			else
			{
				isAllOK = false;
				LOG.warn("The file [" + fileName + "] has no orderLine match, skipping.");
			}
		}

		return isAllOK;
	}

	/**
	 * 
	 * @param fileName
	 * @param orderFile
	 * @param orderLineFile
	 * @param operationType
	 * @param orderFolder
	 * @param orderLineFolder
	 * @return
	 */
	private boolean readAndMoveOrderFile(final String fileName, final File orderFile, final File orderLineFile,
			final String operationType, final String orderFolder, final String orderLineFolder)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("JACKPOT ! I found both order and orderLine file for [" + fileName + "].");
		}

		try
		{
			final boolean isLocalFileOk = readMainOrderFile(orderFile, orderLineFile, operationType);

			//copy file to the right folder (error or archive)
			if (isLocalFileOk)
			{
				moveFile(archiveFolder, orderFile, orderFolder);
				moveFile(archiveFolder, orderLineFile, orderLineFolder);
			}
			else
			{
				moveFile(errorFolder, orderFile, orderFolder);
				moveFile(errorFolder, orderLineFile, orderLineFolder);
			}

		}
		catch (final IOException e)
		{
			LOG.error("There was an error reading the orderMain file", e);
			return false;
		}
		return true;
	}
	

	/**
	 * reads the order main file
	 * 
	 * @param orderFile
	 * @param orderLineFile
	 * @param operationType
	 * @return boolean
	 */
	private boolean readMainOrderFile(final File orderFile, final File orderLineFile, final String operationType)
			throws IOException {
		boolean isLocalFileOk = false;

		final DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
		// gets the default baseStore model
		final BaseStoreModel sharjahBaseStore = baseStoreService
				.getBaseStoreForUid(SharjahCoreConstants.SHARJAH_BASE_STORE_CODE);

		// reads the order main file
		try (CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(orderFile), UTF8),
				SEMICOLON_CHAR, DOUBLE_QUOTES, STARTING_LINE)) {
			String[] fieldsCSV;
			while ((fieldsCSV = csvReader.readNext()) != null) {

				if (fieldsCSV != null) {
					// directs to the right operation
					switch (operationType) {
					case CANCEL:
						isLocalFileOk = cancelOrder(fieldsCSV, dateFormatter, sharjahBaseStore);
						break;
					case RETURN:
						isLocalFileOk = returnOrder(fieldsCSV, orderLineFile, dateFormatter, sharjahBaseStore);
						break;
					case UPDATE:
						isLocalFileOk = updateOrder(fieldsCSV, dateFormatter, sharjahBaseStore);
						break;
					default:
						break;
					}
				}
			}
		} catch (final Exception e) {
			if (LOG.isInfoEnabled())
			{
			LOG.info("error in reading  the order main file with code : " + orderFile.getName(), e);
			}
		}
		return isLocalFileOk;
	}

	/**
	 * reads the information needed to cancel operation
	 * 
	 * @param fieldsCSV
	 * @param dateFormatter
	 * @param baseStore
	 * @return
	 */
	private boolean cancelOrder(final String[] fieldsCSV, final DateFormat dateFormatter, final BaseStoreModel baseStore)
	{
		boolean isLocalFileOk = false;
		
		final String orderCode = fieldsCSV[0];
		final String statusStr = fieldsCSV[25];
		try
		{
			//gets the order
			final OrderModel order = customerAccountService.getOrderForCode(orderCode, baseStore);
			final OrderStatus status = OrderStatus.valueOf(statusStr);
			order.setStatus(status);

			modelService.save(order);
			final OrderCancelRequest orderCancelRequest = new OrderCancelRequest(order,
					Lists.newArrayList(createCancellationEntries(order)));
			try {
				getOrderCancelService().requestOrderCancel(orderCancelRequest, userService.getCurrentUser());
				order.setStatus(OrderStatus.CANCELLED);
				modelService.save(order);
				//update consignment status
				if (CollectionUtils.isNotEmpty(order.getConsignments()))
				{
					for (final ConsignmentModel consignment : order.getConsignments())
					{
						consignment.setStatus(ConsignmentStatus.CANCELLED);
						modelService.save(consignment);
					}
				}
				stockService.releaseStockAfterCancelOrder(orderCancelRequest);
				LOG.info("succes cancelling order with code" + orderCode);
			} catch (final OrderCancelException e) {
				LOG.info("error when cancelling order with code" + orderCode, e);
			}
			final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
					"sendOrderCancelledEmailProcess-" + orderCode + "-" + System.currentTimeMillis(),
					"sendOrderCancelledEmailProcess");
			orderProcessModel.setOrder(order);
			modelService.save(orderProcessModel);
			getBusinessProcessService().startProcess(orderProcessModel);
			
			//create order status update history
			createOrderHistory(order, fieldsCSV, dateFormatter);
			
			//TODO: HERE IT SHOULD CALL THE CANCEL PROCESS
			isLocalFileOk = true;
			
		}
		catch (final ModelNotFoundException e)
		{
			isLocalFileOk = false;
			LOG.error("Order not found for code [" + orderCode + "]", e);
		}
		
		return isLocalFileOk;
	}
	
	/**
	 * 
	 * reads the information needed to return operation
	 * 
	 * in return case, we need to read also the orderLine file, in order to update 'rejected' field in orderEntry
	 * 
	 * @param fieldsCSV
	 * @param orderLineFile
	 * @param baseStore
	 * @return
	 */
	private boolean returnOrder(final String[] fieldsCSV, final File orderLineFile, final DateFormat dateFormatter, final BaseStoreModel baseStore)
	{
		boolean isLocalFileOk = false;
		
		//order code here refers to the ReturnRequest
		final String orderCode = fieldsCSV[0];
		
		final String originalOrderCode = fieldsCSV[1];
		final String statusStr = fieldsCSV[25];
		try
		{
			//gets the order
			final OrderModel order = customerAccountService.getOrderForCode(originalOrderCode, baseStore);
			final OrderStatus status = OrderStatus.valueOf(statusStr);
			order.setStatus(status);
			
			//creates the order history
			createOrderHistory(order, fieldsCSV, dateFormatter);
			
			
			modelService.save(order);
			isLocalFileOk = updateRejectedOrderEntry(orderLineFile, order);
		}
		catch (final ModelNotFoundException e)
		{
			isLocalFileOk = false;
			LOG.error("Order not found for code [" + orderCode + "]", e);
		}
		
		//TODO: Here IT SHOULD call THE RETURN PROCESS
		
		return isLocalFileOk;
	}
	
	/**
	 * 
	 * updating the 'rejected' attribute in the order entry
	 * 
	 * @param orderLineFile
	 * @param order
	 * @return boolean
	 */
	private boolean updateRejectedOrderEntry(final File orderLineFile, final OrderModel order)
	{
		boolean isLocalFileOk = false;

		try (CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(orderLineFile), UTF8), SEMICOLON_CHAR, DOUBLE_QUOTES, STARTING_LINE  ))
		{
			String [] lineCSV;
			while ((lineCSV = csvReader.readNext()) != null)
			{
				final String entryNumberStr = lineCSV[0];
				final String rejectedStr = lineCSV[10];
				updateRejectedEntry(order,entryNumberStr ,rejectedStr);
			}
			isLocalFileOk = true;
		}
		catch(final IOException e)
		{
			LOG.error("There was an error reading the orderLine file for ["+order.getCode()+"]", e);
		}
		return isLocalFileOk;
	}
	
	/**
	 * @param OrderModel
	 * @param entryNumberStr
	 * @param rejectedStr
	 */
	private void updateRejectedEntry(final OrderModel order,final String entryNumberStr ,final String rejectedStr)
	{
			if(NumberUtils.isNumber(entryNumberStr))
			{
				final boolean rejected = Boolean.parseBoolean(rejectedStr);
				final int entryNumber = Integer.parseInt(entryNumberStr);

				//finds the entry to update
				final AbstractOrderEntryModel entry = order.getEntries().stream()
						.filter(x -> entryNumber == x.getEntryNumber().intValue()).findAny().orElse(null);
				if(entry != null){
					entry.setRejected(rejected);
					modelService.save(entry);
				}
				
			}
	}
	
	/**
	 * Gets all the information when it is an order update
	 * @param fieldsCSV
	 * @param dateFormatter
	 * @param baseStore
	 * @return boolean
	 */
	private boolean updateOrder(final String[] fieldsCSV, final DateFormat dateFormatter, final BaseStoreModel baseStore)
	{
		boolean isLocalFileOk = false;
		
		final String orderCode = fieldsCSV[0];
		final String statusStr = fieldsCSV[25];
		final String deliveryStatusStr = fieldsCSV[11];
		final String trackingNumber = fieldsCSV[6];
		
		final String shippingDateStr = fieldsCSV[35];
		final String deliveryDateStr = fieldsCSV[36];
		final String totalprice = fieldsCSV[29];


		
		LOG.info("Order [" + orderCode + "] Status [" + statusStr + "] tracking [" + trackingNumber + "]");

		try
		{
			final OrderModel order = customerAccountService.getOrderForCode(orderCode, baseStore);
			OrderStatus status = OrderStatus.valueOf(statusStr);
			if (SHIPPED.equals(statusStr))
			{
				status = OrderStatus.COMPLETED;
			}

			order.setStatus(status);
			final DeliveryStatus deliveryStatus = DeliveryStatus.valueOf(deliveryStatusStr);
			order.setDeliveryStatus(deliveryStatus);
			modelService.save(order);

			if (DeliveryStatus.SHIPPED.equals(order.getDeliveryStatus()))
			{
				sendSMSifOrderShipped(order, totalprice);
				sendEmailIfOrderShiped(order);
				order.setStatus(OrderStatus.COMPLETED);
				modelService.save(order);
			}

			//creates the order history
			createOrderHistory(order, fieldsCSV, dateFormatter);
			
			updateConsignmentData(trackingNumber, shippingDateStr, deliveryDateStr, statusStr, order, dateFormatter);

			isLocalFileOk = true;
		}
		catch (final ModelNotFoundException e)
		{
			isLocalFileOk = false;
			LOG.error("Order not found for code [" + orderCode + "]", e);
		}
		return isLocalFileOk;
	}
	
	/**
	 * 
	 * @param order
	 * @param price
	 */
	private void sendSMSifOrderShipped(final OrderModel order, final String price)
	{
		String phoneNumber = null;
		if (order.getOwner() != null)
		{
			final CustomerModel customerModel = (CustomerModel) order.getOwner();
			if (customerModel != null)
			{
				if (customerModel.getPhoneContactInfo() != null)
				{
					phoneNumber = customerModel.getPhoneContactInfo().getPhoneNumber();
				}
				else if (customerModel.getPhoneContactInfo() == null && customerModel.getShareholder() != null)
				{
					phoneNumber = customerModel.getShareholder().getMobilePhone();
				}
			}
		}

		if (StringUtils.isEmpty(phoneNumber) && order.getDeliveryAddress() != null)
		{
			phoneNumber = order.getDeliveryAddress().getPhone1();
		}

		if(StringUtils.isNotEmpty(phoneNumber))
		{
			String smsLang = "english";
			if ("ar".equals(i18nService.getCurrentLocale().getLanguage()))
			{
				smsLang = "arabic";
			}
			axonSMSService.sendSMS(phoneNumber, getSMSOrderShippingMessage(price, order.getCode()), smsLang);
		}
	}
	/**
	 * 
	 * @param order
	 */
	private void sendEmailIfOrderShiped(final OrderModel order)
	{
		final List<ConsignmentModel> consignments = new ArrayList<>(order.getConsignments()) ;
		if(!CollectionUtils.isEmpty(consignments))
		{
		for (final ConsignmentModel consignment : consignments) {
			final ConsignmentProcessModel consignmentProcessModel = getBusinessProcessService().createProcess(
					"sendDeliveryEmailProcess-" + consignment.getCode() + "-" + System.currentTimeMillis(),
					"sendDeliveryEmailProcess");
			consignmentProcessModel.setConsignment(consignment);
			modelService.save(consignmentProcessModel);
			getBusinessProcessService().startProcess(consignmentProcessModel);
		}
		}

	}
	
	/**
	 * "#" will be replaced by url of tracking consignement system
	 * @param orderData
	 * @return
	 */
	private String getSMSOrderShippingMessage (final String price,final String orderCode ) {

		final String orderShippingMessageAR = orderShippingMessageAR1 + " " + orderCode + " " + orderShippingMessageAR2 + " "
				+ price + "AED" + " " + orderShippingMessageAR3 + "#" + " " + orderShippingMessageAR4;

		final StringBuilder confirmationText =  new StringBuilder(Locale.ENGLISH.equals(i18nService.getCurrentLocale()) ? orderShippingMessageEN : orderShippingMessageAR );
		confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") +1, orderCode);
		confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") +1, price + "AED");
		confirmationText.replace(confirmationText.indexOf("{"), confirmationText.indexOf("}") + 1, "#");
		return confirmationText.toString();
	}
	
	

	/**
	 * Creates the order history
	 * Mainly to the status update
	 * 
	 * @param order
	 * @param fieldsCSV
	 * @param dateFormatter
	 */
	public void createOrderHistory(final OrderModel order, final String[] fieldsCSV, final DateFormat dateFormatter) throws ModelNotFoundException
	{
		final String modifiedByStr = fieldsCSV[2];
		final String modifiedDateStr = fieldsCSV[3];
		
		final EmployeeModel employee = getEmployee(modifiedByStr);
		final Date modifiedDate = formatDate(modifiedDateStr, dateFormatter);
		if(modifiedDate != null)
		{
			final StringBuilder sbOH = new StringBuilder();
			sbOH.append("Order Update, status [");
			sbOH.append(order.getStatus());
			sbOH.append("]");
			final OrderHistoryEntryModel orderEntry = modelService.create(OrderHistoryEntryModel.class);
			orderEntry.setOrder(order);
			orderEntry.setEmployee(employee);
			orderEntry.setTimestamp(modifiedDate);
			orderEntry.setDescription(sbOH.toString());
			modelService.save(orderEntry);
		}
	}
	
	/**
	 * returns the Employee from the given UID
	 * @param uid
	 * @return EmployeeModel
	 * @throws ModelNotFoundException
	 */
	private EmployeeModel getEmployee(final String uid) throws ModelNotFoundException
	{
		try
		{
			final UserModel user = userService.getUserForUID(uid);
			if(user instanceof EmployeeModel)
			{
				return (EmployeeModel) user;
			}
			else
			{
				throw new ModelNotFoundException(String.format("The user with id %s is not an Employee.", uid));
			}
		}
		catch(final UnknownIdentifierException e)
		{
			if (LOG.isWarnEnabled())
			{
				LOG.warn(String.format("Employee not found for %s, creating a new one.", uid), e);
			}
			return createNewEmployee(uid);
		}
	}
	
	/**
	 * creates a new employee with the given UID
	 * @param uid
	 * @return EmployeeModel
	 */
	private EmployeeModel createNewEmployee(final String uid)
	{
		final EmployeeModel emp = modelService.create(EmployeeModel.class);
		
		emp.setUid(uid);
		
		modelService.save(emp);
		return emp;
	}
	/**
	 * Move the original file to a folder
	 * 
	 * @param folder
	 * @param originalFile
	 * @throws IOException
	 */
	private void moveFile(final String folderType, final File originalFile, final String folder) throws IOException
	{
		final StringBuilder sbFolder = new StringBuilder();
		sbFolder.append(folder);
		sbFolder.append("/");
		sbFolder.append(folderType);
		SharjahOrderExportUtils.checkFolder(sbFolder.toString());
		final File movedFile = new File(sbFolder.toString(), originalFile.getName());
		if (movedFile.exists())
		{
			movedFile.delete();
		}
		FileUtils.moveFile(originalFile, movedFile);
	}

	/**
	 * updates the trackingID from consignment Expecting only one consignment per order in Phase 1, if more, please
	 * revise this method
	 * 
	 * @param trackingNumber
	 * @param shippingDateStr
	 * @param deliveryDateStr
	 * @param order
	 * @param dateFormatter
	 */
	private void updateConsignmentData(final String trackingNumber,
			final String shippingDateStr, final String deliveryDateStr, final String statusStr, final OrderModel order,
			final DateFormat dateFormatter)
	{
		if (CollectionUtils.isNotEmpty(order.getConsignments()))
		{
			for (final ConsignmentModel consignment : order.getConsignments())
			{
				consignment.setTrackingID(trackingNumber);
				
				consignment.setShippingDate(formatDate(shippingDateStr, dateFormatter));
				consignment.setDeliveryDate(formatDate(deliveryDateStr, dateFormatter));
				
				if (SHIPPED.equals(statusStr))
				{
					consignment.setStatus(ConsignmentStatus.SHIPPED);
				}
				
				modelService.save(consignment);
			}
		}
	}
	
	/**
	 * Converts String to date, if pattern is right
	 * @param dateStr
	 * @param dateFormatter
	 * @return
	 */
	private Date formatDate(final String dateStr, final DateFormat dateFormatter)
	{
		Date date = null;
		//convert date
		try
		{
			if(StringUtils.isNotEmpty(dateStr))
			{
				date = dateFormatter.parse(dateStr);
			}
			
		}
		catch(final ParseException e)
		{
			LOG.error("There was an error parsing ["+dateStr+"]", e);
		}
		return date;
	}

	/**
	 * 
	 * Reads all order_xxxxx.csv from the given folder
	 * 
	 * @param folder
	 * @return
	 */
	public File[] readOrderFiles(final String folder)
	{
		final File file = new File(folder);

		//creates the folder if it does not exist
		if (!file.exists())
		{
			if (LOG.isInfoEnabled())
			{
				LOG.info("Folder [" + folder + "] does not exist. Creating a brand new one !");
			}
			file.mkdirs();
		}

		//check the file pattern to recognize if it is a media to be read
		final FilenameFilter filterByName = (dir, name) -> {
			final Matcher matcher = ORDER_FILE_PATTERN.matcher(name);
			return matcher.matches();
		};
		return file.listFiles(filterByName);
	}

	/**
	 * return the file ORDERLINE corresponding to the order already read
	 * 
	 * @param code
	 * @param folder
	 * @return
	 */
	public File findOrderLine(final String code, final String folder)
	{
		final String[] nameBreak = code.split("_");
		final StringBuilder sb = new StringBuilder();

		sb.append(folder);
		sb.append("/");
		sb.append("orderLine_");
		sb.append(nameBreak[1]);

		LOG.debug("Looking for [" + sb.toString() + "]");
		final File lineFile = new File(sb.toString());
		//if there is no orderLine corresponding to the order main file that has been read
		//return null, so it will skip the order file (main)
		if (!lineFile.exists())
		{
			return null;
		}
		return lineFile;
	}
	
	/**
	 * Create a collection of {@link OrderCancelEntry} from the list of order entries in the order.
	 *
	 * @param order
	 *           - the order
	 * @return collection of cancellation entries; never <tt>null</tt>
	 */
	public Collection<OrderCancelEntry> createCancellationEntries(final OrderModel order)
	{
		return order.getEntries().stream().map(entry -> createCancellationEntry(entry)).collect(Collectors.toList());
	}

	/**
	 * Create a {@link OrderCancelEntry} from an {@link AbstractOrderEntryModel}.
	 *
	 * @param orderEntry
	 *           - the order entry
	 * @return a new cancellation entry
	 */
	protected OrderCancelEntry createCancellationEntry(final AbstractOrderEntryModel orderEntry)
	{
		final OrderCancelEntry entry = new OrderCancelEntry(orderEntry, ((OrderEntryModel) orderEntry).getQuantityPending()
				.longValue());
		entry.setCancelReason(CancelReason.OTHER);
		return entry;
	}
	/**
	 * 
	 * @param orderUpdateFolder
	 */
	public void setOrderUpdateFolder(final String orderUpdateFolder)
	{
		this.orderUpdateFolder = orderUpdateFolder;
	}
	/**
	 * 
	 * @param orderUpdateLineFolder
	 */
	public void setOrderUpdateLineFolder(final String orderUpdateLineFolder)
	{
		this.orderUpdateLineFolder = orderUpdateLineFolder;
	}
	/**
	 * 
	 * @param orderCancelFolder
	 */
	public void setOrderCancelFolder(final String orderCancelFolder)
	{
		this.orderCancelFolder = orderCancelFolder;
	}
	/**
	 * 
	 * @param orderCancelLineFolder
	 */
	public void setOrderCancelLineFolder(final String orderCancelLineFolder)
	{
		this.orderCancelLineFolder = orderCancelLineFolder;
	}
	/**
	 * 
	 * @param orderReturnFolder
	 */
	public void setOrderReturnFolder(final String orderReturnFolder)
	{
		this.orderReturnFolder = orderReturnFolder;
	}
	/**
	 * 
	 * @param orderReturnLineFolder
	 */
	public void setOrderReturnLineFolder(final String orderReturnLineFolder)
	{
		this.orderReturnLineFolder = orderReturnLineFolder;
	}
	/**
	 * 
	 * @param customerAccountService
	 */
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}
	/**
	 * 
	 * @param baseStoreService
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}
	/**
	 * 
	 * @param modelService
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
	/**
	 * 
	 * @param errorFolder
	 */
	public void setErrorFolder(final String errorFolder)
	{
		this.errorFolder = errorFolder;
	}
	/**
	 * 
	 * @param archiveFolder
	 */
	public void setArchiveFolder(final String archiveFolder)
	{
		this.archiveFolder = archiveFolder;
	}
	/**
	 * 
	 * @param userService
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
	/**
	 * 
	 * @return
	 */
	public AxonSMSService getAxonSMSService() {
		return axonSMSService;
	}
	/**
	 * 
	 * @param axonSMSService
	 */
	public void setAxonSMSService(final AxonSMSService axonSMSService) {
		this.axonSMSService = axonSMSService;
	}
	/**
	 * 
	 * @return
	 */
	public OrderFacade getOrderFacade() {
		return orderFacade;
	}
	/**
	 * 
	 * @param orderFacade
	 */
	public void setOrderFacade(final OrderFacade orderFacade) {
		this.orderFacade = orderFacade;
	}
	/**
	 * 
	 * @return
	 */
	public I18NService getI18nService() {
		return i18nService;
	}
	/**
	 * 
	 * @param i18nService
	 */
	public void setI18nService(final I18NService i18nService) {
		this.i18nService = i18nService;
	}
	/**
	 * 
	 * @return
	 */
	public BusinessProcessService getBusinessProcessService() {
		return businessProcessService;
	}
	/**
	 * 
	 * @param businessProcessService
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService) {
		this.businessProcessService = businessProcessService;
	}
	/**
	 * 
	 * @return
	 */
	public OrderCancelService getOrderCancelService() {
		return orderCancelService;
	}
	/**
	 * 
	 * @param orderCancelService
	 */
	public void setOrderCancelService(final OrderCancelService orderCancelService) {
		this.orderCancelService = orderCancelService;
	}
	/**
	 * 
	 * @return
	 */
	public SharjahStockService getStockService() {
		return stockService;
	}
	/**
	 * 
	 * @param stockService
	 */
	public void setStockService(final SharjahStockService stockService) {
		this.stockService = stockService;
	}
}
