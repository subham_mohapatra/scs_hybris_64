package com.sharjah.core.media.service.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sharjah.core.constants.SharjahCoreConstants;
import com.sharjah.core.media.service.SharjahProduct360MediaImportService;
import com.sharjah.core.product.service.SharjahProductService;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

/**
 * 
 * @author Wael Sakhri @author issam.maiza
 * 
 * Services to Import 360 product media according to the configuration
 * (media folder to import and archive or delete data options)
*/

public class DefaultSharjahProduct360MediaImportService implements SharjahProduct360MediaImportService {

	private static final Logger LOG = LogManager.getLogger(DefaultSharjahProduct360MediaImportService.class);

	enum ImportStatus {
		CREATED, UPDATED, ERROR
	}

	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private MediaService mediaService;
	@Resource
	private SharjahProductService productService;
	@Resource
	private ModelService modelService;
	@Resource
	private ConfigurationService configurationService;



	private final static String NAME_PATTERN = "[0-9]+_v360_[0-9]+.(jpg|pdf)";
	private final static String NAME = "NAME";
	private final static String MIME = "MIME";
	private final static String EXTENSION = "EXTENSION";
	private final static String BARCODE = "BARCODE";
	private final static String SEQUENCE_BUMBER = "SEQUENCE_BUMBER";
	private final static String MEDIA_FILE_HOLDER = "media360.import.folder";
	private final static String MEDIA_FILE_ARCHIVE = "media360.import.archive";
	private final static String MEDIA_FILE_ARCHIVE_ACTIVE = "media360.import.archive.active";	
	private final static String MEDIA_FILE_ARCHIVE_ERROR = "media360.import.error.folder";
	private final static String PDF_EXTENSION = "pdf";
	private final static String PDF_MIME = "application/pdf";
	private final static String JPG_MIME = "image/jpg";
	private final static String JPG_EXTENSION = "jpg";
	private Pattern pattern;
	private MediaFolderModel sharjahMediaFolder;
	private CatalogVersionModel sharjahCatalogVersion;

	/**
	 * 
	 */
	@Override
	public void importProduct360Media()
	{
		LOG.info("Start verifying diffrent folders");
		final File actualMediaFolder = new File((String) configurationService.getConfiguration().getProperty(MEDIA_FILE_HOLDER));
		final File archiveMediaFolder = new File((String) configurationService.getConfiguration().getProperty(MEDIA_FILE_ARCHIVE));
		final File errorMediaFolder = new File((String) configurationService.getConfiguration().getProperty(MEDIA_FILE_ARCHIVE_ERROR));
		if (!actualMediaFolder.exists())
		{
			actualMediaFolder.mkdir();
		}
		if (!archiveMediaFolder.exists())
		{
			archiveMediaFolder.mkdir();
		}
		if (!errorMediaFolder.exists())
		{
			errorMediaFolder.mkdir();
		}
		if (LOG.isDebugEnabled()){
		LOG.info("End verifying diffrent folders");
		}

		if (LOG.isDebugEnabled()){
		LOG.info("Start product 360 media import ...");
		}

		sharjahCatalogVersion = catalogVersionService.getCatalogVersion(
				SharjahCoreConstants.SHARJAH_PRODUCT_CATALOG_CODE, SharjahCoreConstants.CATALOG_STAGED_CODE);

		sharjahMediaFolder = mediaService.getFolder(SharjahCoreConstants.SHARJAH_MEDIA_FOLDER_CODE);

		if (actualMediaFolder != null && actualMediaFolder.list() != null) {
			processMedias(actualMediaFolder);
		}
		if (LOG.isDebugEnabled()){
		LOG.info("Finish product 360 media import ...");
		}
	}

	/**
	 * Method to prcess Media from actual folder
	 * @param actualMediaFolder
	 * 
	 */
	private void processMedias(final File actualMediaFolder) {
		for (final File file : actualMediaFolder.listFiles()) {

			if (isValidFileName(file.getName())) {
				final Map<String, String> fileInfo = getFileInfo(file.getName());
				try {
					processMedia(file, fileInfo);
				} catch (final FileNotFoundException e) {
					LOG.error("Error occured while processing media import", e);
				}
			}
		}
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean isValidFileName(final String fileName) {
		pattern = Pattern.compile(NAME_PATTERN);
		final Matcher m = pattern.matcher(fileName);

		if (m.find()) {
			return true;
		}

		return false;
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	private Map<String, String> getFileInfo(final String fileName) {

		final Map<String, String> fileInfo = new HashMap<>();

		final String[] splited = fileName.split("_");

		fileInfo.put(NAME, fileName);
		fileInfo.put(BARCODE, splited[0]);

		final String[] extension = splited[2].split("\\.");

		fileInfo.put(SEQUENCE_BUMBER, extension[0]);

		if (PDF_EXTENSION.equals(extension[1])) {
			fileInfo.put(MIME, PDF_MIME);
			fileInfo.put(EXTENSION, PDF_EXTENSION);
		} else {
			fileInfo.put(MIME, JPG_MIME);
			fileInfo.put(EXTENSION, JPG_EXTENSION);
		}

		return fileInfo;
	}

	/**
	 * 
	 * @param file
	 * @param fileInfo
	 * @return
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("resource")
	private MediaModel processMedia(final File file, final Map<String, String> fileInfo) throws FileNotFoundException {

		sharjahMediaFolder = mediaService.getFolder(SharjahCoreConstants.SHARJAH_MEDIA_FOLDER_CODE);
		sharjahCatalogVersion = catalogVersionService.getCatalogVersion(
				SharjahCoreConstants.SHARJAH_PRODUCT_CATALOG_CODE, SharjahCoreConstants.CATALOG_STAGED_CODE);

		final ProductModel product = productService.findProductByCodeForMedia(sharjahCatalogVersion, fileInfo.get(BARCODE));

		MediaModel mediaModel = null;
		InputStream mediaInputStream = null;

		ImportStatus status = ImportStatus.CREATED;

		try {

			if (product != null) {
				mediaModel = mediaService.getMedia(sharjahCatalogVersion, getMediaCode(product, fileInfo));

				mediaInputStream = updateMedia(mediaModel, file, fileInfo);

				status = ImportStatus.UPDATED;
			} else {
				if (LOG.isDebugEnabled()){
				LOG.info("product with code " + fileInfo.get(BARCODE) + "dose not existe and file with code"
						+ file.getName() + "will be moved to error folder");
				}
				status = ImportStatus.ERROR;
			}

		} catch (final UnknownIdentifierException uie) {

			LOG.info("Media for code  " + getMediaCode(product, fileInfo)+ "will be created now and setted to product with code" + product.getCode(), uie);
			
			mediaInputStream = createMedia(product, file, fileInfo);

		} catch (final Exception e) {

			LOG.error("An error occured while importing media : " + fileInfo, e);

			status = ImportStatus.ERROR;

		} finally {

			if (mediaInputStream != null) {
				IOUtils.closeQuietly(mediaInputStream);
			}

		}

		manageImportedMedia(file, status);

		return mediaModel;
	}

	/**
	 * 
	 * @param mediaModel
	 * @param file
	 * @param fileInfo
	 * @return
	 * @throws FileNotFoundException
	 */
	private InputStream updateMedia(final MediaModel mediaModel, final File file, final Map<String, String> fileInfo)
			throws FileNotFoundException {
		if (mediaModel != null) {
			final InputStream mediaInputStream = new BufferedInputStream(new FileInputStream(file));
			mediaService.setStreamForMedia(mediaModel, mediaInputStream, fileInfo.get(NAME), fileInfo.get(MIME));
			modelService.refresh(mediaModel);

			return mediaInputStream;
		}

		return null;
	}

	/**
	 * 
	 * @param product
	 * @param file
	 * @param fileInfo
	 * @return
	 * @throws FileNotFoundException
	 */
	private InputStream createMedia(final ProductModel product, final File file, final Map<String, String> fileInfo)
			throws FileNotFoundException {

		final InputStream mediaInputStream = new BufferedInputStream(new FileInputStream(file));
		try {

		final MediaModel mediaModel = modelService.create(MediaModel.class);
		mediaModel.setCode(getMediaCode(product, fileInfo));
		mediaModel.setCatalogVersion(sharjahCatalogVersion);
		mediaModel.setFolder(sharjahMediaFolder);
		mediaModel.setProduct(product);

		modelService.save(mediaModel);
		mediaService.setStreamForMedia(mediaModel, mediaInputStream, fileInfo.get(NAME), fileInfo.get(MIME));
	    } catch (Exception e) {
	    	LOG.info("error when create Media for product with EAN" + product.getEan(), e);
	    }
		return mediaInputStream;
	}

	/**
	 * 
	 * @param product
	 * @param fileInfo
	 * @return
	 */
	private String getMediaCode(final ProductModel product, final Map<String, String> fileInfo) {

		final StringBuilder mediaCode = new StringBuilder();

		mediaCode.append(product.getCode() + "_");
		mediaCode.append(fileInfo.get(SEQUENCE_BUMBER));
		if (PDF_EXTENSION.equals(fileInfo.get(EXTENSION))) {
			mediaCode.append("/BASEPDF");
		} else {
			mediaCode.append("/BASEIMAGE");
		}

		return mediaCode.toString();
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	private String getDestinationFolderName(final ImportStatus status) {
		String destinationFolderName = null;
		if (status.equals(ImportStatus.CREATED) || status.equals(ImportStatus.UPDATED))
		{
			destinationFolderName = (String) configurationService.getConfiguration().getProperty(MEDIA_FILE_ARCHIVE);
		}
		if (status.equals(ImportStatus.ERROR))
		{
			destinationFolderName = (String) configurationService.getConfiguration().getProperty(MEDIA_FILE_ARCHIVE_ERROR);
		}

		return destinationFolderName;

	}

	/**
	 * 
	 * @param file
	 * @param status
	 */
	private void manageImportedMedia(final File file, final ImportStatus status) {

		if (Config.getBoolean(MEDIA_FILE_ARCHIVE_ACTIVE, false)) {

			final File destinationFolder = new File(getDestinationFolderName(status));

			moveFile(file, destinationFolder);

		} else {
			file.delete();
		}
	}

	/**
	 * move file to destination
	 * @param src, destinationFolder
	 * IOException
	 */
	private void moveFile(final File src, final File destinationFolder) {
		if (src != null && destinationFolder != null) 
		{
			final Path srcFile = src.toPath();
			final Path destFile = new File(destinationFolder ,src.getName()).toPath();
			try {
				Files.move(srcFile, destFile, StandardCopyOption.REPLACE_EXISTING);
			} catch (final IOException e) {
				if (LOG.isDebugEnabled()){
				LOG.info("Cannot archive file " + src.toString(), e);
				}
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public ConfigurationService getConfigurationService() {
		return configurationService;
	}

	/**
	 * 
	 * @param configurationService
	 */
	public void setConfigurationService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}
}
