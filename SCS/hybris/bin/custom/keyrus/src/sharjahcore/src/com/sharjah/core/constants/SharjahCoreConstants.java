/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sharjah.core.constants;

import de.hybris.platform.util.Config;

/**
 * Global class for all SharjahCore constants. You can add global constants for your extension into this class.
 */
public final class SharjahCoreConstants extends GeneratedSharjahCoreConstants
{
	public static final String EXTENSIONNAME = "sharjahcore";

	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS ="quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS ="quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	
	public static final String SHARJAH_PRODUCT_CATALOG_CODE = "scsProductCatalog";
	public static final String CATALOG_STAGED_CODE = "Staged";
	public static final String SHARJAH_MEDIA_FOLDER_CODE = "scs";
	
	public static final String SHARJAH_BASE_STORE_CODE = "scs";
	
	
	public static final String INDEXEDED_PROPERTY_STICKER_END_DATE = "stickerEndDate";
	public static final String INDEXEDED_PROPERTY_STICKER_START_DATE = "stickerStartDate";
	public static final String INDEXEDED_PROPERTY_STICKER = "stickers";
	
	public static final String PAYMENT_PROVIDER_COD = "CashOnDelivery";

	private SharjahCoreConstants()
	{
		//empty
	}

	public static String getCronJobMediaSuccessFolder(final String brand)
	{
		return Config.getString("media."+brand+".import.cronjob.folder.success", null);
	}

	public static String getCronJobMediaErrorFolder(final String brand)
	{
		return Config.getString("media."+brand+".import.cronjob.folder.error", null);
	}
	
}
