package com.sharjah.core.product.service;

/**
 * Interface of the Media Import Exception
 * 
 * @author Luiz.Henriques
 *
 */
public class MediaImportException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MediaImportException(String message, Throwable cause) {
		super(message, cause);
	}

	public MediaImportException(String message) {
		super(message);
	}

	public MediaImportException(Throwable cause) {
		super(cause);
	}
}
