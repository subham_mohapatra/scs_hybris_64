package com.sharjah.core.returns.strategy;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.strategy.ReturnableCheck;

import java.util.Collection;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class SharjahEligibleToReturnCheckStrategy implements ReturnableCheck
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean perform(final OrderModel order, final AbstractOrderEntryModel entry, final long returnQuantity)
	{
		final boolean isEligibleToReturn = true;
		
		if (Boolean.FALSE.equals(entry.getProduct().getEligibleToReturn()))
		{
			return false;
		}
		else if (Boolean.TRUE.equals(entry.getProduct().getEligibleToReturn()))
		{
			return true;
		}else{
			final Collection<CategoryModel> productCategories = entry.getProduct().getSupercategories();
			for (final CategoryModel category : productCategories)
			{
				if (category.getEligibleToReturn() == null || Boolean.FALSE.equals(category.getEligibleToReturn()))
				{
					return false;
				}
			}
		}
		return isEligibleToReturn;
	}

}
