package com.sharjah.core.storefinder.impl;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;
import de.hybris.platform.commerceservices.storefinder.data.PointOfServiceDistanceData;
import de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData;
import de.hybris.platform.commerceservices.storefinder.impl.DefaultStoreFinderService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.sharjah.core.pos.dao.SharjahPointOfServiceDao;


/**
 * Class of the find store service that extends DefaultStoreFinderService
 * 
 * @author Ben Amor Bassem
 * 
 * @param <ITEM>
 *           type of item to be searched that extends {@link PointOfServiceDistanceData}, distance to point of service.
 */
@Service
public class DefaultSharjahStoreFinderService<ITEM extends PointOfServiceDistanceData> extends DefaultStoreFinderService<ITEM>

{
	private SharjahPointOfServiceDao pointOfServiceDao;

	private static final Logger LOG = Logger.getLogger(DefaultSharjahStoreFinderService.class);

	@Override
	public StoreFinderSearchPageData<ITEM> locationSearch(final BaseStoreModel baseStore, final String locationText,
			final PageableData pageableData)
	{
		final GeoPoint geoPoint = new GeoPoint();

		if (StringUtils.isNotEmpty(locationText))
		{
			try
			{
				// Resolve the address to a point
				final GPS resolvedPoint = getGeoWebServiceWrapper()
						.geocodeAddress(generateGeoAddressForSearchQuery(baseStore, locationText));

				geoPoint.setLatitude(resolvedPoint.getDecimalLatitude());
				geoPoint.setLongitude(resolvedPoint.getDecimalLongitude());

				return doSearch(baseStore, locationText, geoPoint, pageableData, baseStore.getMaxRadiusForPoSSearch());
			}
			catch (final GeoServiceWrapperException ex)
			{
				if (LOG.isInfoEnabled())
				{
					LOG.info("Failed to resolve location for [" + locationText + "]", ex);
				}
			}
		}

		// Return no results
		return createSearchResult(locationText, geoPoint, Collections.<ITEM> emptyList(), createPaginationData());
	}

	@Override
	protected StoreFinderSearchPageData<ITEM> doSearch(final BaseStoreModel baseStore, final String locationText,
			final GeoPoint centerPoint, final PageableData pageableData, final Double maxRadiusKm)
	{
		Collection<PointOfServiceModel> posResults;

		final int resultRangeStart = pageableData.getCurrentPage() * pageableData.getPageSize();
		final int resultRangeEnd = (pageableData.getCurrentPage() + 1) * pageableData.getPageSize();

		if (maxRadiusKm != null)
		{
			//	posResults = getPointsOfServiceNear(centerPoint, maxRadiusKm.doubleValue(), baseStore);
			//	if (CollectionUtils.isEmpty(posResults))
			//	{
				posResults = getPointOfServiceDao().getPointsOfServicesByDisplayName(locationText);
			//	}
		}
		else
		{
//			final Map<String, Object> paramMap = new HashMap<String, Object>();
//			paramMap.put("baseStore", baseStore);
//			paramMap.put("type", PointOfServiceTypeEnum.STORE);
//			posResults = getPointOfServiceGenericDao().find(paramMap);
			
			posResults = getPointOfServiceDao().getPointsOfServices();
		}

		if (posResults != null)
		{
			// Sort all the POS
			final List<ITEM> orderedResults = calculateDistances(centerPoint, posResults);
			final PaginationData paginationData = createPagination(pageableData, posResults.size());
			// Slice the required range window out of the results
			final List<ITEM> orderedResultsWindow = orderedResults.subList(Math.min(orderedResults.size(), resultRangeStart),
					Math.min(orderedResults.size(), resultRangeEnd));

			return createSearchResult(locationText, centerPoint, orderedResultsWindow, paginationData);
		}

		// Return no results
		return createSearchResult(locationText, centerPoint, Collections.<ITEM> emptyList(), createPagination(pageableData, 0));
	}

	@Override
	public SharjahPointOfServiceDao getPointOfServiceDao()
	{
		return pointOfServiceDao;
	}

	/**
	 * 
	 * @param pointOfServiceDao
	 */
	public void setPointOfServiceDao(final SharjahPointOfServiceDao pointOfServiceDao)
	{
		this.pointOfServiceDao = pointOfServiceDao;
	}

}
