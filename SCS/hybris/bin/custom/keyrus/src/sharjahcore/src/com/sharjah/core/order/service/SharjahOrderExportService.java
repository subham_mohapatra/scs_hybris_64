package com.sharjah.core.order.service;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

/**
 * Exports order into a file
 * 
 * @author Luiz.Henriques
 *
 */
public interface SharjahOrderExportService
{

	/**
	 * process the export file process
	 * @param order
	 * @param exportType value containing new/return/cancel values
	 * @return boolean
	 */
	boolean exportOrder(OrderModel order, String exportType);
	
	/**
	 * process the export file process
	 * @param returnRequest
	 * @param exportType value containing new/return/cancel values
	 * @return boolean
	 */
	boolean exportOrder(ReturnRequestModel returnRequest, String exportType);
	
}
