package com.sharjah.core.deliveryarea.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import com.sharjah.core.deliveryarea.dao.SharjahDeliveryCityDao;
import com.sharjah.core.model.DeliveryCityModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahDeliveryCityDao extends DefaultGenericDao<DeliveryCityModel> implements SharjahDeliveryCityDao
{

	/**
	 * Default constructor.
	 */
	public DefaultSharjahDeliveryCityDao()
	{
		super(DeliveryCityModel._TYPECODE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryCityModel> findAll()
	{
		return find();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryCityModel findDeliveryCityByCode(final String cityCode)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(DeliveryCityModel.CODE, cityCode);
		final List<DeliveryCityModel> cities = find(params);
		if (CollectionUtils.isNotEmpty(cities))
		{
			return cities.get(0);
		}

		return null;
	}

}
