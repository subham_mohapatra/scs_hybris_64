package com.sharjah.core.sitemap.generator.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.CategoryPageSiteMapGenerator;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author Issam Maiza
 *
 */
public class SharjahCategoryPageSiteMapGenerator extends CategoryPageSiteMapGenerator{

	@Override
	protected List<CategoryModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final String query = "SELECT {c.pk} FROM {Category AS c JOIN CatalogVersion AS cv ON {c.catalogVersion}={cv.pk} "
				+ " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
				+ " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}}  WHERE {site.pk} = ?site "
				+ " AND NOT exists ({{select {cr.pk} from {CategoriesForRestriction as cr} where {cr.target} = {c.pk} }})"
				+ " AND ({{SELECT {pa.pk} FROM {CategoryPage AS pa JOIN CatalogVersion AS cv ON {pa.catalogVersion}={cv.pk}} WHERE {pa.uid} = 'productGrid' and {pa.siteMapEligiblePage} = ?siteMapEligiblePage and {cv.version} = 'Staged'}})";

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("site", siteModel);
		params.put("siteMapEligiblePage", Boolean.TRUE);
		return doSearch(query, params, CategoryModel.class);
	}
}
