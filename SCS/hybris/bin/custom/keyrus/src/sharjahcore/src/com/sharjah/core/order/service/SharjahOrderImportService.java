package com.sharjah.core.order.service;

/**
 * Perform the order update/cancel/return process, read files and set status
 * Match order with orderLine files, is not matching, skip
 */
public interface SharjahOrderImportService
{
	
	/**
	 * Perform the order update process, read files and set status
	 * Match order with orderLine files, is not matching, skip
	 * 
	 * @return boolean
	 */
	boolean readUpdateOrders();
	
	/**
	 * Perform the order cancel process, read files and set status
	 * Match order with orderLine files, is not matching, skip
	 * 
	 * @return boolean
	 */
	boolean readCancelOrders();
	
	/**
	 * Perform the order return process, read files and set status
	 * Match order with orderLine files, is not matching, skip
	 * 
	 * @return boolean
	 */
	boolean readReturnOrders();
	
}
