package com.sharjah.core.cronjobs;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.sharjah.core.customer.service.SharjahCustomerExportService;
import com.sharjah.core.model.cronjob.SharjahCronjobModel;


/**
 * Exports new and updated customers to a file.
 *
 * @author Pablo François
 */
public class CustomerExportJob extends AbstractJobPerformable<SharjahCronjobModel>
{
	private static final Logger LOGGER = Logger.getLogger(CustomerExportJob.class);

	private static final String QUERY = "SELECT {pk} FROM {Customer} WHERE {modifiedTime} >= ?threshold";

	@Resource(name = "defaultSharjahCustomerExportService")
	private SharjahCustomerExportService customerExportService;

	private List<CustomerModel> searchCustomers(final Date startDate)
	{
		final Date threshold = startDate != null ? startDate : new Date(0);
		final SearchResult<CustomerModel> searchResult = flexibleSearchService.search(QUERY,
				ImmutableMap.of("threshold", threshold));
		return searchResult.getResult();
	}

	/**
	 * Perform the job.
	 */
	@Override
	public PerformResult perform(final SharjahCronjobModel cronjob)
	{
		LOGGER.debug("Performing CustomerExportJob");
		final List<CustomerModel> customers = searchCustomers(cronjob.getLastSuccessfulRun());

		LOGGER.info(String.format("CustomerExportJob found %d new/updated customer(s) to export", customers.size()));
		try
		{
			if (CollectionUtils.isNotEmpty(customers))
			{
				customerExportService.exportCustomers(customers);
			}
		}
		catch (final IOException e)
		{
			LOGGER.error(e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		cronjob.setLastSuccessfulRun(cronjob.getStartTime());
		modelService.save(cronjob);

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}
