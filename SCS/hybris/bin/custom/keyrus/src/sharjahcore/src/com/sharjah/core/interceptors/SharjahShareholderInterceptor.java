package com.sharjah.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sharjah.core.model.ShareholderModel;


/**
 * Class to validate the shareholder's number during the shareholder creation in the backoffice
 *
 */
public class SharjahShareholderInterceptor implements ValidateInterceptor
{
	private static final Pattern SHAREHOLDER_PATTERN = Pattern.compile("(W|G|S)(\\d+)");

	@Override
	public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException
	{
		if (model instanceof ShareholderModel)
		{
			final ShareholderModel shareholder = (ShareholderModel) model;
			final String shareholderNumber = shareholder.getShareholderNumber();
			if (shareholderNumber != null)
			{
				final Matcher matcher = SHAREHOLDER_PATTERN.matcher(shareholderNumber);
				if (!matcher.matches())
				{
					throw new InterceptorException("Shareholder Number is invalid.");
				}
			}
			else
			{
				throw new InterceptorException("Shareholder Number cannot be null.");
			}
		}
	}

}
