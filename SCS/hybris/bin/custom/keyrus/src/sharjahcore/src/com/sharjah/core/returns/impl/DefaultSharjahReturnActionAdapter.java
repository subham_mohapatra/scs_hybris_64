package com.sharjah.core.returns.impl;

import de.hybris.platform.processengine.BusinessProcessEvent;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.returns.ReturnActionAdapter;
import de.hybris.platform.returns.model.ReturnProcessModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

/**
 * @author Luiz Henriques
 * @author Pablo François
 */
public class DefaultSharjahReturnActionAdapter implements ReturnActionAdapter
{

	private static final String CONFIRM_OR_CANCEL_REFUND_ACTION_EVENT_NAME = "ConfirmOrCancelRefundEvent";
	private static final String FAIL_CAPTURE_ACTION_EVENT_NAME = "FailCaptureActionEvent";
	private static final String APPROVE_CANCEL_GOODS_EVENT_NAME = "ApproveOrCancelGoodsEvent";
	private static final String WAIT_FOR_FAIL_CAPTURE_ACTION = "waitForFailCaptureAction";
	private static final String WAIT_FOR_CONFIRM_OR_CANCEL_REFUND_ACTION = "waitForConfirmOrCancelReturnAction";
	private static final String WAIT_FOR_GOODS_ACTION = "waitForGoodsAction";

	protected static final String APPROVAL_CHOICE = "approveReturn";
	private static final String ACCEPT_GOODS_CHOICE = "acceptGoods";
	private static final String CANCEL_REFUND_CHOICE = "cancelReturn";

	private BusinessProcessService businessProcessService;


	@Override
	public void requestReturnApproval(final ReturnRequestModel returnRequest)
	{
		final String eventName = returnRequest.getReturnProcess().iterator().next().getCode() + "_"
				+ CONFIRM_OR_CANCEL_REFUND_ACTION_EVENT_NAME;
		businessProcessService.triggerEvent(BusinessProcessEvent.builder(eventName)
				.withChoice(APPROVAL_CHOICE)
				.withEventTriggeringInTheFutureDisabled().build());
	}

	@Override
	public void requestReturnReception(final ReturnRequestModel returnRequest)
	{
		final String eventName = returnRequest.getReturnProcess().iterator().next().getCode() + "_"
				+ APPROVE_CANCEL_GOODS_EVENT_NAME;
		businessProcessService.triggerEvent(BusinessProcessEvent.builder(eventName)
				.withChoice(ACCEPT_GOODS_CHOICE)
				.withEventTriggeringInTheFutureDisabled().build());
	}

	@Override
	public void requestReturnCancellation(final ReturnRequestModel returnRequest)
	{
		String event = null;
		final ReturnProcessModel returnProcess = returnRequest.getReturnProcess().iterator().next();
		final String nextAction = returnProcess.getCurrentTasks().iterator().next().getAction();
		switch (nextAction)
		{
			case WAIT_FOR_FAIL_CAPTURE_ACTION:
				event = FAIL_CAPTURE_ACTION_EVENT_NAME;
				break;
			case WAIT_FOR_CONFIRM_OR_CANCEL_REFUND_ACTION:
				event = CONFIRM_OR_CANCEL_REFUND_ACTION_EVENT_NAME;
				break;
			case WAIT_FOR_GOODS_ACTION:
				event = APPROVE_CANCEL_GOODS_EVENT_NAME;
				break;

			default:
				break;
		}

		businessProcessService.triggerEvent(BusinessProcessEvent.builder(returnProcess.getCode() + "_" + event)
				.withChoice(CANCEL_REFUND_CHOICE)
				.withEventTriggeringInTheFutureDisabled()
				.build());
	}

	@Override
	public void requestManualPaymentReversalForReturnRequest(final ReturnRequestModel returnRequest)
	{
		throw new UnsupportedOperationException("Method not implemented");
	}

	@Override
	public void requestManualTaxReversalForReturnRequest(final ReturnRequestModel returnRequest)
	{
		throw new UnsupportedOperationException("Method not implemented");
	}

	/**
	 * 
	 * @param businessProcessService
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

}
