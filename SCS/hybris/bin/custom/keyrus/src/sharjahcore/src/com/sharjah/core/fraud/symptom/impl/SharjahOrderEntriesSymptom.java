package com.sharjah.core.fraud.symptom.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.fraud.impl.FraudServiceResponse;
import de.hybris.platform.fraud.symptom.impl.OrderEntriesSymptom;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import com.sharjah.core.model.FraudSymptomModel;
import com.sharjah.core.fraud.service.impl.FraudConfigService;


/**
 * Override the native symptom to read fraud configuration from a type in the model, instead of some Spring XML file.
 *
 * @author Pablo François
 */
public class SharjahOrderEntriesSymptom extends OrderEntriesSymptom
{

	private static final String ENTRIES_SYMPTOM_NAME = "fraud.symptom.entries";

	@Resource
	ConfigurationService configurationService;
	@Resource
	FraudConfigService fraudConfigService;

	@Override
	public FraudServiceResponse recognizeSymptom(final FraudServiceResponse fraudResponse, final AbstractOrderModel order)
	{
		final String symptomName = configurationService.getConfiguration().getString(ENTRIES_SYMPTOM_NAME);
		final FraudSymptomModel symptom = fraudConfigService.findFraudSymptomByName(symptomName);
		setIncrement(symptom.getIncrement());

		return super.recognizeSymptom(fraudResponse, order);
	}

}
