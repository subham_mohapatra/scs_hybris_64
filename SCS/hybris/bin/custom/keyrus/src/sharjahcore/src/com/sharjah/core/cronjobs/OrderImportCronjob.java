package com.sharjah.core.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.sharjah.core.order.service.SharjahOrderImportService;

public class OrderImportCronjob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LogManager.getLogger(OrderImportCronjob.class);
	
	private SharjahOrderImportService orderImportService;
	
	
	@Override
	public PerformResult perform(final CronJobModel cronjob)
	{
		if (LOG.isInfoEnabled())
		{
			LOG.info("Triggering Import OrderUpdate job 1/3");
		}
		boolean isAllOk = orderImportService.readUpdateOrders();
		if (LOG.isInfoEnabled())
		{
			LOG.info("Import OrderUpdate has finished. The total result is ["+isAllOk+"]");
		}
		
		if (LOG.isInfoEnabled())
		{
			LOG.info("Triggering Import CancelOrder job 2/3");
		}
		isAllOk = orderImportService.readCancelOrders();
		if (LOG.isInfoEnabled())
		{
			LOG.info("Import CancelOrder has finished. The total result is ["+isAllOk+"]");
		}
		
		if (LOG.isInfoEnabled())
		{
			LOG.info("Triggering Import ReturnOrder job 3/3");
		}
		isAllOk = orderImportService.readReturnOrders();
		if (LOG.isInfoEnabled())
		{
			LOG.info("Import ReturnOrder has finished. The total result is ["+isAllOk+"]");
		}
		
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
	
	public void setOrderImportService(final SharjahOrderImportService orderImportService)
	{
		this.orderImportService = orderImportService;
	}

}
