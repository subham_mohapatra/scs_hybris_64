package com.sharjah.core.cronjobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.sharjah.core.deliveryslots.service.DeliverySlotService;
import com.sharjah.core.model.CalendarDeliverySlotModel;
import com.sharjah.core.model.DeliverySlotByPOSModel;
import com.sharjah.core.model.NonWorkingDaysModel;
import com.sharjah.core.model.cronjob.CalendarCronjobModel;


/**
 * Creates a calendar for the Delivery Slots.
 */
public class DeliverySlotCalendarJob extends AbstractJobPerformable<CalendarCronjobModel>
{
	@Resource(name = "defaultDeliverySlotService")
	private DeliverySlotService deliverySlotService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.
	 * CronJobModel )
	 */
	@Override
	public PerformResult perform(final CalendarCronjobModel initCalendarCronjob)
	{
		final BaseStoreModel store = initCalendarCronjob.getBaseStore();
		final List<PointOfServiceModel> listPos = store.getPointsOfService();

		if (initCalendarCronjob.isForceDelete())
		{
			deliverySlotService.deleteSlotsAfter(LocalDate.now());

			// Reset forceDelete to false to avoid accidentally deleting all slots the next time the job runs
			initCalendarCronjob.setForceDelete(false);
			modelService.save(initCalendarCronjob);
		}

		listPos.stream()
				.forEach(pos -> updatePos(pos, initCalendarCronjob.getPeriod()));

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void updatePos(final PointOfServiceModel pos, final Integer period)
	{
		final List<DeliverySlotByPOSModel> slotConfigs = pos.getDeliverySlots();

		final LocalDate startDate = LocalDate.now();

		final List<CalendarDeliverySlotModel> calendarSlots = new ArrayList<>();
		calendarSlots.addAll(pos.getCalendars());

		for (int i = 0; i < period.intValue(); i++)
		{
			final LocalDate currentDate = startDate.plus(i, ChronoUnit.DAYS);
			if (dateCreated(currentDate, pos.getCalendars()))
			{
				continue;
			}

			slotConfigs.stream()
					.filter(slotConfig -> slotConfig.getDayOfWeek().getCode().equals(currentDate.getDayOfWeek().toString()))
					.forEach(slotConfig -> calendarSlots.add(createCalendarSlot(slotConfig, currentDate, pos.getNonWorkingDays())));
		}

		pos.getCalendars().stream().forEach(calendarSlot -> updateWithNonWorkingDays(calendarSlot, pos));
		pos.setCalendars(calendarSlots);
		modelService.save(pos);
	}

	private void updateWithNonWorkingDays(final CalendarDeliverySlotModel day, final PointOfServiceModel pos)
	{
		final LocalDate dayLocalDate = day.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		final boolean nonWorkingDay = currentDateInNonWorkingDays(dayLocalDate, pos.getNonWorkingDays());

		if (day.getCapacity() >= 0 && nonWorkingDay)
		{
			day.setCapacity(-1);
			modelService.save(day);
		}
		else if (day.getCapacity() < 0 && !nonWorkingDay)
		{
			final List<DeliverySlotByPOSModel> deliveries = pos.getDeliverySlots();
			deliveries.stream().filter(ds -> ds.getDayOfWeek().getCode().equals(dayLocalDate.getDayOfWeek().toString()))
					.forEach(ds -> {
						day.setCapacity(ds.getCapacity());
						modelService.save(day);
					});
		}
	}

	private CalendarDeliverySlotModel createCalendarSlot(final DeliverySlotByPOSModel ds, final LocalDate currentDate,
			final List<NonWorkingDaysModel> nonWorkingDays)
	{
		final CalendarDeliverySlotModel calendar = new CalendarDeliverySlotModel();
		final Instant instant = currentDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
		calendar.setDate(Date.from(instant));
		calendar.setPos(ds.getPos());
		calendar.setDeliverySlot(ds.getDeliverySlot());
		if (currentDateInNonWorkingDays(currentDate, nonWorkingDays))
		{
			calendar.setCapacity(-1);
		}
		else
		{
			calendar.setCapacity(ds.getCapacity());
		}
		modelService.save(calendar);

		return calendar;
	}

	private boolean currentDateInNonWorkingDays(final LocalDate currentDate, final List<NonWorkingDaysModel> nonWorkingDays)
	{
		for (final NonWorkingDaysModel nonWorkingDay : nonWorkingDays)
		{
			final LocalDate nonWorkingDayDate = nonWorkingDay.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			if (currentDate.isEqual(nonWorkingDayDate))
			{
				return true;
			}
		}

		return false;
	}

	private boolean dateCreated(final LocalDate currentDate, final List<CalendarDeliverySlotModel> calendars)
	{
		for (final CalendarDeliverySlotModel calendar : calendars)
		{
			final LocalDate date = calendar.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			if (currentDate.isEqual(date))
			{
				return true;
			}
		}

		return false;
	}

	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		super.modelService = modelService;
	}

}
