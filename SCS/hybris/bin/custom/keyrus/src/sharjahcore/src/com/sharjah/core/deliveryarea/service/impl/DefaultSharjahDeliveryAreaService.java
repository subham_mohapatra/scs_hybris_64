package com.sharjah.core.deliveryarea.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.sharjah.core.comparators.SharjahDeliveryCityModelComparator;
import com.sharjah.core.deliveryarea.dao.SharjahDeliveryAreaDao;
import com.sharjah.core.deliveryarea.dao.SharjahDeliveryCityDao;
import com.sharjah.core.deliveryarea.service.SharjahDeliveryAreaService;
import com.sharjah.core.model.DeliveryAreaModel;
import com.sharjah.core.model.DeliveryCityModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public class DefaultSharjahDeliveryAreaService implements SharjahDeliveryAreaService
{

	@Resource
	private SharjahDeliveryAreaDao sharjahDeliveryAreaDao;

	@Resource
	private SharjahDeliveryCityDao sharjahDeliveryCityDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryCityModel> findAllDeliveryCities()
	{
		final List<DeliveryCityModel> deliveryCityModel = sharjahDeliveryCityDao.findAll();
		if (CollectionUtils.isNotEmpty(deliveryCityModel))
		{
			final List<DeliveryCityModel> deliveryCities = new ArrayList<>(deliveryCityModel);
			Collections.sort(deliveryCities, new SharjahDeliveryCityModelComparator());
			//deliveryCities.add(0, new DeliveryCityModel());
			return deliveryCities;
		}
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryAreaModel findDeliveryAreaByCode(final String code)
	{
		return sharjahDeliveryAreaDao.findDeliveryAreaByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryCityModel findDeliveryCityByCode(final String cityCode)
	{
		return sharjahDeliveryCityDao.findDeliveryCityByCode(cityCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryAreaModel> findAreasForCityCode(final String cityCode)
	{
		final DeliveryCityModel city = findDeliveryCityByCode(cityCode);
		if (city != null)
		{
			return city.getDeliveryAreas();
		}
		else
		{
			return null;
		}
	}

}
