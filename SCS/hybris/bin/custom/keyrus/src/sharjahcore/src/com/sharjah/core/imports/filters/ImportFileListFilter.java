package com.sharjah.core.imports.filters;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.springframework.integration.file.filters.FileListFilter;


/**
 * Filters files whose {@link File#lastModified()} plus {@link #age} is inferior to the current time.
 * <p>
 * The time resolution is in seconds.
 */
public class ImportFileListFilter implements FileListFilter<File>
{

	private static final long DEFAULT_AGE = 60;

	private volatile long age = DEFAULT_AGE;

	private volatile Pattern pattern = Pattern.compile(".*");

	private final Queue<String> seen = new LinkedBlockingQueue<>(100);

	private final Set<String> seenSet = new HashSet<>();

	private final Object monitor = new Object();

	public ImportFileListFilter()
	{
		//empty
	}

	/**
	 * Constructor.
	 *
	 * @param age
	 */
	public ImportFileListFilter(final long age)
	{
		this.age = age;
	}

	public void setAge(final long age)
	{
		setAge(age, TimeUnit.SECONDS);
	}

	/**
	 * Set the age.
	 *
	 * @param age
	 *           Age value
	 * @param unit
	 *           The time unit
	 */
	public void setAge(final long age, final TimeUnit unit)
	{
		this.age = unit.toSeconds(age);
	}

	public void setPattern(final String pattern)
	{
		this.pattern = Pattern.compile(pattern);
	}

	@Override
	public List<File> filterFiles(final File[] files)
	{
		final List<File> list = new ArrayList<>();
		final long now = System.currentTimeMillis() / 1000;
		for (final File file : files)
		{
			synchronized (this.monitor)
			{
				if (file.lastModified() / 1000 + this.age <= now && pattern.matcher(file.getName()).matches()
						&& !seenSet.contains(file.getName()) && !file.isHidden())
				{
					list.add(file);
					if (!seen.offer(file.getName()))
					{
						final String removedFileName = seen.poll();
						seenSet.remove(removedFileName);
						seen.add(file.getName());
					}
					seenSet.add(file.getName());
				}
			}
		}
		return list;
	}

}
