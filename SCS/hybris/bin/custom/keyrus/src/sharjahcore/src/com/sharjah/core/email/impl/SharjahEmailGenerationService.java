package com.sharjah.core.email.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.processengine.model.BusinessProcessModel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;



/**
 * Extends default hybris class to add missing functionality.
 *
 * @author Ben Amor Bassem
 */
public class SharjahEmailGenerationService extends DefaultEmailGenerationService
{

	@Value("#{configurationService.configuration.getProperty('order.emails.bcc')}")
	private String bccOrder;

	private static final String SEPARATOR = ",";

	/**
	 * Override to add missing functionality.
	 */
	@Override
	protected EmailMessageModel createEmailMessage(final String emailSubject, final String emailBody,
			final AbstractEmailContext<BusinessProcessModel> emailContext)
	{
		final List<EmailAddressModel> toEmails = new ArrayList<>();
		final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getToEmail(),
				emailContext.getToDisplayName());
		toEmails.add(toAddress);
		final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getFromEmail(),
				emailContext.getFromDisplayName());

		final List<EmailAddressModel> bccEmails = new ArrayList<EmailAddressModel>();

		if (!emailSubject.contains("shopping") && !emailSubject.contains("Registration") && !emailSubject.contains("Password"))
		{
			if (bccOrder.contains(SEPARATOR))
			{
				final String[] bccList = bccOrder.split(SEPARATOR);
				for (final String bcc : bccList)
				{
					final EmailAddressModel bccEmailAddressModel = generateBccEmailList(bcc, emailContext.getToDisplayName());
					bccEmails.add(bccEmailAddressModel);
				}
			}
			else
			{
				bccEmails.add(generateBccEmailList(bccOrder, emailContext.getToDisplayName()));
			}
		}



		return getEmailService().createEmailMessage(toEmails, new ArrayList<EmailAddressModel>(),
				bccEmails, fromAddress, emailContext.getFromEmail(), emailSubject, emailBody, null);
	}

	/**
	 * 
	 * @param bcc
	 * @param displayName
	 * @return
	 */
	private EmailAddressModel generateBccEmailList(final String bcc, final String displayName)
	{
		final EmailAddressModel emailModel = getEmailService().getOrCreateEmailAddressForEmail(bcc, displayName);
		return emailModel;
	}
}
