package com.sharjah.core.cartthresholdconfiguration.dao;

import com.sharjah.core.model.CartThresholdConfigurationModel;


/**
 * 
 * @author Abderrazzak.Blej
 *
 */
public interface SharjahCartThresholdConfigurationDao
{

	/**
	 * Get cart threshold configuration by code.
	 * 
	 * @param code
	 *           The code to find
	 * @return The cart threshold configuration
	 */
	CartThresholdConfigurationModel getCartThresholdConfigurationByCode(String code);
}
