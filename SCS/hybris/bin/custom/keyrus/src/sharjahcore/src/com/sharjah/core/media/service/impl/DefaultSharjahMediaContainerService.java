package com.sharjah.core.media.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.sharjah.core.media.dao.SharjahMediaContainerDao;
import com.sharjah.core.media.service.SharjahMediaContainerService;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.MediaContainerDao;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaContainerService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

/**
 * 
 * @author Luiz.Henriques
 *
 */
public class DefaultSharjahMediaContainerService extends DefaultMediaContainerService implements SharjahMediaContainerService
{

    private SharjahMediaContainerDao mediaContainerDao;

    /*
     * (non-Javadoc)
     * @see com.sharjah.core.media.service.SharjahMediaContainerService#findMediaContainerForQualifier(java.lang.String, de.hybris.platform.catalog.model.CatalogVersionModel)
     */
    @Override
    public MediaContainerModel findMediaContainerForQualifier(final String qualifier, final CatalogVersionModel catalogVersion)
    {
        ServicesUtil.validateParameterNotNull(qualifier, "Media container qualifier cannot be null");
        ServicesUtil.validateParameterNotNull(catalogVersion, "Media container catalog version cannot be null");
        MediaContainerModel result = null;
        final List<MediaContainerModel> results = mediaContainerDao.findMediaContainersByQualifier(qualifier,
                catalogVersion);
        if (CollectionUtils.isNotEmpty(results) && (results.size() == 1))
        {
            result = results.iterator().next();
        }
        return result;
    }

    /**
     * @param mediaContainerDao
     *           the mediaContainerDao to set
     */
    @Override
    public void setMediaContainerDao(final MediaContainerDao mediaContainerDao)
    {
        this.mediaContainerDao = (SharjahMediaContainerDao) mediaContainerDao;
        super.setMediaContainerDao(mediaContainerDao);
    }


}
