package com.sharjah.core.cronjobs;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.sharjah.core.model.cronjob.SharjahCronjobModel;
import com.sharjah.core.product.service.SharjahProductService;


public class ProductPublicationCronjob extends AbstractJobPerformable<SharjahCronjobModel>
{
	private static final Logger LOG = Logger.getLogger(ProductPublicationCronjob.class);

	private SharjahProductService sharjahProductService;

	@Override
	public PerformResult perform(final SharjahCronjobModel cronjob)
	{
		final Date launchDate = cronjob.getLastSuccessfulRun() != null ? cronjob.getLastSuccessfulRun() : new Date();

		if (LOG.isInfoEnabled())
		{
			LOG.info("ProductPublicationJob with code <" + cronjob.getCode() + "> for locale <"
					+ cronjob.getSessionLanguage().getIsocode() + "> started. Last successful run: " + launchDate);
		}

		try
		{
			final Collection<ProductModel> selectedProducts = sharjahProductService
					.getProductsToEligibilityForPublication(launchDate);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(
						"<" + selectedProducts.size() + "> products were modified since <" + launchDate + ">.");
			}

			selectedProducts.stream().forEach(product -> sharjahProductService.checkProductEligibility(product));

		}
		catch (final ModelSavingException e)
		{
			LOG.error("ProductPublicationJob with code <" + cronjob.getCode() + "> for locale <"
					+ cronjob.getSessionLanguage().getIsocode() + "> error.", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		cronjob.setLastSuccessfulRun(cronjob.getStartTime());
		modelService.save(cronjob);
		if (LOG.isInfoEnabled())
		{
			LOG.info("ProductPublicationJob with code <" + cronjob.getCode() + "> for locale <"
					+ cronjob.getSessionLanguage().getIsocode() + "> finished. ");
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 * 
	 * @return
	 */
	public SharjahProductService getSharjahProductService()
	{
		return sharjahProductService;
	}

	/**
	 * 
	 * @param sharjahProductService
	 */
	public void setSharjahProductService(final SharjahProductService sharjahProductService)
	{
		this.sharjahProductService = sharjahProductService;
	}
}
