package com.sharjah.core.order;

/**
 * Type for order exports.
 *
 * @author Pablo François
 */
public enum OrderExportType
{

	NEW("new"), RETURN("return"), CANCEL("cancel");

	private final String code;

	private OrderExportType(final String code)
	{
		this.code = code.intern();
	}

	public String getCode()
	{
		return code;
	}

}
