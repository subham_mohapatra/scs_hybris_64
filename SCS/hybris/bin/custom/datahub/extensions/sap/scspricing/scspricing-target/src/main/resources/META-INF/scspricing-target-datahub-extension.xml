<!--
  ~ [y] hybris Platform
  ~
  ~ Copyright (c) 2017 SAP SE or an SAP affiliate company.
  ~ All rights reserved.
  ~
  ~ This software is the confidential and proprietary information of SAP
  ~ ("Confidential Information"). You shall not disclose such Confidential
  ~ Information and shall use it only in accordance with the terms of the
  ~ license agreement you entered into with SAP.
  -->
<extension xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.hybris.com/schema/"
		   xsi:schemaLocation="http://www.hybris.com/schema/ http://www.hybris.com/schema/datahub-metadata-schema-1.3.0.xsd"
		   name="scspricing-target">

	<dependencies>
		<dependency>
			<extension>scspricing-canonical</extension>
			<extension>sappricing-target</extension>
		</dependency>
	</dependencies>

	<targetSystems>
	
		<targetSystem>
			<name>HybrisCore</name>
			<type>HybrisCore</type>
          	<exportURL>${targetsystem.hybriscore.url}</exportURL>
          	<userName>${targetsystem.hybriscore.username}</userName>
          	<password>${targetsystem.hybriscore.password}</password>
			<exportCodes>
				<exportCode>#% impex.setLocale( Locale.ENGLISH )</exportCode>
			</exportCodes>
			<targetItems>
				<item>
					<type>PriceRow</type>
					<description>Hybris platform representation of a PriceRow</description>
					<exportCode>PriceRow</exportCode>
					<canonicalItemSource>CanonicalPrice</canonicalItemSource>
                    <updatable>true</updatable>
					<attributes>
						<attribute override="true">
							<name>product</name>
							<transformationExpression>scsProductId == null ? null : scsProductId + ':' + resolve('CanonicalPricingSalesAreaMapping', '${sapcoreconfiguration.pool}').catalogVersion</transformationExpression>
							<exportCode>product(code,catalogVersion(catalog(id),version))[forceWrite=true, unique=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>price</name>
							<transformationExpression>amount</transformationExpression>
							<exportCode>price</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>currency</name>
							<transformationExpression>currency</transformationExpression>
							<exportCode>currency(isocode)</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute override="true">
							<name>net</name>
							<!-- Always set to TRUE -->
							<transformationExpression></transformationExpression>
							<exportCode>net[default=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>unit</name>
							<transformationExpression>unit</transformationExpression>
							<exportCode>unit(code)</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>unitFactor</name>
							<transformationExpression>unitFactor</transformationExpression>
							<exportCode>unitFactor</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>minqtd</name>
							<transformationExpression>scaleQuantity == null ? 1 : scaleQuantity</transformationExpression>
							<exportCode>minqtd</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>startTime</name>
							<transformationExpression>startDate+'000000'</transformationExpression>
							<exportCode>startTime[dateformat=yyyyMMddHHmmss]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>endTime</name>
							<transformationExpression>endDate+'235959'</transformationExpression>
							<exportCode>endTime[dateformat=yyyyMMddHHmmss]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>sapConditionId</name>
							<transformationExpression>conditionId</transformationExpression>
							<exportCode>sapConditionId[unique=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>catalogVersion</name>
							<transformationExpression>resolve('CanonicalPricingSalesAreaMapping', '${sapcoreconfiguration.pool}').catalogVersion</transformationExpression>
							<exportCode>catalogVersion(catalog(id),version)[unique=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>pg</name>
							<transformationExpression>productGroup</transformationExpression>
							<exportCode>pg(code)[forceWrite=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>ug</name>
							<transformationExpression>userGroup</transformationExpression>
							<exportCode>ug(code)</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
					</attributes>
				</item>

				<item>
					<type>DiscountRow</type>
					<description>Hybris platform representation of a DiscountRow</description>
					<exportCode>DiscountRow</exportCode>
					<canonicalItemSource>CanonicalDiscount</canonicalItemSource>
                    <updatable>true</updatable>
					<attributes>
						<attribute override="true">
							<name>product</name>
							<transformationExpression>scsProductId == null ? null : scsProductId + ':' + resolve('CanonicalPricingSalesAreaMapping', '${sapcoreconfiguration.pool}').catalogVersion</transformationExpression>
							<exportCode>product(code,catalogVersion(catalog(id),version))[forceWrite=true, unique=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>value</name>
							<transformationExpression>amount.contains('-') ? amount.replace('-','') : "-" + amount</transformationExpression>
							<exportCode>value</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>currency</name>
							<transformationExpression>currency=='%'?'':currency</transformationExpression>
							<exportCode>currency(isocode)</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>startTime</name>
							<transformationExpression>startDate+'000000'</transformationExpression>
							<exportCode>startTime[dateformat=yyyyMMddHHmmss]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>endTime</name>
							<transformationExpression>endDate+'235959'</transformationExpression>
							<exportCode>endTime[dateformat=yyyyMMddHHmmss]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>discount</name>
							<transformationExpression>conditionType</transformationExpression>
							<exportCode>discount(code)</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>catalogVersion</name>
							<transformationExpression>resolve('CanonicalPricingSalesAreaMapping', '${sapcoreconfiguration.pool}').catalogVersion</transformationExpression>
							<exportCode>catalogVersion(catalog(id),version)[unique=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>sapConditionId</name>
							<transformationExpression>conditionId</transformationExpression>
							<exportCode>sapConditionId[unique=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>pg</name>
							<transformationExpression>productGroup</transformationExpression>
							<exportCode>pg(code)[forceWrite=true]</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
						<attribute>
							<name>ug</name>
							<transformationExpression>userGroup</transformationExpression>
							<exportCode>ug(code)</exportCode>
							<mandatoryInHeader>true</mandatoryInHeader>
						</attribute>
					</attributes>
				</item>
			</targetItems>
		</targetSystem>
	
	</targetSystems>

</extension>
